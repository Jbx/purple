#include "math/box.h"
#include "render/attribute.h"
#include "utils/color.h"

// STL
#include <optional>

namespace AssimpImporter {

struct Mesh
{
  AttributeLayout layout;
  std::vector<float> vertices;
  std::vector<unsigned int> indices;
  std::optional<int> materialIndex;
  bool isTextured = false;
  Box boundingBox = Box::Invalid;
  glm::mat4 transformation = glm::mat4(1.f);
};

struct Material
{
  std::optional<std::string> name;
  std::optional<bool> twoSided;
  std::optional<bool> wireframe;
  std::optional<float> opacity;
  std::optional<Color> ambientColor;
  std::optional<Color> diffuseColor;
  std::optional<Color> specularColor;
  std::optional<float> shininess;
  std::optional<std::string> ambientTextureFileName;
  std::optional<std::string> diffuseTextureFileName;
  std::optional<std::string> specularTextureFileName;
  std::optional<Color> baseColor;
  std::optional<float> metallic;
  std::optional<float> roughness;
  std::optional<std::string> metallicRoughnessTextureFileName;
  std::optional<std::string> baseColorTextureFileName;
  std::optional<std::string> metallicTextureFileName;
  std::optional<std::string> emissionColorTextureFileName;
  std::optional<std::string> roughnessTextureFileName;
  std::optional<std::string> ambientOcclusionFileName;
};

struct Scene
{
  std::vector<Mesh> meshes;
  std::vector<Material> materials;
};

enum Component
{
  VertexPosition,
  VertexNormal,
  VertexColor,
  VertexTexCoord,
  VertexTangent,
  VertexBitangent
};

using VertexLayout = std::vector<Component>;

struct Options
{
  VertexLayout layout = {VertexPosition, VertexNormal, VertexColor};
  bool normalize = false;
  float scale = 1.0f;
  bool flipY = false;
  bool preTransform = false;
};

Scene import(
    std::string const& filePath,
    Options const& options = Options());

} // namespace AssimpImporter
