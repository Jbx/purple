#include "assimp_importer.h"
#include "utils/assert.h"
#include "math/math.h"

// Assimp
#include <assimp/Importer.hpp>
#include <assimp/BaseImporter.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


static inline glm::vec3 vec3_cast(aiVector3D const& v) { return glm::vec3(v.x, v.y, v.z); }
static inline glm::vec2 vec2_cast(aiVector3D const& v) { return glm::vec2(v.x, v.y); }
static inline glm::quat quat_cast(aiQuaternion const& q) { return glm::quat(q.w, q.x, q.y, q.z); }
static inline glm::mat4 mat4_cast(aiMatrix4x4 const& m) { return glm::transpose(glm::make_mat4(&m.a1)); }
static inline glm::mat4 mat4_cast(aiMatrix3x3 const& m) { return glm::transpose(glm::make_mat3(&m.a1)); }
static inline std::string string_cast(aiString const& s) { return std::string(s.C_Str()); }
static inline Color color_cast(aiColor4D const& c) { return Color{c.r, c.g, c.b, c.a}; }

namespace AssimpImporter {

glm::mat4 worldTransformation(aiNode const* node)
{
  glm::mat4 transformation = glm::mat4(1.f);
  auto currentNode = node;
  while (currentNode)
  {
    transformation = mat4_cast(currentNode->mTransformation) * transformation;
    currentNode = currentNode->mParent;
  }
  return transformation;
}

Mesh importMesh(
    aiScene const* assimpScene,
    aiMesh const* assimpMesh,
    Options const& options,
    glm::mat4 const& transf,
    Mesh& mesh)
{
  mesh.materialIndex = assimpMesh->mMaterialIndex;
  mesh.isTextured = assimpMesh->HasTextureCoords(0);

  aiVector3D nullVector(0.0f, 0.0f, 0.0f);
  aiColor4D diffuseColor(0.0f, 0.0f, 0.0f, 1.0f);
  assimpScene->mMaterials[assimpMesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);

  auto normalMatrix = glm::inverseTranspose(transf);

  for (int i = 0; i < assimpMesh->mNumVertices; ++i)
  {
    const aiVector3D& position = assimpMesh->mVertices[i];
    const aiVector3D& normal = assimpMesh->mNormals[i];
    const aiColor4D& color = (assimpMesh->HasVertexColors(0)) ? assimpMesh->mColors[0][i] : diffuseColor;
    const aiVector3D& texCoord = (assimpMesh->HasTextureCoords(0)) ? assimpMesh->mTextureCoords[0][i] : nullVector;
    const aiVector3D& tangent = (assimpMesh->HasTangentsAndBitangents()) ? assimpMesh->mTangents[i] : nullVector;
    const aiVector3D& bitangent = (assimpMesh->HasTangentsAndBitangents()) ? assimpMesh->mBitangents[i] : nullVector;

    for (auto& component : options.layout)
    {
      switch (component) {
      case VertexPosition:
      {
        auto pos = transf * glm::vec4(vec3_cast(position), 1.0);
        mesh.vertices.push_back(pos.x);
        mesh.vertices.push_back(options.flipY ? -pos.y : pos.y);
        mesh.vertices.push_back(pos.z);
        mesh.boundingBox.merge(pos);
      } break;
      case VertexNormal:
      {
        auto n = normalMatrix * glm::vec4(vec3_cast(normal), 1.0);
        mesh.vertices.push_back(n.x);
        mesh.vertices.push_back(options.flipY ? -n.y : n.y);
        mesh.vertices.push_back(n.z);
      } break;
      case VertexTexCoord:
        mesh.vertices.push_back(texCoord.x);
        mesh.vertices.push_back(texCoord.y);
        break;
      case VertexColor:
        mesh.vertices.push_back(color.r);
        mesh.vertices.push_back(color.g);
        mesh.vertices.push_back(color.b);
        mesh.vertices.push_back(color.a);
        break;
      case VertexTangent:
        mesh.vertices.push_back(tangent.x);
        mesh.vertices.push_back(tangent.y);
        mesh.vertices.push_back(tangent.z);
        break;
      case VertexBitangent:
        mesh.vertices.push_back(bitangent.x);
        mesh.vertices.push_back(bitangent.y);
        mesh.vertices.push_back(bitangent.z);
        break;
      };
    }
  }

  uint32_t indexBase = static_cast<uint32_t>(mesh.indices.size());
  for (uint32_t faceIndex = 0; faceIndex < assimpMesh->mNumFaces; ++faceIndex)
  {
    switch (assimpMesh->mFaces[faceIndex].mNumIndices)
    {
    case 3:
      mesh.indices.push_back(indexBase + assimpMesh->mFaces[faceIndex].mIndices[0]);
      mesh.indices.push_back(indexBase + assimpMesh->mFaces[faceIndex].mIndices[1]);
      mesh.indices.push_back(indexBase + assimpMesh->mFaces[faceIndex].mIndices[2]);
      break;
    }
  }

  return mesh;
}

void importMeshes(
    aiScene const* scene,
    aiNode const* node,
    Options const& options,
    std::vector<Mesh>& meshes)
{
  auto nodeTransformation = worldTransformation(node);

  AttributeLayout layout;

  std::transform(
        options.layout.begin(),
        options.layout.end(),
        std::back_inserter(layout),
        [](auto const& component)
  {
    switch (component) {
    case VertexPosition: return Vec3;
    case VertexNormal: return Vec3;
    case VertexTexCoord: return Vec2;
    case VertexColor: return Vec4;
    case VertexTangent: return Vec3;
    case VertexBitangent: return Vec3;
    }
    ASSERT(false, "Vertex component not handled");
    return Vec3;
  });

  for (int i = 0; i < node->mNumMeshes; ++i)
  {
    Mesh mesh{};
    mesh.layout = layout;

    importMesh(
          scene,
          scene->mMeshes[node->mMeshes[i]],
        options,
        options.preTransform ? nodeTransformation : glm::mat4(1.f),
        mesh);

    if (!options.preTransform)
    {
      mesh.transformation = nodeTransformation;
    }

    if (mesh.indices.size() > 0)
    {
      meshes.emplace_back(mesh);
      LOG_INFO("Bounding box ", mesh.boundingBox.toString());
      LOG_INFO("Vertex count ", mesh.vertices.size());
      LOG_INFO("Indice count ", mesh.indices.size());
      LOG_INFO("Transformation ", mesh.transformation);
    }
    else
    {
      LOG_INFO("Mesh ignored, only triangles meshes are supported");
    }
  }

  for (int i = 0; i < node->mNumChildren; ++i)
  {
    importMeshes(scene, node->mChildren[i], options, meshes);
  }
}

void importMaterials(
    aiScene const* scene,
    std::vector<Material>& materials)
{
  LOG_DEBUG("Material count: ", scene->mNumMaterials);

  for (int i = 0; i < scene->mNumMaterials; ++i)
  {
    auto assimpMaterial = scene->mMaterials[i];
    auto& material = materials.emplace_back(Material{});

    aiString name;
    if (assimpMaterial->Get(AI_MATKEY_NAME, name) == aiReturn_SUCCESS)
    {
      material.name = string_cast(name);
    }

    bool twoSided;
    if (assimpMaterial->Get(AI_MATKEY_TWOSIDED, twoSided) == aiReturn_SUCCESS)
    {
      material.twoSided = twoSided;
    }

    bool wireframe;
    if (assimpMaterial->Get(AI_MATKEY_ENABLE_WIREFRAME, wireframe) == aiReturn_SUCCESS)
    {
      material.wireframe = wireframe;
    }

    bool opacity;
    if (assimpMaterial->Get(AI_MATKEY_OPACITY, opacity) == aiReturn_SUCCESS)
    {
      material.opacity = opacity;
    }

    aiColor4D ambientColor;
    if (assimpMaterial->Get(AI_MATKEY_COLOR_AMBIENT, ambientColor) == aiReturn_SUCCESS)
    {
      material.ambientColor = color_cast(ambientColor);
    }

    aiColor4D diffuseColor;
    if (assimpMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor) == aiReturn_SUCCESS)
    {
      material.diffuseColor = color_cast(diffuseColor);
    }

    aiColor4D specularColor;
    if (assimpMaterial->Get(AI_MATKEY_COLOR_SPECULAR, specularColor) == aiReturn_SUCCESS)
    {
      material.specularColor = color_cast(specularColor);
    }

    float shininess;
    if (assimpMaterial->Get(AI_MATKEY_SHININESS, shininess) == aiReturn_SUCCESS)
    {
      material.shininess = shininess;
    }

    aiString ambientTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_AMBIENT, 0, &ambientTextureFileName) == aiReturn_SUCCESS)
    {
      material.ambientTextureFileName = string_cast(ambientTextureFileName);
    }

    aiString diffuseTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &diffuseTextureFileName) == aiReturn_SUCCESS)
    {
      material.diffuseTextureFileName = string_cast(diffuseTextureFileName);
    }

    aiString specularTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_SPECULAR, 0, &specularTextureFileName) == aiReturn_SUCCESS)
    {
      material.specularTextureFileName = string_cast(specularTextureFileName);
    }

    aiColor4D baseColor;
    if (assimpMaterial->Get(AI_MATKEY_BASE_COLOR, baseColor) == aiReturn_SUCCESS)
    {
      material.baseColor = color_cast(baseColor);
    }

    float metallic;
    if (assimpMaterial->Get(AI_MATKEY_METALLIC_FACTOR, metallic) == aiReturn_SUCCESS)
    {
      material.metallic = metallic;
    }

    float roughness;
    if (assimpMaterial->Get(AI_MATKEY_ROUGHNESS_FACTOR, roughness) == aiReturn_SUCCESS)
    {
      material.roughness = roughness;
    }

    aiString unknowTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_UNKNOWN, 0, &unknowTextureFileName) == aiReturn_SUCCESS)
    {
      material.metallicRoughnessTextureFileName = string_cast(unknowTextureFileName);
    }

    aiString baseColorTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_BASE_COLOR, 0, &baseColorTextureFileName) == aiReturn_SUCCESS)
    {
      material.baseColorTextureFileName = string_cast(baseColorTextureFileName);
    }

    aiString metallicTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_METALNESS, 0, &metallicTextureFileName) == aiReturn_SUCCESS)
    {
      material.metallicTextureFileName = string_cast(metallicTextureFileName);
    }

    aiString emissionColorTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_EMISSION_COLOR, 0, &emissionColorTextureFileName) == aiReturn_SUCCESS)
    {
      material.emissionColorTextureFileName = string_cast(emissionColorTextureFileName);
    }

    aiString roughnessTextureFileName;
    if (assimpMaterial->GetTexture(aiTextureType_DIFFUSE_ROUGHNESS, 0, &roughnessTextureFileName) == aiReturn_SUCCESS)
    {
      material.roughnessTextureFileName = string_cast(roughnessTextureFileName);
    }

    aiString ambientOcclusionFileName;
    if (assimpMaterial->GetTexture(aiTextureType_AMBIENT_OCCLUSION, 0, &ambientOcclusionFileName) == aiReturn_SUCCESS)
    {
      material.ambientOcclusionFileName = string_cast(ambientOcclusionFileName);
    }
  }
}

Scene import(
    std::string const& filePath,
    Options const& options)
{
  Scene scene;

  Assimp::Importer importer;

  aiScene const* assimpScene =
      importer.ReadFile(filePath, aiProcessPreset_TargetRealtime_Quality);

  if (!assimpScene)
  {
    LOG_ERROR("Error loading file ", filePath);
    return scene;
  }

  LOG_INFO("Texture count: ", assimpScene->mNumTextures);

  importMeshes(
        assimpScene,
        assimpScene->mRootNode,
        options,
        scene.meshes);

  importMaterials(assimpScene, scene.materials);

  Box globalBoundingBox = Box::Invalid;
  for (auto const& mesh : scene.meshes)
  {
    auto transformedBoundingBox = mesh.boundingBox.transformed(mesh.transformation);
    globalBoundingBox.merge(transformedBoundingBox);
  }

  if (options.normalize)
  {
    auto size = globalBoundingBox.getSize();
    auto maxLength = std::max(std::max(size.x, size.y), size.z);
    auto scaleFactor = 1.f / maxLength;

    for (auto& mesh : scene.meshes)
    {
      mesh.transformation =
          glm::scale(glm::vec3(scaleFactor)) *
          glm::translate(-globalBoundingBox.getCenter()) *
          mesh.transformation;
    }
  }

  if (options.scale != 1.0f)
  {
    for (auto& mesh : scene.meshes)
    {
      mesh.transformation =
          glm::scale(glm::vec3(options.scale)) *
          glm::translate(-globalBoundingBox.getCenter()) *
          mesh.transformation;
    }
  }

  globalBoundingBox = Box::Invalid;
  for (auto const& mesh : scene.meshes)
  {
    auto transformedBoundingBox = mesh.boundingBox.transformed(mesh.transformation);
    globalBoundingBox.merge(transformedBoundingBox);
  }

  LOG_INFO("Global bounding box ", globalBoundingBox.toString());

  return scene;
}

} // namespace AssimpImporter
