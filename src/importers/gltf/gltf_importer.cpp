#include "gltf_importer.h"
#include "utils/assert.h"
#include "utils/buffer_view.h"

// tinygltf
#undef APIENTRY // avoid warning with MSVC
#define TINYGLTF_IMPLEMENTATION
#include <tiny_gltf.h>

// STL
#include <algorithm>

namespace GLTFImporter {

TextureFilter textureFilter(int filter)
{
  switch (filter)
  {
  case TINYGLTF_TEXTURE_FILTER_NEAREST:
    return TextureFilter::Nearest;
  case TINYGLTF_TEXTURE_FILTER_LINEAR:
    return TextureFilter::Linear;
  case TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST:
    return TextureFilter::NearestMipMapLinear;
  case TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST:
    return TextureFilter::LinearMipMapNearest;
  case TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR:
    return TextureFilter::NearestMipMapLinear;
  case TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_LINEAR:
    return TextureFilter::LinearMipMapLinear;
  }
  return TextureFilter::Nearest;
}

TextureWrapMode wrapMode(int wrapMode)
{
  switch (wrapMode)
  {
  case TINYGLTF_TEXTURE_WRAP_REPEAT:
    return TextureWrapMode::Repeat;
  case TINYGLTF_TEXTURE_WRAP_CLAMP_TO_EDGE:
    return TextureWrapMode::ClampToEdge;
  case TINYGLTF_TEXTURE_WRAP_MIRRORED_REPEAT:
    return TextureWrapMode::MirrorRepeat;
  }
  return TextureWrapMode::Repeat;
}

constexpr size_t componentSize(int componentType)
{
  switch (componentType)
  {
  case TINYGLTF_COMPONENT_TYPE_BYTE:
    return sizeof(int8_t);
  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
    return sizeof(uint8_t);
  case TINYGLTF_COMPONENT_TYPE_SHORT:
    return sizeof(int16_t);
  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
    return sizeof(uint16_t);
  case TINYGLTF_COMPONENT_TYPE_INT:
    return sizeof(int32_t);
  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
    return sizeof(uint32_t);
  case TINYGLTF_COMPONENT_TYPE_FLOAT:
    return sizeof(float);
  case TINYGLTF_COMPONENT_TYPE_DOUBLE:
    return sizeof(double);
  }
  assert(false && "Component type not handled");
  return sizeof(int8_t);
}

AlphaMode alphaMode(std::string const& alphaModeStr)
{
  if (alphaModeStr == "BLEND")
    return AlphaMode::Blend;
  else if (alphaModeStr == "MASK")
    return AlphaMode::Mask;
  else if (alphaModeStr == "OPAQUE")
    return AlphaMode::Opaque;
  return AlphaMode::Opaque;
}

template <typename T>
struct Attribute
{
  BufferView<T> buffer;
  size_t stride;
};

template <typename T>
Attribute<T> toAttribute(tinygltf::Model const& model, int index)
{
  const auto& accessor = model.accessors[index];
  const auto& bufferView = model.bufferViews[accessor.bufferView];

  BufferView view(
        reinterpret_cast<const T*>(&(model.buffers[bufferView.buffer].data[accessor.byteOffset + bufferView.byteOffset])), accessor.count);

  size_t stride = accessor.ByteStride(bufferView) / sizeof(T);

  return {view, stride};
}

void convertNode(
    tinygltf::Model const& model,
    tinygltf::Node const& inNode,
    Node& node)
{
  // Generate local node matrix
  glm::vec3 translation = glm::vec3(0.0f);
  if (inNode.translation.size() == 3)
  {
    translation = glm::make_vec3(inNode.translation.data());
  }

  glm::mat4 rotation = glm::mat4(1.0f);
  if (inNode.rotation.size() == 4)
  {
    glm::quat q = glm::make_quat(inNode.rotation.data());
    rotation = glm::mat4(q);
  }

  glm::vec3 scale = glm::vec3(1.0f);
  if (inNode.scale.size() == 3)
  {
    scale = glm::make_vec3(inNode.scale.data());
  }

  glm::mat4 matrix(1.f);
  if (inNode.matrix.size() == 16)
  {
    matrix = glm::make_mat4x4(inNode.matrix.data());
  }

  node.localTransformation =
      glm::translate(translation) *
      rotation *
      glm::scale(scale) *
      matrix;

  if (inNode.mesh > -1)
  {
    node.mesh = inNode.mesh;
  }

  for (auto const& inChildIndex : inNode.children)
  {
    auto& child = node.children.emplace_back(Node{});
    convertNode(model, model.nodes[inChildIndex], child);
  }
}

Result import(
    std::string const& filePath)
{
  Model model;
  tinygltf::Model inModel;
  std::string error;
  std::string warning;
  std::string ext = std::filesystem::path(filePath).extension().string();
  auto path = std::filesystem::path(filePath).parent_path();

  auto pathFromURI = [&path](std::string const& uri)
  {
    return (path / uri).make_preferred().string();
  };

  bool ret = false;
  if (ext == ".glb")
  {
    LOG_DEBUG("Reading binary glTF");
    // assume binary glTF.
    ret = tinygltf::TinyGLTF().LoadBinaryFromFile(
          &inModel,
          &error,
          &warning,
          filePath);
  }
  else
  {
    LOG_DEBUG("Reading ASCII glTF");
    // assume ascii glTF.
    ret = tinygltf::TinyGLTF().LoadASCIIFromFile(
          &inModel,
          &error,
          &warning,
          filePath);
  }

  if (!warning.empty())
  {
    LOG_WARNING("Warning: ", warning);
  }

  if (!error.empty())
  {
    LOG_ERROR("Error: ", error);
  }

  if (!ret)
  {
    LOG_ERROR("Failed to parse glTF");
    return {false, model};
  }

  for (auto const& inMaterial : inModel.materials)
  {
    auto& material = model.materials.emplace_back(Material{});
    material.baseColorFactor = glm::vec4(
          inMaterial.pbrMetallicRoughness.baseColorFactor[0],
        inMaterial.pbrMetallicRoughness.baseColorFactor[1],
        inMaterial.pbrMetallicRoughness.baseColorFactor[2],
        inMaterial.pbrMetallicRoughness.baseColorFactor[3]);
    material.emissiveFactor = glm::vec4(
          inMaterial.emissiveFactor[0],
        inMaterial.emissiveFactor[1],
        inMaterial.emissiveFactor[2],
        1.0);
    material.metallicFactor = static_cast<float>(inMaterial.pbrMetallicRoughness.metallicFactor);

    material.roughnessFactor = static_cast<float>(inMaterial.pbrMetallicRoughness.roughnessFactor);
    material.alphaMode = alphaMode(inMaterial.alphaMode);
    material.alphaCutoff = static_cast<float>(inMaterial.alphaCutoff);
    material.doubleSided = inMaterial.doubleSided;

    if (inMaterial.pbrMetallicRoughness.baseColorTexture.index > -1)
    {
      auto const& texture =
          inModel.textures.at(inMaterial.pbrMetallicRoughness.baseColorTexture.index);

      int uvIndex = inMaterial.pbrMetallicRoughness.baseColorTexture.texCoord;

      Sampler sampler;
      if (texture.sampler > -1)
      {
        auto const& s =
            inModel.samplers.at(texture.sampler);

        sampler = {
          textureFilter(s.minFilter),
          textureFilter(s.magFilter),
          wrapMode(s.wrapS),
          wrapMode(s.wrapT),
          wrapMode(s.wrapT)
        };
      }

      auto const& image =
          inModel.images.at(texture.source);

      material.colorMap = Texture{pathFromURI(image.uri), sampler, uvIndex};
    }

    if (inMaterial.pbrMetallicRoughness.metallicRoughnessTexture.index > -1)
    {
      auto const& texture =
          inModel.textures.at(inMaterial.pbrMetallicRoughness.metallicRoughnessTexture.index);

      int uvIndex = inMaterial.pbrMetallicRoughness.metallicRoughnessTexture.texCoord;

      Sampler sampler;
      if (texture.sampler > -1)
      {
        auto const& s =
            inModel.samplers.at(texture.sampler);

        sampler = {
          textureFilter(s.minFilter),
          textureFilter(s.magFilter),
          wrapMode(s.wrapS),
          wrapMode(s.wrapT),
          wrapMode(s.wrapT)
        };
      }

      auto const& image =
          inModel.images.at(texture.source);

      material.physicalDescriptorMap = Texture{pathFromURI(image.uri), sampler, uvIndex};
    }

    if (inMaterial.normalTexture.index > -1)
    {
      auto const& texture =
          inModel.textures.at(inMaterial.normalTexture.index);

      int uvIndex = inMaterial.normalTexture.texCoord;

      Sampler sampler;
      if (texture.sampler > -1)
      {
        auto const& s =
            inModel.samplers.at(texture.sampler);

        sampler = {
          textureFilter(s.minFilter),
          textureFilter(s.magFilter),
          wrapMode(s.wrapS),
          wrapMode(s.wrapT),
          wrapMode(s.wrapT)
        };
      }

      auto const& image =
          inModel.images.at(texture.source);

      material.normalMap = Texture{pathFromURI(image.uri), sampler, uvIndex};
    }

    if (inMaterial.occlusionTexture.index > -1)
    {
      auto const& texture =
          inModel.textures.at(inMaterial.occlusionTexture.index);

      int uvIndex = inMaterial.occlusionTexture.texCoord;

      Sampler sampler;
      if (texture.sampler > -1)
      {
        auto const& s =
            inModel.samplers.at(texture.sampler);

        sampler = {
          textureFilter(s.minFilter),
          textureFilter(s.magFilter),
          wrapMode(s.wrapS),
          wrapMode(s.wrapT),
          wrapMode(s.wrapT)
        };
      }

      auto const& image =
          inModel.images.at(texture.source);

      material.aoMap = Texture{pathFromURI(image.uri), sampler, uvIndex};
    }

    if (inMaterial.emissiveTexture.index > -1)
    {
      auto const& texture =
          inModel.textures.at(inMaterial.emissiveTexture.index);

      int uvIndex = inMaterial.emissiveTexture.texCoord;

      Sampler sampler;
      if (texture.sampler > -1)
      {
        auto const& s =
            inModel.samplers.at(texture.sampler);

        sampler = {
          textureFilter(s.minFilter),
          textureFilter(s.magFilter),
          wrapMode(s.wrapS),
          wrapMode(s.wrapT),
          wrapMode(s.wrapT)
        };
      }

      auto const& image =
          inModel.images.at(texture.source);

      material.emissiveMap = Texture{pathFromURI(image.uri), sampler, uvIndex};
    }
  }

  for (auto const& inMesh : inModel.meshes)
  {
    auto& mesh = model.meshes.emplace_back(Mesh{});

    for (auto const& inPrimitive : inMesh.primitives)
    {
      if (inPrimitive.material > -1)
      {
        auto& primitive = mesh.primitives.emplace_back(Primitive{});

        struct
        {
          Attribute<float> positions;
          std::optional<Attribute<float>> normals;
          std::optional<Attribute<float>> uv0;
          std::optional<Attribute<float>> uv1;
          std::optional<Attribute<float>> colors;
          //std::optional<Attribute<unsigned int>> joints;
          //std::optional<Attribute<float>> weights;          
        } p;

        auto positionAttributeIt = inPrimitive.attributes.find("POSITION");
        ASSERT(
              positionAttributeIt != inPrimitive.attributes.end(),
              "Position attribute is required");

        p.positions = toAttribute<float>(inModel, positionAttributeIt->second);

        auto normalAttributeIt = inPrimitive.attributes.find("NORMAL");
        if (normalAttributeIt != inPrimitive.attributes.end())
        {
          p.normals = toAttribute<float>(inModel, normalAttributeIt->second);
        }

        auto texCoord0It = inPrimitive.attributes.find("TEXCOORD_0");
        if (texCoord0It != inPrimitive.attributes.end())
        {
          p.uv0 = toAttribute<float>(inModel, texCoord0It->second);
        }

        auto texCoord1It = inPrimitive.attributes.find("TEXCOORD_1");
        if (texCoord1It != inPrimitive.attributes.end())
        {
          p.uv1 = toAttribute<float>(inModel, texCoord1It->second);
        }

        auto colorIt = inPrimitive.attributes.find("COLOR_0");
        if (colorIt != inPrimitive.attributes.end())
        {
          p.colors = toAttribute<float>(inModel, colorIt->second);
        }

        //      auto jointIt = primitive.attributes.find("JOINTS_0");
        //      if (jointIt != primitive.attributes.end())
        //      {
        //        p.joints = toAttribute<unsigned int>(model, jointIt->second);
        //      }

        //      auto weightIt = primitive.attributes.find("WEIGHTS_0");
        //      if (weightIt != primitive.attributes.end())
        //      {
        //        p.weights = toAttribute<float>(model, weightIt->second);
        //      }        

        primitive.vertices = std::vector<Vertex>(p.positions.buffer.size());

        primitive.min = glm::vec3(std::numeric_limits<float>::max());
        primitive.max = glm::vec3(std::numeric_limits<float>::min());

        for (int v = 0; v < primitive.vertices.size(); ++v)
        {
          auto& vertex = primitive.vertices[v];
          vertex.position = glm::make_vec3(&p.positions.buffer[v * p.positions.stride]);                  
          vertex.normal = p.normals ? glm::normalize(glm::vec3(glm::make_vec3(&p.normals->buffer[v * p.normals->stride]))) : glm::vec3(0.0f);
          vertex.uv0 = p.uv0 ? glm::make_vec2(&p.uv0->buffer[v * p.uv0->stride]) : glm::vec3(0.0f);
          vertex.uv0.y = 1. - vertex.uv0.y;
          vertex.uv1 = p.uv1 ? glm::make_vec2(&p.uv1->buffer[v * p.uv1->stride]) : glm::vec3(0.0f);
          vertex.uv1.y = 1. - vertex.uv1.y;
          vertex.color = p.colors ? glm::make_vec4(&p.colors->buffer[v * p.colors->stride]) : glm::vec4(1.0f);

          primitive.min = glm::min(primitive.min, vertex.position);
          primitive.max = glm::max(primitive.max, vertex.position);

          //        bool hasSkin = p.joints && p.weights;
          //        if (hasSkin)
          //        {
          //          vertex.joint = glm::make_vec4(&p.joints->buffer[v * p.joints->stride]);
          //          vertex.weight = glm::make_vec4(&p.weights->buffer[v * p.weights->stride]);
          //        }
        }

        bool hasIndices = inPrimitive.indices > -1;
        if (hasIndices)
        {
          auto const& accessor = inModel.accessors[inPrimitive.indices > -1 ? inPrimitive.indices : 0];
          auto const& bufferView = inModel.bufferViews[accessor.bufferView];
          auto const& buffer = inModel.buffers[bufferView.buffer];

          const void *dataPtr = &(buffer.data[accessor.byteOffset + bufferView.byteOffset]);

          auto& indices = primitive.indices.emplace();
          switch (accessor.componentType)
          {
          case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT:
          {
            const uint32_t *buf = static_cast<const uint32_t*>(dataPtr);
            std::copy(
                  buf,
                  buf + accessor.count,
                  std::back_inserter(indices));
            break;
          }

          case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT:
          {
            const uint16_t *buf = static_cast<const uint16_t*>(dataPtr);
            std::copy(
                  buf,
                  buf + accessor.count,
                  std::back_inserter(indices));
            break;
          }

          case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE:
          {
            const uint8_t *buf = static_cast<const uint8_t*>(dataPtr);
            std::copy(
                  buf,
                  buf + accessor.count,
                  std::back_inserter(indices));
            break;
          }

          default:
            LOG_ERROR("Component type not handled");
            break;
          }
        }

        primitive.material = inPrimitive.material;
      }
    }
  }

  const auto& scene = inModel.scenes[inModel.defaultScene > -1 ? inModel.defaultScene : 0];

  for (int inNodeIndex : scene.nodes)
  {
    auto& node = model.nodes.emplace_back(Node{});
    convertNode(inModel, inModel.nodes[inNodeIndex], node);
  }

  return {true, model};
}

} // namespace GLTFImporter
