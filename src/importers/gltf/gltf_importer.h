#pragma once

#include "math/math.h"
#include "render/texture_interface.h"

// STL
#include <filesystem>
#include <variant>
#include <optional>
#include <vector>

namespace GLTFImporter {

enum class Workflow
{
  MetallicRoughness,
  SpecularGlosiness,
};

enum class AlphaMode
{
  Blend,
  Mask,
  Opaque
};

struct Texture
{
  std::string path;
  Sampler sampler;
  int uvIndex = 0;
};

struct Material
{
  glm::vec4 baseColorFactor = glm::vec4(1.f);
  glm::vec4 emissiveFactor = glm::vec4(1.f);
  glm::vec4 diffuseFactor = glm::vec4(0.f);
  glm::vec4 specularFactor = glm::vec4(0.f, 0.f, 0.f, 1.f);
  Workflow workflow = Workflow::MetallicRoughness;
  int baseColorTextureSet = 0;
  int physicalDescriptorTextureSet = 0;
  int normalTextureSet = 0;
  int occlusionTextureSet = 0;
  int emissiveTextureSet = 0;
  float metallicFactor = 1.f;
  float roughnessFactor = 1.f;
  AlphaMode alphaMode = AlphaMode::Opaque;
  float alphaCutoff = 1.f;
  bool doubleSided = false;

  std::optional<Texture> colorMap;
  std::optional<Texture> physicalDescriptorMap;
  std::optional<Texture> normalMap;
  std::optional<Texture> aoMap;
  std::optional<Texture> emissiveMap;
};

struct Vertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 uv0;
  glm::vec2 uv1;
  glm::vec4 color;
};

struct Primitive
{
  std::vector<Vertex> vertices;
  std::optional<std::vector<unsigned int>> indices;
  glm::vec3 min;
  glm::vec3 max;
  int material;
};

struct Mesh
{
  std::vector<Primitive> primitives;
};

struct Node
{
  std::vector<Node> children;
  glm::mat4 localTransformation = glm::mat4(1.f);
  std::optional<int> mesh;
};

struct Model
{
  std::vector<Material> materials;
  std::vector<Mesh> meshes;
  std::vector<Node> nodes;
};

struct Result
{
  bool success;
  Model model;
};

Result import(
    std::string const& filePath);

} // namespace GLTFImporter

