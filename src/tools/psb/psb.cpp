#include "resource_limits.h"

// glslang
#include "glslang_c_interface.h"

// SPIRV-Cross
#include "spirv_glsl.hpp"

// JSON
#include "json.hpp"

// STL
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <sstream>
#include <regex>
#include <filesystem>

void printShaderSource(
    std::string const& text)
{
  int line = 1;

  std::cout << std::endl << std::setfill('0') << std::setw(3) << line << " ";

  for (auto c : text)
  {
    if (c == '\n')
    {
      std::cout << std::endl << std::setfill('0') << std::setw(3) << ++line << " ";
    }
    else
    {
      std::cout << c;
    }
  }

  std::cout << std::endl;
}

std::string readContent(
    std::string const& filePath)
{
  std::ifstream file(filePath);

  if (!file.is_open())
  {
    throw std::runtime_error("failed to open file!");
  }

  std::stringstream buffer;
  buffer << file.rdbuf();

  return buffer.str();
}

void replaceIncludes(
    std::string& source,
    std::string const& path)
{
  const std::regex expr("#include ((<[^>]+>)|(\"[^\"]+\"))");

  std::smatch match;
  std::string s = source;
  while (std::regex_search(s, match, expr))
  {
    size_t startPos = match.str().find_first_of("\"") + 1;
    std::string includeFilePath = match.str().substr(startPos, match.str().size() - startPos - 1);

    std::filesystem::path filePath = path;
    filePath /= includeFilePath;

    std::string includeSource = readContent(filePath.string());
    replaceIncludes(includeSource, path);

    source.replace(match.position(), match.length(), includeSource);

    s = match.suffix();
  }
}

std::string readShaderFile(
    std::string const& filePath)
{
  std::string source = readContent(filePath);

  auto path = std::filesystem::path(filePath).parent_path();
  replaceIncludes(source, path.string());
  return source;
}

glslang_stage_t glslangShaderStageFromFileName(
    std::string const& fileName)
{
  if (fileName.ends_with(".vert"))
    return GLSLANG_STAGE_VERTEX;

  if (fileName.ends_with(".frag"))
    return GLSLANG_STAGE_FRAGMENT;

  if (fileName.ends_with(".geom"))
    return GLSLANG_STAGE_GEOMETRY;

  if (fileName.ends_with(".comp"))
    return GLSLANG_STAGE_COMPUTE;

  if (fileName.ends_with(".tesc"))
    return GLSLANG_STAGE_TESSCONTROL;

  if (fileName.ends_with(".tese"))
    return GLSLANG_STAGE_TESSEVALUATION;

  return GLSLANG_STAGE_VERTEX;
}

bool compileShader(
    glslang_stage_t stage,
    std::string const& shaderSource,
    std::vector<unsigned int>& spirvBinary)
{
  const glslang_input_t input =
  {
    .language = GLSLANG_SOURCE_GLSL,
    .stage = stage,
    .client = GLSLANG_CLIENT_VULKAN,
    .client_version = GLSLANG_TARGET_VULKAN_1_1,
    .target_language = GLSLANG_TARGET_SPV,
    .target_language_version = GLSLANG_TARGET_SPV_1_3,
    .code = shaderSource.c_str(),
    .default_version = 110,
    .default_profile = GLSLANG_NO_PROFILE,
    .force_default_version_and_profile = false,
    .forward_compatible = false,
    .messages = GLSLANG_MSG_DEFAULT_BIT,
    .resource = &DefaultBuiltInResource,
  };

  glslang_shader_t* shader = glslang_shader_create(&input);

  if (!glslang_shader_preprocess(shader, &input))
  {
    std::cerr << "GLSL preprocessing failed" << std::endl;
    std::cerr << glslang_shader_get_info_log(shader) << std::endl;
    std::cerr << glslang_shader_get_info_debug_log(shader) << std::endl;
    printShaderSource(input.code);
    return false;
  }

  if (!glslang_shader_parse(shader, &input))
  {
    std::cerr << "GLSL parsing failed" << std::endl;
    std::cerr << glslang_shader_get_info_log(shader) << std::endl;
    std::cerr << glslang_shader_get_info_debug_log(shader) << std::endl;
    printShaderSource(glslang_shader_get_preprocessed_code(shader));
    return false;
  }

  glslang_program_t* program = glslang_program_create();
  glslang_program_add_shader(program, shader);

  if (!glslang_program_link(program, GLSLANG_MSG_SPV_RULES_BIT | GLSLANG_MSG_VULKAN_RULES_BIT))
  {
    std::cerr << "GLSL linking failed" << std::endl;
    std::cerr << glslang_shader_get_info_log(shader) << std::endl;
    std::cerr << glslang_shader_get_info_debug_log(shader) << std::endl;
    return false;
  }

  glslang_program_SPIRV_generate(program, stage);

  spirvBinary.resize(glslang_program_SPIRV_get_size(program));
  glslang_program_SPIRV_get(program, spirvBinary.data());

  {
    const char* spirv_messages =
        glslang_program_SPIRV_get_messages(program);

    if (spirv_messages)
    {
      std::cerr << spirv_messages << std::endl;
    }
  }

  glslang_program_delete(program);
  glslang_shader_delete(shader);

  return true;
}

std::vector<unsigned int> convertToGLSL(
    std::vector<unsigned int> const& spirvBinary,
    uint32_t version,
    bool es)
{
  spirv_cross::CompilerGLSL glsl(spirvBinary);

  spirv_cross::CompilerGLSL::Options options = {
    .version = version,
    .es = es,
    .vertex = {.fixup_clipspace = true},
  };
  glsl.set_common_options(options);

  auto str = glsl.compile();
  return std::vector<unsigned int>(str.begin(), str.end());
}

int main(int argc, char** argv)
{
  std::cout << "Purple Shader Builder v1.0" << std::endl << std::endl;

  if (argc < 2)
  {
    std::cout << "Didn't specify input file." << std::endl;
    return -1;
  }

  if (argc > 3)
  {
    std::cout << "Too many arguments." << std::endl;
    return -1;
  }

  std::string inputFilePath(argv[1]);
  std::string ouputPath;

  if (argc == 3)
  {
    ouputPath = std::string(argv[2]);
  }

  glslang_initialize_process();

  auto path = std::filesystem::path(inputFilePath).parent_path();
  auto filename = std::filesystem::path(inputFilePath).filename();

  std::cout << "Process file " << filename.string() << "..." << std::endl;

  std::string inputSource = readShaderFile(inputFilePath);

  std::cout << "Compile to SPIRV..." << std::endl;

  std::vector<unsigned int> spirvBinary;
  if (!compileShader(glslangShaderStageFromFileName(inputFilePath), inputSource, spirvBinary))
  {
    std::cout << "Compile to SPIRV [NOK]" << std::endl;
    return -1;
  }
  std::cout << "Compile to SPIRV [OK]" << std::endl;

  std::cout << "Convert to GLSL 330..." << std::endl;
  auto glsl330Source = convertToGLSL(spirvBinary, 330, false);

  std::cout << "Convert to GLSL 300 ES..." << std::endl;
  auto glsl300ESSource = convertToGLSL(spirvBinary, 300, true);

  std::cout << "Convert to GLSL [OK]" << std::endl;

  auto outputFilename = filename;
  outputFilename.replace_extension(filename.extension().string() + ".json");

  nlohmann::json json = {
    {"SPIRV", spirvBinary},
    {"GLSL_330", glsl330Source},
    {"GLSL_300_ES", glsl300ESSource},
  };

  std::filesystem::path outputFilePath;
  if (!ouputPath.empty())
  {
    outputFilePath = ouputPath;
  }
  else
  {
    outputFilePath = path;
  }

  outputFilePath = outputFilePath.make_preferred();
  std::cout << "Output path " << outputFilePath.string() << std::endl;

  std::filesystem::create_directories(outputFilePath);

  outputFilePath /= outputFilename;
  outputFilePath = outputFilePath.make_preferred();

  std::cout << "Save file " << outputFilePath.string() << "..." << std::endl;

  std::ofstream file(outputFilePath.string(), std::ios::out | std::ios::binary);
  if (!file.is_open())
  {
    std::cout << "Error opening file " << outputFilePath << std::endl;
    return -1;
  }

  std::vector<uint8_t> cbor;
  nlohmann::json::to_cbor(json, cbor);
  file.write(reinterpret_cast<const char*>(cbor.data()), cbor.size());
  std::cout << "Save file " << outputFilePath.string() << " [OK]" << std::endl;

  glslang_finalize_process();

  std::cout << "Done." << std::endl;
}
