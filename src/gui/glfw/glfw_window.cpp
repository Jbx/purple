#include "glfw_window.h"

// STL
#include <iostream>

bool isOpenGLAPI(GraphicsAPI api)
{
  return
      api == GraphicsAPI::OpenGL_3_3 ||
      api == GraphicsAPI::OpenGL_4_5;
}

bool isVulkanAPI(GraphicsAPI api)
{
  return
      api == GraphicsAPI::Vulkan_1_2;
}

GLFWWindow::GLFWWindow(
    std::string const& title,
    GraphicsAPI api,
    SizeI const& size,
    bool fullscreen,
    int sampleCount)
  : m_api(api)
  , m_width(size.width)
  , m_height(size.height)
  , m_sampleCount(sampleCount)
{
  switch (api)
  {
  case GraphicsAPI::OpenGL_3_3:
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    break;
  case GraphicsAPI::OpenGL_4_5:
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    break;
  case GraphicsAPI::Vulkan_1_2:
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    break;
  }

  glfwWindowHint(GLFW_SAMPLES, m_sampleCount);

  m_window = glfwCreateWindow(m_width, m_height, title.c_str(), nullptr, nullptr);

  if (!m_window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  setFullscreen(fullscreen);

  if (isOpenGLAPI(m_api))
  {
    glfwMakeContextCurrent(m_window);
    glfwSwapInterval(1);
  }

  glfwSetWindowUserPointer(m_window, this);

  glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, int width, int height) {
    static_cast<GLFWWindow*>(glfwGetWindowUserPointer(window))->onResize(width, height);
  });

  glfwSetKeyCallback(m_window, [](GLFWwindow *window, int key, int scancode, int action, int mods) {
    static_cast<GLFWWindow*>(glfwGetWindowUserPointer(window))->onKey(key, scancode, action, mods);
  });

  glfwSetCursorPosCallback(m_window, [](GLFWwindow *window, double x, double y) {
    static_cast<GLFWWindow*>(glfwGetWindowUserPointer(window))->onMouseMove(x, y);
  });

  glfwSetMouseButtonCallback(m_window, [](GLFWwindow *window, int button, int action, int mods) {
    static_cast<GLFWWindow*>(glfwGetWindowUserPointer(window))->onMouseButton(button, action, mods);
  });

  glfwSetScrollCallback(m_window, [](GLFWwindow *window, double xOffset, double yOffset) {
    static_cast<GLFWWindow*>(glfwGetWindowUserPointer(window))->onScroll(xOffset, yOffset);
  });
}

GLFWWindow::~GLFWWindow()
{
  glfwDestroyWindow(m_window);
  glfwTerminate();
}

void GLFWWindow::setFullscreen(bool fullscreen)
{
  glfwSetWindowMonitor(
        m_window,
        fullscreen ? glfwGetPrimaryMonitor() : nullptr,
        m_x,
        m_y,
        m_width,
        m_height,
        GLFW_DONT_CARE);
}

void GLFWWindow::setSize(SizeI const size)
{
  glfwSetWindowSize(m_window, size.width, size.height);
}

int GLFWWindow::getX() const
{
  return m_x;
}

int GLFWWindow::getY() const
{
  return m_y;
}

int GLFWWindow::getWidth() const
{
  return m_width;
}

int GLFWWindow::getHeight() const
{
  return m_height;
}

int GLFWWindow::getScreenCount()
{
  int count;
  glfwGetMonitors(&count);
  return count;
}

SizeI GLFWWindow::getScreenSize(unsigned int screenIndex)
{
  SizeI screenSize;
  int count;
  GLFWmonitor** monitors = glfwGetMonitors(&count);
  if (screenIndex < count)
  {
    const GLFWvidmode* mode = glfwGetVideoMode(monitors[screenIndex]);
    screenSize = {mode->width, mode->height};
  }
  return screenSize;
}

void GLFWWindow::run()
{
  while (!glfwWindowShouldClose(m_window))
  {
    glfwPollEvents();
    renderLoop();

    if (isOpenGLAPI(m_api))
    {
      glfwSwapBuffers(m_window);
    }
  }
}

void GLFWWindow::onKey(int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
  {
    glfwSetWindowShouldClose(m_window, GL_TRUE);
  }
}

void GLFWWindow::onMouseMove(double x, double y) {}

void GLFWWindow::onMouseButton(int button, int action, int mods) {}

void GLFWWindow::onScroll(double xOffset, double yOffset) {}

void GLFWWindow::onResize(int width, int height)
{
  m_width = width;
  m_height = height;
}

void GLFWWindow::renderLoop() {}
