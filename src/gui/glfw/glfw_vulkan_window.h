#pragma once

// GLFW
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "glfw_window.h"
#include "glfw_window_options.h"
#include "render/vulkan/vulkan_context_interface.h"
#include "render/vulkan/vulkan_context_helper.h"

// STL
#include <functional>
#include <memory>


class GLFWVulkanWindow : public GLFWWindow, public IVulkanContext
{
public:
  GLFWVulkanWindow(
      GLFWWindowOptions const& options);

  ~GLFWVulkanWindow() override;

  virtual void drawFrame() = 0;

protected:   
  void onResize(int width, int height) override;
  void renderLoop() override;  

  void recreateSwapChain();

  // IVulkanContext impl
  VkInstance getInstance() const override;
  VkPhysicalDevice getPhysicalDevice() const override;
  VkPhysicalDeviceProperties const& getPhysicalDeviceProperties() const override;
  VkDevice getDevice() const override;
  VkQueue getGraphicsQueue() const override;
  VkCommandPool getCommandPool() const override;
  VkDescriptorPool getDescriptorPool() const override;
  VkRenderPass getDefaultRenderPass() const override;
  VkCommandBuffer getCurrentCommandBuffer() const override;
  VkFramebuffer getCurrentFramebuffer() const override;  
  unsigned int getConcurrentFrameCount() const override;
  SizeI getSwapChainImageSize() const override;
  int getCurrentFrame() const override;
  VkSampleCountFlagBits getSampleCountFlagBits() const override;
  VkFormat getColorFormat() const override;
  VkFormat getDepthStencilFormat() const override;

private:
  VkInstance m_instance;
  VkDebugUtilsMessengerEXT m_debugMessenger;
  VkSurfaceKHR m_surface;
  VulkanPhysicalDevice m_physicalDevice;
  VulkanLogicalDevice m_logicalDevice;
  VulkanSwapChain m_swapChain;
  VkCommandPool m_commandPool;
  VkDescriptorPool m_descriptorPool;
  std::vector<VkCommandBuffer> m_commandBuffers;
  VulkanSyncObjects m_syncObjects;
  uint32_t m_imageIndex = 0;
  uint32_t m_currentFrame = 0;
  bool m_framebufferResized = false;
  bool m_vsync = true;
  bool m_enableValidationLayers = false;
};

