#include "render/opengl/opengl.h"
#include "glfw_opengl_window.h"

// STL
#include <stdexcept>

GLFWOpenGLWindow::GLFWOpenGLWindow(
    GLFWWindowOptions const& options)
  : GLFWWindow(options.title, GraphicsAPI::OpenGL_3_3, options.size, options.fullscreen, options.sampleCount)
  , m_vsync(options.vsync)
{
  glfwWindowHint(GLFW_SRGB_CAPABLE, 1);
  initOpenGL();
}

OpenGLVersion GLFWOpenGLWindow::getOpenGLVersion() const
{
  return {3, 3, false};
}

SizeI GLFWOpenGLWindow::getSize() const
{
  return {m_width, m_height};
}

int GLFWOpenGLWindow::getSampleCount() const
{
  return m_sampleCount;
}

void GLFWOpenGLWindow::onResize(int width, int height)
{
  GLFWWindow::onResize(width, height);
  m_framebufferResized = true;  
}

void GLFWOpenGLWindow::renderLoop()
{
  drawFrame();

  if (m_framebufferResized)
  {
    m_framebufferResized = false;
    recreateSwapChain();
  }
}

void GLFWOpenGLWindow::recreateSwapChain()
{
  SizeI framebufferSize;

  while (framebufferSize.width == 0 || framebufferSize.height == 0)
  {
    glfwGetFramebufferSize(m_window, &framebufferSize.width, &framebufferSize.height);
    glfwWaitEvents();
  }
}

