#include "glfw_app.h"

// STL
#include <iostream>

static void onError(int error, char const* description)
{
  std::cout << "Error: " << description << std::endl;
}

GLFWApp::GLFWApp()
{
  glfwSetErrorCallback(onError);
  if (!glfwInit())
    exit(EXIT_FAILURE);
}
