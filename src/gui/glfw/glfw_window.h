#pragma once

#include "utils/size.h"

// GLFW
#include <GLFW/glfw3.h>

// STL
#include <string>

enum class GraphicsAPI
{
  OpenGL_3_3,
  OpenGL_4_5,
  Vulkan_1_2
};

class GLFWWindow
{
public:
  GLFWWindow(
      std::string const& title,
      GraphicsAPI api,
      SizeI const& size,
      bool fullscreen,
      int sampleCount);

  virtual ~GLFWWindow();

  void setFullscreen(bool fullscreen);
  void setSize(SizeI const size);

  int getX() const;
  int getY() const;
  int getWidth() const;
  int getHeight() const;

  static int getScreenCount();
  static SizeI getScreenSize(unsigned int screenIndex);

  void run();

protected:
  virtual void renderLoop();

  virtual void onKey(int key, int scancode, int action, int mods);

  virtual void onMouseMove(double x, double y);
  virtual void onMouseButton(int button, int action, int mods);
  virtual void onScroll(double xOffset, double yOffset);

  virtual void onResize(int width, int height);

  GraphicsAPI m_api;
  GLFWwindow* m_window = nullptr;
  int m_x = 100;
  int m_y = 100;
  int m_width = 800;
  int m_height = 600;
  int m_sampleCount = 1;
};


