#include "glfw_vulkan_window.h"
#include "utils/log_manager.h"

// STL
#include <stdexcept>

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

GLFWVulkanWindow::GLFWVulkanWindow(
    GLFWWindowOptions const& options)
  : GLFWWindow(options.title, GraphicsAPI::Vulkan_1_2, options.size, options.fullscreen, options.sampleCount)
  , m_vsync(options.vsync)
  , m_enableValidationLayers(
      enableValidationLayers &&
      VulkanContextHelper::checkValidationLayerSupport())
{
  uint32_t glfwExtensionCount = 0;
  const char** glfwExtensions;
  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
  std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

  m_instance = VulkanContextHelper::createInstance(options.title, m_enableValidationLayers, extensions);

  if (m_enableValidationLayers)
  {
    m_debugMessenger = VulkanContextHelper::createDebugMessenger(m_instance);
  }

  if (glfwCreateWindowSurface(m_instance, m_window, nullptr, &m_surface) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create window surface!");
  }

  m_physicalDevice = VulkanContextHelper::pickPhysicalDevice(m_instance, m_surface);

  LOG_INFO("Device name: ", m_physicalDevice.properties.deviceName);

  m_logicalDevice = VulkanContextHelper::createLogicalDevice(
        m_physicalDevice.device,
        m_surface,
        m_enableValidationLayers);

  SizeI framebufferSize;
  glfwGetFramebufferSize(m_window, &framebufferSize.width, &framebufferSize.height);

  m_swapChain = VulkanContextHelper::createSwapChain(
        m_physicalDevice,
        m_logicalDevice.device,
        m_surface,
        framebufferSize,
        m_vsync,
        m_sampleCount);

  m_commandPool = VulkanContextHelper::createCommandPool(
        m_physicalDevice.device,
        m_logicalDevice.device);

  m_descriptorPool = VulkanContextHelper::createDescriptorPool(
        m_logicalDevice.device);

  m_commandBuffers = VulkanContextHelper::createCommandBuffers(
        m_logicalDevice.device,
        m_commandPool);

  m_syncObjects = VulkanContextHelper::createSyncObjects(
        m_logicalDevice.device);
}

GLFWVulkanWindow::~GLFWVulkanWindow()
{
  VulkanContextHelper::releaseSwapChain(m_logicalDevice.device, m_swapChain);
  VulkanContextHelper::releaseSyncObjects(m_logicalDevice.device, m_syncObjects);
  VulkanContextHelper::releaseCommandPool(m_logicalDevice.device, m_commandPool);
  VulkanContextHelper::releaseDescriptorPool(m_logicalDevice.device, m_descriptorPool);
  VulkanContextHelper::releaseLogicalDevice(m_logicalDevice);

  if (m_enableValidationLayers)
  {
    VulkanContextHelper::releaseDebugMessenger(m_instance, m_debugMessenger);
  }

  VulkanContextHelper::releaseSurface(m_instance, m_surface);
  VulkanContextHelper::releaseInstance(m_instance);
}

void GLFWVulkanWindow::onResize(int width, int height)
{
  GLFWWindow::onResize(width, height);
  m_framebufferResized = true;  
}

void GLFWVulkanWindow::renderLoop()
{
  vkWaitForFences(m_logicalDevice.device, 1, &m_syncObjects.inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

  VkResult result = vkAcquireNextImageKHR(
        m_logicalDevice.device,
        m_swapChain.swapChain,
        UINT64_MAX,
        m_syncObjects.imageAvailableSemaphores[m_currentFrame],
        VK_NULL_HANDLE,
        &m_imageIndex);

  if (result == VK_ERROR_OUT_OF_DATE_KHR)
  {
    recreateSwapChain();
    return;
  }
  else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
  {
    throw std::runtime_error("failed to acquire swap chain image!");
  }

  vkResetFences(m_logicalDevice.device, 1, &m_syncObjects.inFlightFences[m_currentFrame]);
  vkResetCommandBuffer(m_commandBuffers[m_currentFrame], 0);

  VkCommandBufferBeginInfo beginInfo{};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  if (vkBeginCommandBuffer(m_commandBuffers[m_currentFrame], &beginInfo) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to begin recording command buffer!");
  }

  drawFrame();

  if (vkEndCommandBuffer(m_commandBuffers[m_currentFrame]) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to record command buffer!");
  }

  VkSubmitInfo submitInfo{};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

  VkSemaphore waitSemaphores[] = {m_syncObjects.imageAvailableSemaphores[m_currentFrame]};
  VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = waitSemaphores;
  submitInfo.pWaitDstStageMask = waitStages;

  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &m_commandBuffers[m_currentFrame];

  VkSemaphore signalSemaphores[] = {m_syncObjects.renderFinishedSemaphores[m_currentFrame]};
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = signalSemaphores;

  if (vkQueueSubmit(m_logicalDevice.graphicsQueue, 1, &submitInfo, m_syncObjects.inFlightFences[m_currentFrame]) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to submit draw command buffer!");
  }

  VkPresentInfoKHR presentInfo{};
  presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

  presentInfo.waitSemaphoreCount = 1;
  presentInfo.pWaitSemaphores = signalSemaphores;

  VkSwapchainKHR swapChains[] = {m_swapChain.swapChain};
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = swapChains;

  presentInfo.pImageIndices = &m_imageIndex;

  result = vkQueuePresentKHR(m_logicalDevice.presentQueue, &presentInfo);

  if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || m_framebufferResized)
  {
    m_framebufferResized = false;
    recreateSwapChain();
  }
  else if (result != VK_SUCCESS)
  {
    throw std::runtime_error("failed to present swap chain image!");
  }

  m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void GLFWVulkanWindow::recreateSwapChain()
{
  SizeI framebufferSize;

  while (framebufferSize.width == 0 || framebufferSize.height == 0)
  {
    glfwGetFramebufferSize(m_window, &framebufferSize.width, &framebufferSize.height);
    glfwWaitEvents();
  }

  vkDeviceWaitIdle(m_logicalDevice.device);

  VulkanContextHelper::releaseSwapChain(m_logicalDevice.device, m_swapChain);

  m_swapChain = VulkanContextHelper::createSwapChain(
        m_physicalDevice,
        m_logicalDevice.device,
        m_surface,
        framebufferSize,
        m_vsync,
        m_sampleCount);
}

VkInstance GLFWVulkanWindow::getInstance() const
{
  return m_instance;
}

VkPhysicalDevice GLFWVulkanWindow::getPhysicalDevice() const
{
  return m_physicalDevice.device;
}

const VkPhysicalDeviceProperties &GLFWVulkanWindow::getPhysicalDeviceProperties() const
{
  return m_physicalDevice.properties;
}

VkDevice GLFWVulkanWindow::getDevice() const
{
  return m_logicalDevice.device;
}

VkQueue GLFWVulkanWindow::getGraphicsQueue() const
{
  return m_logicalDevice.graphicsQueue;
}

VkCommandPool GLFWVulkanWindow::getCommandPool() const
{
  return m_commandPool;
}

VkDescriptorPool GLFWVulkanWindow::getDescriptorPool() const
{
  return m_descriptorPool;
}

VkRenderPass GLFWVulkanWindow::getDefaultRenderPass() const
{
  return m_swapChain.renderPass;
}

VkCommandBuffer GLFWVulkanWindow::getCurrentCommandBuffer() const
{
  return m_commandBuffers[m_currentFrame];
}

VkFramebuffer GLFWVulkanWindow::getCurrentFramebuffer() const
{
  return m_swapChain.framebuffers[m_imageIndex];
}

unsigned int GLFWVulkanWindow::getConcurrentFrameCount() const
{
  return MAX_FRAMES_IN_FLIGHT;
}

SizeI GLFWVulkanWindow::getSwapChainImageSize() const
{
  return {static_cast<int>(m_swapChain.extent.width), static_cast<int>(m_swapChain.extent.height)};
}

int GLFWVulkanWindow::getCurrentFrame() const
{
  return m_currentFrame;
}

VkSampleCountFlagBits GLFWVulkanWindow::getSampleCountFlagBits() const
{
  return m_swapChain.sampleCount;
}

VkFormat GLFWVulkanWindow::getColorFormat() const
{
  return m_swapChain.colorFormat;
}

VkFormat GLFWVulkanWindow::getDepthStencilFormat() const
{
  return m_swapChain.depthStencilFormat;
}
