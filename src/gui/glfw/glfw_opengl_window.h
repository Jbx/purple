#pragma once

// GLFW
#include <GLFW/glfw3.h>

#include "glfw_window.h"
#include "glfw_window_options.h"
#include "render/opengl/opengl_context_interface.h"

// STL
#include <functional>
#include <memory>


class GLFWOpenGLWindow : public GLFWWindow, public IOpenGLContext
{
public:
  GLFWOpenGLWindow(
      GLFWWindowOptions const& options);

  virtual void drawFrame() = 0;

  // IOpenGLContext impl
  OpenGLVersion getOpenGLVersion() const override;
  SizeI getSize() const override;
  int getSampleCount() const override;

protected:   
  void onResize(int width, int height) override;
  void renderLoop() override;  

  void recreateSwapChain();

private:
  bool m_framebufferResized = false;
  bool m_vsync = true;
};

