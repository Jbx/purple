#pragma once

#include "utils/size.h"

// STL
#include <string>

struct GLFWWindowOptions
{
  std::string title;
  SizeI size = {1280, 720};
  bool fullscreen = false;
  int sampleCount = 1;
  bool vsync = true;
};
