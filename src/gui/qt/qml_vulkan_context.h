#include "render/vulkan/vulkan_context_interface.h"

// Qt
#include <QQuickWindow>

class QmlVulkanContext : public IVulkanContext
{
public:
  QmlVulkanContext(QQuickWindow& window);
  ~QmlVulkanContext() override;

  VkInstance getInstance() const override;
  VkPhysicalDevice getPhysicalDevice() const override;
  VkPhysicalDeviceProperties const& getPhysicalDeviceProperties() const override;
  VkDevice getDevice() const override;
  VkQueue getGraphicsQueue() const override;
  VkCommandPool getCommandPool() const override;
  VkRenderPass getDefaultRenderPass() const override;
  VkCommandBuffer getCurrentCommandBuffer() const override;
  VkFramebuffer getCurrentFramebuffer() const override;
  VkDescriptorPool getDescriptorPool() const override;
  unsigned int getConcurrentFrameCount() const override;
  SizeI getSwapChainImageSize() const override;
  int getCurrentFrame() const override;
  VkSampleCountFlagBits getSampleCountFlagBits() const override;
  VkFormat getColorFormat() const override;
  VkFormat getDepthStencilFormat() const override;

private:
  QQuickWindow& m_window;
  VkInstance m_instance;
  VkPhysicalDevice m_physicalDevice;
  VkPhysicalDeviceProperties m_physicalDeviceProperties;
  VkDevice m_device;
  VkCommandPool m_commandPool;
};
