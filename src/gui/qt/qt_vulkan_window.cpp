#include "qt_vulkan_window.h"

QtVulkanWindowRendererProxy::QtVulkanWindowRendererProxy(QVulkanWindow &window, IQtVulkanRenderer &renderer, int sampleCount)
    : m_window(window)
    , m_renderer(renderer)
{
    const auto counts = window.supportedSampleCounts();
    if (counts.contains(sampleCount))
    {
        m_window.setSampleCount(sampleCount);
    }
}

void QtVulkanWindowRendererProxy::initResources()
{
    m_context = std::make_unique<VulkanContext>(m_window);
    m_renderer.init(m_context->m_engine);
}

void QtVulkanWindowRendererProxy::releaseResources()
{
    m_context.reset();
}

void QtVulkanWindowRendererProxy::initSwapChainResources()
{
    m_renderer.onResize(
        m_context->m_engine,
        m_window.width(),
        m_window.height());
}

void QtVulkanWindowRendererProxy::startNextFrame()
{
    m_renderer.draw(m_context->m_engine);

    m_window.frameReady();
    m_window.requestUpdate();
}

QtVulkanWindow::QtVulkanWindow(IQtVulkanRenderer &renderer, int sampleCount)
    : m_renderer(renderer)
    , m_sampleCount(sampleCount)
{

}

QVulkanWindowRenderer *QtVulkanWindow::createRenderer()
{
    return new QtVulkanWindowRendererProxy(*this, m_renderer, m_sampleCount);
}
