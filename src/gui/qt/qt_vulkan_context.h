#include "render/vulkan/vulkan_context_interface.h"

// Qt
#include <QVulkanWindow>

class QtVulkanContext : public IVulkanContext
{
public:
  QtVulkanContext(QVulkanWindow& window);

  ~QtVulkanContext() override;

  VkInstance getInstance() const override;
  VkPhysicalDevice getPhysicalDevice() const override;
  VkPhysicalDeviceProperties const& getPhysicalDeviceProperties() const override;
  VkDevice getDevice() const override;
  VkQueue getGraphicsQueue() const override;
  VkCommandPool getCommandPool() const override;
  VkDescriptorPool getDescriptorPool() const override;
  VkRenderPass getDefaultRenderPass() const override;
  VkCommandBuffer getCurrentCommandBuffer() const override;
  VkFramebuffer getCurrentFramebuffer() const override;
  unsigned int getConcurrentFrameCount() const override;
  SizeI getSwapChainImageSize() const override;
  int getCurrentFrame() const override;
  VkSampleCountFlagBits getSampleCountFlagBits() const override;
  VkFormat getColorFormat() const override;
  VkFormat getDepthStencilFormat() const override;

private:
  QVulkanWindow& m_window;
  VkDescriptorPool m_descriptorPool;
};
