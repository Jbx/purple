#pragma once

#include "qt_vulkan_context.h"
#include "engine/engine.h"
#include "render/vulkan/vulkan_renderer.h"

// Qt
#include <QVulkanWindow>

using VulkanEngine = Engine<VulkanRenderer>;


class IQtVulkanRenderer
{
public:
  virtual void init(VulkanEngine& engine) = 0;
  virtual void draw(VulkanEngine& engine) = 0;
  virtual void onResize(VulkanEngine& engine, int width, int height) = 0;
};

class QtVulkanWindowRendererProxy : public QVulkanWindowRenderer
{
public:
  QtVulkanWindowRendererProxy(
      QVulkanWindow& window,
      IQtVulkanRenderer& renderer,
      int sampleCount);

  void initResources() override;
  void releaseResources() override;
  void initSwapChainResources() override;
  void startNextFrame() override;

protected:
  QVulkanWindow& m_window;
  IQtVulkanRenderer& m_renderer;

  class VulkanContext
  {
  public:
    VulkanContext(QVulkanWindow& window)
      : m_context(window)
      , m_renderer(m_context)
      , m_engine(m_renderer) {}

  private:
    friend class QtVulkanWindowRendererProxy;
    QtVulkanContext m_context;
    VulkanRenderer m_renderer;
    Engine<VulkanRenderer> m_engine;
  };

  std::unique_ptr<VulkanContext> m_context;
};

class QtVulkanWindow : public QVulkanWindow
{
public:
  QtVulkanWindow(IQtVulkanRenderer& renderer, int sampleCount);

private:
  QVulkanWindowRenderer *createRenderer() override;

  IQtVulkanRenderer& m_renderer;
  int m_sampleCount;
};
