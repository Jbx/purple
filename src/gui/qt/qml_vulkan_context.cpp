#include "qml_vulkan_context.h"
#include "render/vulkan/vulkan_context_helper.h"

// GLM
#include <glm/mat4x4.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Qt
#include <QVulkanInstance>
#include <QVulkanFunctions>


QmlVulkanContext::QmlVulkanContext(QQuickWindow& window)
  : m_window(window)
{
  QSGRendererInterface *rif = m_window.rendererInterface();

  QVulkanInstance* vulkanInstance = reinterpret_cast<QVulkanInstance *>(
      rif->getResource(&m_window, QSGRendererInterface::VulkanInstanceResource));

  m_instance = vulkanInstance->vkInstance();

  m_physicalDevice = *reinterpret_cast<VkPhysicalDevice *>(rif->getResource(&m_window, QSGRendererInterface::PhysicalDeviceResource));
  vkGetPhysicalDeviceProperties(m_physicalDevice, &m_physicalDeviceProperties);

  m_device = *reinterpret_cast<VkDevice *>(rif->getResource(&m_window, QSGRendererInterface::DeviceResource));

  m_commandPool = VulkanContextHelper::createCommandPool(
        m_physicalDevice,
        m_device);
}

QmlVulkanContext::~QmlVulkanContext()
{
  VulkanContextHelper::releaseCommandPool(m_device, m_commandPool);
}

VkInstance QmlVulkanContext::getInstance() const
{
  return m_instance;
}

VkPhysicalDevice QmlVulkanContext::getPhysicalDevice() const
{
  return m_physicalDevice;
}

VkPhysicalDeviceProperties const& QmlVulkanContext::getPhysicalDeviceProperties() const
{
  return m_physicalDeviceProperties;
}

VkDevice QmlVulkanContext::getDevice() const
{
  return m_device;
}

VkQueue QmlVulkanContext::getGraphicsQueue() const
{
  QSGRendererInterface *rif = m_window.rendererInterface();
  return *reinterpret_cast<VkQueue *>(rif->getResource(&m_window, QSGRendererInterface::CommandQueueResource));
}

VkCommandPool QmlVulkanContext::getCommandPool() const
{
  return m_commandPool;
}

VkRenderPass QmlVulkanContext::getDefaultRenderPass() const
{
  QSGRendererInterface *rif = m_window.rendererInterface();
  return *reinterpret_cast<VkRenderPass *>(rif->getResource(&m_window, QSGRendererInterface::RenderPassResource));
}

VkCommandBuffer QmlVulkanContext::getCurrentCommandBuffer() const
{
  QSGRendererInterface *rif = m_window.rendererInterface();
  return  *reinterpret_cast<VkCommandBuffer *>(
              rif->getResource(&m_window, QSGRendererInterface::CommandListResource));
}

VkFramebuffer QmlVulkanContext::getCurrentFramebuffer() const
{
  return VkFramebuffer();
}

unsigned int QmlVulkanContext::getConcurrentFrameCount() const
{
  return m_window.graphicsStateInfo().currentFrameSlot;
}

SizeI QmlVulkanContext::getSwapChainImageSize() const
{
  auto size = m_window.size();
  return {size.width(), size.height()};
}

int QmlVulkanContext::getCurrentFrame() const
{
  return m_window.graphicsStateInfo().framesInFlight;
}

VkSampleCountFlagBits QmlVulkanContext::getSampleCountFlagBits() const
{
  return VK_SAMPLE_COUNT_1_BIT;
}

VkFormat QmlVulkanContext::getColorFormat() const
{
  return VkFormat();
}

VkFormat QmlVulkanContext::getDepthStencilFormat() const
{
  return VkFormat();
}
