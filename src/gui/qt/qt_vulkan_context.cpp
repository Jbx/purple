#include "qt_vulkan_context.h"
#include "render/vulkan/vulkan_context_helper.h"


QtVulkanContext::QtVulkanContext(QVulkanWindow& window)
  : m_window(window)
{
  m_descriptorPool = VulkanContextHelper::createDescriptorPool(
        m_window.device());
}

QtVulkanContext::~QtVulkanContext()
{
  VulkanContextHelper::releaseDescriptorPool(
        m_window.device(), m_descriptorPool);
}

VkInstance QtVulkanContext::getInstance() const
{
  return m_window.vulkanInstance()->vkInstance();
}

VkPhysicalDevice QtVulkanContext::getPhysicalDevice() const
{
  return m_window.physicalDevice();
}

VkPhysicalDeviceProperties const& QtVulkanContext::getPhysicalDeviceProperties() const
{
  return *m_window.physicalDeviceProperties();
}

VkQueue QtVulkanContext::getGraphicsQueue() const
{
  return m_window.graphicsQueue();
}

VkCommandPool QtVulkanContext::getCommandPool() const
{
  return m_window.graphicsCommandPool();
}

VkDescriptorPool QtVulkanContext::getDescriptorPool() const
{
  return m_descriptorPool;
}

VkDevice QtVulkanContext::getDevice() const
{
  return m_window.device();
}

VkRenderPass QtVulkanContext::getDefaultRenderPass() const
{
  return m_window.defaultRenderPass();
}

VkCommandBuffer QtVulkanContext::getCurrentCommandBuffer() const
{
  return m_window.currentCommandBuffer();
}

VkFramebuffer QtVulkanContext::getCurrentFramebuffer() const
{
  return m_window.currentFramebuffer();
}

unsigned int QtVulkanContext::getConcurrentFrameCount() const
{
  return m_window.concurrentFrameCount();
}

SizeI QtVulkanContext::getSwapChainImageSize() const
{
  auto size = m_window.swapChainImageSize();
  return {size.width(), size.height()};
}

int QtVulkanContext::getCurrentFrame() const
{
  return m_window.currentFrame();
}

VkSampleCountFlagBits QtVulkanContext::getSampleCountFlagBits() const
{
  return m_window.sampleCountFlagBits();
}

VkFormat QtVulkanContext::getColorFormat() const
{
  return m_window.colorFormat();
}

VkFormat QtVulkanContext::getDepthStencilFormat() const
{
  return m_window.depthStencilFormat();
}

