#include "texture_container.h"
#include "utils/assert.h"

// STL
#include <algorithm>

TextureContainer::TextureContainer(
    TextureList const& textures)
  : m_textures(textures)
{}

TextureList const& TextureContainer::getTextures() const
{
  return m_textures;
}

size_t TextureContainer::size() const
{
  return m_textures.size();
}

bool TextureContainer::empty() const
{
  return m_textures.empty();
}

void TextureContainer::update(std::string const& name, Ref<ITexture> const& texture)
{
  auto it = std::find_if(
        m_textures.begin(),
        m_textures.end(),
        [&name](auto const& texture) { return texture.first == name; });
  ASSERT(it != m_textures.end(), "Texture \"", name, "\" not found!");
  it->second = texture;
}

Ref<ITexture> TextureContainer::operator[](std::string const& name)
{
  auto it = std::find_if(
        m_textures.begin(),
        m_textures.end(),
        [&name](auto const& texture) { return texture.first == name; });
  ASSERT(it != m_textures.end(), "Texture \"", name, "\" not found!");
  return it->second;
}

bool TextureContainer::contains(std::string const& name) const
{
  auto it = std::find_if(
        m_textures.begin(),
        m_textures.end(),
        [&name](auto const& property) { return property.first == name; });
  return it != m_textures.end();
}
