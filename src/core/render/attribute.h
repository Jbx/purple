#pragma once

// STL
#include <vector>
#include <numeric>
#include <cstddef>

enum AttributeType
{
  Float,
  Vec2,
  Vec3,
  Vec4
};

constexpr size_t count(AttributeType type)
{
  switch (type)
  {
  case Float:
    return 1;
  case Vec2:
    return 2;
  case Vec3:
    return 3;
  case Vec4:
    return 4;
  }
  return 0; // avoid warning
}

constexpr size_t size(AttributeType type)
{
  return count(type) * sizeof(float);
}

using AttributeLayout = std::vector<AttributeType>;

static size_t offsetOf(
    AttributeLayout const& attributes,
    std::vector<AttributeType>::const_iterator const& it)
{
  return static_cast<size_t>(std::accumulate(
                               attributes.begin(),
                               it,
                               static_cast<size_t>(0),
                               [](size_t currentOffset, auto const& attribute)
                             {
                               return currentOffset + size(attribute);
                             }));
}

static size_t stride(AttributeLayout const& attributes)
{
  return offsetOf(attributes, attributes.end());
}
