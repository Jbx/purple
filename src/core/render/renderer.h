#pragma once

#include "attribute.h"
#include "render_states.h"
#include "shader.h"
#include "utils/buffer_view.h"
#include "images/image_interface.h"
#include "pipeline_interface.h"
#include "texture_container.h"
#include "vertex_buffer_interface.h"
#include "uniform_buffer_interface.h"
#include "framebuffer_interface.h"
#include "render_pass_interface.h"

template<typename Impl>
class Renderer
{
public:  
  template<typename VertexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices)
  {
    return self().createVertexBuffer(layout, vertices);
  }

  template<typename VertexType, typename IndexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices)
  {
    return self().createVertexBuffer(layout, vertices, indices);
  }

  Scoped<IUniformBuffer> createUniformBuffer(
      std::vector<NamedPropertyBuffer> const& properties,
      TextureContainer const& textures)
  {
    return self().createUniformBuffer(
          properties,
          textures);
  }

  Scoped<ITexture> createTexture2D(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler)
  {
    return self().createTexture2D(image, generateMipmap, sampler);
  }

  Scoped<ITexture> createTexture2D(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler)
  {
    return self().createTexture2D(format, size, generateMipmap, sampler);
  }

  Scoped<ITexture> createTextureCube(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler)
  {
    return self().createTextureCube(image, generateMipmap, sampler);
  }

  Scoped<ITexture> createTextureCube(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler)
  {
    return self().createTextureCube(format, size, generateMipmap, sampler);
  }

  Scoped<IPipeline> createPipeline(
      IRenderPass const& renderPass,
      Shader const& shader,
      RenderStates const& renderStates,
      unsigned int uniformBufferCount,
      TextureContainer const& textures)
  {
    return self().createPipeline(
          renderPass,
          shader,
          renderStates,
          uniformBufferCount,
          textures);
  }

  Scoped<IFramebuffer> createFramebuffer(
      IRenderPass const& renderPass,
      std::vector<Attachment> const& attachments,
      SizeI const& size)
  {
    return self().createFramebuffer(
          renderPass,
          attachments,
          size);
  }

  IRenderPass& getDefaultRenderPass()
  {
    return self().getDefaultRenderPass();
  }

  IFramebuffer& getDefaultFramebuffer()
  {
    return self().getDefaultFramebuffer();
  }

  Scoped<IRenderPass> createRenderPass(
      std::vector<AttachmentFormat> const& attachments,
      int sampleCount)
  {
    return self().createRenderPass(
          attachments,
          sampleCount);
  }

private:
  Impl& self() { return static_cast<Impl&>(*this); }
  Impl const& self() const { return static_cast<Impl const&>(*this); }
};
