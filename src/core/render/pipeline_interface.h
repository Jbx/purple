#pragma once

#include "render_pass_interface.h"

// STL
#include <memory>

class IPipeline
{
public:
  virtual ~IPipeline() = default;
  virtual void bind(IRenderPass const& renderPass) = 0;
};
