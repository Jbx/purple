#pragma once

#include "utils/ref.h"
#include "utils/size.h"

enum class TextureFormat
{
  RGBA_UNorm,
  RGBA_16F,
  RGBA_32F,
  Depth
};

enum class TextureTarget
{
  Target2D,
  TargetCube
};

enum class TextureFilter
{
  Nearest,
  Linear,
  NearestMipMapNearest,
  NearestMipMapLinear,
  LinearMipMapNearest,
  LinearMipMapLinear
};

enum class TextureWrapMode
{
  Repeat,
  MirrorRepeat,
  ClampToEdge,
  ClampToBorder
};

struct Sampler
{
  TextureFilter minFilter = TextureFilter::Nearest;
  TextureFilter magFilter = TextureFilter::Nearest;
  TextureWrapMode wrapModeS = TextureWrapMode::Repeat;
  TextureWrapMode wrapModeT = TextureWrapMode::Repeat;
  TextureWrapMode wrapModeR = TextureWrapMode::Repeat;
};

class ITexture
{
public:
  virtual ~ITexture() = default;

  virtual SizeI getSize() const = 0;
  virtual TextureFormat getFormat() const = 0;
  virtual TextureTarget getTarget() const = 0;
};

enum class AttachmentPoint
{
  Color,
  Depth
};

enum class CubeMapFace
{
  PositiveX,
  NegativeX,
  PositiveY,
  NegativeY,
  PositiveZ,
  NegativeZ,
  All
};

struct Attachment
{
  Ref<ITexture> texture;
  AttachmentPoint attachmentPoint = AttachmentPoint::Color;
  CubeMapFace face = CubeMapFace::PositiveX;
  int mipLevel = 0;
};

using AttachmentFormat = std::pair<AttachmentPoint, TextureFormat>;

static AttachmentFormat fromAttachment(Attachment const& attachment)
{
  return AttachmentFormat{attachment.attachmentPoint, attachment.texture->getFormat()};
}

