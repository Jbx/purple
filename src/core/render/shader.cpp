#include "shader.h"
#include "utils/assert.h"

// STL
#include <fstream>

nlohmann::json json_parse(std::string const& filePath)
{
  std::ifstream file(filePath, std::ios::out | std::ios::binary);
  if (!file.is_open())
  {
    LOG_ERROR("Error opening shader file ", filePath);
  }

  std::vector<std::uint8_t> data(
        (std::istreambuf_iterator<char>(file)),
        std::istreambuf_iterator<char>());
  return nlohmann::json::from_cbor(data);
}

ShaderProgram::ShaderProgram(
    std::string const& vertexShaderFilePath,
    std::string const& geometryShaderFilePath,
    std::string const& fragmentShaderFilePath)
  : m_vertexShaderSource(json_parse(vertexShaderFilePath))
  , m_geometryShaderSource(json_parse(geometryShaderFilePath))
  , m_fragmentShaderSource(json_parse(fragmentShaderFilePath))
{
}

ShaderProgram::ShaderProgram(
    std::string const& vertexShaderFilePath,
    std::string const& fragmentShaderFilePath)
  : m_vertexShaderSource(json_parse(vertexShaderFilePath))
  , m_fragmentShaderSource(json_parse(fragmentShaderFilePath))
{
}

std::string toString(ShaderProgram::ShaderVersion version)
{
  switch (version)
  {
  case ShaderProgram::SPIRV:
    return "SPIRV";
  case ShaderProgram::GLSL_330:
    return "GLSL_330";
  case ShaderProgram::GLSL_300_ES:
    return "GLSL_300_ES";
  }

  ASSERT(false, "Unknow version!");
  return std::string();
}

std::vector<unsigned int> get(nlohmann::json const& json, ShaderProgram::ShaderVersion version)
{
  auto it = json.find(toString(version));
  if (it != json.end())
  {
    return it.value();
  }
  ASSERT(false, "Shader version not found!");
  return std::vector<unsigned int>();
}

std::vector<unsigned int> ShaderProgram::getVertexShaderSourceCode(ShaderVersion version) const
{
  return get(m_vertexShaderSource, version);
}

std::optional<std::vector<unsigned int>> ShaderProgram::getGeometryShaderSourceCode(ShaderVersion version) const
{
  if (m_geometryShaderSource)
  {
    return get(*m_geometryShaderSource, version);
  }
  return std::optional<std::vector<unsigned int>>();
}

std::vector<unsigned int> ShaderProgram::getFragmentShaderSourceCode(ShaderVersion version) const
{
  return get(m_fragmentShaderSource, version);
}

Shader::Shader(
    ShaderProgram const& program,
    AttributeLayout const& attributeLayout)
  : m_program(program)
  , m_attributeLayout(attributeLayout)
{
}

ShaderProgram const& Shader::getProgram() const
{
  return m_program;
}

AttributeLayout const& Shader::getAttributeLayout() const
{
  return m_attributeLayout;
}
