#pragma once

#include "utils/size.h"
#include "texture_interface.h"

// STL
#include <vector>

class IFramebuffer
{
public:
  virtual ~IFramebuffer() = default;

  virtual bool isDefaultFramebuffer() const = 0;

  virtual SizeI getSize() const = 0;
  virtual std::vector<AttachmentPoint> getAttachments() const = 0;
};
