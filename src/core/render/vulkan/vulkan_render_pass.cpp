#include "vulkan_render_pass.h"
#include "vulkan_converter.h"
#include "vulkan_framebuffer.h"
#include "vulkan_utils.h"
#include "utils/assert.h"

// STL
#include <algorithm>

DefaultVulkanRenderPass::DefaultVulkanRenderPass(
    IVulkanContext& context)
  : m_context(context)
{

}

VkRenderPass DefaultVulkanRenderPass::getRenderPass() const
{
  return m_context.getDefaultRenderPass();
}

VkSampleCountFlagBits DefaultVulkanRenderPass::getSampleCountFlagBits() const
{
  return m_context.getSampleCountFlagBits();
}

int DefaultVulkanRenderPass::getSampleCount() const
{
  return toSampleCount(m_context.getSampleCountFlagBits());
}

bool DefaultVulkanRenderPass::isDefaultRenderPass() const
{
  return true;
}

std::vector<AttachmentPoint> DefaultVulkanRenderPass::getAttachments() const
{
  return {AttachmentPoint::Color, AttachmentPoint::Depth};
}

void DefaultVulkanRenderPass::setClearColor(Color const& clearColor)
{
  m_clearColor = clearColor;
}

void DefaultVulkanRenderPass::setClearDepthValue(float clearDepthValue)
{
  m_clearDepthValue = clearDepthValue;
}

void DefaultVulkanRenderPass::setClearStencilValue(unsigned int clearStencilValue)
{
  m_clearStencilValue = clearStencilValue;
}

void DefaultVulkanRenderPass::begin(
    IFramebuffer const& framebuffer,
    RectI const& viewport)
{
  SizeI size = m_context.getSwapChainImageSize();

  std::array<VkClearValue, 3> clearValues;
  clearValues[0].color = { m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a };
  clearValues[1].depthStencil = { m_clearDepthValue, m_clearStencilValue };
  clearValues[2].color = { m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a };

  VkRenderPassBeginInfo renderPassInfo{};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  renderPassInfo.renderPass = m_context.getDefaultRenderPass();
  renderPassInfo.framebuffer = static_cast<IVulkanFramebuffer const&>(framebuffer).getFramebuffer();
  renderPassInfo.renderArea.offset = {0, 0};
  renderPassInfo.renderArea.extent.width = size.width;
  renderPassInfo.renderArea.extent.height = size.height;
  renderPassInfo.clearValueCount = m_context.getSampleCountFlagBits() > VK_SAMPLE_COUNT_1_BIT ? 3 : 2;
  renderPassInfo.pClearValues = clearValues.data();

  vkCmdBeginRenderPass(m_context.getCurrentCommandBuffer(), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

  // Flip the viewport to be consistent with OpenGL
  // see https://www.saschawillems.de/blog/2019/03/29/flipping-the-vulkan-viewport/
  // see https://anki3d.org/vulkan-coordinate-system/

  // TODO: handle viewport offset
  VkViewport vp;
  vp.x = viewport.x;
  vp.y = viewport.height - viewport.y;
  vp.width = viewport.width;
  vp.height = -(viewport.height - viewport.y);
  vp.minDepth = 0;
  vp.maxDepth = 1;
  vkCmdSetViewport(m_context.getCurrentCommandBuffer(), 0, 1, &vp);

  VkRect2D scissor;
  scissor.offset.x = 0;
  scissor.offset.y = 0;
  scissor.extent.width = size.width;
  scissor.extent.height = size.height;
  vkCmdSetScissor(m_context.getCurrentCommandBuffer(), 0, 1, &scissor);
}

void DefaultVulkanRenderPass::end()
{
  vkCmdEndRenderPass(m_context.getCurrentCommandBuffer());
}

VulkanRenderPass::VulkanRenderPass(
    IVulkanContext& context,
    std::vector<AttachmentFormat> const& attachments,
    int sampleCount)
  : m_context(context)
  , m_sampleCount(toVkSampleCount(sampleCount))
{
  ASSERT(!attachments.empty(), "Render pass must have at least one attachment!");

  std::vector<VkFormat> colorFormats;
  std::optional<VkFormat> depthStencilFormat;

  for (auto const& attachment : attachments)
  {
    if (attachment.first == AttachmentPoint::Color)
    {
      switch (attachment.second)
      {
      case TextureFormat::RGBA_UNorm:
        colorFormats.emplace_back(VK_FORMAT_R8G8B8A8_UNORM);
        break;

      case TextureFormat::RGBA_16F:
        colorFormats.emplace_back(VK_FORMAT_R16G16B16A16_SFLOAT);
        break;

      case TextureFormat::RGBA_32F:
        colorFormats.emplace_back(VK_FORMAT_R32G32B32A32_SFLOAT);
        break;

      default:
        break;
      }
    }

    if (attachment.first == AttachmentPoint::Depth)
    {
      ASSERT(!depthStencilFormat, "More than one depth stencil attachment is not supported!");
      depthStencilFormat.emplace(VK_FORMAT_D16_UNORM);
    }

    m_attachmentPoints.push_back(attachment.first);
  }

  m_renderPass = VulkanUtils::createRenderPass(
        m_context.getDevice(),
        colorFormats,
        depthStencilFormat,
        m_sampleCount);
}

VulkanRenderPass::~VulkanRenderPass()
{
  VulkanUtils::releaseRenderPass(m_context.getDevice(), m_renderPass);  
}

VkRenderPass VulkanRenderPass::getRenderPass() const
{
  return m_renderPass;
}

VkSampleCountFlagBits VulkanRenderPass::getSampleCountFlagBits() const
{
  return m_sampleCount;
}

bool VulkanRenderPass::isDefaultRenderPass() const
{
  return false;
}

std::vector<AttachmentPoint> VulkanRenderPass::getAttachments() const
{
  return m_attachmentPoints;
}

int VulkanRenderPass::getSampleCount() const
{
  return toSampleCount(m_sampleCount);
}

void VulkanRenderPass::setClearColor(Color const& clearColor)
{
  m_clearColor = clearColor;
}

void VulkanRenderPass::setClearDepthValue(float clearDepthValue)
{
  m_clearDepthValue = clearDepthValue;
}

void VulkanRenderPass::setClearStencilValue(unsigned int clearStencilValue)
{
  m_clearStencilValue = clearStencilValue;
}

void VulkanRenderPass::begin(
    IFramebuffer const& framebuffer,
    RectI const& viewport)
{
  SizeI size = framebuffer.getSize();

  std::vector<VkClearValue> clearValues;

  for (auto const& attachmentPoint : m_attachmentPoints)
  {
    if (attachmentPoint == AttachmentPoint::Color)
    {
      clearValues.emplace_back(VkClearValue{.color = {m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a}});
    }

    if (attachmentPoint == AttachmentPoint::Depth)
    {
      clearValues.emplace_back(VkClearValue{.depthStencil = {m_clearDepthValue, m_clearStencilValue}});
    }
  }

  VkRenderPassBeginInfo renderPassInfo{};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  renderPassInfo.renderPass = m_renderPass;
  renderPassInfo.framebuffer = static_cast<IVulkanFramebuffer const&>(framebuffer).getFramebuffer();
  renderPassInfo.renderArea.offset = {0, 0};
  renderPassInfo.renderArea.extent.width = size.width;
  renderPassInfo.renderArea.extent.height = size.height;
  renderPassInfo.clearValueCount = clearValues.size();
  renderPassInfo.pClearValues = clearValues.data();

  VkViewport vp;
  vp.x = viewport.x;
  vp.y = viewport.y;
  vp.width = viewport.width;
  vp.height = viewport.height;
  vp.minDepth = 0;
  vp.maxDepth = 1;
  vkCmdSetViewport(m_context.getCurrentCommandBuffer(), 0, 1, &vp);

  VkRect2D scissor;
  scissor.offset.x = 0;
  scissor.offset.y = 0;
  scissor.extent.width = size.width;
  scissor.extent.height = size.height;
  vkCmdSetScissor(m_context.getCurrentCommandBuffer(), 0, 1, &scissor);

  vkCmdBeginRenderPass(m_context.getCurrentCommandBuffer(), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
}

void VulkanRenderPass::end()
{
  vkCmdEndRenderPass(m_context.getCurrentCommandBuffer());
}
