#pragma once

#include "vulkan_context_interface.h"
#include "vulkan_utils.h"
#include "render/texture_interface.h"

class IVulkanTexture : public ITexture
{
public:
  virtual VkImageView getView() const = 0;  
  virtual VkSampler getSampler() const = 0;
};

class VulkanTexture final : public IVulkanTexture
{
public:
  VulkanTexture(
      IVulkanContext& context,
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  VulkanTexture(
      IVulkanContext& context,
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  ~VulkanTexture() override;

  SizeI getSize() const override;
  TextureFormat getFormat() const override;
  TextureTarget getTarget() const override;

  VkImageView getView() const override; 
  VkSampler getSampler() const override;

private:
  IVulkanContext& m_context;
  VulkanUtils::VulkanTexture m_texture;
  SizeI m_size;
  TextureFormat m_format;
};

class VulkanTextureCube final : public IVulkanTexture
{
public:
  VulkanTextureCube(
      IVulkanContext& context,
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  VulkanTextureCube(
      IVulkanContext& context,
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  ~VulkanTextureCube() override;

  SizeI getSize() const override;
  TextureFormat getFormat() const override;
  TextureTarget getTarget() const override;

  VkImageView getView() const override;  
  VkSampler getSampler() const override;

  VkImageView getView(int faceIndex, int mipLevel) const;

private:
  IVulkanContext& m_context;
  VulkanUtils::VulkanTextureCube m_texture;
  SizeI m_size;
  TextureFormat m_format;
};
