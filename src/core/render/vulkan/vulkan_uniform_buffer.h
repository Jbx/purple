#pragma once

#include "render/uniform_buffer_interface.h"
#include "render/texture_container.h"
#include "vulkan_context_interface.h"
#include "vulkan_utils.h"

class VulkanPipeline;

class VulkanUniformBuffer final : public IUniformBuffer
{
public:
  VulkanUniformBuffer(
      IVulkanContext& context,
      std::vector<NamedPropertyBuffer> const& properties,
      TextureContainer const& textures);

  ~VulkanUniformBuffer() override;

  void update(
      IPipeline const& pipeline,
      std::vector<NamedPropertyBuffer> const& properties) override;

  void bind(IPipeline const& pipeline) override;

private:
  friend class VulkanPipeline;
  IVulkanContext& m_context;
  VkDescriptorSetLayout m_descriptorSetLayout;
  VkDescriptorSet m_descriptorSet;
  std::vector<VulkanUtils::VulkanBuffer> m_buffers;
};

