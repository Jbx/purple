#include "vulkan_utils.h"
#include "utils/assert.h"

// STL
#include <algorithm>
#include <cstring>
#include <stdexcept>
#include <fstream>
#include <cmath>

namespace VulkanUtils {

uint32_t findMemoryType(
    VkPhysicalDevice physicalDevice,
    uint32_t typeFilter,
    VkMemoryPropertyFlags properties)
{
  VkPhysicalDeviceMemoryProperties memProperties;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

  for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
  {
    if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
    {
      return i;
    }
  }

  throw std::runtime_error("failed to find suitable memory type!");
}

VkFormat findSupportedFormat(
    VkPhysicalDevice physicalDevice,
    std::vector<VkFormat> const& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features)
{
  for (VkFormat format : candidates)
  {
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);

    if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
    {
      return format;
    }
    else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
    {
      return format;
    }
  }

  throw std::runtime_error("failed to find supported format!");
}

VkFormat findDepthStencilFormat(
    VkPhysicalDevice physicalDevice)
{
  return findSupportedFormat(
        physicalDevice,
        {VK_FORMAT_D32_SFLOAT_S8_UINT,
         VK_FORMAT_D32_SFLOAT,
         VK_FORMAT_D24_UNORM_S8_UINT,
         VK_FORMAT_D16_UNORM_S8_UINT,
         VK_FORMAT_D16_UNORM},
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
        );
}

VulkanBuffer createBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkDeviceSize size,
    VkBufferUsageFlags usage,
    VkMemoryPropertyFlags properties)
{
  VkBufferCreateInfo bufferInfo{};
  bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  bufferInfo.size = size;
  bufferInfo.usage = usage;
  bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VulkanBuffer buffer;
  if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer.buffer) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create buffer!");
  }

  VkMemoryRequirements memRequirements;
  vkGetBufferMemoryRequirements(device, buffer.buffer, &memRequirements);

  VkMemoryAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, properties);

  if (vkAllocateMemory(device, &allocInfo, nullptr, &buffer.memory) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to allocate buffer memory!");
  }

  vkBindBufferMemory(device, buffer.buffer, buffer.memory, 0);

  return buffer;
}

void copyBuffer(
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    VkBuffer srcBuffer,
    VkBuffer dstBuffer,
    VkDeviceSize size)
{
  VkCommandBufferAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = commandPool;
  allocInfo.commandBufferCount = 1;

  VkCommandBuffer commandBuffer;
  vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

  VkCommandBufferBeginInfo beginInfo{};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(commandBuffer, &beginInfo);

  VkBufferCopy copyRegion{};
  copyRegion.size = size;
  vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

  vkEndCommandBuffer(commandBuffer);

  VkSubmitInfo submitInfo{};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;

  vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
  vkQueueWaitIdle(graphicsQueue);

  vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

VulkanBuffer createUniformBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    size_t bufferSize)
{
  return createBuffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
}

VulkanImage createImage(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    uint32_t mipLevels,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageAspectFlags aspect,
    VkMemoryPropertyFlags properties)
{
  VkImageCreateInfo imageInfo{};
  imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent = { width, height, 1 };
  imageInfo.mipLevels = mipLevels;
  imageInfo.arrayLayers = 1;
  imageInfo.format = format;
  imageInfo.tiling = tiling;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = usage;
  imageInfo.samples = sampleCount;
  imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VulkanImage image;
  if (vkCreateImage(device, &imageInfo, nullptr, &image.image) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create image!");
  }

  VkMemoryRequirements memRequirements;
  vkGetImageMemoryRequirements(device, image.image, &memRequirements);

  VkMemoryAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, properties);

  if (vkAllocateMemory(device, &allocInfo, nullptr, &image.memory) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to allocate image memory!");
  }

  vkBindImageMemory(device, image.image, image.memory, 0);

  VkImageViewCreateInfo viewInfo{};
  viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  viewInfo.image = image.image;
  viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  viewInfo.format = format;
  viewInfo.subresourceRange.aspectMask = aspect;
  viewInfo.subresourceRange.baseMipLevel = 0;
  viewInfo.subresourceRange.levelCount = mipLevels;
  viewInfo.subresourceRange.baseArrayLayer = 0;
  viewInfo.subresourceRange.layerCount = 1;

  if (vkCreateImageView(device, &viewInfo, nullptr, &image.view) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create texture image view!");
  }

  // Create image view for each mip map level
  viewInfo.subresourceRange.levelCount = 1;

  for (int mipLevel = 0; mipLevel < mipLevels; ++mipLevel)
  {
    viewInfo.subresourceRange.baseMipLevel = mipLevel;

    auto& mipmapView = image.mipmapViews.emplace_back(VkImageView{});
    if (vkCreateImageView(device, &viewInfo, nullptr, &mipmapView) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create texture image view!");
    }
  }

  return image;
}

VulkanImageCube createCubeImage(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    uint32_t mipLevels,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageAspectFlags aspect,
    VkMemoryPropertyFlags properties)
{
  VkImageCreateInfo imageInfo{};
  imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent = { width, height, 1 };
  imageInfo.mipLevels = mipLevels;
  imageInfo.arrayLayers = 6;
  imageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
  imageInfo.format = format;
  imageInfo.tiling = tiling;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = usage;
  imageInfo.samples = sampleCount;
  imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VulkanImageCube image;
  if (vkCreateImage(device, &imageInfo, nullptr, &image.image) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create image!");
  }

  VkMemoryRequirements memRequirements;
  vkGetImageMemoryRequirements(device, image.image, &memRequirements);

  VkMemoryAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, memRequirements.memoryTypeBits, properties);

  if (vkAllocateMemory(device, &allocInfo, nullptr, &image.memory) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to allocate image memory!");
  }

  vkBindImageMemory(device, image.image, image.memory, 0);

  VkImageViewCreateInfo viewInfo{};
  viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  viewInfo.image = image.image;
  viewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
  viewInfo.format = format;
  viewInfo.components = { VK_COMPONENT_SWIZZLE_R };
  viewInfo.subresourceRange.aspectMask = aspect;
  viewInfo.subresourceRange.baseMipLevel = 0;
  viewInfo.subresourceRange.levelCount = mipLevels;
  viewInfo.subresourceRange.baseArrayLayer = 0;
  viewInfo.subresourceRange.layerCount = 6;

  if (vkCreateImageView(device, &viewInfo, nullptr, &image.view) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create texture image view!");
  }

  viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  viewInfo.subresourceRange.layerCount = 1;

  // Create image view for each face
  for (uint32_t i = 0; i < 6; i++)
  {
    viewInfo.subresourceRange.baseArrayLayer = i;
    if (vkCreateImageView(device, &viewInfo, nullptr, &image.faceViews[i].view) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create texture image view!");
    }

    viewInfo.subresourceRange.levelCount = 1;

    // Create image view for each mip map level
    for (int mipLevel = 0; mipLevel < mipLevels; ++mipLevel)
    {
      viewInfo.subresourceRange.baseMipLevel = mipLevel;

      auto& mipmapView = image.faceViews[i].mipmapViews.emplace_back(VkImageView{});
      if (vkCreateImageView(device, &viewInfo, nullptr, &mipmapView) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to create texture image view!");
      }
    }
  }

  return image;
}

void releaseImage(
    VkDevice device,
    VulkanImage image)
{
  vkDestroyImageView(device, image.view, nullptr);
  for (auto const& mipmapView : image.mipmapViews)
  {
    vkDestroyImageView(device, mipmapView, nullptr);
  }
  vkDestroyImage(device, image.image, nullptr);
  vkFreeMemory(device, image.memory, nullptr);
}

void releaseImage(
    VkDevice device,
    VulkanImageCube image)
{
  vkDestroyImageView(device, image.view, nullptr);
  for (auto const& faceView : image.faceViews)
  {
    vkDestroyImageView(device, faceView.view, nullptr);
    for (auto mipmapView : faceView.mipmapViews)
    {
      vkDestroyImageView(device, mipmapView, nullptr);
    }
  }
  vkDestroyImage(device, image.image, nullptr);
  vkFreeMemory(device, image.memory, nullptr);
}

void updateUniformBuffers(
    VkDevice device,
    VulkanBuffer uniformBuffer,
    std::vector<char> const& uniformBufferObject)
{
  void* data;
  vkMapMemory(device, uniformBuffer.memory, 0, uniformBufferObject.size(), 0, &data);
  std::memcpy(data, uniformBufferObject.data(), uniformBufferObject.size());
  vkUnmapMemory(device, uniformBuffer.memory);
}

VkCommandBuffer beginSingleTimeCommands(
    VkDevice device,
    VkCommandPool commandPool)
{
  VkCommandBufferAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = commandPool;
  allocInfo.commandBufferCount = 1;

  VkCommandBuffer commandBuffer;
  vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

  VkCommandBufferBeginInfo beginInfo{};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(commandBuffer, &beginInfo);

  return commandBuffer;
}

void endSingleTimeCommands(
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    VkCommandBuffer commandBuffer)
{
  vkEndCommandBuffer(commandBuffer);

  VkSubmitInfo submitInfo{};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;

  vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
  vkQueueWaitIdle(graphicsQueue);

  vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

void transitionImageLayout(
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    VkImage image,
    VkImageLayout oldLayout,
    VkImageLayout newLayout,
    uint32_t mipLevels,
    uint32_t layerCount)
{
  VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);

  VkImageMemoryBarrier barrier{};
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.oldLayout = oldLayout;
  barrier.newLayout = newLayout;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.image = image;
  barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = mipLevels;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = layerCount;

  VkPipelineStageFlags sourceStage;
  VkPipelineStageFlags destinationStage;

  if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
      newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
  {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

    sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  }
  else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
           newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
  {
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }
  else
  {
    throw std::invalid_argument("unsupported layout transition!");
  }

  vkCmdPipelineBarrier(
        commandBuffer,
        sourceStage, destinationStage,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
        );

  endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
}

void copyDataToBuffer(
    VkDevice device,
    VkDeviceMemory memory,
    const void* data,
    size_t dataSize)
{
  void* dataUnused;
  vkMapMemory(device, memory, 0, dataSize, 0, &dataUnused);
  std::memcpy(dataUnused, data, static_cast<size_t>(dataSize));
  vkUnmapMemory(device, memory);
}

void copyBufferToImage(
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    VkBuffer buffer,
    VkImage image,
    uint32_t width,
    uint32_t height,
    uint32_t layerIndex = 0,
    uint32_t mipLevel = 0,
    size_t offset = 0)
{
  VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);

  VkBufferImageCopy region{};
  region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel = mipLevel;
  region.imageSubresource.baseArrayLayer = layerIndex;
  region.imageSubresource.layerCount = 1;
  region.imageExtent.width = width >> mipLevel;
  region.imageExtent.height = height >> mipLevel;
  region.imageExtent.depth = 1;
  region.bufferOffset = offset;

  vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

  endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
}


constexpr VkFormat toVkFormat(ImageFormat format)
{
  switch (format)
  {
  case ImageFormat::RGB8:
    return VK_FORMAT_R8G8B8_UNORM;
  case ImageFormat::RGBA8:
    return VK_FORMAT_R8G8B8A8_UNORM;
  case ImageFormat::RGBA16F:
    return VK_FORMAT_R16G16B16A16_SFLOAT;
  case ImageFormat::RGBA_S3TC_DXT1:
    return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
  case ImageFormat::RGBA_S3TC_DXT3:
    return VK_FORMAT_BC2_UNORM_BLOCK;
  case ImageFormat::RGBA_S3TC_DXT5:
    return VK_FORMAT_BC3_UNORM_BLOCK;
  case ImageFormat::SRGB_ALPHA_BPTC_UNORM:
    return VK_FORMAT_BC7_SRGB_BLOCK;
  }
  assert(false && "Format not handled");
  return VK_FORMAT_R8G8B8_UNORM;
}

int computeMipLevels(SizeI const& size)
{
  return static_cast<uint32_t>(
        std::floor(std::log2(std::max(size.width, size.height)))) + 1;
}

VulkanTexture createTexture2D(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    IImage const& image,
    VkSampleCountFlagBits sampleCount,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  SizeI size = image.getSize();

  bool hasMipMap = image.getMipLevelCount() > 1;
  int mipLevels = hasMipMap ? image.getMipLevelCount() : (generateMipmap ? computeMipLevels(size) : 1);

  VkDeviceSize dataSize = image.getDataSize();

  auto textureImage = createImage(
        physicalDevice,
        device,
        size.width,
        size.height,
        mipLevels,
        sampleCount,
        toVkFormat(image.getFormat()),
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VulkanBuffer stagingBuffer = createBuffer(
        physicalDevice,
        device,
        dataSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  copyDataToBuffer(
        device,
        stagingBuffer.memory,
        image.getData(),
        static_cast<size_t>(dataSize));

  transitionImageLayout(
        device,
        graphicsQueue,
        commandPool,
        textureImage.image,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        mipLevels,
        1);

  for (uint32_t level = 0; level < (hasMipMap ? mipLevels : 1); level++)
  {
    copyBufferToImage(
          device,
          graphicsQueue,
          commandPool,
          stagingBuffer.buffer,
          textureImage.image,
          size.width,
          size.height,
          0,
          level,
          image.getOffset(0, level));
  }

  releaseBuffer(device, stagingBuffer);

  if (generateMipmap && !hasMipMap)
  {
    generateMipmaps(
          physicalDevice,
          device,
          graphicsQueue,
          commandPool,
          textureImage.image,
          toVkFormat(image.getFormat()),
          size,
          1);
  }
  else
  {
    transitionImageLayout(
          device,
          graphicsQueue,
          commandPool,
          textureImage.image,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
          mipLevels,
          1);
  }

  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  if (vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create sampler");
  }

  return {textureImage, sampler};
}

VulkanTextureCube createTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    IImage const& image,
    VkSampleCountFlagBits sampleCount,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  ASSERT(
        image.getFaceCount() == 6,
        "Cube map need an image array of 6 images");

  SizeI size = image.getSize();

  bool hasMipMap = image.getMipLevelCount() > 1;
  int mipLevels = hasMipMap ? image.getMipLevelCount() : (generateMipmap ? computeMipLevels(size) : 1);

  VkDeviceSize dataSize = image.getDataSize();
  auto format = toVkFormat(image.getFormat());

  auto textureImage = createCubeImage(
        physicalDevice,
        device,
        size.width,
        size.height,
        mipLevels,
        sampleCount,
        format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VulkanBuffer stagingBuffer = createBuffer(
        physicalDevice,
        device,
        dataSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  copyDataToBuffer(
        device,
        stagingBuffer.memory,
        image.getData(),
        dataSize);

  transitionImageLayout(
        device,
        graphicsQueue,
        commandPool,
        textureImage.image,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        mipLevels,
        6);

  for (uint32_t face = 0; face < 6; face++)
  {
    for (uint32_t level = 0; level < (hasMipMap ? mipLevels : 1); level++)
    {
      copyBufferToImage(
            device,
            graphicsQueue,
            commandPool,
            stagingBuffer.buffer,
            textureImage.image,
            size.width,
            size.height,
            face,
            level,
            image.getOffset(face, level));
    }
  }

  if (generateMipmap && !hasMipMap)
  {
    generateMipmaps(
          physicalDevice,
          device,
          graphicsQueue,
          commandPool,
          textureImage.image,
          format,
          size,
          6);
  }
  else
  {
    transitionImageLayout(
          device,
          graphicsQueue,
          commandPool,
          textureImage.image,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
          mipLevels,
          6);
  }

  releaseBuffer(device, stagingBuffer);

  // Create sampler
  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler);

  return {textureImage, sampler};
}

VulkanTexture createColorTexture(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  int mipLevels = generateMipmap ? computeMipLevels({static_cast<int>(width), static_cast<int>(height)}) : 1;

  auto textureImage = createImage(
        physicalDevice,
        device,
        width,
        height,
        mipLevels,
        sampleCount,
        format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  if (vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create sampler");
  }

  return {textureImage, sampler};
}

VulkanTexture createDepthTexture(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  int mipLevels = generateMipmap ? computeMipLevels({static_cast<int>(width), static_cast<int>(height)}) : 1;

  auto textureImage = createImage(
        physicalDevice,
        device,
        width,
        height,
        mipLevels,
        sampleCount,
        format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_DEPTH_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  if (vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create sampler");
  }

  return {textureImage, sampler};
}

VulkanTextureCube createColorTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  int mipLevels = generateMipmap ? computeMipLevels({static_cast<int>(width), static_cast<int>(height)}) : 1;

  auto textureImage = createCubeImage(
        physicalDevice,
        device,
        width,
        height,
        mipLevels,
        sampleCount,
        format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  if (vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create sampler");
  }

  return {textureImage, sampler};
}

VulkanTextureCube createDepthTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR)
{
  int mipLevels = generateMipmap ? computeMipLevels({static_cast<int>(width), static_cast<int>(height)}) : 1;

  auto textureImage = createCubeImage(
        physicalDevice,
        device,
        width,
        height,
        mipLevels,
        sampleCount,
        format,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_DEPTH_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  VkSamplerCreateInfo samplerCreateInfo{};
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.minFilter = minFilter;
  samplerCreateInfo.magFilter = magFilter;
  samplerCreateInfo.mipmapMode = mipmapMode;
  samplerCreateInfo.addressModeU = addressModeS;
  samplerCreateInfo.addressModeV = addressModeT;
  samplerCreateInfo.addressModeW = addressModeR;
  samplerCreateInfo.maxAnisotropy = 1.0f;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = static_cast<float>(mipLevels);

  VkSampler sampler;
  if (vkCreateSampler(device, &samplerCreateInfo, nullptr, &sampler) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create sampler");
  }

  return {textureImage, sampler};
}

void generateMipmaps(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    VkImage image,
    VkFormat imageFormat,
    SizeI const& size,
    uint32_t layerCount)
{
  uint32_t mipLevels = computeMipLevels(size);

  // Check if image format supports linear blitting
  VkFormatProperties formatProperties;
  vkGetPhysicalDeviceFormatProperties(physicalDevice, imageFormat, &formatProperties);

  if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
  {
    throw std::runtime_error("texture image format does not support linear blitting!");
  }

  // Mip-chain generation requires support for blit source and destination
  if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_BLIT_SRC_BIT &&
        formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_BLIT_DST_BIT))
  {
    throw std::runtime_error("Mip-chain generation requires support for blit source and destination");
  }

  VkCommandBuffer commandBuffer = beginSingleTimeCommands(device, commandPool);

  VkImageMemoryBarrier barrier{};
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.image = image;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = layerCount;
  barrier.subresourceRange.levelCount = 1;

  int32_t mipWidth = size.width;
  int32_t mipHeight = size.height;

  for (uint32_t i = 1; i < mipLevels; i++)
  {
    barrier.subresourceRange.baseMipLevel = i - 1;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

    vkCmdPipelineBarrier(commandBuffer,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                         0, nullptr,
                         0, nullptr,
                         1, &barrier);

    VkImageBlit blit{};
    blit.srcOffsets[0] = { 0, 0, 0 };
    blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
    blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.srcSubresource.mipLevel = i - 1;
    blit.srcSubresource.baseArrayLayer = 0;
    blit.srcSubresource.layerCount = layerCount;
    blit.dstOffsets[0] = {0, 0, 0};
    blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
    blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.dstSubresource.mipLevel = i;
    blit.dstSubresource.baseArrayLayer = 0;
    blit.dstSubresource.layerCount = layerCount;

    vkCmdBlitImage(commandBuffer,
                   image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                   image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                   1, &blit,
                   VK_FILTER_LINEAR);

    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    vkCmdPipelineBarrier(commandBuffer,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                         0, nullptr,
                         0, nullptr,
                         1, &barrier);

    if (mipWidth > 1) mipWidth /= 2;
    if (mipHeight > 1) mipHeight /= 2;
  }

  barrier.subresourceRange.baseMipLevel = mipLevels - 1;
  barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
  barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

  vkCmdPipelineBarrier(commandBuffer,
                       VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                       0, nullptr,
                       0, nullptr,
                       1, &barrier);

  endSingleTimeCommands(device, graphicsQueue, commandPool, commandBuffer);
}

void releaseTexture(
    VkDevice device,
    VulkanTexture texture)
{
  vkDestroySampler(device, texture.sampler, nullptr);
  releaseImage(device, texture.image);
}

void releaseTexture(
    VkDevice device,
    VulkanTextureCube texture)
{
  vkDestroySampler(device, texture.sampler, nullptr);
  releaseImage(device, texture.image);
}

VulkanFramebuffer createFramebuffer(
    VkDevice device,
    VkRenderPass renderPass,
    std::vector<VkImageView> const& attachments,
    uint32_t width,
    uint32_t height)
{
  VkFramebufferCreateInfo framebufferInfo{};

  framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  framebufferInfo.renderPass = renderPass;
  framebufferInfo.attachmentCount = attachments.size();
  framebufferInfo.pAttachments = attachments.data();
  framebufferInfo.width = width;
  framebufferInfo.height = height;
  framebufferInfo.layers = 1;

  VkFramebuffer framebuffer;
  if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &framebuffer) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create shader module");
  }

  return framebuffer;
}

void releaseFramebuffer(
    VkDevice device,
    VulkanFramebuffer framebuffer)
{
  vkDestroyFramebuffer(device, framebuffer, nullptr);
}

VkShaderModule createShader(
    VkDevice device,
    std::vector<unsigned int> const& source)
{
  VkShaderModuleCreateInfo shaderInfo{};
  shaderInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  shaderInfo.codeSize = source.size() * sizeof(unsigned int);
  shaderInfo.pCode = source.data();

  VkShaderModule shaderModule;
  if (vkCreateShaderModule(device, &shaderInfo, nullptr, &shaderModule) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create shader module");
  }

  return shaderModule;
}

void releaseShader(
    VkDevice device,
    VkShaderModule shader)
{
  vkDestroyShaderModule(device, shader, nullptr);
}

VkRenderPass createRenderPass(
    VkDevice device,
    std::vector<VkFormat> const& colorAttachments,
    std::optional<VkFormat> const& depthStencilAttachment,
    VkSampleCountFlagBits sampleCount)
{
  const bool msaa = sampleCount > VK_SAMPLE_COUNT_1_BIT;

  VkAttachmentDescription colorAttachmentDescription{};
  VkAttachmentDescription depthStencilAttachmentDescription{};
  VkAttachmentDescription colorAttachmentResolveDescription{};

  std::vector<VkAttachmentReference> colorAttachmentRefs;
  std::optional<VkAttachmentReference> depthStencilAttachmentRef;
  std::vector<VkAttachmentReference> colorAttachmentResolveRefs;
  std::vector<VkAttachmentDescription> attachments;

  int attachmentIndex = 0;

  for (auto colorAttachment : colorAttachments)
  {
    colorAttachmentDescription.format = colorAttachment;
    colorAttachmentDescription.samples = sampleCount;
    colorAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachmentDescription.finalLayout = msaa ? VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    auto& colorAttachmentRef = colorAttachmentRefs.emplace_back(VkAttachmentReference{});
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    colorAttachmentRef.attachment = attachmentIndex++;

    attachments.push_back(colorAttachmentDescription);
  }

  if (depthStencilAttachment)
  {
    depthStencilAttachmentDescription.format = *depthStencilAttachment;
    depthStencilAttachmentDescription.samples = sampleCount;
    depthStencilAttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthStencilAttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    depthStencilAttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthStencilAttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthStencilAttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthStencilAttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

    depthStencilAttachmentRef.emplace(VkAttachmentReference{});
    depthStencilAttachmentRef->layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    depthStencilAttachmentRef->attachment = attachmentIndex++;

    attachments.push_back(depthStencilAttachmentDescription);
  }

  if (msaa)
  {
    for (auto colorAttachment : colorAttachments)
    {
      colorAttachmentResolveDescription.format = colorAttachment;
      colorAttachmentResolveDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      colorAttachmentResolveDescription.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      colorAttachmentResolveDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      colorAttachmentResolveDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      colorAttachmentResolveDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      colorAttachmentResolveDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      colorAttachmentResolveDescription.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

      auto& colorAttachmentResolveRef = colorAttachmentResolveRefs.emplace_back(VkAttachmentReference{});
      colorAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
      colorAttachmentResolveRef.attachment = attachmentIndex++;

      attachments.push_back(colorAttachmentResolveDescription);
    }
  }

  VkSubpassDescription subpassDescription{};
  subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpassDescription.colorAttachmentCount = colorAttachmentRefs.size();
  subpassDescription.pColorAttachments = colorAttachmentRefs.data();

  if (depthStencilAttachment)
  {
    subpassDescription.pDepthStencilAttachment = &depthStencilAttachmentRef.value();
  }

  if (msaa && !colorAttachmentResolveRefs.empty())
  {
    subpassDescription.pResolveAttachments = colorAttachmentResolveRefs.data();
  }

  std::array<VkSubpassDependency, 2> dependencies;
  dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[0].dstSubpass = 0;
  dependencies[0].srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
  dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  dependencies[1].srcSubpass = 0;
  dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
  dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  VkRenderPassCreateInfo renderPassInfo{};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  renderPassInfo.attachmentCount = attachments.size();
  renderPassInfo.pAttachments = attachments.data();
  renderPassInfo.subpassCount = 1;
  renderPassInfo.pSubpasses = &subpassDescription;
  renderPassInfo.dependencyCount = dependencies.size();
  renderPassInfo.pDependencies = dependencies.data();

  VkRenderPass renderPass;
  if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create render pass!");
  }

  return renderPass;
}

void releaseRenderPass(
    VkDevice device,
    VkRenderPass renderPass)
{
  vkDestroyRenderPass(device, renderPass, nullptr);
}

void releaseBuffer(
    VkDevice device,
    VulkanBuffer buffer)
{
  vkDestroyBuffer(device, buffer.buffer, nullptr);
  vkFreeMemory(device, buffer.memory, nullptr);
}

}
