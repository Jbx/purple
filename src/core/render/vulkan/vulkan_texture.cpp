#include "vulkan_texture.h"
#include "vulkan_converter.h"
#include "utils/assert.h"

// STL
#include <stdexcept>

VulkanTexture::VulkanTexture(
    IVulkanContext& context,
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(image.getSize())
  , m_format(TextureFormat::RGBA_UNorm)
{
  m_texture = VulkanUtils::createTexture2D(
        m_context.getPhysicalDevice(),
        m_context.getDevice(),
        m_context.getGraphicsQueue(),
        m_context.getCommandPool(),
        image,
        VK_SAMPLE_COUNT_1_BIT,
        toVkFilter(sampler.minFilter),
        toVkFilter(sampler.magFilter),
        generateMipmap,
        toVkMipmapMode(sampler.minFilter),
        toVkWrapMode(sampler.wrapModeS),
        toVkWrapMode(sampler.wrapModeT),
        toVkWrapMode(sampler.wrapModeR));
}

VulkanTexture::VulkanTexture(
    IVulkanContext& context,
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(size)
  , m_format(format)
{
  switch (format)
  {
  case TextureFormat::RGBA_UNorm:
  case TextureFormat::RGBA_16F:
  case TextureFormat::RGBA_32F:
    m_texture = VulkanUtils::createColorTexture(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          size.width,
          size.height,
          VK_SAMPLE_COUNT_1_BIT,
          toVkFormat(format),
          toVkFilter(sampler.minFilter),
          toVkFilter(sampler.magFilter),
          generateMipmap,
          toVkMipmapMode(sampler.minFilter),
          toVkWrapMode(sampler.wrapModeS),
          toVkWrapMode(sampler.wrapModeT),
          toVkWrapMode(sampler.wrapModeR));
    break;

  case TextureFormat::Depth:
    m_texture = VulkanUtils::createDepthTexture(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          size.width,
          size.height,
          VK_SAMPLE_COUNT_1_BIT,
          toVkFormat(format),
          toVkFilter(sampler.minFilter),
          toVkFilter(sampler.magFilter),
          generateMipmap,
          toVkMipmapMode(sampler.minFilter),
          toVkWrapMode(sampler.wrapModeS),
          toVkWrapMode(sampler.wrapModeT),
          toVkWrapMode(sampler.wrapModeR));
    break;

  default:
    ASSERT(false, "Texture format not supported!");
    break;
  }
}

VulkanTexture::~VulkanTexture()
{
  VulkanUtils::releaseTexture(m_context.getDevice(), m_texture);
}

SizeI VulkanTexture::getSize() const
{
  return m_size;
}

TextureFormat VulkanTexture::getFormat() const
{
  return m_format;
}

TextureTarget VulkanTexture::getTarget() const
{
  return TextureTarget::Target2D;
}

VkImageView VulkanTexture::getView() const
{
  return m_texture.image.view;
}

VkSampler VulkanTexture::getSampler() const
{
  return m_texture.sampler;
}

VulkanTextureCube::VulkanTextureCube(
    IVulkanContext& context,
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(image.getSize())
  , m_format(TextureFormat::RGBA_UNorm)
{
  m_texture = VulkanUtils::createTextureCube(
        m_context.getPhysicalDevice(),
        m_context.getDevice(),
        m_context.getGraphicsQueue(),
        m_context.getCommandPool(),
        image,
        VK_SAMPLE_COUNT_1_BIT,
        toVkFilter(sampler.minFilter),
        toVkFilter(sampler.magFilter),
        generateMipmap,
        toVkMipmapMode(sampler.minFilter),
        toVkWrapMode(sampler.wrapModeS),
        toVkWrapMode(sampler.wrapModeT),
        toVkWrapMode(sampler.wrapModeR));
}

VulkanTextureCube::VulkanTextureCube(
    IVulkanContext& context,
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(size)
  , m_format(format)
{
  switch (format)
  {
  case TextureFormat::RGBA_UNorm:
  case TextureFormat::RGBA_16F:
  case TextureFormat::RGBA_32F:
    m_texture = VulkanUtils::createColorTextureCube(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          size.width,
          size.height,
          VK_SAMPLE_COUNT_1_BIT,
          toVkFormat(format),
          toVkFilter(sampler.minFilter),
          toVkFilter(sampler.magFilter),
          generateMipmap,
          toVkMipmapMode(sampler.minFilter),
          toVkWrapMode(sampler.wrapModeS),
          toVkWrapMode(sampler.wrapModeT),
          toVkWrapMode(sampler.wrapModeR));
    break;

  case TextureFormat::Depth:
    m_texture = VulkanUtils::createDepthTextureCube(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          size.width,
          size.height,
          VK_SAMPLE_COUNT_1_BIT,
          toVkFormat(format),
          toVkFilter(sampler.minFilter),
          toVkFilter(sampler.magFilter),
          generateMipmap,
          toVkMipmapMode(sampler.minFilter),
          toVkWrapMode(sampler.wrapModeS),
          toVkWrapMode(sampler.wrapModeT),
          toVkWrapMode(sampler.wrapModeR));
    break;

  default:
    ASSERT(false, "Texture format not supported!");
    break;
  }
}

VulkanTextureCube::~VulkanTextureCube()
{
  VulkanUtils::releaseTexture(m_context.getDevice(), m_texture);
}

SizeI VulkanTextureCube::getSize() const
{
  return m_size;
}

TextureFormat VulkanTextureCube::getFormat() const
{
  return m_format;
}

TextureTarget VulkanTextureCube::getTarget() const
{
  return TextureTarget::TargetCube;
}

VkImageView VulkanTextureCube::getView() const
{
  return m_texture.image.view;
}

VkSampler VulkanTextureCube::getSampler() const
{
  return m_texture.sampler;
}

VkImageView VulkanTextureCube::getView(int faceIndex, int mipLevel) const
{
  return m_texture.image.faceViews[faceIndex].mipmapViews[mipLevel];
}
