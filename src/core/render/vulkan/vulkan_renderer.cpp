#include "vulkan_renderer.h"
#include "vulkan_uniform_buffer.h"
#include "vulkan_pipeline.h"
#include "vulkan_texture.h"
#include "vulkan_framebuffer.h"

VulkanRenderer::VulkanRenderer(IVulkanContext& context)
  : m_context(context)
  , m_defaultRenderPass(context)
  , m_defaultFramebuffer(context)
{

}

Scoped<IUniformBuffer> VulkanRenderer::createUniformBuffer(
    std::vector<NamedPropertyBuffer> const& properties,
    TextureContainer const& textures)
{
  return createScoped<VulkanUniformBuffer>(
        m_context,
        properties,
        textures);
}

Scoped<ITexture> VulkanRenderer::createTexture2D(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<VulkanTexture>(
        m_context,
        image,
        generateMipmap,
        sampler);
}

Scoped<ITexture> VulkanRenderer::createTexture2D(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<VulkanTexture>(
        m_context,
        format,        
        size,
        generateMipmap,
        sampler);
}

Scoped<ITexture> VulkanRenderer::createTextureCube(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<VulkanTextureCube>(
        m_context,
        image,
        generateMipmap,
        sampler);
}

Scoped<ITexture> VulkanRenderer::createTextureCube(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<VulkanTextureCube>(
        m_context,
        format,
        size,
        generateMipmap,
        sampler);
}

Scoped<IPipeline> VulkanRenderer::createPipeline(
    IRenderPass const& renderPass,
    Shader const& shader,
    RenderStates const& renderStates,
    unsigned int uniformBufferCount,
    TextureContainer const& textures)
{
  return createScoped<VulkanPipeline>(
        m_context,
        static_cast<IVulkanRenderPass const&>(renderPass),
        shader,
        renderStates,
        uniformBufferCount,
        textures);
}

Scoped<IFramebuffer> VulkanRenderer::createFramebuffer(
    IRenderPass const& renderPass,
    std::vector<Attachment> const& attachments,
    SizeI const& size)
{
  return createScoped<VulkanFramebuffer>(
        m_context,
        static_cast<IVulkanRenderPass const&>(renderPass),
        attachments,
        size);
}

IRenderPass& VulkanRenderer::getDefaultRenderPass()
{
  return m_defaultRenderPass;
}

IFramebuffer& VulkanRenderer::getDefaultFramebuffer()
{
  return m_defaultFramebuffer;
}

Scoped<IRenderPass> VulkanRenderer::createRenderPass(
    std::vector<AttachmentFormat> const& attachments,
    int sampleCount)
{
  return createScoped<VulkanRenderPass>(
        m_context,
        attachments,
        sampleCount);
}

