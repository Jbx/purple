#pragma once

#include "render/texture_interface.h"
#include "vulkan_context_interface.h"
#include "render/render_pass_interface.h"

// STL
#include <vector>

class IVulkanRenderPass : public IRenderPass
{
public:
  virtual VkRenderPass getRenderPass() const = 0;
  virtual VkSampleCountFlagBits getSampleCountFlagBits() const = 0;
};

class DefaultVulkanRenderPass final : public IVulkanRenderPass
{
public:
  DefaultVulkanRenderPass(
      IVulkanContext& context);

  VkRenderPass getRenderPass() const override;
  VkSampleCountFlagBits getSampleCountFlagBits() const override;

  bool isDefaultRenderPass() const override;

  std::vector<AttachmentPoint> getAttachments() const override;
  int getSampleCount() const override;

  void setClearColor(Color const& clearColor) override;
  void setClearDepthValue(float clearDepthValue) override;
  void setClearStencilValue(unsigned int clearStencilValue) override;

  void begin(
      IFramebuffer const& framebuffer,
      RectI const& viewport) override;

  void end() override;  

private:
  IVulkanContext& m_context;
  Color m_clearColor {0.f, 0.f, 0.f, 1.f};
  float m_clearDepthValue = 1.f;
  unsigned int m_clearStencilValue = 0.f;
};

class VulkanRenderPass final : public IVulkanRenderPass
{
public:
  VulkanRenderPass(
      IVulkanContext& context,
      std::vector<AttachmentFormat> const& attachments,
      int sampleCount);

  ~VulkanRenderPass() override;

  VkRenderPass getRenderPass() const override;
  VkSampleCountFlagBits getSampleCountFlagBits() const override;

  bool isDefaultRenderPass() const override;

  std::vector<AttachmentPoint> getAttachments() const override;
  int getSampleCount() const override;

  void setClearColor(Color const& clearColor) override;
  void setClearDepthValue(float clearDepthValue) override;
  void setClearStencilValue(unsigned int clearStencilValue) override;

  void begin(
      IFramebuffer const& framebuffer,
      RectI const& viewport) override;

  void end() override;  

private:
  IVulkanContext& m_context;
  VkRenderPass m_renderPass;
  std::vector<AttachmentPoint> m_attachmentPoints;
  VkSampleCountFlagBits m_sampleCount;
  Color m_clearColor {0.f, 0.f, 0.f, 1.f};
  float m_clearDepthValue = 1.f;
  unsigned int m_clearStencilValue = 0.f;
};
