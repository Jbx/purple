#pragma once

#include "render/attribute.h"
#include "render/vertex_buffer_interface.h"
#include "vulkan_context_interface.h"
#include "vulkan_utils.h"
#include "utils/assert.h"
#include "utils/ref.h"

static VkVertexInputBindingDescription getBindingDescription(AttributeLayout const& attributes)
{
  VkVertexInputBindingDescription bindingDescription{};
  bindingDescription.binding = 0;
  bindingDescription.stride = stride(attributes);
  bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
  return bindingDescription;
}

static VkFormat fromAttributeFormat(AttributeType format)
{
  switch (format)
  {
  case Float:
    return VK_FORMAT_R32_SFLOAT;
  case Vec2:
    return VK_FORMAT_R32G32_SFLOAT;
  case Vec3:
    return VK_FORMAT_R32G32B32_SFLOAT;
  case Vec4:
    return VK_FORMAT_R32G32B32A32_SFLOAT;
  }
  ASSERT(false, "Unknown format");
  return VK_FORMAT_UNDEFINED;
}

static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions(AttributeLayout const& attributes)
{
  std::vector<VkVertexInputAttributeDescription> attributeDescriptions(attributes.size());

  unsigned int i = 0;
  auto it = attributes.begin();
  for (; it != attributes.end(); ++it, ++i)
  {
    attributeDescriptions[i].binding = 0;
    attributeDescriptions[i].location = i;
    attributeDescriptions[i].format = fromAttributeFormat(*it);
    attributeDescriptions[i].offset = offsetOf(attributes, it);
  }

  return attributeDescriptions;
}

class VulkanVertexBuffer final : public IVertexBuffer
{
public:
  template<typename VertexType>
  static Scoped<VulkanVertexBuffer> create(
      IVulkanContext& context,
      BufferView<VertexType> const& vertices)
  {
    return createScoped<VulkanVertexBuffer>(
          context, vertices);
  }

  template<typename VertexType>
  VulkanVertexBuffer(
      IVulkanContext& context,
      BufferView<VertexType> const& vertices)
    : m_context(context)
    , m_vertexBuffer(
        VulkanUtils::createVertexBuffer(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          m_context.getCommandPool(),
          m_context.getGraphicsQueue(),
          vertices))
    , m_size(static_cast<uint32_t>(vertices.size())) { }

  ~VulkanVertexBuffer() override;

  void bind() override;
  void draw(IPipeline const& pipeline) override;

private:
  IVulkanContext& m_context;
  VulkanUtils::VulkanBuffer m_vertexBuffer;
  unsigned int m_size;
};

template<typename IndexType>
static VkIndexType indexType()
{
  ASSERT(false, "Type not handled");
  return VK_INDEX_TYPE_UINT16;
}

template<>
VkIndexType indexType<unsigned short>()
{
  return VK_INDEX_TYPE_UINT16;
}

template<>
VkIndexType indexType<unsigned int>()
{
  return VK_INDEX_TYPE_UINT32;
}

class VulkanIndexedVertexBuffer final : public IVertexBuffer
{
public:
  template<typename VertexType, typename IndexType>
  static Scoped<VulkanIndexedVertexBuffer> create(
      IVulkanContext& context,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices)
  {
    return createScoped<VulkanIndexedVertexBuffer>(
          context, vertices, indices);
  }

  template<typename VertexType, typename IndexType>
  VulkanIndexedVertexBuffer(
      IVulkanContext& context,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices)
    : m_context(context)
    , m_vertexBuffer(
        VulkanUtils::createVertexBuffer(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          m_context.getCommandPool(),
          m_context.getGraphicsQueue(),
          vertices))
    , m_indexBuffer(
        VulkanUtils::createIndexBuffer(
          m_context.getPhysicalDevice(),
          m_context.getDevice(),
          m_context.getCommandPool(),
          m_context.getGraphicsQueue(),
          indices))
    , m_size(static_cast<uint32_t>(indices.size()))
    , m_indexType(indexType<IndexType>()){ }

  ~VulkanIndexedVertexBuffer() override;

  void bind() override;
  void draw(IPipeline const& pipeline) override;

private:
  IVulkanContext& m_context;
  VulkanUtils::VulkanBuffer m_vertexBuffer;
  VulkanUtils::VulkanBuffer m_indexBuffer;
  unsigned int m_size;
  VkIndexType m_indexType;
};
