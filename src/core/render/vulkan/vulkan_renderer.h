#pragma once

#include "render/renderer.h"
#include "vulkan_framebuffer.h"
#include "vulkan_render_pass.h"
#include "vulkan_context_interface.h"
#include "vulkan_vertex_buffer.h"

class VulkanRenderer final : public Renderer<VulkanRenderer>
{
public:
  VulkanRenderer(IVulkanContext& context);

  template<typename VertexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices);

  template<typename VertexType, typename IndexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices);

  Scoped<IUniformBuffer> createUniformBuffer(
      std::vector<NamedPropertyBuffer> const& properties,
      TextureContainer const& textures);

  Scoped<ITexture> createTexture2D(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTexture2D(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTextureCube(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTextureCube(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<IPipeline> createPipeline(
      IRenderPass const& renderPass,
      Shader const& shader,
      RenderStates const& renderStates,
      unsigned int uniformBufferCount,
      TextureContainer const& textures);

  Scoped<IFramebuffer> createFramebuffer(
      IRenderPass const& renderPass,
      std::vector<Attachment> const& attachments,
      SizeI const& size);  

  IRenderPass& getDefaultRenderPass();

  IFramebuffer& getDefaultFramebuffer();

  Scoped<IRenderPass> createRenderPass(
      std::vector<AttachmentFormat> const& attachments,
      int sampleCount);

private:
  IVulkanContext& m_context;
  DefaultVulkanRenderPass m_defaultRenderPass;
  DefaultVulkanFramebuffer m_defaultFramebuffer;
};

template<typename VertexType>
Scoped<IVertexBuffer> VulkanRenderer::createVertexBuffer(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices)
{
  return createScoped<VulkanVertexBuffer>(m_context, vertices);
}

template<typename VertexType, typename IndexType>
Scoped<IVertexBuffer> VulkanRenderer::createVertexBuffer(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices,
    BufferView<IndexType> const& indices)
{
  return createScoped<VulkanIndexedVertexBuffer>(m_context, vertices, indices);
}
