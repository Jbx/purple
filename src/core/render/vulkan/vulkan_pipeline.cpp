#include "vulkan_pipeline.h"
#include "vulkan_vertex_buffer.h"
#include "vulkan_converter.h"


VulkanPipeline::VulkanPipeline(
    IVulkanContext& context,
    IVulkanRenderPass const& renderPass,
    Shader const& shader,
    RenderStates const& renderStates,
    unsigned int uniformBufferCount,
    TextureContainer const& textures)
  : m_context(context)
{  
  // Create descriptor set layout
  std::vector<VkDescriptorSetLayoutBinding> bindings;

  int layoutBinding = 0;

  for (int i = 0; i < uniformBufferCount; ++i)
  {
    VkDescriptorSetLayoutBinding uboLayoutBinding{};
    uboLayoutBinding.binding = layoutBinding++;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.pImmutableSamplers = nullptr;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
    bindings.push_back(uboLayoutBinding);
  }

  for (int i = 0; i < textures.size(); ++i)
  {
    VkDescriptorSetLayoutBinding samplerLayoutBinding{};
    samplerLayoutBinding.binding = layoutBinding++;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = nullptr;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    bindings.push_back(samplerLayoutBinding);
  }

  VkDescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  layoutInfo.bindingCount = bindings.size();
  layoutInfo.pBindings = bindings.data();

  if (vkCreateDescriptorSetLayout(m_context.getDevice(), &layoutInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create descriptor set layout!");
  }

  // Shaders
  auto vertShaderModule = VulkanUtils::createShader(
        m_context.getDevice(), shader.getProgram().getVertexShaderSourceCode(ShaderProgram::SPIRV));
  auto fragShaderModule = VulkanUtils::createShader(
        m_context.getDevice(), shader.getProgram().getFragmentShaderSourceCode(ShaderProgram::SPIRV));

  VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
  vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
  vertShaderStageInfo.module = vertShaderModule;
  vertShaderStageInfo.pName = "main";

  VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
  fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  fragShaderStageInfo.module = fragShaderModule;
  fragShaderStageInfo.pName = "main";

  VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

  VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

  auto bindingDescription = getBindingDescription(shader.getAttributeLayout());
  auto attributeDescriptions = getAttributeDescriptions(shader.getAttributeLayout());

  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

  VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
  inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  inputAssembly.topology = toVkPrimitiveTopology(renderStates.primitiveTopology);
  inputAssembly.primitiveRestartEnable = VK_FALSE;

  VkPipelineViewportStateCreateInfo viewportState{};
  viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewportState.viewportCount = 1;
  viewportState.scissorCount = 1;

  VkPipelineRasterizationStateCreateInfo rasterizer{};
  rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizer.depthClampEnable = VK_FALSE;
  rasterizer.rasterizerDiscardEnable = VK_FALSE;
  rasterizer.polygonMode = toVkPolygonMode(renderStates.polygonMode);
  rasterizer.lineWidth = renderStates.lineWidth;
  rasterizer.cullMode = toVkCullMode(renderStates.cullMode);
  rasterizer.frontFace = renderPass.isDefaultRenderPass() ? VK_FRONT_FACE_COUNTER_CLOCKWISE : VK_FRONT_FACE_CLOCKWISE;
  rasterizer.depthBiasEnable = renderStates.depthBiasEnabled;
  rasterizer.depthBiasConstantFactor = renderStates.depthBiasConstantFactor;
  rasterizer.depthBiasClamp = renderStates.depthBiasClamp;
  rasterizer.depthBiasSlopeFactor = renderStates.depthBiasSlopeFactor;

  VkPipelineMultisampleStateCreateInfo multisampling{};
  multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampling.sampleShadingEnable = VK_FALSE;
  multisampling.rasterizationSamples = renderPass.getSampleCountFlagBits();

  VkPipelineDepthStencilStateCreateInfo depthStencil{};
  depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  depthStencil.depthTestEnable = renderStates.depthTestEnabled;
  depthStencil.depthWriteEnable = renderStates.depthWriteEnabled;
  depthStencil.depthCompareOp = toVkDepthCompareOperation(renderStates.depthCompareOperation);

  VkPipelineColorBlendAttachmentState colorBlendAttachment{};
  colorBlendAttachment.colorWriteMask = renderStates.colorWriteEnabled ?
      VK_COLOR_COMPONENT_R_BIT |
      VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT |
      VK_COLOR_COMPONENT_A_BIT : 0;
  colorBlendAttachment.blendEnable = renderStates.blendingEnabled;
  colorBlendAttachment.srcColorBlendFactor = toVkBlendFactor(renderStates.srcColorBlendFactor);
  colorBlendAttachment.dstColorBlendFactor = toVkBlendFactor(renderStates.dstColorBlendFactor);
  colorBlendAttachment.colorBlendOp = toVkBlendOperation(renderStates.colorBlendOperation);
  colorBlendAttachment.srcAlphaBlendFactor = toVkBlendFactor(renderStates.srcAlphaBlendFactor);
  colorBlendAttachment.dstAlphaBlendFactor = toVkBlendFactor(renderStates.dstAlphaBlendFactor);
  colorBlendAttachment.alphaBlendOp = toVkBlendOperation(renderStates.alphaBlendOperation);

  auto attachments = renderPass.getAttachments();
  int colorAttachmentCount = std::count(
        attachments.begin(),
        attachments.end(),
        AttachmentPoint::Color);

  VkPipelineColorBlendStateCreateInfo colorBlending{};
  colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  colorBlending.attachmentCount = colorAttachmentCount;
  colorBlending.pAttachments = &colorBlendAttachment;

  VkPipelineCacheCreateInfo pipelineCacheInfo{};
  pipelineCacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
  if (vkCreatePipelineCache(
        m_context.getDevice(),
        &pipelineCacheInfo,
        nullptr,
        &m_pipelineCache) != VK_SUCCESS)
  {
    throw std::runtime_error("Failed to create pipeline cache");
  }

  VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
  pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutInfo.setLayoutCount = 1;
  pipelineLayoutInfo.pSetLayouts = &m_descriptorSetLayout;

  if (vkCreatePipelineLayout(
        m_context.getDevice(),
        &pipelineLayoutInfo,
        nullptr,
        &m_pipelineLayout) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create pipeline layout!");
  }

  VkPipelineDynamicStateCreateInfo dynamicStateInfo{};
  dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  VkDynamicState dynamicEnable[] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
  dynamicStateInfo.dynamicStateCount = sizeof(dynamicEnable) / sizeof(VkDynamicState);
  dynamicStateInfo.pDynamicStates = dynamicEnable;

  VkGraphicsPipelineCreateInfo pipelineInfo{};
  pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipelineInfo.stageCount = 2;
  pipelineInfo.pStages = shaderStages;
  pipelineInfo.pVertexInputState = &vertexInputInfo;
  pipelineInfo.pInputAssemblyState = &inputAssembly;
  pipelineInfo.pViewportState = &viewportState;
  pipelineInfo.pRasterizationState = &rasterizer;
  pipelineInfo.pMultisampleState = &multisampling;
  pipelineInfo.pColorBlendState = &colorBlending;
  pipelineInfo.layout = m_pipelineLayout;
  pipelineInfo.renderPass = renderPass.getRenderPass();
  pipelineInfo.subpass = 0;
  pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
  pipelineInfo.pDepthStencilState = &depthStencil;
  pipelineInfo.pDynamicState = &dynamicStateInfo;

  if (vkCreateGraphicsPipelines(
        m_context.getDevice(),
        m_pipelineCache,
        1,
        &pipelineInfo,
        nullptr,
        &m_pipeline) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create graphics pipeline!");
  }

  VulkanUtils::releaseShader(m_context.getDevice(), vertShaderModule);
  VulkanUtils::releaseShader(m_context.getDevice(), fragShaderModule);
}

VulkanPipeline::~VulkanPipeline()
{
  vkDeviceWaitIdle(m_context.getDevice());
  vkDestroyPipeline(m_context.getDevice(), m_pipeline, nullptr);
  vkDestroyPipelineLayout(m_context.getDevice(), m_pipelineLayout, nullptr);
  vkDestroyPipelineCache(m_context.getDevice(), m_pipelineCache, nullptr);
  vkDestroyDescriptorSetLayout(m_context.getDevice(), m_descriptorSetLayout, nullptr);
}

void VulkanPipeline::bind(IRenderPass const& renderPass)
{
  vkCmdBindPipeline(
        m_context.getCurrentCommandBuffer(),
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        m_pipeline);
}
