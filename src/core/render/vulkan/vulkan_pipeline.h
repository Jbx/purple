#pragma once

#include "render/shader.h"
#include "render/pipeline_interface.h"
#include "render/render_states.h"
#include "render/texture_container.h"
#include "vulkan_render_pass.h"
#include "vulkan_context_interface.h"

class VulkanPipeline final : public IPipeline
{
public:
  VulkanPipeline(
      IVulkanContext& context,
      IVulkanRenderPass const& renderPass,
      Shader const& shader,
      RenderStates const& renderStates,
      unsigned int uniformBufferCount,
      TextureContainer const& textures);

  ~VulkanPipeline() override;

  void bind(IRenderPass const& renderPass) override;

private:
  friend class VulkanUniformBuffer;

  IVulkanContext& m_context;
  VkDescriptorSetLayout m_descriptorSetLayout;
  VkPipelineCache m_pipelineCache;
  VkPipelineLayout m_pipelineLayout;
  VkPipeline m_pipeline;
};
