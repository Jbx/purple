#include "vulkan_uniform_buffer.h"
#include "vulkan_texture.h"
#include "vulkan_pipeline.h"

// STL
#include <algorithm>

VulkanUniformBuffer::VulkanUniformBuffer(
    IVulkanContext& context,
    std::vector<NamedPropertyBuffer> const& properties,
    TextureContainer const& textures)
  : m_context(context)
{
  // Create descriptor set layout
  std::vector<VkDescriptorSetLayoutBinding> bindings;

  int layoutBinding = 0;

  for (auto const& property : properties)
  {
    VkDescriptorSetLayoutBinding uboLayoutBinding{};
    uboLayoutBinding.binding = layoutBinding++;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.pImmutableSamplers = nullptr;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
    bindings.push_back(uboLayoutBinding);
  }

  for (auto const& texture : textures)
  {
    VkDescriptorSetLayoutBinding samplerLayoutBinding{};
    samplerLayoutBinding.binding = layoutBinding++;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    bindings.push_back(samplerLayoutBinding);
  }

  VkDescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  layoutInfo.bindingCount = bindings.size();
  layoutInfo.pBindings = bindings.data();

  if (vkCreateDescriptorSetLayout(m_context.getDevice(), &layoutInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to create descriptor set layout!");
  }

  // Create descriptor sets
  VkDescriptorSetAllocateInfo allocInfo{};
  allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  allocInfo.descriptorPool = m_context.getDescriptorPool();
  allocInfo.descriptorSetCount = 1;
  allocInfo.pSetLayouts = &m_descriptorSetLayout;

  if (vkAllocateDescriptorSets(m_context.getDevice(), &allocInfo, &m_descriptorSet) != VK_SUCCESS)
  {
    throw std::runtime_error("failed to allocate descriptor sets!");
  }

  // used to retain buffer and image infos until update
  std::vector<VkDescriptorBufferInfo> descriptorBufferInfos;
  std::vector<VkDescriptorImageInfo> descriptorImageInfos;

  std::vector<VkWriteDescriptorSet> descriptorWrites;

  if (!properties.empty())
  {
    for (auto const& property : properties)
    {
      auto& buffer = m_buffers.emplace_back(
            VulkanUtils::createUniformBuffer(
              m_context.getPhysicalDevice(),
              m_context.getDevice(),
              property.buffer.size()));

      auto& bufferInfo = descriptorBufferInfos.emplace_back(VkDescriptorBufferInfo{});
      bufferInfo.buffer = buffer.buffer;
      bufferInfo.offset = 0;
      bufferInfo.range = property.buffer.size();
    }

    auto& uniformDescriptorWrite = descriptorWrites.emplace_back(VkWriteDescriptorSet{});
    uniformDescriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    uniformDescriptorWrite.dstSet = m_descriptorSet;
    uniformDescriptorWrite.dstBinding = 0;
    uniformDescriptorWrite.dstArrayElement = 0;
    uniformDescriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformDescriptorWrite.descriptorCount = descriptorBufferInfos.size();
    uniformDescriptorWrite.pBufferInfo = descriptorBufferInfos.data();
  }

  if (!textures.empty())
  {
    for (auto const& texture : textures.getTextures())
    {
      auto vulkanTexture = std::static_pointer_cast<IVulkanTexture>(texture.second);

      auto& imageInfo = descriptorImageInfos.emplace_back(VkDescriptorImageInfo{});
      imageInfo.imageLayout = vulkanTexture->getFormat() == TextureFormat::Depth ?
            VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL : VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
      imageInfo.imageView = vulkanTexture->getView();
      imageInfo.sampler = vulkanTexture->getSampler();
    }

    auto& samplerDescriptorWrite = descriptorWrites.emplace_back(VkWriteDescriptorSet{});
    samplerDescriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    samplerDescriptorWrite.dstSet = m_descriptorSet;
    samplerDescriptorWrite.dstBinding = descriptorBufferInfos.size();
    samplerDescriptorWrite.dstArrayElement = 0;
    samplerDescriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerDescriptorWrite.descriptorCount = descriptorImageInfos.size();
    samplerDescriptorWrite.pImageInfo = descriptorImageInfos.data();
  }

  vkUpdateDescriptorSets(
        m_context.getDevice(),
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr);
}

VulkanUniformBuffer::~VulkanUniformBuffer()
{
  vkDeviceWaitIdle(m_context.getDevice());
  vkDestroyDescriptorSetLayout(m_context.getDevice(), m_descriptorSetLayout, nullptr);

  for (auto& buffer : m_buffers)
  {
    VulkanUtils::releaseBuffer(m_context.getDevice(), buffer);
  }
}

void VulkanUniformBuffer::update(
    IPipeline const& pipeline,
    std::vector<NamedPropertyBuffer> const& properties)
{
  int uniformBufferIndex = 0;
  for (auto const& property : properties)
  {
    VulkanUtils::updateUniformBuffers(
          m_context.getDevice(),
          m_buffers.at(uniformBufferIndex++),
          property.buffer);
  }
}

void VulkanUniformBuffer::bind(IPipeline const& pipeline)
{
  vkCmdBindDescriptorSets(
        m_context.getCurrentCommandBuffer(),
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        static_cast<VulkanPipeline const&>(pipeline).m_pipelineLayout,
        0, 1,
        &m_descriptorSet,
        0, nullptr);
}

