#include "vulkan_vertex_buffer.h"

VulkanVertexBuffer::~VulkanVertexBuffer()
{
  VulkanUtils::releaseBuffer(m_context.getDevice(), m_vertexBuffer);
}

void VulkanVertexBuffer::bind()
{
  VkBuffer vertexBuffers[] = {m_vertexBuffer.buffer};
  VkDeviceSize offsets[] = {0};
  vkCmdBindVertexBuffers(m_context.getCurrentCommandBuffer(), 0, 1, vertexBuffers, offsets);
}

void VulkanVertexBuffer::draw(IPipeline const& /*pipeline*/)
{
  vkCmdDraw(m_context.getCurrentCommandBuffer(), m_size, 1, 0, 0);
}

VulkanIndexedVertexBuffer::~VulkanIndexedVertexBuffer()
{
  VulkanUtils::releaseBuffer(m_context.getDevice(), m_vertexBuffer);
  VulkanUtils::releaseBuffer(m_context.getDevice(), m_indexBuffer);
}

void VulkanIndexedVertexBuffer::bind()
{
  VkBuffer vertexBuffers[] = {m_vertexBuffer.buffer};
  VkDeviceSize offsets[] = {0};
  vkCmdBindVertexBuffers(m_context.getCurrentCommandBuffer(), 0, 1, vertexBuffers, offsets);
  vkCmdBindIndexBuffer(m_context.getCurrentCommandBuffer(), m_indexBuffer.buffer, 0, m_indexType);
}

void VulkanIndexedVertexBuffer::draw(IPipeline const& /*pipeline*/)
{
  vkCmdDrawIndexed(m_context.getCurrentCommandBuffer(), m_size, 1, 0, 0, 0);
}
