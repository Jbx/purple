#pragma once

#include "utils/size.h"
#include "vulkan_utils.h"

// STL
#include <vector>
#include <string>

static constexpr uint32_t MAX_FRAMES_IN_FLIGHT = 2;

static constexpr uint32_t DESCRIPTOR_SET_POOL_SIZE = 512;
static constexpr uint32_t DESCRIPTOR_TYPE_COUNT = 3; // uniforms, samplers, and input attachments
static constexpr uint32_t UBUFFER_BINDING_COUNT = 8;
static constexpr uint32_t SAMPLER_BINDING_COUNT = 32;
static constexpr uint32_t TARGET_BINDING_COUNT = 8;

struct VulkanPhysicalDevice
{
  VkPhysicalDevice device = VK_NULL_HANDLE;
  VkPhysicalDeviceProperties properties;
  uint32_t hostVisibleMemIndex;
  VkSampleCountFlagBits maxSampleCount;
};

struct VulkanLogicalDevice
{
  VkDevice device;
  VkQueue graphicsQueue;
  VkQueue presentQueue;
};

struct VulkanSwapChain
{
  VkSwapchainKHR swapChain;
  std::vector<VkImage> images;
  VkExtent2D extent;
  std::vector<VkImageView> imageViews;
  VkRenderPass renderPass;
  std::vector<VkFramebuffer> framebuffers;
  VulkanUtils::VulkanImage colorImage;
  VulkanUtils::VulkanImage depthImage;
  VkSampleCountFlagBits sampleCount;
  VkFormat colorFormat;
  VkFormat depthStencilFormat;
};

struct VulkanSyncObjects
{
  std::vector<VkSemaphore> imageAvailableSemaphores;
  std::vector<VkSemaphore> renderFinishedSemaphores;
  std::vector<VkFence> inFlightFences;
};

class VulkanContextHelper
{
public:
  static bool checkValidationLayerSupport();

  static VkInstance createInstance(
      std::string const& applicationName,
      bool enableValidationLayers,
      std::vector<const char*> const& extensions);

  static void releaseInstance(
      VkInstance instance);

  static void releaseSurface(
      VkInstance instance,
      VkSurfaceKHR surface);

  static VkDebugUtilsMessengerEXT createDebugMessenger(
      VkInstance instance);

  static void releaseDebugMessenger(
      VkInstance instance,
      VkDebugUtilsMessengerEXT debugMessenger);

  static VulkanPhysicalDevice pickPhysicalDevice(
      VkInstance instance,
      VkSurfaceKHR surface);

  static VulkanLogicalDevice createLogicalDevice(
      VkPhysicalDevice physicalDevice,
      VkSurfaceKHR surface,
      bool enableValidationLayers);

  static void releaseLogicalDevice(
      VulkanLogicalDevice logicalDevice);

  static VulkanSwapChain createSwapChain(
      VulkanPhysicalDevice physicalDevice,
      VkDevice device,
      VkSurfaceKHR surface,
      SizeI const& framebufferSize,
      bool vsync,
      int sampleCount);

  static void releaseSwapChain(
      VkDevice device,
      VulkanSwapChain swapChain);

  static VkCommandPool createCommandPool(
      VkPhysicalDevice physicalDevice,
      VkDevice device);

  static void releaseCommandPool(
      VkDevice device,
      VkCommandPool commandPool);

  static std::vector<VkCommandBuffer> createCommandBuffers(
      VkDevice device,
      VkCommandPool commandPool);

  static VkDescriptorPool createDescriptorPool(
      VkDevice device);

  static void releaseDescriptorPool(
      VkDevice device,
      VkDescriptorPool descriptorPool);

  static VulkanSyncObjects createSyncObjects(
      VkDevice device);

  static void releaseSyncObjects(
      VkDevice device,
      VulkanSyncObjects objects);
};
