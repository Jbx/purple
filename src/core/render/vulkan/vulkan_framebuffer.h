#pragma once

#include "vulkan_context_interface.h"
#include "render/framebuffer_interface.h"
#include "vulkan_render_pass.h"

// STL
#include <vector>

class IVulkanFramebuffer : public IFramebuffer
{
public:
  virtual VkSampleCountFlagBits getSampleCountFlagBits() const = 0;
  virtual VkFramebuffer getFramebuffer() const = 0;
};

class DefaultVulkanFramebuffer final : public IVulkanFramebuffer
{
public:
  DefaultVulkanFramebuffer(
      IVulkanContext& context);

  VkSampleCountFlagBits getSampleCountFlagBits() const override;
  VkFramebuffer getFramebuffer() const override;

  bool isDefaultFramebuffer() const override;

  SizeI getSize() const override;
  std::vector<AttachmentPoint> getAttachments() const override;  

private:
  IVulkanContext& m_context;
};

class VulkanFramebuffer final : public IVulkanFramebuffer
{
public:
  VulkanFramebuffer(
      IVulkanContext& context,
      IVulkanRenderPass const& renderPass,
      std::vector<Attachment> const& attachments,
      SizeI const& size);

  ~VulkanFramebuffer() override;

  VkSampleCountFlagBits getSampleCountFlagBits() const override;
  VkFramebuffer getFramebuffer() const override;

  bool isDefaultFramebuffer() const override;

  SizeI getSize() const override;
  std::vector<AttachmentPoint> getAttachments() const override;

private:
  IVulkanContext& m_context;
  VkFramebuffer m_framebuffer;
  SizeI m_size;
  std::vector<AttachmentPoint> m_attachmentPoints;
};
