#pragma once

#include "images/image_interface.h"
#include "utils/buffer_view.h"
#include "images/image_interface.h"

// STL
#include <cstring>
#include <optional>
#include <string>
#include <vector>

// Vulkan
#include <vulkan/vulkan.h>

namespace VulkanUtils {

struct VulkanImage
{
  VkImage image;
  VkDeviceMemory memory;
  VkImageView view;
  std::vector<VkImageView> mipmapViews;
};

struct VulkanFaceView
{
  VkImageView view;
  std::vector<VkImageView> mipmapViews;
};

struct VulkanImageCube
{
  VkImage image;
  VkDeviceMemory memory;
  VkImageView view;
  std::array<VulkanFaceView, 6> faceViews;
};

struct VulkanBuffer
{
  VkBuffer buffer;
  VkDeviceMemory memory;
};

struct VulkanTexture
{
  VulkanImage image;
  VkSampler sampler;
};

struct VulkanTextureCube
{
  VulkanImageCube image;
  VkSampler sampler;
};

struct VulkanAttachment
{
  VkFormat format;
  VkImageView view;
};

using VulkanFramebuffer = VkFramebuffer;
using VulkanRenderPass = VkRenderPass;


uint32_t findMemoryType(
    VkPhysicalDevice physicalDevice,
    uint32_t typeFilter,
    VkMemoryPropertyFlags properties);

VkFormat findSupportedFormat(
    VkPhysicalDevice physicalDevice,
    std::vector<VkFormat> const& candidates,
    VkImageTiling tiling,
    VkFormatFeatureFlags features);

VkFormat findDepthStencilFormat(
    VkPhysicalDevice physicalDevice);

VulkanImage createImage(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    uint32_t mipLevels,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageAspectFlags aspect,
    VkMemoryPropertyFlags properties);

VulkanImageCube createCubeImage(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    uint32_t mipLevels,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageAspectFlags aspect,
    VkMemoryPropertyFlags properties);

void releaseImage(
    VkDevice device,
    VulkanImage image);

void releaseImage(
    VkDevice device,
    VulkanImageCube image);

VulkanTexture createTexture2D(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    IImage const& image,
    VkSampleCountFlagBits sampleCount,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

VulkanTextureCube createTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    IImage const& image,
    VkSampleCountFlagBits sampleCount,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

VulkanTexture createColorTexture(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

VulkanTexture createDepthTexture(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

VulkanTextureCube createColorTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

VulkanTextureCube createDepthTextureCube(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    uint32_t width,
    uint32_t height,
    VkSampleCountFlagBits sampleCount,
    VkFormat format,
    VkFilter minFilter,
    VkFilter magFilter,
    bool generateMipmap,
    VkSamplerMipmapMode mipmapMode,
    VkSamplerAddressMode addressModeS,
    VkSamplerAddressMode addressModeT,
    VkSamplerAddressMode addressModeR);

void generateMipmaps(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkQueue graphicsQueue,
    VkCommandPool commandPool,
    VkImage image,
    VkFormat imageFormat,
    SizeI const& size,
    uint32_t layerCount);

void releaseTexture(
    VkDevice device,
    VulkanTexture texture);

void releaseTexture(
    VkDevice device,
    VulkanTextureCube texture);

VulkanFramebuffer createFramebuffer(
    VkDevice device,
    VkRenderPass renderPass,
    std::vector<VkImageView> const& attachments,
    uint32_t width,
    uint32_t height);

void releaseFramebuffer(
    VkDevice device,
    VulkanFramebuffer framebuffer);

VkShaderModule createShader(
    VkDevice device,
    std::vector<unsigned int> const& source);

void releaseShader(
    VkDevice device,
    VkShaderModule shader);

VkRenderPass createRenderPass(
    VkDevice device,
    std::vector<VkFormat> const& colorAttachments,
    std::optional<VkFormat> const& depthStencilAttachment,
    VkSampleCountFlagBits sampleCount);

void releaseRenderPass(
    VkDevice device,
    VkRenderPass renderPass);

VulkanBuffer createBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkDeviceSize size,
    VkBufferUsageFlags usage,
    VkMemoryPropertyFlags properties);

void copyBuffer(
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    VkBuffer srcBuffer,
    VkBuffer dstBuffer,
    VkDeviceSize size);

template<typename T>
VulkanBuffer createVertexBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    BufferView<T> const& data);

template<typename T>
VulkanBuffer createIndexBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    BufferView<T> const& data);

VulkanBuffer createUniformBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    size_t size);

template<typename T>
void updateUniformBuffers(
    VkDevice device,
    VulkanBuffer uniformBuffer,
    T const& uniformBufferObject);

void updateUniformBuffers(
    VkDevice device,
    VulkanBuffer uniformBuffer,
    std::vector<char> const& uniformBufferObject);

void releaseBuffer(
    VkDevice device,
    VulkanBuffer buffer);
}

// Implementations

namespace VulkanUtils {

template<typename T>
VulkanBuffer createVertexBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    BufferView<T> const& vertices)
{
  VkDeviceSize bufferSize = sizeof(T) * vertices.size();

  VulkanBuffer stagingBuffer = createBuffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  void* data;
  vkMapMemory(device, stagingBuffer.memory, 0, bufferSize, 0, &data);
  std::memcpy(data, vertices.data(), (size_t) bufferSize);
  vkUnmapMemory(device, stagingBuffer.memory);

  VulkanBuffer buffer = createBuffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  copyBuffer(
        device,
        commandPool,
        graphicsQueue,
        stagingBuffer.buffer,
        buffer.buffer,
        bufferSize);

  releaseBuffer(device, stagingBuffer);
  return buffer;
}

template<typename T>
VulkanBuffer createIndexBuffer(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkCommandPool commandPool,
    VkQueue graphicsQueue,
    BufferView<T> const& indices)
{
  VkDeviceSize bufferSize = sizeof(T) * indices.size();

  VulkanBuffer stagingBuffer = createBuffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  void* data;
  vkMapMemory(device, stagingBuffer.memory, 0, bufferSize, 0, &data);
  std::memcpy(data, indices.data(), (size_t) bufferSize);
  vkUnmapMemory(device, stagingBuffer.memory);

  VulkanBuffer buffer = createBuffer(
        physicalDevice,
        device,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

  copyBuffer(
        device,
        commandPool,
        graphicsQueue,
        stagingBuffer.buffer,
        buffer.buffer,
        bufferSize);

  releaseBuffer(device, stagingBuffer);
  return buffer;
}

template<typename T>
void updateUniformBuffers(
    VkDevice device,
    VulkanBuffer uniformBuffer,
    T const& uniformBufferObject)
{
  void* data;
  vkMapMemory(device, uniformBuffer.memory, 0, sizeof(uniformBufferObject), 0, &data);
  std::memcpy(data, &uniformBufferObject, sizeof(uniformBufferObject));
  vkUnmapMemory(device, uniformBuffer.memory);
}

}
