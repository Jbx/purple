#include "vulkan_framebuffer.h"
#include "vulkan_texture.h"
#include "utils/assert.h"

// STL
#include <algorithm>

DefaultVulkanFramebuffer::DefaultVulkanFramebuffer(
    IVulkanContext& context)
  : m_context(context)
{

}

VkSampleCountFlagBits DefaultVulkanFramebuffer::getSampleCountFlagBits() const
{
  return m_context.getSampleCountFlagBits();
}

VkFramebuffer DefaultVulkanFramebuffer::getFramebuffer() const
{
  return m_context.getCurrentFramebuffer();
}

bool DefaultVulkanFramebuffer::isDefaultFramebuffer() const
{
  return true;
}

SizeI DefaultVulkanFramebuffer::getSize() const
{
  return m_context.getSwapChainImageSize();
}

std::vector<AttachmentPoint> DefaultVulkanFramebuffer::getAttachments() const
{
  return {AttachmentPoint::Color, AttachmentPoint::Depth};
}

VulkanFramebuffer::VulkanFramebuffer(
    IVulkanContext& context,
    IVulkanRenderPass const& renderPass,
    std::vector<Attachment> const& attachments,
    SizeI const& size)
  : m_context(context)
  , m_size(size)
{
  ASSERT(!attachments.empty(), "Framebuffer must have at least one attachment!");
  ASSERT(!m_size.isNull() && m_size.isValid(), "Framebuffer size must not be null!");

  std::vector<VkImageView> fbAttachments;

  std::transform(
        attachments.begin(),
        attachments.end(),
        std::back_inserter(m_attachmentPoints),
        [](auto const& attachment)
  {
    return attachment.attachmentPoint;
  });

  std::transform(
        attachments.begin(),
        attachments.end(),
        std::back_inserter(fbAttachments),
        [](auto const& attachment)
  {
    auto texture = std::static_pointer_cast<IVulkanTexture>(attachment.texture);

    if (texture->getTarget() == TextureTarget::Target2D)
    {
      return texture->getView();
    }
    else if (texture->getTarget() == TextureTarget::TargetCube)
    {
      auto textureCube = std::static_pointer_cast<VulkanTextureCube>(texture);

      switch (attachment.face)
      {
      case CubeMapFace::PositiveX:
      case CubeMapFace::NegativeX:
      case CubeMapFace::PositiveY:
      case CubeMapFace::NegativeY:
      case CubeMapFace::PositiveZ:
      case CubeMapFace::NegativeZ:
        return textureCube->getView(static_cast<int>(attachment.face), attachment.mipLevel);
      case CubeMapFace::All:
        return texture->getView();
      }
    }
    ASSERT(false, "Texture target not handled!");
    return texture->getView();
  });

  m_framebuffer = VulkanUtils::createFramebuffer(
        m_context.getDevice(),
        renderPass.getRenderPass(),
        fbAttachments,
        m_size.width,
        m_size.height);
}

VulkanFramebuffer::~VulkanFramebuffer()
{
  VulkanUtils::releaseFramebuffer(m_context.getDevice(), m_framebuffer);
}

VkSampleCountFlagBits VulkanFramebuffer::getSampleCountFlagBits() const
{
  return VK_SAMPLE_COUNT_1_BIT;
}

VkFramebuffer VulkanFramebuffer::getFramebuffer() const
{
  return m_framebuffer;
}

bool VulkanFramebuffer::isDefaultFramebuffer() const
{
  return false;
}

SizeI VulkanFramebuffer::getSize() const
{
  return m_size;
}

std::vector<AttachmentPoint> VulkanFramebuffer::getAttachments() const
{
  return m_attachmentPoints;
}
