#pragma once

#include "utils/size.h"
#include "math/math.h"

// Vulkan
#include <vulkan/vulkan.h>

class IVulkanContext
{
public:
  virtual ~IVulkanContext() = default;
  virtual VkInstance getInstance() const = 0;
  virtual VkPhysicalDevice getPhysicalDevice() const = 0;
  virtual VkPhysicalDeviceProperties const& getPhysicalDeviceProperties() const = 0;
  virtual VkDevice getDevice() const = 0;
  virtual VkQueue getGraphicsQueue() const = 0;
  virtual VkCommandPool getCommandPool() const = 0;
  virtual VkRenderPass getDefaultRenderPass() const = 0;
  virtual VkCommandBuffer getCurrentCommandBuffer() const = 0;
  virtual VkFramebuffer getCurrentFramebuffer() const = 0;
  virtual VkDescriptorPool getDescriptorPool() const = 0;
  virtual unsigned int getConcurrentFrameCount() const = 0;
  virtual SizeI getSwapChainImageSize() const = 0;
  virtual int getCurrentFrame() const = 0;
  virtual VkSampleCountFlagBits getSampleCountFlagBits() const = 0;
  virtual VkFormat getColorFormat() const = 0;
  virtual VkFormat getDepthStencilFormat() const = 0;
};
