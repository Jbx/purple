#pragma once

#include "images/image_interface.h"
#include "render/render_states.h"
#include "render/texture_interface.h"
#include "utils/assert.h"

// Vulkan
#include <vulkan/vulkan.h>

constexpr VkPrimitiveTopology toVkPrimitiveTopology(PrimitiveTopology topology)
{
  switch (topology)
  {
  case PrimitiveTopology::Points:
    return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
  case PrimitiveTopology::Lines:
    return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
  case PrimitiveTopology::LineStrip:
    return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
  case PrimitiveTopology::Triangles:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  case PrimitiveTopology::TriangleStrip:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
  case PrimitiveTopology::TriangleFan:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
  case PrimitiveTopology::LineListWithAdjacency:
    return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
  case PrimitiveTopology::LineStripWithAdjacency:
    return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
  case PrimitiveTopology::TriangleListWithAdjacency:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
  case PrimitiveTopology::TriangleStripWithAdjacency:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
  }
  assert(false && "Topology not handled");
  return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
}

constexpr VkPolygonMode toVkPolygonMode(PolygonMode mode)
{
  switch (mode)
  {
  case PolygonMode::Fill:
    return VK_POLYGON_MODE_FILL;
  case PolygonMode::Line:
    return VK_POLYGON_MODE_LINE;
  case PolygonMode::Point:
    return VK_POLYGON_MODE_POINT;
  }
  assert(false && "Fill mode not handled");
  return VK_POLYGON_MODE_FILL;
}

constexpr VkCullModeFlags toVkCullMode(CullMode mode)
{
  switch (mode)
  {
  case CullMode::None:
    return VK_CULL_MODE_NONE;
  case CullMode::Front:
    return VK_CULL_MODE_FRONT_BIT;
  case CullMode::Back:
    return VK_CULL_MODE_BACK_BIT;
  case CullMode::FrontAndBack:
    return VK_CULL_MODE_FRONT_AND_BACK;
  }
  assert(false && "Cull mode not handled");
  return VK_CULL_MODE_NONE;
}

constexpr VkFrontFace toVkFrontFace(WindingOrder order)
{
  switch (order)
  {
  case WindingOrder::CounterClockwise:
    return VK_FRONT_FACE_COUNTER_CLOCKWISE;
  case WindingOrder::Clockwise:
    return VK_FRONT_FACE_CLOCKWISE;
  }
  assert(false && "Winding order not handled");
  return VK_FRONT_FACE_COUNTER_CLOCKWISE;
}

constexpr VkBlendFactor toVkBlendFactor(BlendFactor factor)
{
  switch (factor)
  {
  case BlendFactor::Zero:
    return VK_BLEND_FACTOR_ZERO;
  case BlendFactor::One:
    return VK_BLEND_FACTOR_ONE;
  case BlendFactor::SrcColor:
    return VK_BLEND_FACTOR_SRC_COLOR;
  case BlendFactor::OneMinusSrcColor:
    return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
  case BlendFactor::DstColor:
    return VK_BLEND_FACTOR_DST_COLOR;
  case BlendFactor::OneMinusDstColor:
    return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
  case BlendFactor::SrcAlpha:
    return VK_BLEND_FACTOR_SRC_ALPHA;
  case BlendFactor::OneMinusSrcAlpha:
    return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  case BlendFactor::DstAlpha:
    return VK_BLEND_FACTOR_DST_ALPHA;
  case BlendFactor::OneMinusDstAlpha:
    return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
  case BlendFactor::ConstantColor:
    return VK_BLEND_FACTOR_CONSTANT_COLOR;
  case BlendFactor::OneMinusConstantColor:
    return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
  case BlendFactor::ConstantAlpha:
    return VK_BLEND_FACTOR_CONSTANT_ALPHA;
  case BlendFactor::OneMinusConstantAlpha:
    return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
  case BlendFactor::SrcAlphaSaturate:
    return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
  }
  assert(false && "Blend factor not handled");
  return VK_BLEND_FACTOR_ZERO;
}

constexpr VkBlendOp toVkBlendOperation(BlendOperation operation)
{
  switch (operation)
  {
  case BlendOperation::Add:
    return VK_BLEND_OP_ADD;
  case BlendOperation::Substract:
    return VK_BLEND_OP_SUBTRACT;
  case BlendOperation::ReverseSubstract:
    return VK_BLEND_OP_REVERSE_SUBTRACT;
  case BlendOperation::Min:
    return VK_BLEND_OP_MIN;
  case BlendOperation::Max:
    return VK_BLEND_OP_MAX;
  }
  assert(false && "Operation not handled");
  return VK_BLEND_OP_ADD;
}

constexpr VkCompareOp toVkDepthCompareOperation(DepthCompareOperation operation)
{
  switch (operation)
  {
  case DepthCompareOperation::Never:
    return VK_COMPARE_OP_NEVER;
  case DepthCompareOperation::Less:
    return VK_COMPARE_OP_LESS;
  case DepthCompareOperation::Equal:
    return VK_COMPARE_OP_EQUAL;
  case DepthCompareOperation::LessOrEqual:
    return VK_COMPARE_OP_LESS_OR_EQUAL;
  case DepthCompareOperation::Greater:
    return VK_COMPARE_OP_GREATER;
  case DepthCompareOperation::NotEqual:
    return VK_COMPARE_OP_NOT_EQUAL;
  case DepthCompareOperation::GreaterOrEqual:
    return VK_COMPARE_OP_GREATER_OR_EQUAL;
  case DepthCompareOperation::Always:
    return VK_COMPARE_OP_ALWAYS;
  }
  assert(false && "Operation not handled");
  return VK_COMPARE_OP_NEVER;
}

constexpr VkFilter toVkFilter(TextureFilter filter)
{
  switch (filter)
  {
  case TextureFilter::Nearest:
  case TextureFilter::NearestMipMapNearest:
  case TextureFilter::NearestMipMapLinear:
    return VK_FILTER_NEAREST;
  case TextureFilter::Linear:
  case TextureFilter::LinearMipMapNearest:
  case TextureFilter::LinearMipMapLinear:
    return VK_FILTER_LINEAR;
  }
  assert(false && "Filter mode not handled");
  return VK_FILTER_NEAREST;
}

constexpr VkSamplerMipmapMode toVkMipmapMode(TextureFilter filter)
{
  switch (filter)
  {
  case TextureFilter::Linear:
  case TextureFilter::Nearest:
  case TextureFilter::NearestMipMapNearest:
  case TextureFilter::LinearMipMapNearest:
    return VK_SAMPLER_MIPMAP_MODE_NEAREST;
  case TextureFilter::NearestMipMapLinear:
  case TextureFilter::LinearMipMapLinear:
    return VK_SAMPLER_MIPMAP_MODE_LINEAR;
  }
  assert(false && "Filter mode not handled");
  return VK_SAMPLER_MIPMAP_MODE_NEAREST;
}

constexpr VkSamplerAddressMode toVkWrapMode(TextureWrapMode wrapMode)
{
  switch (wrapMode)
  {
  case TextureWrapMode::Repeat:
    return VK_SAMPLER_ADDRESS_MODE_REPEAT;
  case TextureWrapMode::MirrorRepeat:
    return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
  case TextureWrapMode::ClampToEdge:
    return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  case TextureWrapMode::ClampToBorder:
    return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
  }
  assert(false && "Wrap mode not handled");
  return VK_SAMPLER_ADDRESS_MODE_REPEAT;
}

constexpr int toSampleCount(VkSampleCountFlagBits sampleCountFlagBits)
{
  switch (sampleCountFlagBits)
  {
  case VK_SAMPLE_COUNT_1_BIT:
    return 1;
  case VK_SAMPLE_COUNT_2_BIT:
    return 2;
  case VK_SAMPLE_COUNT_4_BIT:
    return 4;
  case VK_SAMPLE_COUNT_8_BIT:
    return 8;
  case VK_SAMPLE_COUNT_16_BIT:
    return 16;
  case VK_SAMPLE_COUNT_32_BIT:
    return 32;
  case VK_SAMPLE_COUNT_64_BIT:
    return 64;
  default:
    return 1;
  }
  assert(false && "Sample count not handled");
  return 1;
}

constexpr VkSampleCountFlagBits toVkSampleCount(int sampleCount)
{
  switch (sampleCount)
  {
  case 1:
    return VK_SAMPLE_COUNT_1_BIT;
  case 2:
    return VK_SAMPLE_COUNT_2_BIT;
  case 4:
    return VK_SAMPLE_COUNT_4_BIT;
  case 8:
    return VK_SAMPLE_COUNT_8_BIT;
  case 16:
    return VK_SAMPLE_COUNT_16_BIT;
  case 32:
    return VK_SAMPLE_COUNT_32_BIT;
  case 64:
    return VK_SAMPLE_COUNT_64_BIT;
  default:
    return VK_SAMPLE_COUNT_1_BIT;
  }
  assert(false && "Sample count not handled");
  return VK_SAMPLE_COUNT_1_BIT;
}

constexpr VkFormat toVkFormat(TextureFormat format)
{
  switch (format)
  {
  case TextureFormat::RGBA_UNorm:
    return VK_FORMAT_R8G8B8A8_UNORM;
  case TextureFormat::RGBA_16F:
    return VK_FORMAT_R16G16B16A16_SFLOAT;
  case TextureFormat::RGBA_32F:
    return VK_FORMAT_R32G32B32A32_SFLOAT;
  case TextureFormat::Depth:
    return VK_FORMAT_D16_UNORM;
  }
  assert(false && "Texture format not handled");
  return VK_FORMAT_R8G8B8A8_UNORM;
}

