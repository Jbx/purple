#pragma once

#include "utils/color.h"
#include "math/math.h"

// STL
#include <set>
#include <string>
#include <variant>
#include <vector>

enum class PropertyType
{
  Int,
  Float,

  Vec2,
  Vec3,
  Vec4,

  Mat2,
  Mat4,

  Color
};

class Property
{
public:
  Property(int value);
  Property(float value);
  Property(glm::vec2 const& value);
  Property(glm::vec3 const& value);
  Property(glm::vec4 const& value);
  Property(glm::mat2 const& value);
  Property(glm::mat4 const& value);
  Property(Color const& value);

  Property(Property const& value);
  Property& operator=(Property const& value);

  PropertyType getType() const;
  size_t getSize() const;
  size_t getAlignment() const;

  int toInt() const;
  float toFloat() const;
  glm::vec2 const& toVec2() const;
  glm::vec3 const& toVec3() const;
  glm::vec4 const& toVec4() const;
  glm::mat2 const& toMat2() const;
  glm::mat4 const& toMat4() const;
  Color const& toColor() const;

  std::string toString() const;

private:
  PropertyType m_type;

  std::variant<
  int,
  float,
  glm::vec2,
  glm::vec3,
  glm::vec4,
  glm::mat2,
  glm::mat4,
  Color> m_value;
};

using PropertyKeyValue = std::pair<std::string, Property>;
using PropertyList = std::vector<PropertyKeyValue>;
using PropertyBuffer = std::vector<char>;

class PropertyContainer
{
public:
  PropertyContainer(std::initializer_list<PropertyKeyValue> const& properties);
  PropertyContainer(PropertyList const& properties);

  PropertyList const& getProperties() const;

  void update(std::string const& name, Property const& value);
  Property& operator[](std::string const& name);

  bool contains(std::string const& name) const;
  bool isEmpty() const;

  size_t getAlignedBufferSize() const;
  PropertyBuffer asAlignedBuffer() const;

  void dump();

private:
  PropertyList m_properties;
};

