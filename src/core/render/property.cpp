#include "property.h"
#include "utils/assert.h"
#include "math/math.h"

// STL
#include <numeric>
#include <cstring>

Property::Property(int value)
  : m_type(PropertyType::Int)
  , m_value(value)
{

}

Property::Property(float value)
  : m_type(PropertyType::Float)
  , m_value(value)
{

}

Property::Property(glm::vec2 const& value)
  : m_type(PropertyType::Vec2)
  , m_value(value)
{

}

Property::Property(glm::vec3 const& value)
  : m_type(PropertyType::Vec3)
  , m_value(value)
{

}

Property::Property(glm::vec4 const& value)
  : m_type(PropertyType::Vec4)
  , m_value(value)
{

}

Property::Property(glm::mat2 const& value)
  : m_type(PropertyType::Mat2)
  , m_value(value)
{

}

Property::Property(glm::mat4 const& value)
  : m_type(PropertyType::Mat4)
  , m_value(value)
{

}

Property::Property(Color const& value)
  : m_type(PropertyType::Color)
  , m_value(value)
{

}

Property::Property(Property const& property)
  : m_type(property.m_type)
  , m_value(property.m_value)
{

}

Property& Property::operator=(Property const& property)
{
  ASSERT(m_type == property.m_type, "Property type error");
  m_type = property.m_type;
  m_value = property.m_value;
  return *this;
}

PropertyType Property::getType() const
{
  return m_type;
}

size_t Property::getSize() const
{
  switch (m_type)
  {
  case PropertyType::Int:
    return sizeof(int);
  case PropertyType::Float:
    return sizeof(float);
  case PropertyType::Vec2:
    return sizeof(glm::vec2);
  case PropertyType::Vec3:
    return sizeof(glm::vec3);
  case PropertyType::Vec4:
    return sizeof(glm::vec4);
  case PropertyType::Mat2:
    return sizeof(glm::mat2);
  case PropertyType::Mat4:
    return sizeof(glm::mat4);
  case PropertyType::Color:
    return sizeof(Color);
  }

  ASSERT(false, "Unknown type");
  return 0; // avoid warning
}

size_t Property::getAlignment() const
{
  switch (m_type)
  {
  case PropertyType::Int:
  case PropertyType::Float:
    return sizeof(float);
  case PropertyType::Vec2:
    return 2 * sizeof(float);
  case PropertyType::Vec3:
  case PropertyType::Vec4:
  case PropertyType::Mat2:
  case PropertyType::Mat4:
  case PropertyType::Color:
    return 4 * sizeof(float);
  }

  ASSERT(false, "Unknown type");
  return 0; // avoid warning
}

int Property::toInt() const
{
  ASSERT(m_type == PropertyType::Int, "Property type error");
  return std::get<int>(m_value);
}

float Property::toFloat() const
{
  ASSERT(m_type == PropertyType::Float, "Property type error");
  return std::get<float>(m_value);
}

glm::vec2 const& Property::toVec2() const
{
  ASSERT(m_type == PropertyType::Vec2, "Property type error");
  return std::get<glm::vec2>(m_value);
}

glm::vec3 const& Property::toVec3() const
{
  ASSERT(m_type == PropertyType::Vec3, "Property type error");
  return std::get<glm::vec3>(m_value);
}

glm::vec4 const& Property::toVec4() const
{
  ASSERT(m_type == PropertyType::Vec4, "Property type error");
  return std::get<glm::vec4>(m_value);
}

glm::mat2 const& Property::toMat2() const
{
  ASSERT(m_type == PropertyType::Mat2, "Property type error");
  return std::get<glm::mat2>(m_value);
}

glm::mat4 const& Property::toMat4() const
{
  ASSERT(m_type == PropertyType::Mat4, "Property type error");
  return std::get<glm::mat4>(m_value);
}

Color const& Property::toColor() const
{
  ASSERT(m_type == PropertyType::Color, "Property type error");
  return std::get<Color>(m_value);
}

std::string Property::toString() const
{
  switch (m_type)
  {
  case PropertyType::Int:
    return std::to_string(std::get<int>(m_value));
  case PropertyType::Float:
    return std::to_string(std::get<float>(m_value));
  case PropertyType::Vec2:
    return glm::to_string(std::get<glm::vec2>(m_value));
  case PropertyType::Vec3:
    return glm::to_string(std::get<glm::vec3>(m_value));
  case PropertyType::Vec4:
    return glm::to_string(std::get<glm::vec4>(m_value));
  case PropertyType::Mat2:
    return glm::to_string(std::get<glm::mat2>(m_value));
  case PropertyType::Mat4:
    return glm::to_string(std::get<glm::mat4>(m_value));
  case PropertyType::Color:
    return std::get<Color>(m_value).toString();
  }

  ASSERT(false, "Unknown type");
  return 0; // avoid warning
}

PropertyContainer::PropertyContainer(std::initializer_list<PropertyKeyValue> const& properties)
  : m_properties(properties)
{}

PropertyContainer::PropertyContainer(PropertyList const& properties)
  : m_properties(properties)
{}

PropertyList const& PropertyContainer::getProperties() const
{
  return m_properties;
}

void PropertyContainer::update(std::string const& name, Property const& property)
{
  auto it = std::find_if(
        m_properties.begin(),
        m_properties.end(),
        [&name](auto const& property) { return property.first == name; });
  ASSERT(it != m_properties.end(), "Property \"", name, "\" not found!");
  it->second = property;
}

Property& PropertyContainer::operator[](std::string const& name)
{
  auto it = std::find_if(
        m_properties.begin(),
        m_properties.end(),
        [&name](auto const& property) { return property.first == name; });
  ASSERT(it != m_properties.end(), "Property \"", name, "\" not found!");
  return it->second;
}

bool PropertyContainer::contains(std::string const& name) const
{
  auto it = std::find_if(
        m_properties.begin(),
        m_properties.end(),
        [&name](auto const& property) { return property.first == name; });
  return it != m_properties.end();
}

bool PropertyContainer::isEmpty() const
{
  return m_properties.empty();
}

int roundUp(int num, int multiple)
{
  if (multiple == 0)
    return num;

  int remainder = num % multiple;
  if (remainder == 0)
    return num;

  return num + multiple - remainder;
}

size_t PropertyContainer::getAlignedBufferSize() const
{
  auto size = static_cast<size_t>(
        std::accumulate(
          m_properties.begin(),
          m_properties.end(),
          0,
          [](int size, auto const& property) {
    int offset = std::ceil(size / static_cast<float>(property.second.getAlignment())) * property.second.getAlignment();
    return offset + static_cast<int>(property.second.getSize());
  }));

  return roundUp(size, 16);
}

PropertyBuffer PropertyContainer::asAlignedBuffer() const
{
  PropertyBuffer data(getAlignedBufferSize());
  size_t size = 0;

  for (auto const& [name, property] : m_properties)
  {
    size_t offset = std::ceil(size / static_cast<float>(property.getAlignment())) * property.getAlignment();
    size = offset + property.getSize();

    switch (property.getType())
    {
    case PropertyType::Int:
    {
      auto val = property.toInt();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Float:
    {
      auto val = property.toFloat();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Vec2:
    {
      auto val = property.toVec2();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Vec3:
    {
      auto val = property.toVec3();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Vec4:
    {
      auto val = property.toVec4();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Mat2:
    {
      auto val = property.toMat2();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Mat4:
    {
      auto val = property.toMat4();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    case PropertyType::Color:
    {
      auto val = property.toColor();
      std::memcpy(data.data() + offset, &val, property.getSize());
    } break;
    }
  }
  return data;
}

void PropertyContainer::dump()
{
  for (auto const& [name, property] : m_properties)
  {
    LOG_INFO(name, " ", property.toString());
  }
}
