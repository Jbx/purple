#pragma once

#include "render/attribute.h"

// JSON
#include "json.hpp"

// STL
#include <memory>
#include <string>
#include <optional>

class ShaderProgram
{
public:
  ShaderProgram(
      std::string const& vertexShaderFilePath,
      std::string const& geometryShaderFilePath,
      std::string const& fragmentShaderFilePath);

  ShaderProgram(
      std::string const& vertexShaderFilePath,
      std::string const& fragmentShaderFilePath);

  enum ShaderVersion
  {
    SPIRV,
    GLSL_330,
    GLSL_300_ES
  };  

  std::vector<unsigned int> getVertexShaderSourceCode(ShaderVersion version) const;
  std::optional<std::vector<unsigned int>> getGeometryShaderSourceCode(ShaderVersion version) const;
  std::vector<unsigned int> getFragmentShaderSourceCode(ShaderVersion version) const;

private: 
  nlohmann::json m_vertexShaderSource;
  std::optional<nlohmann::json> m_geometryShaderSource;
  nlohmann::json m_fragmentShaderSource;
};

class Shader
{
public:
  Shader(
      ShaderProgram const& program,
      AttributeLayout const& attributeLayout);

  ShaderProgram const& getProgram() const;
  AttributeLayout const& getAttributeLayout() const;

public:
  ShaderProgram m_program;
  AttributeLayout m_attributeLayout;
};
