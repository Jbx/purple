#pragma once

#include "pipeline_interface.h"

class IVertexBuffer
{
public:
  virtual ~IVertexBuffer() = default;
  virtual void bind() = 0;
  virtual void draw(IPipeline const& pipeline) = 0;
};

