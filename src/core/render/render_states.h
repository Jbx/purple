#pragma once

enum class BlendFactor
{
  Zero,
  One,
  SrcColor,
  OneMinusSrcColor,
  DstColor,
  OneMinusDstColor,
  SrcAlpha,
  OneMinusSrcAlpha,
  DstAlpha,
  OneMinusDstAlpha,
  ConstantColor,
  OneMinusConstantColor,
  ConstantAlpha,
  OneMinusConstantAlpha,
  SrcAlphaSaturate
};

enum class BlendOperation
{
  Add,
  Substract,
  ReverseSubstract,
  Min,
  Max
};

enum class CullMode
{
  None,
  Front,
  Back,
  FrontAndBack
};

enum class DepthCompareOperation
{
  Never,
  Less,
  Equal,
  LessOrEqual,
  Greater,
  NotEqual,
  GreaterOrEqual,
  Always
};

enum class PolygonMode
{
  Fill,
  Line,
  Point
};

enum class PrimitiveTopology
{
  Points,
  Lines,
  LineStrip,
  Triangles,
  TriangleStrip,
  TriangleFan,
  LineListWithAdjacency,
  LineStripWithAdjacency,
  TriangleListWithAdjacency,
  TriangleStripWithAdjacency
};

enum class WindingOrder
{
  CounterClockwise,
  Clockwise
};

struct RenderStates
{
  PrimitiveTopology primitiveTopology = PrimitiveTopology::Triangles;
  PolygonMode polygonMode = PolygonMode::Fill;
  float lineWidth = 1.0;
  CullMode cullMode = CullMode::Back;
  bool depthTestEnabled = true;
  bool depthWriteEnabled = true;
  bool colorWriteEnabled = true;
  DepthCompareOperation depthCompareOperation = DepthCompareOperation::LessOrEqual;
  bool blendingEnabled = false;
  BlendFactor srcColorBlendFactor = BlendFactor::SrcAlpha;
  BlendFactor dstColorBlendFactor = BlendFactor::OneMinusSrcAlpha;
  BlendOperation colorBlendOperation = BlendOperation::Add;
  BlendFactor srcAlphaBlendFactor = BlendFactor::One;
  BlendFactor dstAlphaBlendFactor = BlendFactor::Zero;
  BlendOperation alphaBlendOperation = BlendOperation::Add;
  bool depthBiasEnabled = false;
  float depthBiasConstantFactor = 1.25f;
  float depthBiasClamp = 0.f;
  float depthBiasSlopeFactor = 1.75f;
};
