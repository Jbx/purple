#pragma once

#include "texture_interface.h"

// STL
#include <string>
#include <vector>

using TextureKeyValue = std::pair<std::string, Ref<ITexture>>;
using TextureList = std::vector<TextureKeyValue>;

class TextureContainer
{
public:
  TextureContainer(
      TextureList const& textures);

  TextureList const& getTextures() const;

  size_t size() const;
  bool empty() const;

  auto begin() const noexcept { return m_textures.begin(); }
  auto end() const noexcept { return m_textures.end(); }

  void update(std::string const& name, Ref<ITexture> const& texture);
  Ref<ITexture> operator[](std::string const& name);

  bool contains(std::string const& name) const;

private:
  TextureList m_textures;
};


