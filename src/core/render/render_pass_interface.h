#pragma once

#include "render/framebuffer_interface.h"
#include "utils/color.h"
#include "utils/rect.h"

// STL
#include <vector>

class IRenderPass
{
public:
  virtual ~IRenderPass() = default;

  virtual bool isDefaultRenderPass() const = 0;

  virtual std::vector<AttachmentPoint> getAttachments() const = 0;
  virtual int getSampleCount() const = 0;

  virtual void setClearColor(Color const& clearColor) = 0;
  virtual void setClearDepthValue(float clearDepthValue) = 0;
  virtual void setClearStencilValue(unsigned int clearStencilValue) = 0;  

  virtual void begin(
      IFramebuffer const& framebuffer,
      RectI const& viewport) = 0;

  virtual void end() = 0;  
};
