#pragma once

#include "render/property.h"

// STL
#include <memory>

class IPipeline;

struct NamedPropertyBuffer
{
  std::string name;
  PropertyBuffer buffer;
};

class IUniformBuffer
{
public:
  virtual ~IUniformBuffer() = default;

  virtual void update(
      IPipeline const& pipeline,
      std::vector<NamedPropertyBuffer> const& properties) = 0;

  virtual void bind(IPipeline const& pipeline) = 0;
};
