#include "opengl_framebuffer.h"
#include "opengl_texture.h"
#include "opengl.h"
#include "utils/assert.h"

// STL
#include <algorithm>

DefaultOpenGLFramebuffer::DefaultOpenGLFramebuffer(
    IOpenGLContext& context)
  : m_context(context)
{

}

bool DefaultOpenGLFramebuffer::isDefaultFramebuffer() const
{
  return true;
}

SizeI DefaultOpenGLFramebuffer::getSize() const
{
  return m_context.getSize();
}

std::vector<AttachmentPoint> DefaultOpenGLFramebuffer::getAttachments() const
{
  return {AttachmentPoint::Color, AttachmentPoint::Depth};
}

unsigned int DefaultOpenGLFramebuffer::getId() const
{
  return 0;
}

OpenGLFramebuffer::OpenGLFramebuffer(
    IOpenGLContext& context,
    std::vector<Attachment> const& attachments,
    SizeI const& size)
  : m_context(context)
  , m_size(size)
{
  ASSERT(!attachments.empty(), "Framebuffer must have at least one attachment!");
  ASSERT(!m_size.isNull() && m_size.isValid(), "Framebuffer size must not be null!");

  glGenFramebuffers(1, &m_FBO);
  glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

  unsigned int colorIdx = 0;

  for (auto const& attachment : attachments)
  {
    auto texture = std::static_pointer_cast<IOpenGLTexture>(attachment.texture);

    if (texture->getTarget() == TextureTarget::Target2D)
    {
      if (attachment.attachmentPoint == AttachmentPoint::Color)
      {
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,
              GL_COLOR_ATTACHMENT0 + colorIdx,
              GL_TEXTURE_2D,
              texture->getId(),
              attachment.mipLevel);
        colorIdx++;
      }

      if (attachment.attachmentPoint == AttachmentPoint::Depth)
      {
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,
              GL_DEPTH_ATTACHMENT,
              GL_TEXTURE_2D,
              texture->getId(),
              attachment.mipLevel);
      }
    }
    else if (texture->getTarget() == TextureTarget::TargetCube)
    {
      int faceIndex = static_cast<int>(attachment.face);
      if (attachment.attachmentPoint == AttachmentPoint::Color)
      {
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,
              GL_COLOR_ATTACHMENT0 + colorIdx,
              GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceIndex,
              texture->getId(),
              attachment.mipLevel);
        colorIdx++;
      }

      if (attachment.attachmentPoint == AttachmentPoint::Depth)
      {
        glFramebufferTexture2D(
              GL_FRAMEBUFFER,
              GL_DEPTH_ATTACHMENT,
              GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceIndex,
              texture->getId(),
              attachment.mipLevel);
      }
    }

    m_attachmentPoints.push_back(attachment.attachmentPoint);
  }

  ASSERT(
        glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE,
        "Framebuffer is not complete!");
}

OpenGLFramebuffer::~OpenGLFramebuffer()
{
  glDeleteFramebuffers(1, &m_FBO);
}

bool OpenGLFramebuffer::isDefaultFramebuffer() const
{
  return false;
}

SizeI OpenGLFramebuffer::getSize() const
{
  return m_size;
}

std::vector<AttachmentPoint> OpenGLFramebuffer::getAttachments() const
{
  return m_attachmentPoints;
}

unsigned int OpenGLFramebuffer::getId() const
{
  return m_FBO;
}
