#pragma once

#include "opengl_context_interface.h"
#include "render/texture_interface.h"
#include "images/image_interface.h"
#include "images/image_interface.h"

class IOpenGLTexture : public ITexture
{
public:
  virtual unsigned int getId() const = 0;
  virtual void bind() = 0;
};

class OpenGLTexture final : public IOpenGLTexture
{
public:
  OpenGLTexture(
      IOpenGLContext& context,
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  OpenGLTexture(
      IOpenGLContext& context,
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  ~OpenGLTexture() override;

  SizeI getSize() const override;
  TextureFormat getFormat() const override;
  TextureTarget getTarget() const override;

  unsigned int getId() const override;
  void bind() override;

private:
  friend class OpenGLUniformBuffer;
  IOpenGLContext& m_context;
  SizeI m_size;
  TextureFormat m_format;
  unsigned int m_id;
};

class OpenGLTextureCube final : public IOpenGLTexture
{
public:
  OpenGLTextureCube(
      IOpenGLContext& context,
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  OpenGLTextureCube(
      IOpenGLContext& context,
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  ~OpenGLTextureCube() override;

  SizeI getSize() const override;
  TextureFormat getFormat() const override;
  TextureTarget getTarget() const override;

  unsigned int getId() const override;
  void bind() override;

private:
  IOpenGLContext& m_context;
  SizeI m_size;
  TextureFormat m_format;
  unsigned int m_id;
};

