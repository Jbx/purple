#pragma once

#include "utils/size.h"

struct OpenGLVersion
{
  int minVersion;
  int maxVersion;
  bool es;
};

class IOpenGLContext
{
public:   
  virtual ~IOpenGLContext() = default;
  virtual OpenGLVersion getOpenGLVersion() const = 0;
  virtual SizeI getSize() const = 0;
  virtual int getSampleCount() const = 0;
};
