#pragma once

#include "render/render_states.h"
#include "render/texture_interface.h"
#include "utils/assert.h"

#include "opengl.h"

constexpr GLenum toGLPrimitiveTopology(PrimitiveTopology topology)
{
  switch (topology)
  {
  case PrimitiveTopology::Points:
    return GL_POINTS;
  case PrimitiveTopology::Lines:
    return GL_LINES;
  case PrimitiveTopology::LineStrip:
    return GL_LINE_STRIP;
  case PrimitiveTopology::Triangles:
    return GL_TRIANGLES;
  case PrimitiveTopology::TriangleStrip:
    return GL_TRIANGLE_STRIP;
  case PrimitiveTopology::TriangleFan:
    return GL_TRIANGLE_FAN;
#ifdef PURPLE_SUPPORT_WASM
  default:
    ASSERT(false, "Topology not supported!");
#else
  case PrimitiveTopology::LineListWithAdjacency:
    return GL_LINES_ADJACENCY;
  case PrimitiveTopology::LineStripWithAdjacency:
    return GL_LINE_STRIP_ADJACENCY;
  case PrimitiveTopology::TriangleListWithAdjacency:
    return GL_TRIANGLES_ADJACENCY;
  case PrimitiveTopology::TriangleStripWithAdjacency:
    return GL_TRIANGLE_STRIP_ADJACENCY;
#endif // PURPLE_SUPPORT_WASM
  }
  return GL_POINTS;
}

#ifndef PURPLE_SUPPORT_WASM
constexpr GLenum toGLPolygonMode(PolygonMode mode)
{
  switch (mode)
  {
  case PolygonMode::Fill:
    return GL_FILL;
  case PolygonMode::Line:
    return GL_LINE;
  case PolygonMode::Point:
    return GL_POINT;
  }
  return GL_FILL;
}
#endif // PURPLE_SUPPORT_WASM

constexpr GLenum toGLCullMode(CullMode mode)
{
  switch (mode)
  {
  case CullMode::None:
    return GL_NONE;
  case CullMode::Front:
    return GL_FRONT;
  case CullMode::Back:
    return GL_BACK;
  case CullMode::FrontAndBack:
    return GL_FRONT_AND_BACK;
  }
  return GL_NONE;
}

constexpr GLenum toGLFrontFace(WindingOrder order)
{
  switch (order)
  {
  case WindingOrder::CounterClockwise:
    return GL_CCW;
  case WindingOrder::Clockwise:
    return GL_CW;
  }
  return GL_CW;
}

constexpr GLenum toGLBlendFactor(BlendFactor factor)
{
  switch (factor)
  {
  case BlendFactor::Zero:
    return GL_ZERO;
  case BlendFactor::One:
    return GL_ONE;
  case BlendFactor::SrcColor:
    return GL_SRC_COLOR;
  case BlendFactor::OneMinusSrcColor:
    return GL_ONE_MINUS_SRC_COLOR;
  case BlendFactor::DstColor:
    return GL_DST_COLOR;
  case BlendFactor::OneMinusDstColor:
    return GL_ONE_MINUS_DST_COLOR;
  case BlendFactor::SrcAlpha:
    return GL_SRC_ALPHA;
  case BlendFactor::OneMinusSrcAlpha:
    return GL_ONE_MINUS_SRC_ALPHA;
  case BlendFactor::DstAlpha:
    return GL_DST_ALPHA;
  case BlendFactor::OneMinusDstAlpha:
    return GL_ONE_MINUS_DST_ALPHA;
  case BlendFactor::ConstantColor:
    return GL_CONSTANT_COLOR;
  case BlendFactor::OneMinusConstantColor:
    return GL_ONE_MINUS_CONSTANT_COLOR;
  case BlendFactor::ConstantAlpha:
    return GL_CONSTANT_ALPHA;
  case BlendFactor::OneMinusConstantAlpha:
    return GL_ONE_MINUS_CONSTANT_ALPHA;
  case BlendFactor::SrcAlphaSaturate:
    return GL_SRC_ALPHA_SATURATE;
  }
  return GL_ZERO;
}

constexpr GLenum toGLBlendOperation(BlendOperation operation)
{
  switch (operation)
  {
  case BlendOperation::Add:
    return GL_FUNC_ADD;
  case BlendOperation::Substract:
    return GL_FUNC_SUBTRACT;
  case BlendOperation::ReverseSubstract:
    return GL_FUNC_REVERSE_SUBTRACT;
  case BlendOperation::Min:
    return GL_MIN;
  case BlendOperation::Max:
    return GL_MAX;
  }
  return GL_FUNC_ADD;
}

constexpr GLenum toGLDepthCompareOperation(DepthCompareOperation operation)
{
  switch (operation)
  {
  case DepthCompareOperation::Never:
    return GL_NEVER;
  case DepthCompareOperation::Less:
    return GL_LESS;
  case DepthCompareOperation::Equal:
    return GL_EQUAL;
  case DepthCompareOperation::LessOrEqual:
    return GL_LEQUAL;
  case DepthCompareOperation::Greater:
    return GL_GREATER;
  case DepthCompareOperation::NotEqual:
    return GL_NOTEQUAL;
  case DepthCompareOperation::GreaterOrEqual:
    return GL_GEQUAL;
  case DepthCompareOperation::Always:
    return GL_ALWAYS;
  }
  return GL_NEVER;
}

constexpr GLenum toGLFilter(TextureFilter filter)
{
  switch (filter)
  {
  case TextureFilter::Nearest:
    return GL_NEAREST;
  case TextureFilter::NearestMipMapNearest:
    return GL_NEAREST_MIPMAP_NEAREST;
  case TextureFilter::NearestMipMapLinear:
    return GL_NEAREST_MIPMAP_LINEAR;
  case TextureFilter::Linear:
    return GL_LINEAR;
  case TextureFilter::LinearMipMapNearest:
    return GL_LINEAR_MIPMAP_NEAREST;
  case TextureFilter::LinearMipMapLinear:
    return GL_LINEAR_MIPMAP_LINEAR;
  }
  return GL_NEAREST;
}

constexpr GLenum toGLWrapMode(TextureWrapMode wrapMode)
{
  switch (wrapMode)
  {
  case TextureWrapMode::Repeat:
    return GL_REPEAT;
  case TextureWrapMode::MirrorRepeat:
    return GL_MIRRORED_REPEAT;
  case TextureWrapMode::ClampToEdge:
    return GL_CLAMP_TO_EDGE;
#ifdef PURPLE_SUPPORT_WASM
  default:
    ASSERT(false, "Wrap mode not supported!");
#else
  case TextureWrapMode::ClampToBorder:
    return GL_CLAMP_TO_BORDER;
#endif // PURPLE_SUPPORT_WASM
  }
  return GL_REPEAT;
}
