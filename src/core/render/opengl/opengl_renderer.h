#pragma once

#include "render/renderer.h"
#include "opengl_context_interface.h"
#include "opengl_render_pass.h"
#include "opengl_framebuffer.h"
#include "opengl_vertex_buffer.h"

class OpenGLRenderer final : public Renderer<OpenGLRenderer>
{
public:
  OpenGLRenderer(IOpenGLContext& context);

  template<typename VertexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices);

  template<typename VertexType, typename IndexType>
  Scoped<IVertexBuffer> createVertexBuffer(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices);

  Scoped<IUniformBuffer> createUniformBuffer(
      std::vector<NamedPropertyBuffer> const& properties,
      TextureContainer const& textures);

  Scoped<ITexture> createTexture2D(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTexture2D(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTextureCube(
      IImage const& image,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<ITexture> createTextureCube(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap,
      Sampler const& sampler);

  Scoped<IPipeline> createPipeline(
      IRenderPass const& renderPass,
      Shader const& shader,
      RenderStates const& renderStates,
      unsigned int uniformBufferCount,
      TextureContainer const& textures);

  Scoped<IFramebuffer> createFramebuffer(
      IRenderPass const& renderPass,
      std::vector<Attachment> const& attachments,
      SizeI const& size);

  IRenderPass& getDefaultRenderPass();

  IFramebuffer& getDefaultFramebuffer();

  Scoped<IRenderPass> createRenderPass(
      std::vector<AttachmentFormat> const& attachments,
      int sampleCount);

private:
  IOpenGLContext& m_context;
  DefaultOpenGLRenderPass m_defaultRenderPass;
  DefaultOpenGLFramebuffer m_defaultFramebuffer;
};

template<typename VertexType>
Scoped<IVertexBuffer> OpenGLRenderer::createVertexBuffer(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices)
{
  return createScoped<OpenGLVertexBuffer>(m_context, layout, vertices);
}

template<typename VertexType, typename IndexType>
Scoped<IVertexBuffer> OpenGLRenderer::createVertexBuffer(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices,
    BufferView<IndexType> const& indices)
{
  return createScoped<OpenGLIndexedVertexBuffer>(m_context, layout, vertices, indices);
}
