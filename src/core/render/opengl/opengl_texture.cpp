#include "opengl_texture.h"
#include "opengl_converter.h"
#include "utils/assert.h"

// STL
#include <cmath>

#ifndef GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT 0x83F1
#endif

#ifndef GL_COMPRESSED_RGBA_S3TC_DXT3_EXT
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT  0x83F2
#endif

#ifndef GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT  0x83F3
#endif

#ifndef GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM
#define GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM  0x8E8D
#endif

GLenum toGLFormat(ImageFormat format)
{
  switch (format)
  {
  case ImageFormat::RGB8:
    return GL_RGB;
  case ImageFormat::RGBA8:
    return GL_RGBA;
  case ImageFormat::RGBA16F:
    return GL_RGBA16F;
  case ImageFormat::RGBA_S3TC_DXT1:
    return GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
  case ImageFormat::RGBA_S3TC_DXT3:
    return GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
  case ImageFormat::RGBA_S3TC_DXT5:
    return GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
  case ImageFormat::SRGB_ALPHA_BPTC_UNORM:
    return GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM;
  }
  ASSERT(false, "Format not handled");
  return GL_RGB8;
}

GLenum toGLDataType(ImageDataType dataType)
{
  switch (dataType)
  {
  case ImageDataType::UByte:
    return GL_UNSIGNED_BYTE;
  case ImageDataType::Byte:
    return GL_BYTE;
  case ImageDataType::UShort:
    return GL_UNSIGNED_SHORT;
  case ImageDataType::Short:
    return GL_SHORT;
  case ImageDataType::UInt:
    return GL_UNSIGNED_INT;
  case ImageDataType::Int:
    return GL_INT;
  case ImageDataType::HalfFloat:
    return GL_HALF_FLOAT;
  case ImageDataType::Float:
    return GL_FLOAT;
  }
  ASSERT(false, "Data type not handled");
  return GL_RGB8;
}


GLenum toGLDataType(ImageFormat format)
{
  switch (format)
  {
  case ImageFormat::RGB8:
  case ImageFormat::RGBA8:
    return GL_UNSIGNED_BYTE;
  case ImageFormat::RGBA16F:
    return GL_HALF_FLOAT;
  case ImageFormat::RGBA_S3TC_DXT1:
  case ImageFormat::RGBA_S3TC_DXT3:
  case ImageFormat::RGBA_S3TC_DXT5:
  case ImageFormat::SRGB_ALPHA_BPTC_UNORM:
    return GL_UNSIGNED_BYTE;
  }
  ASSERT(false, "Format not handled");
  return GL_UNSIGNED_BYTE;
}

OpenGLTexture::OpenGLTexture(
    IOpenGLContext& context,
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(image.getSize())
  , m_format(TextureFormat::RGBA_UNorm)
{
  bool hasMipMap = image.getMipLevelCount() > 1;

  glGenTextures(1, &m_id);
  glBindTexture(GL_TEXTURE_2D, m_id);

  for (int level = 0; level < (hasMipMap ? image.getMipLevelCount() : 1); level++)
  {
    int mipWidth = std::max(1, m_size.width >> level);
    int mipHeight = std::max(1, m_size.height >> level);

    if (image.isCompressed())
    {
      glCompressedTexImage2D(
            GL_TEXTURE_2D,
            level,
            toGLFormat(image.getFormat()),
            mipWidth,
            mipHeight,
            0,
            image.getDataSize(level),
            image.getData() + image.getOffset(0, level));
    }
    else
    {
      glTexImage2D(
            GL_TEXTURE_2D,
            level,
            toGLFormat(image.getFormat()),
            mipWidth,
            mipHeight,
            0,
            GL_RGBA,
            toGLDataType(image.getFormat()),
            image.getData() + image.getOffset(0, level));
    }
  }

  if (generateMipmap && !hasMipMap)
  {
    glGenerateMipmap(GL_TEXTURE_2D);
  }

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, toGLFilter(sampler.minFilter));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, toGLFilter(sampler.magFilter));

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, toGLWrapMode(sampler.wrapModeS));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, toGLWrapMode(sampler.wrapModeT));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, toGLWrapMode(sampler.wrapModeR));
}

OpenGLTexture::OpenGLTexture(
    IOpenGLContext& context,
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(size)
  , m_format(format)
{
  glGenTextures(1, &m_id);
  glBindTexture(GL_TEXTURE_2D, m_id);

  switch (format)
  {
  case TextureFormat::RGBA_UNorm:
    glTexImage2D(
          GL_TEXTURE_2D,
          0,
          GL_RGBA,
          m_size.width,
          m_size.height,
          0,
          GL_RGBA,
          GL_UNSIGNED_BYTE,
          nullptr);
    break;

  case TextureFormat::RGBA_16F:
    glTexImage2D(
          GL_TEXTURE_2D,
          0,
          GL_RGBA16F,
          m_size.width,
          m_size.height,
          0,
          GL_RGBA,
          GL_FLOAT,
          nullptr);
    break;

  case TextureFormat::RGBA_32F:
    glTexImage2D(
          GL_TEXTURE_2D,
          0,
          GL_RGBA32F,
          m_size.width,
          m_size.height,
          0,
          GL_RGBA,
          GL_FLOAT,
          nullptr);
    break;

  case TextureFormat::Depth:
  {
    glTexImage2D(
          GL_TEXTURE_2D,
          0,
      #ifndef PURPLE_SUPPORT_WASM
          GL_DEPTH_COMPONENT,
      #else
          GL_DEPTH_COMPONENT16,
      #endif // PURPLE_SUPPORT_WASM
          m_size.width,
          m_size.height,
          0,
          GL_DEPTH_COMPONENT,
          GL_UNSIGNED_INT,
          nullptr);
  } break;

  default:
    ASSERT(false, "Texture format not supported!");
    break;
  }

  if (generateMipmap)
  {
    glGenerateMipmap(GL_TEXTURE_2D);
  }

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, toGLFilter(sampler.minFilter));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, toGLFilter(sampler.magFilter));

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, toGLWrapMode(sampler.wrapModeS));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, toGLWrapMode(sampler.wrapModeT));
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, toGLWrapMode(sampler.wrapModeR));
}

OpenGLTexture::~OpenGLTexture()
{
  glDeleteTextures(1, &m_id);
}

SizeI OpenGLTexture::getSize() const
{
  return m_size;
}

TextureFormat OpenGLTexture::getFormat() const
{
  return m_format;
}

TextureTarget OpenGLTexture::getTarget() const
{
  return TextureTarget::Target2D;
}

unsigned int OpenGLTexture::getId() const
{
  return m_id;
}

void OpenGLTexture::bind()
{
  glBindTexture(GL_TEXTURE_2D, m_id);
}

OpenGLTextureCube::OpenGLTextureCube(
    IOpenGLContext& context,
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(image.getSize())
  , m_format(TextureFormat::RGBA_UNorm)
{
  bool hasMipMap = image.getMipLevelCount() > 1;

  glGenTextures(1, &m_id);
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

  std::array<GLenum, 6> targets = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  for (int level = 0; level < (hasMipMap ? image.getMipLevelCount() : 1); level++)
  {
    int mipWidth = std::max(1, m_size.width >> level);
    int mipHeight = std::max(1, m_size.height >> level);

    for (int i = 0; i < targets.size(); ++i)
    {
      if (image.isCompressed())
      {
        glCompressedTexImage2D(
              targets[i],
              level,
              toGLFormat(image.getFormat()),
              mipWidth,
              mipHeight,
              0,
              image.getDataSize(level),
              image.getData() + image.getOffset(i, level));
      }
      else
      {
        glTexImage2D(
              targets[i],
              level,
              toGLFormat(image.getFormat()),
              mipWidth,
              mipHeight,
              0,
              GL_RGBA,
              toGLDataType(image.getFormat()),
              image.getData() + image.getOffset(i, level));
      }
    }
  }

  if (generateMipmap && !hasMipMap)
  {
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, toGLFilter(sampler.minFilter));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, toGLFilter(sampler.magFilter));

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, toGLWrapMode(sampler.wrapModeS));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, toGLWrapMode(sampler.wrapModeT));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, toGLWrapMode(sampler.wrapModeR));
}

OpenGLTextureCube::OpenGLTextureCube(
    IOpenGLContext& context,
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
  : m_context(context)
  , m_size(size)
  , m_format(format)
{
  glGenTextures(1, &m_id);
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

  std::array<GLenum, 6> targets = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  for (int i = 0; i < targets.size(); ++i)
  {
    switch (format)
    {
    case TextureFormat::RGBA_UNorm:
      glTexImage2D(
            targets[i],
            0,
            GL_RGBA,
            m_size.width,
            m_size.height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            nullptr);
      break;

    case TextureFormat::RGBA_16F:
      glTexImage2D(
            targets[i],
            0,
            GL_RGBA16F,
            m_size.width,
            m_size.height,
            0,
            GL_RGBA,
            GL_FLOAT,
            nullptr);
      break;

    case TextureFormat::RGBA_32F:
      glTexImage2D(
            targets[i],
            0,
            GL_RGBA32F,
            m_size.width,
            m_size.height,
            0,
            GL_RGBA,
            GL_FLOAT,
            nullptr);
      break;

    case TextureFormat::Depth:
      glTexImage2D(
            targets[i],
            0,
            GL_DEPTH_COMPONENT,
            m_size.width,
            m_size.height,
            0,
            GL_DEPTH_COMPONENT,
            GL_UNSIGNED_BYTE,
            nullptr);
      break;

    default:
      ASSERT(false, "Texture format not supported!");
      break;
    }
  }

  if (generateMipmap)
  {
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, toGLFilter(sampler.minFilter));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, toGLFilter(sampler.magFilter));

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, toGLWrapMode(sampler.wrapModeS));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, toGLWrapMode(sampler.wrapModeT));
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, toGLWrapMode(sampler.wrapModeR));
}

OpenGLTextureCube::~OpenGLTextureCube()
{
  glDeleteTextures(1, &m_id);
}

SizeI OpenGLTextureCube::getSize() const
{
  return m_size;
}

TextureFormat OpenGLTextureCube::getFormat() const
{
  return m_format;
}

TextureTarget OpenGLTextureCube::getTarget() const
{
  return TextureTarget::TargetCube;
}

unsigned int OpenGLTextureCube::getId() const
{
  return m_id;
}

void OpenGLTextureCube::bind()
{
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
}
