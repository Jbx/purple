#pragma once

#include "render/framebuffer_interface.h"
#include "render/opengl/opengl_context_interface.h"

// STL
#include <vector>

class IOpenGLFramebuffer : public IFramebuffer
{
public:
  virtual unsigned int getId() const = 0;
};

class DefaultOpenGLFramebuffer final : public IOpenGLFramebuffer
{
public:
  DefaultOpenGLFramebuffer(
      IOpenGLContext& context);

  bool isDefaultFramebuffer() const override;

  SizeI getSize() const override;
  std::vector<AttachmentPoint> getAttachments() const override;

  unsigned int getId() const override;

private:
  IOpenGLContext& m_context;
};

class OpenGLFramebuffer final : public IOpenGLFramebuffer
{
public:
  OpenGLFramebuffer(
      IOpenGLContext& context,
      std::vector<Attachment> const& attachments,
      SizeI const& size);

  ~OpenGLFramebuffer() override;

  bool isDefaultFramebuffer() const override;

  SizeI getSize() const override;
  std::vector<AttachmentPoint> getAttachments() const override;

  unsigned int getId() const override;

private:
  IOpenGLContext& m_context;
  SizeI m_size;
  std::vector<AttachmentPoint> m_attachmentPoints;
  unsigned int m_FBO;
};
