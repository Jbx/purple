#pragma once

#include "render/uniform_buffer_interface.h"
#include "render/texture_container.h"
#include "opengl_context_interface.h"

// STL
#include <map>

class OpenGLPipeline;

class OpenGLUniformBuffer final : public IUniformBuffer
{
public:
  OpenGLUniformBuffer(
      IOpenGLContext& context,
      std::vector<NamedPropertyBuffer> const& properties,
      TextureContainer const& textures);

  ~OpenGLUniformBuffer() override;

  void update(
      IPipeline const& pipeline,
      std::vector<NamedPropertyBuffer> const& properties) override;

  void bind(IPipeline const& pipeline) override;

private:
  friend class OpenGLPipeline;
  IOpenGLContext& m_context;
  TextureContainer m_textures;
  std::map<std::string, unsigned int> m_UBOs;
};

