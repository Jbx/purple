#include "opengl_pipeline.h"
#include "opengl_converter.h"
#include "utils/log_manager.h"

unsigned int createShader(std::vector<unsigned int> const& source, GLenum shaderType)
{
  unsigned int shaderId = glCreateShader(shaderType);
  std::string s(source.begin(), source.end());
  const char *c_str = s.c_str();

  glShaderSource(shaderId, 1, &c_str, nullptr);
  glCompileShader(shaderId);

  int success;
  glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    int logSize;
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);

    if (logSize > 0)
    {
      std::vector<char> infoLog(static_cast<std::size_t>(logSize));
      glGetShaderInfoLog(shaderId, logSize, nullptr, infoLog.data());
      std::string errorMessages(infoLog.begin(), infoLog.end());
      LOG_ERROR("Error building shader program: ", errorMessages);
    }
  }
  return shaderId;
}

ShaderProgram::ShaderVersion fromOpenGLVersion(OpenGLVersion version)
{
  return version.es ? ShaderProgram::GLSL_300_ES : ShaderProgram::GLSL_330;
}

OpenGLPipeline::OpenGLPipeline(
    IOpenGLContext& context,
    Shader const& shader,
    RenderStates const& renderStates,
    unsigned int uniformBufferCount,
    TextureContainer const& textures)
  : m_context(context)
  , m_renderStates(renderStates)
{
  unsigned int vertexShaderId = createShader(
        shader.getProgram().getVertexShaderSourceCode(fromOpenGLVersion(m_context.getOpenGLVersion())),
        GL_VERTEX_SHADER);

  unsigned int fragmentShaderId = createShader(
        shader.getProgram().getFragmentShaderSourceCode(fromOpenGLVersion(m_context.getOpenGLVersion())),
        GL_FRAGMENT_SHADER);

  if (vertexShaderId > 0 && fragmentShaderId > 0)
  {
    m_shaderProgramId = glCreateProgram();
    glAttachShader(m_shaderProgramId, vertexShaderId);
    glAttachShader(m_shaderProgramId, fragmentShaderId);

    glLinkProgram(m_shaderProgramId);

    // check for linking errors
    int success;
    glGetProgramiv(m_shaderProgramId, GL_LINK_STATUS, &success);
    if (!success)
    {
      int logSize;
      glGetProgramiv(m_shaderProgramId, GL_INFO_LOG_LENGTH, &logSize);

      if (logSize > 0)
      {
        std::vector<char> infoLog(static_cast<std::size_t>(logSize));
        glGetProgramInfoLog(m_shaderProgramId, logSize, nullptr, infoLog.data());
        std::string errorMessages = std::string(infoLog.begin(), infoLog.end());
        LOG_ERROR("Error linking shader program: " + errorMessages);
      }
    }

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    if (success)
    {
      glUseProgram(m_shaderProgramId);

      for (int uniformBlockIndex = 0; uniformBlockIndex < uniformBufferCount; ++uniformBlockIndex)
      {
        glUniformBlockBinding(m_shaderProgramId, uniformBlockIndex, uniformBlockIndex);
      }

      int samplerIndex = 0;
      for (auto const& texture : textures)
      {
        int location = glGetUniformLocation(m_shaderProgramId, texture.first.c_str());
        ASSERT(location >= 0, "Location for ", texture.first, " not found");
        glUniform1i(location, samplerIndex++);
      }
    }
  }
}

OpenGLPipeline::~OpenGLPipeline()
{
  glUseProgram(0);
  glDeleteProgram(m_shaderProgramId);
}

void OpenGLPipeline::bind(IRenderPass const& renderPass)
{
  glUseProgram(m_shaderProgramId);

#ifndef PURPLE_SUPPORT_WASM
  glPolygonMode(GL_FRONT_AND_BACK, toGLPolygonMode(m_renderStates.polygonMode));
#endif // PURPLE_SUPPORT_WASM
  glLineWidth(m_renderStates.lineWidth);
  if (m_renderStates.cullMode != CullMode::None)
  {
    glCullFace(toGLCullMode(m_renderStates.cullMode));
  }
  m_renderStates.cullMode == CullMode::None ? glDisable(GL_CULL_FACE) : glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  m_renderStates.depthTestEnabled ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
  glDepthMask(m_renderStates.depthWriteEnabled);

//#ifndef PURPLE_SUPPORT_WASM
//  glColorMaski(
//      static_cast<IOpenGLRenderPass const &>(renderPass).getFramebufferId(),
//      m_renderStates.colorWriteEnabled,
//      m_renderStates.colorWriteEnabled,
//      m_renderStates.colorWriteEnabled,
//      m_renderStates.colorWriteEnabled);
//#endif // PURPLE_SUPPORT_WASM

  glDepthFunc(toGLDepthCompareOperation(m_renderStates.depthCompareOperation));
  m_renderStates.blendingEnabled ? glEnable(GL_BLEND) : glDisable(GL_BLEND);
  glBlendFuncSeparate(
      toGLBlendFactor(m_renderStates.srcColorBlendFactor),
      toGLBlendFactor(m_renderStates.dstColorBlendFactor),
      toGLBlendFactor(m_renderStates.srcAlphaBlendFactor),
      toGLBlendFactor(m_renderStates.dstAlphaBlendFactor));
  glBlendEquationSeparate(
      toGLBlendOperation(m_renderStates.colorBlendOperation),
      toGLBlendOperation(m_renderStates.alphaBlendOperation));

  m_renderStates.depthBiasEnabled &&m_renderStates.polygonMode == PolygonMode::Fill ?
    glEnable(GL_POLYGON_OFFSET_FILL) : glDisable(GL_POLYGON_OFFSET_FILL);

#ifndef PURPLE_SUPPORT_WASM
  m_renderStates.depthBiasEnabled &&m_renderStates.polygonMode == PolygonMode::Line ?
    glEnable(GL_POLYGON_OFFSET_LINE) : glDisable(GL_POLYGON_OFFSET_LINE);

  m_renderStates.depthBiasEnabled &&m_renderStates.polygonMode == PolygonMode::Point ?
    glEnable(GL_POLYGON_OFFSET_POINT) : glDisable(GL_POLYGON_OFFSET_POINT);
#endif // PURPLE_SUPPORT_WASM

  glPolygonOffset(m_renderStates.depthBiasSlopeFactor, m_renderStates.depthBiasConstantFactor);
}
