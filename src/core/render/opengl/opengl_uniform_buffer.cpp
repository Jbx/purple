#include "opengl_uniform_buffer.h"
#include "opengl_pipeline.h"
#include "opengl_texture.h"
#include "opengl.h"

OpenGLUniformBuffer::OpenGLUniformBuffer(
    IOpenGLContext& context,
    std::vector<NamedPropertyBuffer> const& properties,
    TextureContainer const& textures)
  : m_context(context)
  , m_textures(textures)
{
  int uniformBufferIndex = 0;
  for (auto const& property : properties)
  {
    unsigned int& ubo = m_UBOs[property.name];
    glGenBuffers(1, &ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo);
    glBufferData(GL_UNIFORM_BUFFER, property.buffer.size(), nullptr, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, property.buffer.size(), property.buffer.data());
    glBindBufferBase(GL_UNIFORM_BUFFER, uniformBufferIndex, ubo);
    uniformBufferIndex++;
  }
}

OpenGLUniformBuffer::~OpenGLUniformBuffer()
{
  for (auto const& ubo : m_UBOs)
  {
    glDeleteBuffers(1, &ubo.second);
  }
}

void OpenGLUniformBuffer::update(
    IPipeline const& pipeline,
    std::vector<NamedPropertyBuffer> const& properties)
{
  for (auto const& property : properties)
  {
    auto const& ubo = m_UBOs[property.name];
    int uniformBufferIndex =
        glGetUniformBlockIndex(
          static_cast<OpenGLPipeline const&>(pipeline).m_shaderProgramId,
          property.name.c_str());

    glBindBuffer(GL_UNIFORM_BUFFER, ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, property.buffer.size(), property.buffer.data());
    glBindBufferRange(GL_UNIFORM_BUFFER, uniformBufferIndex, ubo, 0, property.buffer.size());
  }
}

void OpenGLUniformBuffer::bind(IPipeline const& pipeline)
{
  int textureIndex = 0;
  for (auto const& texture : m_textures)
  {
    auto openglTexture = std::static_pointer_cast<IOpenGLTexture>(texture.second);
    glActiveTexture(GL_TEXTURE0 + textureIndex);
    openglTexture->bind();

    textureIndex++;
  }
}

