#pragma once

#include "render/shader.h"
#include "render/pipeline_interface.h"
#include "render/render_states.h"
#include "opengl_context_interface.h"
#include "render/texture_container.h"

class OpenGLPipeline final : public IPipeline
{
public:
  OpenGLPipeline(
      IOpenGLContext& context,
      Shader const& shader,
      RenderStates const& renderStates,
      unsigned int uniformBufferCount,
      TextureContainer const& textures);

  ~OpenGLPipeline() override;

  void bind(IRenderPass const& renderPass) override;

private:
  friend class OpenGLVertexBuffer;
  friend class OpenGLIndexedVertexBuffer;
  friend class OpenGLUniformBuffer;
  IOpenGLContext& m_context;
  RenderStates m_renderStates;
  unsigned int m_shaderProgramId;
};
