#include "opengl_vertex_buffer.h"
#include "opengl_converter.h"
#include "render/opengl/opengl_pipeline.h"

OpenGLVertexBuffer::~OpenGLVertexBuffer()
{  
  glDeleteVertexArrays(1, &m_VAO);
  glDeleteBuffers(1, &m_VBO);
}

void OpenGLVertexBuffer::bind()
{
  glBindVertexArray(m_VAO);
}

void OpenGLVertexBuffer::draw(IPipeline const& pipeline)
{
  auto primitiveTopology = static_cast<OpenGLPipeline const&>(pipeline).m_renderStates.primitiveTopology;
  glDrawArrays(toGLPrimitiveTopology(primitiveTopology), 0, m_elementCount);
}

OpenGLIndexedVertexBuffer::~OpenGLIndexedVertexBuffer()
{
  glDeleteVertexArrays(1, &m_VAO);
  glDeleteBuffers(1, &m_VBO);
  glDeleteBuffers(1, &m_IBO);
}

void OpenGLIndexedVertexBuffer::bind()
{
  glBindVertexArray(m_VAO);
}

void OpenGLIndexedVertexBuffer::draw(IPipeline const& pipeline)
{
  auto primitiveTopology = static_cast<OpenGLPipeline const&>(pipeline).m_renderStates.primitiveTopology;
  glDrawElements(toGLPrimitiveTopology(primitiveTopology), m_elementCount, GL_UNSIGNED_INT, nullptr);
}
