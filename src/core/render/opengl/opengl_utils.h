#pragma once

#include "opengl.h"

// STL
#include <string>
#include <set>
#include <sstream>
#include <iostream>

bool hasGLExtension(std::string const& extension)
{
  GLint extensionCount = 0;
  glGetIntegerv(GL_NUM_EXTENSIONS, &extensionCount);

  std::set<std::string> extensions;
  for (int i = 0; i < extensionCount; ++i)
  {
      extensions.insert((const char*)glGetStringi(GL_EXTENSIONS, i));
  }

  return extensions.find(extension) != extensions.end();
}
