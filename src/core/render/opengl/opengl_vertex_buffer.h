#pragma once

#include "render/vertex_buffer_interface.h"
#include "render/attribute.h"
#include "opengl_context_interface.h"
#include "opengl.h"
#include "utils/ref.h"
#include "utils/buffer_view.h"


class OpenGLVertexBuffer final : public IVertexBuffer
{
public:
  template<typename VertexType>
  static Scoped<OpenGLVertexBuffer> create(
      IOpenGLContext& context,
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices);

  template<typename VertexType>
  OpenGLVertexBuffer(
      IOpenGLContext& context,
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices);

  ~OpenGLVertexBuffer() override;

  void bind() override;
  void draw(IPipeline const& pipeline) override;

private:
  IOpenGLContext& m_context;
  unsigned int m_elementCount;
  unsigned int m_VBO;
  unsigned int m_VAO;
};

class OpenGLIndexedVertexBuffer final : public IVertexBuffer
{
public:
  template<typename VertexType, typename IndexType>
  static Scoped<OpenGLIndexedVertexBuffer> create(
      IOpenGLContext& context,
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices);

  template<typename VertexType, typename IndexType>
  OpenGLIndexedVertexBuffer(
      IOpenGLContext& context,
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices);

  ~OpenGLIndexedVertexBuffer() override;

  void bind() override;
  void draw(IPipeline const& pipeline) override;

private:
  IOpenGLContext& m_context;
  unsigned int m_elementCount;
  unsigned int m_VBO;
  unsigned int m_IBO;
  unsigned int m_VAO;
};

template<typename VertexType>
Scoped<OpenGLVertexBuffer> OpenGLVertexBuffer::create(
    IOpenGLContext& context,
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices)
{
  return createScoped<OpenGLVertexBuffer>(
        context, vertices);
}

template<typename VertexType>
OpenGLVertexBuffer::OpenGLVertexBuffer(
    IOpenGLContext& context,
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices)
  : m_context(context)
  , m_elementCount((vertices.size() * sizeof(VertexType)) / stride(layout))
{
  glGenVertexArrays(1, &m_VAO);  
  glGenBuffers(1, &m_VBO);

  glBindVertexArray(m_VAO);
  glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
  glBufferData(
        GL_ARRAY_BUFFER,
        static_cast<GLsizeiptr>(vertices.size() * sizeof(VertexType)),
        vertices.data(),
        GL_STATIC_DRAW);

  unsigned int location = 0;

  auto it = layout.begin();
  for (;it != layout.end(); ++it)
  {
    size_t offset = offsetOf(layout, it);

    glEnableVertexAttribArray(location);

    glVertexAttribPointer(
          location,
          count(*it),
          GL_FLOAT,
          GL_FALSE,
          stride(layout),
          reinterpret_cast<void*>(offset));

    location++;
  }
}

template<typename VertexType, typename IndexType>
Scoped<OpenGLIndexedVertexBuffer> OpenGLIndexedVertexBuffer::create(
    IOpenGLContext& context,
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices,
    BufferView<IndexType> const& indices)
{
  return createScoped<OpenGLIndexedVertexBuffer>(
        context, vertices, indices);
}

template<typename VertexType, typename IndexType>
OpenGLIndexedVertexBuffer::OpenGLIndexedVertexBuffer(
    IOpenGLContext& context,
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices,
    BufferView<IndexType> const& indices)
  : m_context(context)
  , m_elementCount(static_cast<uint32_t>(indices.size()))
{
  glGenVertexArrays(1, &m_VAO);
  glBindVertexArray(m_VAO);

  glGenBuffers(1, &m_VBO);
  glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
  glBufferData(
        GL_ARRAY_BUFFER,
        static_cast<GLsizeiptr>(vertices.size() * sizeof(VertexType)),
        vertices.data(),
        GL_STATIC_DRAW);

  unsigned int location = 0;

  auto it = layout.begin();
  for (;it != layout.end(); ++it)
  {
    size_t offset = offsetOf(layout, it);

    glEnableVertexAttribArray(location);

    glVertexAttribPointer(
          location,
          count(*it),
          GL_FLOAT,
          GL_FALSE,
          stride(layout),
          reinterpret_cast<void*>(offset));

    location++;
  }

  glGenBuffers(1, &m_IBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
  glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        static_cast<GLsizeiptr>(indices.size() * sizeof(IndexType)),
        indices.data(),
        GL_STATIC_DRAW);
}
