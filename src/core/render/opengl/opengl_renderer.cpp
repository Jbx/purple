#include "opengl_renderer.h"
#include "opengl_uniform_buffer.h"
#include "opengl_pipeline.h"
#include "opengl_texture.h"
#include "opengl_framebuffer.h"

OpenGLRenderer::OpenGLRenderer(IOpenGLContext& context)
  : m_context(context)
  , m_defaultRenderPass(context)
  , m_defaultFramebuffer(context)
{

}

Scoped<IUniformBuffer> OpenGLRenderer::createUniformBuffer(
    std::vector<NamedPropertyBuffer> const& properties,
    TextureContainer const& textures)
{
  return createScoped<OpenGLUniformBuffer>(
        m_context,
        properties,
        textures);
}

Scoped<ITexture> OpenGLRenderer::createTexture2D(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<OpenGLTexture>(
        m_context,
        image,
        generateMipmap,
        sampler);
}

Scoped<ITexture> OpenGLRenderer::createTexture2D(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<OpenGLTexture>(
        m_context,
        format,
        size,
        generateMipmap,
        sampler);
}

Scoped<ITexture> OpenGLRenderer::createTextureCube(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<OpenGLTextureCube>(
        m_context,
        image,
        generateMipmap,
        sampler);
}

Scoped<ITexture> OpenGLRenderer::createTextureCube(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return createScoped<OpenGLTextureCube>(
        m_context,
        format,
        size,
        generateMipmap,
        sampler);
}

Scoped<IPipeline> OpenGLRenderer::createPipeline(
    IRenderPass const& renderPass,
    Shader const& shader,
    RenderStates const& renderStates,
    unsigned int uniformBufferCount,
    TextureContainer const& textures)
{
  return createScoped<OpenGLPipeline>(
        m_context,
        shader,
        renderStates,
        uniformBufferCount,
        textures);
}

Scoped<IFramebuffer> OpenGLRenderer::createFramebuffer(
    IRenderPass const& renderPass,
    std::vector<Attachment> const& attachments,
    SizeI const& size)
{
  return createScoped<OpenGLFramebuffer>(
        m_context,
        attachments,
        size);
}

IRenderPass& OpenGLRenderer::getDefaultRenderPass()
{
  return m_defaultRenderPass;
}

IFramebuffer& OpenGLRenderer::getDefaultFramebuffer()
{
  return m_defaultFramebuffer;
}

Scoped<IRenderPass> OpenGLRenderer::createRenderPass(
    std::vector<AttachmentFormat> const& attachments,
    int sampleCount)
{
  return createScoped<OpenGLRenderPass>(
        m_context,
        attachments,
        sampleCount);
}

