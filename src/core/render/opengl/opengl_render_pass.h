#pragma once

#include "render/texture_interface.h"
#include "opengl_context_interface.h"
#include "render/render_pass_interface.h"

// STL
#include <vector>

class DefaultOpenGLRenderPass final : public IRenderPass
{
public:
  DefaultOpenGLRenderPass(
      IOpenGLContext& context);

  bool isDefaultRenderPass() const override;

  std::vector<AttachmentPoint> getAttachments() const override;
  int getSampleCount() const override;

  void setClearColor(Color const& clearColor) override;
  void setClearDepthValue(float clearDepthValue) override;
  void setClearStencilValue(unsigned int clearStencilValue) override;

  void begin(
      IFramebuffer const& framebuffer,
      RectI const& viewport) override;

  void end() override;

private:
  IOpenGLContext& m_context;
  Color m_clearColor {0.f, 0.f, 0.f, 1.f};
  float m_clearDepthValue = 1.f;
  unsigned int m_clearStencilValue = 0;
};

class OpenGLRenderPass final : public IRenderPass
{
public:
  OpenGLRenderPass(
      IOpenGLContext& context,
      std::vector<AttachmentFormat> const& attachments,
      int sampleCount);

  bool isDefaultRenderPass() const override;

  std::vector<AttachmentPoint> getAttachments() const override;
  int getSampleCount() const override;

  void setClearColor(Color const& clearColor) override;
  void setClearDepthValue(float clearDepthValue) override;
  void setClearStencilValue(unsigned int clearStencilValue) override;

  void begin(
      IFramebuffer const& framebuffer,
      RectI const& viewport) override;

  void end() override;

private:
  IOpenGLContext& m_context;
  std::vector<AttachmentPoint> m_attachmentPoints;
  int m_sampleCount;
  Color m_clearColor {0.f, 0.f, 0.f, 1.f};
  float m_clearDepthValue = 1.f;
  unsigned int m_clearStencilValue = 0.f;
};
