#include "opengl_render_pass.h"
#include "opengl_framebuffer.h"
#include "opengl.h"

// STL
#include <algorithm>

DefaultOpenGLRenderPass::DefaultOpenGLRenderPass(
    IOpenGLContext& context)
  : m_context(context)
{

}

bool DefaultOpenGLRenderPass::isDefaultRenderPass() const
{
  return true;
}

std::vector<AttachmentPoint> DefaultOpenGLRenderPass::getAttachments() const
{
  return {AttachmentPoint::Color, AttachmentPoint::Depth};
}

int DefaultOpenGLRenderPass::getSampleCount() const
{
  return m_context.getSampleCount();
}

void DefaultOpenGLRenderPass::setClearColor(Color const& clearColor)
{
  m_clearColor = clearColor;
}

void DefaultOpenGLRenderPass::setClearDepthValue(float clearDepthValue)
{
  m_clearDepthValue = clearDepthValue;
}

void DefaultOpenGLRenderPass::setClearStencilValue(unsigned int clearStencilValue)
{
  m_clearStencilValue = clearStencilValue;
}

void DefaultOpenGLRenderPass::begin(
    IFramebuffer const& framebuffer,
    RectI const& viewport)
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
  glClearColor(m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a);
  glClearDepthf(m_clearDepthValue);
  glClearStencil(m_clearStencilValue);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void DefaultOpenGLRenderPass::end()
{

}

OpenGLRenderPass::OpenGLRenderPass(
    IOpenGLContext& context,
    std::vector<AttachmentFormat> const& attachments,
    int sampleCount)
  : m_context(context)
  , m_sampleCount(sampleCount)
{
  std::transform(
        attachments.begin(),
        attachments.end(),
        std::back_inserter(m_attachmentPoints),
        [](auto const& attachment) { return attachment.first; });
}

bool OpenGLRenderPass::isDefaultRenderPass() const
{
  return false;
}

std::vector<AttachmentPoint> OpenGLRenderPass::getAttachments() const
{
  return m_attachmentPoints;
}

int OpenGLRenderPass::getSampleCount() const
{
  return m_sampleCount;
}

void OpenGLRenderPass::setClearColor(Color const& clearColor)
{
  m_clearColor = clearColor;
}

void OpenGLRenderPass::setClearDepthValue(float clearDepthValue)
{
  m_clearDepthValue = clearDepthValue;
}

void OpenGLRenderPass::setClearStencilValue(unsigned int clearStencilValue)
{
  m_clearStencilValue = clearStencilValue;
}

void OpenGLRenderPass::begin(
    IFramebuffer const& framebuffer,
    RectI const& viewport)
{
  glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
  glBindFramebuffer(GL_FRAMEBUFFER, static_cast<IOpenGLFramebuffer const &>(framebuffer).getId());
  glClearColor(m_clearColor.r, m_clearColor.g, m_clearColor.b, m_clearColor.a);
  glClearDepthf(m_clearDepthValue);
  glClearStencil(m_clearStencilValue);

  bool hasColorAttachment =
      std::find(
        m_attachmentPoints.begin(),
        m_attachmentPoints.end(), AttachmentPoint::Color) != m_attachmentPoints.end();

  bool hasDepthAttachment =
      std::find(
        m_attachmentPoints.begin(),
        m_attachmentPoints.end(), AttachmentPoint::Depth) != m_attachmentPoints.end();

  GLbitfield mask = 0;
  if (hasColorAttachment) mask |= GL_COLOR_BUFFER_BIT;
  if (hasDepthAttachment) mask |= GL_DEPTH_BUFFER_BIT;

  glClear(mask);
}

void OpenGLRenderPass::end()
{
}
