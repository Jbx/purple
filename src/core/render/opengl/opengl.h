#pragma once

#if defined PURPLE_SUPPORT_WASM
  #include <GLES3/gl3.h>

  static void initOpenGL() {}

#else
  #include <GL/glew.h>

  static void initOpenGL()
  {
    // init GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_FRAMEBUFFER_SRGB_EXT);
  }

#endif
