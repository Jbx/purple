#pragma once

#include "engine.h"
#include "utils/assert.h"

const int MAX_LIGHTS = 8;

namespace UBO {

struct Light
{
  alignas(4) int type;
  alignas(16) glm::vec3 position;
  alignas(16) glm::vec3 color;
  alignas(4) float intensity;
  alignas(16) glm::vec3 direction;
  alignas(4) float constantAttenuation;
  alignas(4) float linearAttenuation;
  alignas(4) float quadraticAttenuation;
  alignas(4) float cutOffAngle;
};

struct Common
{
  alignas(16) glm::mat4 model;
  alignas(16) glm::mat4 modelNormal;
  alignas(16) glm::mat4 view;
  alignas(16) glm::mat4 projection;
  alignas(16) glm::mat4 viewProjection;
  alignas(16) glm::mat4 modelView;
  alignas(16) glm::mat4 modelViewNormal;
  alignas(16) glm::mat4 mvp;
  alignas(16) glm::vec3 viewPosition;
  alignas(16) glm::vec3 viewCenter;
  alignas(16) Light lights[MAX_LIGHTS];
  alignas(4) int lightCount;
};

const std::string CommonUBOName = "CommonUBO";
const std::string MaterialUBOName = "MaterialUBO";

}

template<typename DataType>
PropertyBuffer toBuffer(DataType const& data)
{
  auto const ptr = reinterpret_cast<const char*>(&data);
  return PropertyBuffer(ptr, ptr + sizeof(data));
}

template<typename RendererImpl>
Engine<RendererImpl>::Engine(Renderer<RendererImpl>& renderer)
  : m_renderer(renderer)
  , m_renderPassCache(renderer)
  , m_pipelineCache(renderer)
{
}

template<typename RendererImpl>
void Engine<RendererImpl>::draw()
{
  for (auto const& [view, framebuffer] : m_offscreenFramebuffers)
  {
    draw(*view,
         *framebuffer,
         m_renderPassCache.get(
           RenderPassKey(view->getAttachments(),
                         view->getSampleCount())));
  }

  draw(*m_screenView,
       m_renderer.getDefaultFramebuffer(),
       m_renderer.getDefaultRenderPass());
}

template<typename RendererImpl>
void Engine<RendererImpl>::draw(std::vector<WeakRef<View>> const& views)
{
  for (auto const& view : views)
  {
    if (view == m_screenView)
    {
      draw(*m_screenView,
           m_renderer.getDefaultFramebuffer(),
           m_renderer.getDefaultRenderPass());
    }
    else
    {
      draw(*view,
           *m_offscreenFramebuffers[view],
           m_renderPassCache.get(
             RenderPassKey(view->getAttachments(),
                           view->getSampleCount())));
    }
  }
}

template<typename RendererImpl>
WeakRef<Scene> Engine<RendererImpl>::createScene()
{
  return m_scenes.emplace_back(Scene::create());
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeScene(WeakRef<Scene> const& scene)
{
  std::erase(m_scenes, scene);
}

template<typename RendererImpl>
WeakRef<View> Engine<RendererImpl>::createOffscreenView(
    SizeI const& size,
    std::vector<Attachment> const& attachments,
    int sampleCount)
{
  ASSERT(!attachments.empty(), "At least one attachment is required!");

  ASSERT(
        std::all_of(
          attachments.begin(),
          attachments.end(),
          [&size](auto const& attachment) {
    return
        attachment.texture->getSize().width >= size.width &&
        attachment.texture->getSize().height >= size.height; }),
        "All attachments must have a size greater or equal to the framebuffer size!");

  auto view = View::create({{0, 0}, size}, attachments, sampleCount);

  auto const& renderPass =
      m_renderPassCache.get(RenderPassKey(attachments, sampleCount));

  auto framebuffer = m_renderer.createFramebuffer(
        renderPass,
        attachments,
        size);

  m_offscreenFramebuffers.insert({view, std::move(framebuffer)});
  return view;
}

template<typename RendererImpl>
WeakRef<View> Engine<RendererImpl>::getScreenView() const
{
  return m_screenView;
}

template<typename RendererImpl>
std::vector<WeakRef<View>> Engine<RendererImpl>::getOffscreenViews() const
{
  std::vector<WeakRef<View>> views;
  std::transform(
        m_offscreenFramebuffers.begin(),
        m_offscreenFramebuffers.end(),
        std::back_inserter(views),
        [](auto const& renderPass) { return renderPass.first; });
  return views;
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeView(WeakRef<View> const& view)
{
  m_offscreenFramebuffers.erase(view);
}

template<typename RendererImpl>
template<typename VertexType>
WeakRef<Geometry> Engine<RendererImpl>::createGeometry(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices)
{
  ASSERT(!vertices.empty(), "No vertices!");

  auto geometry = Geometry::create(layout);
  m_geometries.insert({geometry, m_renderer.createVertexBuffer(layout, vertices)});
  return geometry;
}

template<typename RendererImpl>
template<typename VertexType>
WeakRef<Geometry> Engine<RendererImpl>::createGeometry(
    AttributeLayout const& layout,
    std::vector<VertexType> const& vertices)
{
  return createGeometry(layout, BufferView(vertices));
}

template<typename RendererImpl>
template<typename VertexType, typename IndexType>
WeakRef<Geometry> Engine<RendererImpl>::createGeometry(
    AttributeLayout const& layout,
    BufferView<VertexType> const& vertices,
    BufferView<IndexType> const& indices)
{
  ASSERT(!vertices.empty(), "No vertices!");
  ASSERT(!indices.empty(), "No indices!");

  auto geometry = Geometry::create(layout);
  m_geometries.insert({geometry, m_renderer.createVertexBuffer(layout, vertices, indices)});
  return geometry;
}

template<typename RendererImpl>
template<typename VertexType, typename IndexType>
WeakRef<Geometry> Engine<RendererImpl>::createGeometry(
    AttributeLayout const& layout,
    std::vector<VertexType> const& vertices,
    std::vector<IndexType> const& indices)
{
  return createGeometry(layout, BufferView(vertices), BufferView(indices));
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeGeometry(WeakRef<Geometry> const& geometry)
{
  m_geometries.erase(geometry);
}

template<typename RendererImpl>
WeakRef<ITexture> Engine<RendererImpl>::createTexture2D(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return m_textures.emplace_back(m_renderer.createTexture2D(image, generateMipmap, sampler));
}

template<typename RendererImpl>
WeakRef<ITexture> Engine<RendererImpl>::createTexture2D(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return m_textures.emplace_back(m_renderer.createTexture2D(format, size, generateMipmap, sampler));
}

template<typename RendererImpl>
WeakRef<ITexture> Engine<RendererImpl>::createTextureCube(
    IImage const& image,
    bool generateMipmap,
    Sampler const& sampler)
{
  return m_textures.emplace_back(m_renderer.createTextureCube(image, generateMipmap, sampler));
}

template<typename RendererImpl>
WeakRef<ITexture> Engine<RendererImpl>::createTextureCube(
    TextureFormat format,
    SizeI const& size,
    bool generateMipmap,
    Sampler const& sampler)
{
  return m_textures.emplace_back(m_renderer.createTextureCube(format, size, generateMipmap, sampler));
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeTexture(WeakRef<ITexture> const& texture)
{
  std::erase(m_textures, texture);
}

template<typename RendererImpl>
WeakRef<Material> Engine<RendererImpl>::createMaterial(
    Shader const& shader,
    MaterialDefinition const& definition)
{
  auto material = Material::create(
        shader,
        definition);
  addMaterial(material);
  return material;
}

template<typename RendererImpl>
template <InheritMaterial MaterialT, typename ...Args>
WeakRef<MaterialT> Engine<RendererImpl>::createMaterial(Args&& ...args)
{
  auto material = createRef<MaterialT>(std::forward<Args>(args)...);
  addMaterial(material);
  return material;
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeMaterial(WeakRef<Material> const& material)
{
  std::erase(m_materials, material);
}

template<typename RendererImpl>
WeakRef<Entity> Engine<RendererImpl>::createEntity(
    WeakRef<Geometry> const& geometry,
    WeakRef<MaterialInstance> const& material)
{
  ASSERT(geometry->getLayout() == material->getMaterial().getShader().getAttributeLayout(),
         "Incompatibles attribute layouts!");

  auto entity = Entity::create(geometry, material);

  std::vector<NamedPropertyBuffer> properties;
  if (material->m_material.useCommonUniforms())
  {
    properties.emplace_back(
          NamedPropertyBuffer{UBO::CommonUBOName, toBuffer(UBO::Common{})});
  }

  if (!material->getProperties().isEmpty())
  {
    properties.emplace_back(
          NamedPropertyBuffer{UBO::MaterialUBOName, material->getProperties().asAlignedBuffer()});
  }

  m_uniformBuffers.insert(
        {entity, m_renderer.createUniformBuffer(
         properties, material->getTextures())});

  return m_entities.emplace_back(entity);
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeEntity(WeakRef<Entity> const& entity)
{
  for (auto const& scene : m_scenes)
  {
    scene->removeEntity(entity);
  }

  m_uniformBuffers.erase(entity);
}

template<typename RendererImpl>
WeakRef<Light> Engine<RendererImpl>::createLight(Light::Type type)
{
  return m_lights.emplace_back(Light::create(type));
}

template<typename RendererImpl>
void Engine<RendererImpl>::removeLight(WeakRef<Light> const& light)
{
  for (auto const& scene : m_scenes)
  {
    scene->removeLight(light);
  }
  std::erase(m_lights, light);
}

template<typename RendererImpl>
void Engine<RendererImpl>::draw(
    View const& view,
    IFramebuffer const& framebuffer,
    IRenderPass& renderPass)
{
  renderPass.setClearColor(view.getClearColor());
  renderPass.begin(
        framebuffer,
        view.getViewport().value_or(RectI{{0, 0}, framebuffer.getSize()}));

  if (auto const& scene = view.getScene())
  {
    // Set common properties to UBO
    auto const& camera = view.getCamera();

    UBO::Common commonUBO;
    commonUBO.view = camera.getViewMatrix();

    commonUBO.projection = camera.getProjectionMatrix();
    auto viewProjectionMatrix = camera.getProjectionMatrix() * camera.getViewMatrix();
    commonUBO.viewProjection = viewProjectionMatrix;
    commonUBO.viewPosition = camera.getPosition();
    commonUBO.viewCenter = camera.getCenter();

    commonUBO.lightCount = std::min(static_cast<int>(scene->m_lights.size()), MAX_LIGHTS);
    for (int i = 0; i < commonUBO.lightCount; ++i)
    {
      auto const& light = scene->m_lights[i];
      if (light->isEnabled())
      {
        commonUBO.lights[i].type = light->getType();
        commonUBO.lights[i].position = light->getPosition();
        commonUBO.lights[i].color = glm::vec3(light->getColor().r, light->getColor().g, light->getColor().b);
        commonUBO.lights[i].intensity = light->getIntensity();
        commonUBO.lights[i].direction = light->getDirection();
        commonUBO.lights[i].constantAttenuation = light->getConstantAttenuation();
        commonUBO.lights[i].linearAttenuation = light->getLinearAttenuation();
        commonUBO.lights[i].quadraticAttenuation = light->getQuadraticAttenuation();
        commonUBO.lights[i].cutOffAngle = light->getCutOffAngle();
      }
    }

    auto renderEntities = [&](auto const& material)
    {
      // Get entities using this material
      std::vector<Ref<Entity>> materialEntities;

      std::copy_if(
            scene->m_entities.begin(),
            scene->m_entities.end(),
            std::back_inserter(materialEntities),
            [&material](auto const& entity) { return &entity->m_materialInstance->m_material == material.get(); });

      if (!materialEntities.empty())
      {
        auto& pipeline = m_pipelineCache.get({renderPass, material});
        pipeline.bind(renderPass);

        // Iterate over instance of materials
        for (auto const& materialInstance : material->m_instances)
        {
          // Get entities using this material instance
          std::vector<Ref<Entity>> entities;

          std::copy_if(
                materialEntities.begin(),
                materialEntities.end(),
                std::back_inserter(entities),
                [&materialInstance](auto const& entity) { return entity->m_materialInstance == materialInstance; });

          if (!entities.empty())
          {
            // Retrieve geometries for each entity
            std::map<Ref<Geometry>, std::vector<Ref<Entity>>> geometries;
            for (auto& entity : entities)
            {
              geometries[entity->m_geometry].push_back(entity);
            }

            for (auto const& [geometry, entities] : geometries)
            {
              // Bind corresponding vertex buffer
              auto& vertexBuffer = m_geometries[geometry];
              vertexBuffer->bind();

              for (auto const& entity : entities)
              {
                // Get UBO for this material instance
                auto& uniformBuffer = m_uniformBuffers[entity];

                // Set entity specific properties to UBO
                std::vector<NamedPropertyBuffer> properties;
                if (material->useCommonUniforms())
                {
                  commonUBO.model = entity->getTransformation();
                  commonUBO.modelNormal = glm::inverseTranspose(entity->getTransformation());
                  auto modelViewMatrix = camera.getViewMatrix() * entity->getTransformation();
                  commonUBO.modelView = modelViewMatrix;
                  commonUBO.modelViewNormal = glm::inverseTranspose(modelViewMatrix);
                  commonUBO.mvp = viewProjectionMatrix * entity->getTransformation();

                  properties.emplace_back(
                        NamedPropertyBuffer{UBO::CommonUBOName, toBuffer(commonUBO)});
                }

                if (!entity->m_materialInstance->getProperties().isEmpty())
                {
                  properties.emplace_back(
                        NamedPropertyBuffer{UBO::MaterialUBOName, entity->m_materialInstance->getProperties().asAlignedBuffer()});
                }

                // Update & bind UBO
                uniformBuffer->update(pipeline, properties);
                uniformBuffer->bind(pipeline);

                // And finally draws
                vertexBuffer->draw(pipeline);
              }
            }
          }
        }
      }
    };

    // render opaque entities first
    const auto isOpaqueMaterial = [](auto const& material) { return material->getRenderStates().blendingEnabled == false; };

    for (auto const& material : m_materials)
    {
      if (isOpaqueMaterial(material))
      {
        renderEntities(material);
      }
    }

    // then render transparent entities
    const auto isTransparentMaterial = [](auto const& material) { return material->getRenderStates().blendingEnabled == true; };

    for (auto const& material : m_materials)
    {
      if (isTransparentMaterial(material))
      {
        renderEntities(material);
      }
    }
  }

  renderPass.end();
}

template<typename RendererImpl>
void Engine<RendererImpl>::addMaterial(Ref<Material> const& material)
{
  m_materials.push_back(material);
}
