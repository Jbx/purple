#include "scene.h"

// STL
#include <algorithm>

Ref<Scene> Scene::create()
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public Scene
  {
    EnableMakeShared() : Scene() {}
  };
  return createRef<EnableMakeShared>();
}

void Scene::addEntity(WeakRef<Entity> const& entity)
{
  m_entities.emplace_back(entity);
}

void Scene::removeEntity(WeakRef<Entity> const& entity)
{
  std::erase(m_entities, entity);
}

std::vector<WeakRef<Entity>> Scene::getEntities() const
{
  std::vector<WeakRef<Entity>> handles;
  std::transform(
        m_entities.begin(),
        m_entities.end(),
        std::back_inserter(handles),
        [](auto const& entity) { return entity; });
  return handles;
}

void Scene::addLight(WeakRef<Light> const& light)
{
  m_lights.emplace_back(light);
}

void Scene::removeLight(WeakRef<Light> const& light)
{
  std::erase(m_lights, light);
}

std::vector<WeakRef<Light>> Scene::getLights() const
{
  std::vector<WeakRef<Light>> handles;
  std::transform(
        m_lights.begin(),
        m_lights.end(),
        std::back_inserter(handles),
        [](auto const& light) { return light; });
  return handles;
}
