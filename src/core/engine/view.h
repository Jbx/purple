#pragma once

#include "camera.h"
#include "utils/rect.h"
#include "scene.h"

// STL
#include <memory>

template<typename RendererImpl>
class Engine;

class View
{
public:
  void setScene(Ref<Scene> const& scene);
  Ref<Scene> const& getScene() const;

  Camera& getCamera();
  Camera const& getCamera() const;

  void setClearColor(Color const& color);
  Color const& getClearColor() const;

  std::vector<Attachment> const& getAttachments() const;
  int getSampleCount() const;

  void setViewport(RectI const& viewport);
  std::optional<RectI> const& getViewport() const;

private:
  template<typename> friend class Engine;
  View(
      RectI const& viewport,
      std::vector<Attachment> const& attachments,
      int sampleCount);

  static Ref<View> create(
      RectI const& viewport = {},
      std::vector<Attachment> const& attachments = {},
      int sampleCount = 1);

  Ref<Scene> m_scene;
  std::vector<Attachment> m_attachments;
  int m_sampleCount;
  Camera m_camera;
  Color m_clearColor = {0.f, 0.f, 0.f, 1.f};
  std::optional<RectI> m_viewport;
};

