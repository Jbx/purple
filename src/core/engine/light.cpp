#include "light.h"

void Light::setEnabled(bool enabled)
{
  m_enabled = enabled;
}

void Light::setPosition(glm::vec3 const& position)
{
  m_position = position;
}

void Light::setDirection(glm::vec3 const& direction)
{
  m_direction = direction;
}

void Light::setColor(Color const& color)
{
  m_color = color;
}

void Light::setIntensity(float intensity)
{
  m_intensity = intensity;
}

void Light::setConstantAttenuation(float attenuation)
{
  m_constantAttenuation = attenuation;
}

void Light::setLinearAttenuation(float attenuation)
{
  m_linearAttenuation = attenuation;
}

void Light::setQuadraticAttenuation(float attenuation)
{
  m_quadraticAttenuation = attenuation;
}

void Light::setCutOffAngle(float angle)
{
  m_cutOffAngle = angle;
}

Light::Type Light::getType() const
{
  return m_type;
}

bool Light::isEnabled() const
{
  return m_enabled;
}

glm::vec3 const& Light::getPosition() const
{
  return m_position;
}

glm::vec3 const& Light::getDirection() const
{
  return m_direction;
}

Color Light::getColor() const
{
  return m_color;
}

float Light::getIntensity() const
{
  return m_intensity;
}

float Light::getConstantAttenuation() const
{
  return m_constantAttenuation;
}

float Light::getLinearAttenuation() const
{
  return m_linearAttenuation;
}

float Light::getQuadraticAttenuation() const
{
  return m_quadraticAttenuation;
}

float Light::getCutOffAngle() const
{
  return m_cutOffAngle;
}

Ref<Light> Light::create(Type type)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public Light
  {
    EnableMakeShared(Type type)
      : Light(type) {}
  };
  return createRef<EnableMakeShared>(type);
}

Light::Light(Type type)
  : m_type(type)
{

}
