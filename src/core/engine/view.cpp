#include "view.h"

Ref<View> View::create(
    RectI const& viewport,
    std::vector<Attachment> const& attachments,
    int sampleCount)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public View
  {
    EnableMakeShared(
          RectI const& viewport,
          std::vector<Attachment> const& attachments,
          int sampleCount)
      : View(viewport, attachments, sampleCount) {}
  };
  return createRef<EnableMakeShared>(viewport, attachments, sampleCount);
}

View::View(
    RectI const& viewport,
    std::vector<Attachment> const& attachments,
    int sampleCount)
  : m_attachments(attachments)
  , m_sampleCount(sampleCount)
{
  if (!viewport.isNull())
  {
    m_viewport = viewport;
  }
}

void View::setScene(Ref<Scene> const& scene)
{
  m_scene = scene;
}

Ref<Scene> const& View::getScene() const
{
  return m_scene;
}

Camera& View::getCamera()
{
  return m_camera;
}

Camera const& View::getCamera() const
{
  return m_camera;
}

void View::setClearColor(Color const& color)
{
  m_clearColor = color;
}

Color const& View::getClearColor() const
{
  return m_clearColor;
}

std::vector<Attachment> const& View::getAttachments() const
{
  return m_attachments;
}

int View::getSampleCount() const
{
  return m_sampleCount;
}

void View::setViewport(RectI const& viewport)
{
  m_viewport = viewport;
}

std::optional<RectI> const& View::getViewport() const
{
  return m_viewport;
}
