#include "geometry.h"

Ref<Geometry> Geometry::create(AttributeLayout const& layout)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public Geometry
  {
    EnableMakeShared(AttributeLayout const& layout)
      : Geometry(layout) {}
  };
  return createRef<EnableMakeShared>(layout);
}

Geometry::Geometry(AttributeLayout const& layout)
  : m_layout(layout) {}

AttributeLayout const& Geometry::getLayout() const
{
  return m_layout;
}
