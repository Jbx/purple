#pragma once

#include "math/math.h"

class Camera
{
public:
  enum ProjectionType
  {
    Perspective,
    Orthographic
  };

  void setProjectionType(ProjectionType type);
  void setPosition(glm::vec3 const& position);
  void setCenter(glm::vec3 const& center);
  void setUp(glm::vec3 const& up);
  void setAspectRatio(float aspectRatio);
  void setFieldOfView(float fieldOfView);
  void setLeft(float left);
  void setRight(float right);
  void setBottom(float bottom);
  void setTop(float top);
  void setNearPlane(float near);
  void setFarPlane(float far);

  void setOrtho(
      float left,
      float right,
      float bottom,
      float top,
      float nearPlane,
      float farPlane);

  void setPerspective(
      float fieldOfView,
      float aspectRatio,
      float nearPlane,
      float farPlane);

  ProjectionType getProjectionType() const;
  glm::vec3 const& getPosition() const;
  glm::vec3 const& getCenter() const;
  glm::vec3 const& getUp() const;
  glm::vec3 getForward() const;
  float getAspectRatio() const;
  float getFieldOfView() const;
  float getLeft() const;
  float getRight() const;
  float getBottom() const;
  float getTop() const;
  float getNearPlane() const;
  float getFarPlane() const;

  void translate(glm::vec3 const& translation, bool translateCenter = true);
  void rotate(glm::quat const& rotation);
  void rotateAboutCenter(glm::quat const& rotation);

  glm::quat tiltRotation(float angle) const;
  glm::quat panRotation(float angle) const;
  glm::quat rollRotation(float angle) const;

  void tilt(float angle);
  void pan(float angle);
  void pan(float angle, glm::vec3 const& axis);
  void roll(float angle);

  glm::mat4 getViewMatrix() const;
  glm::mat4 getProjectionMatrix() const;
  glm::mat4 getViewProjectionMatrix() const;

private:
  ProjectionType m_projectionType = Perspective;
  glm::vec3 m_position {0.f, 0.f, 2.f};
  glm::vec3 m_center {0.f, 0.f, 0.f};
  glm::vec3 m_up {0.f, 1.f, 0.f};
  float m_aspectRatio = 1.f;
  float m_fieldOfView = glm::radians(60.f);
  float m_left = -0.5f;
  float m_right = 0.5f;
  float m_bottom = -0.5f;
  float m_top = 0.5f;
  float m_nearPlane = 0.01f;
  float m_farPlane = 100.f;
};
