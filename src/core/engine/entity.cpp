#include "entity.h"

Ref<Entity> Entity::create(
    Ref<Geometry> const& geometry,
    Ref<MaterialInstance> const& materialInstance)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public Entity
  {
    EnableMakeShared(
          Ref<Geometry> const& geometry,
          Ref<MaterialInstance> const& materialInstance)
      : Entity(geometry, materialInstance) {}
  };
  return createRef<EnableMakeShared>(geometry, materialInstance);
}

Entity::Entity(
    Ref<Geometry> const& geometry,
    Ref<MaterialInstance> const& materialInstance)
  : m_geometry(geometry)
  , m_materialInstance(materialInstance)
{

}

void Entity::setTransformation(
    glm::mat4 const& transformation)
{
  m_transformation = transformation;
}

glm::mat4 const& Entity::getTransformation() const
{
  return m_transformation;
}

WeakRef<MaterialInstance> Entity::getMaterialInstance() const
{
  return m_materialInstance;
}
