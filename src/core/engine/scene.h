#pragma once

#include "light.h"
#include "entity.h"

template<typename RendererImpl>
class Engine;

class Scene
{
public:
  void addEntity(WeakRef<Entity> const& entity);
  void removeEntity(WeakRef<Entity> const& entity);

  std::vector<WeakRef<Entity>> getEntities() const;

  void addLight(WeakRef<Light> const& light);
  void removeLight(WeakRef<Light> const& entity);

  std::vector<WeakRef<Light>> getLights() const;

private:
  template<typename> friend class Engine;
  Scene() = default;
  static Ref<Scene> create();

  std::vector<Ref<Entity>> m_entities;
  std::vector<Ref<Light>> m_lights;
};

