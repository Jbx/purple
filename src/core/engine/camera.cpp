#include "camera.h"

void Camera::setProjectionType(ProjectionType type)
{
  m_projectionType = type;
}

void Camera::setPosition(glm::vec3 const& position)
{
  m_position = position;
}

void Camera::setCenter(glm::vec3 const& center)
{
  m_center = center;
}

void Camera::setUp(glm::vec3 const& up)
{
  m_up = up;
}

void Camera::setAspectRatio(float aspectRatio)
{
  m_aspectRatio = aspectRatio;
}

void Camera::setFieldOfView(float fieldOfView)
{
  m_fieldOfView = fieldOfView;
}

void Camera::setLeft(float left)
{
  m_left = left;
}

void Camera::setRight(float right)
{
  m_right = right;
}

void Camera::setBottom(float bottom)
{
  m_bottom = bottom;
}

void Camera::setTop(float top)
{
  m_top = top;
}

void Camera::setNearPlane(float near)
{
  m_nearPlane = near;
}

void Camera::setFarPlane(float far)
{
  m_farPlane = far;
}

void Camera::setOrtho(
    float left,
    float right,
    float bottom,
    float top,
    float nearPlane,
    float farPlane)
{
  m_left = left;
  m_right = right;
  m_bottom = bottom;
  m_top = top;
  m_nearPlane = nearPlane;
  m_farPlane = farPlane;
}

void Camera::setPerspective(
    float fieldOfView,
    float aspectRatio,
    float nearPlane,
    float farPlane)
{
  m_fieldOfView = fieldOfView;
  m_aspectRatio = aspectRatio;
  m_nearPlane = nearPlane;
  m_farPlane = farPlane;
}

Camera::ProjectionType Camera::getProjectionType() const
{
  return m_projectionType;
}

glm::vec3 const& Camera::getPosition() const
{
  return m_position;
}

glm::vec3 const& Camera::getCenter() const
{
  return m_center;
}

glm::vec3 const& Camera::getUp() const
{
  return m_up;
}

glm::vec3 Camera::getForward() const
{
  return glm::normalize(m_center - m_position);
}

float Camera::getAspectRatio() const
{
  return m_aspectRatio;
}

float Camera::getFieldOfView() const
{
  return m_fieldOfView;
}

float Camera::getLeft() const
{
  return m_left;
}

float Camera::getRight() const
{
  return m_right;
}

float Camera::getBottom() const
{
  return m_bottom;
}

float Camera::getTop() const
{
  return m_top;
}

float Camera::getNearPlane() const
{
  return m_nearPlane;
}

float Camera::getFarPlane() const
{
  return m_farPlane;
}

void Camera::translate(glm::vec3 const& translation, bool translateCenter)
{
  m_position += translation;

  if (translateCenter)
  {
    m_center += translation;
  }
}

void Camera::rotate(glm::quat const& rotation)
{
  m_up = rotation * m_up;
  auto viewVector = m_center - m_position;
  auto rotatedViewVector = rotation * viewVector;
  m_center = m_position + rotatedViewVector;
}

void Camera::rotateAboutCenter(glm::quat const& rotation)
{
  m_up = rotation * m_up;
  auto viewVector = m_center - m_position;
  auto rotatedViewVector = rotation * viewVector;
  m_position = m_center - rotatedViewVector;
}

glm::quat Camera::tiltRotation(float angle) const
{
  auto viewVector = m_center - m_position;
  auto xBasis = glm::normalize(glm::cross(m_up, glm::normalize(viewVector)));
  return glm::angleAxis(-angle, xBasis);
}

glm::quat Camera::panRotation(float angle) const
{
  return glm::angleAxis(angle, m_up);
}

glm::quat Camera::rollRotation(float angle) const
{
  glm::vec3 viewVector = m_center - m_position;
  return glm::angleAxis(-angle, viewVector);
}

void Camera::tilt(float angle)
{
  glm::quat q = tiltRotation(angle);
  rotate(q);
}

void Camera::pan(float angle)
{
  glm::quat q = panRotation(-angle);
  rotate(q);
}

void Camera::pan(float angle, const glm::vec3 &axis)
{
  glm::quat q = glm::angleAxis(-angle, axis);
  rotate(q);
}

void Camera::roll(float angle)
{
  glm::quat q = rollRotation(-angle);
  rotate(q);
}

glm::mat4 Camera::getViewMatrix() const
{
  return glm::lookAt(m_position, m_center, m_up);
}

glm::mat4 Camera::getProjectionMatrix() const
{
  switch (m_projectionType)
  {
  case Perspective:
    return glm::perspective(m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane);
  case Orthographic:
    return glm::ortho(m_left, m_right, m_bottom, m_top, m_nearPlane, m_farPlane);
  }
  // Avoid warning
  return glm::mat4();
}

glm::mat4 Camera::getViewProjectionMatrix() const
{
  return getProjectionMatrix() * getViewMatrix();
}

