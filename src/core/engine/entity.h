#pragma once

#include "geometry.h"
#include "material.h"
#include "utils/ref.h"

// STL
#include <memory>

class Entity
{
public:
  void setTransformation(
      glm::mat4 const& transformation);

  glm::mat4 const& getTransformation() const;

  WeakRef<MaterialInstance> getMaterialInstance() const;

private:
  template<typename> friend class Engine;

  static Ref<Entity> create(
      Ref<Geometry> const& geometry,
      Ref<MaterialInstance> const& materialInstance);

  Entity(
      Ref<Geometry> const& geometry,
      Ref<MaterialInstance> const& materialInstance);

  Ref<Geometry> m_geometry;
  Ref<MaterialInstance> m_materialInstance;
  glm::mat4 m_transformation = glm::mat4(1.f);
};

