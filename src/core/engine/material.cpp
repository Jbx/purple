#include "material.h"
#include "utils/assert.h"

Ref<Material> Material::create(Shader const& shader,
    MaterialDefinition const& definition)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public Material
  {
    EnableMakeShared(
          Shader const& shader,
          MaterialDefinition const& materialDefinition)
      : Material(shader, materialDefinition) {}
  };
  return createRef<EnableMakeShared>(
        shader,
        definition);
}

Material::Material(
    Shader const& shader,
    MaterialDefinition const& definition)
  : m_shader(shader)
  , m_renderStates(definition.renderStates)
  , m_properties(definition.properties)
  , m_textures(definition.textures)
  , m_useCommonUniforms(definition.useCommonUniforms)
  , m_defaultInstance(createInstance())
{

}

void Material::setProperty(std::string const& name, Property const& value)
{
  ASSERT(m_properties.contains(name), "Property \"", name, "\" not found!");
  m_properties.update(name, value);
}

void Material::setTexture(std::string const& name, Ref<ITexture> const& texture)
{
  ASSERT(m_textures.contains(name), "Texture \"", name, "\" not found!");
  m_textures.update(name, texture);
}

Shader const& Material::getShader() const
{
  return m_shader;
}

RenderStates const& Material::getRenderStates() const
{
  return m_renderStates;
}

PropertyContainer const& Material::getProperties() const
{
  return m_properties;
}

TextureContainer const& Material::getTextures() const
{
  return m_textures;
}

bool Material::useCommonUniforms() const
{
  return m_useCommonUniforms;
}

WeakRef<MaterialInstance> const& Material::defaultInstance() const
{
  return m_defaultInstance;
}

WeakRef<MaterialInstance> Material::createInstance()
{
  return m_instances.emplace_back(MaterialInstance::create(*this));
}

Ref<MaterialInstance> MaterialInstance::create(Material const& material)
{
  // Allow to use smart pointers with private ctor
  struct EnableMakeShared : public MaterialInstance
  {
    EnableMakeShared(
          Material const& material)
      : MaterialInstance(material) {}
  };
  return createRef<EnableMakeShared>(
        material);
}

MaterialInstance::MaterialInstance(Material const& material)
  : m_material(material)
{

}

void MaterialInstance::setProperty(std::string const& name, Property const& value)
{
  ASSERT(m_material.getProperties().contains(name), "Property \"", name, "\" not found!");
  m_properties.push_back({name, value});
}

void MaterialInstance::setTexture(std::string const& name, Ref<ITexture> const& texture)
{
  ASSERT(m_material.getTextures().contains(name), "Texture \"", name, "\" not found!");
  m_textures.push_back({name, texture});
}

PropertyContainer MaterialInstance::getProperties() const
{
  PropertyContainer properties(m_material.getProperties().getProperties());
  for (auto const& property : m_properties)
  {
    properties.update(property.first, property.second);
  }
  return properties;
}

TextureContainer MaterialInstance::getTextures() const
{
  TextureContainer textures(m_material.getTextures());
  for (auto const& texture : m_textures)
  {
    textures.update(texture.first, texture.second);
  }
  return textures;
}

Material const& MaterialInstance::getMaterial() const
{
  return m_material;
}

