#pragma once


#include "render/renderer.h"
#include "caches/pipeline_cache.h"
#include "caches/render_pass_cache.h"
#include "scene.h"
#include "view.h"
#include "light.h"

// STL
#include <map>
#include <memory>
#include <algorithm>
#include <type_traits>

template <typename T>
concept InheritMaterial = std::is_base_of<Material, T>::value;

template<typename RendererImpl>
class Engine
{
public:  
  Engine(
      Renderer<RendererImpl>& renderer);

  // Render all views in the following order:
  // - Render all offscreen views respecting their creation order
  // - Then render the screen view
  void draw();

  // Render views in the given order
  void draw(std::vector<WeakRef<View>> const& views);

  WeakRef<Scene> createScene();
  void removeScene(WeakRef<Scene> const& scene);

  WeakRef<View> createOffscreenView(
      SizeI const& size,
      std::vector<Attachment> const& attachments,
      int sampleCount = 1);

  WeakRef<View> getScreenView() const;
  std::vector<WeakRef<View>> getOffscreenViews() const;

  void removeView(WeakRef<View> const& view);

  template<typename VertexType>
  WeakRef<Geometry> createGeometry(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices);

  template<typename VertexType>
  WeakRef<Geometry> createGeometry(
      AttributeLayout const& layout,
      std::vector<VertexType> const& vertices);

  template<typename VertexType, typename IndexType>
  WeakRef<Geometry> createGeometry(
      AttributeLayout const& layout,
      BufferView<VertexType> const& vertices,
      BufferView<IndexType> const& indices);

  template<typename VertexType, typename IndexType>
  WeakRef<Geometry> createGeometry(
      AttributeLayout const& layout,
      std::vector<VertexType> const& vertices,
      std::vector<IndexType> const& indices);

  void removeGeometry(WeakRef<Geometry> const& geometry);

  WeakRef<ITexture> createTexture2D(
      IImage const& image,
      bool generateMipmap = true,
      Sampler const& sampler = Sampler {});

  WeakRef<ITexture> createTexture2D(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap = true,
      Sampler const& sampler = Sampler {});

  WeakRef<ITexture> createTextureCube(
      IImage const& image,
      bool generateMipmap = true,
      Sampler const& sampler = Sampler {});

  WeakRef<ITexture> createTextureCube(
      TextureFormat format,
      SizeI const& size,
      bool generateMipmap = true,
      Sampler const& sampler = Sampler {});

  void removeTexture(WeakRef<ITexture> const& texture);

  WeakRef<Material> createMaterial(
      Shader const& shader,
      MaterialDefinition const& definition = MaterialDefinition());

  template <InheritMaterial MaterialT, typename ...Args>
  WeakRef<MaterialT> createMaterial(Args&& ...args);

  void removeMaterial(WeakRef<Material> const& material);

  WeakRef<Entity> createEntity(
      WeakRef<Geometry> const& geometry,
      WeakRef<MaterialInstance> const& material);

  void removeEntity(WeakRef<Entity> const& entity);

  WeakRef<Light> createLight(Light::Type type);
  void removeLight(WeakRef<Light> const& light);

private:
  void draw(
      View const& view,
      IFramebuffer const& framebuffer,
      IRenderPass& renderPass);

  void addMaterial(Ref<Material> const& material);

  Renderer<RendererImpl>& m_renderer;
  Ref<View> m_screenView = View::create();
  std::map<Ref<View>, Scoped<IFramebuffer>> m_offscreenFramebuffers;
  std::map<Ref<Geometry>, Scoped<IVertexBuffer>> m_geometries;
  std::vector<Ref<ITexture>> m_textures;
  std::map<Ref<Entity>, Scoped<IUniformBuffer>> m_uniformBuffers;
  std::vector<Ref<Entity>> m_entities;
  std::vector<Ref<Light>> m_lights;
  RenderPassCache<RendererImpl> m_renderPassCache;
  PipelineCache<RendererImpl> m_pipelineCache;
  std::vector<Ref<Material>> m_materials;
  std::vector<Ref<Scene>> m_scenes;
};

#include "engine.inl"

