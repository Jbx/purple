#pragma once

#include "utils/color.h"
#include "utils/ref.h"
#include "math/math.h"

class Light
{  
public:
  enum Type
  {
    Point,
    Directional,
    Spot
  };

  void setEnabled(bool enabled);
  void setPosition(glm::vec3 const& position);
  void setDirection(glm::vec3 const& direction);
  void setColor(Color const& color);
  void setIntensity(float intensity);
  void setConstantAttenuation(float attenuation);
  void setLinearAttenuation(float attenuation);
  void setQuadraticAttenuation(float attenuation);
  void setCutOffAngle(float angle);

  Type getType() const;
  bool isEnabled() const;
  glm::vec3 const& getPosition() const;
  glm::vec3 const& getDirection() const;
  Color getColor() const;
  float getIntensity() const;
  float getConstantAttenuation() const;
  float getLinearAttenuation() const;
  float getQuadraticAttenuation() const;
  float getCutOffAngle() const;

private:
  template<typename> friend class Engine;

  static Ref<Light> create(Type type);

  Light(Type type);

  Type m_type = Light::Point;
  bool m_enabled = true;
  glm::vec3 m_position;
  glm::vec3 m_direction = glm::vec3(0.f, -1.f, 0.f);
  Color m_color = Color::WHITE;
  float m_intensity = 0.5f;
  float m_constantAttenuation = 0.f;
  float m_linearAttenuation = 1.f;
  float m_quadraticAttenuation = 0.f;
  float m_cutOffAngle = glm::radians(45.f);
};
