#pragma once

#include "render/renderer.h"
#include "utils/hash.h"
#include "utils/log_manager.h"

class RenderPassKey
{
public:
  RenderPassKey(std::vector<Attachment> const& attachments, int sampleCount)
    : m_sampleCount(sampleCount)
  {
    std::transform(
          attachments.begin(),
          attachments.end(),
          std::back_inserter(m_attachments),
          [](auto const& attachment) { return fromAttachment(attachment); });
  }

  std::vector<AttachmentFormat> const& getAttachments() const { return m_attachments; }
  int getSampleCount() const {return m_sampleCount; }

  bool operator==(RenderPassKey const& key) const
  {
    return
        m_attachments == key.m_attachments &&
        m_sampleCount == key.m_sampleCount;
  }

private:
  std::vector<AttachmentFormat> m_attachments;
  int m_sampleCount;
};

template<typename RendererImpl>
class RenderPassCache
{
public:
  RenderPassCache(Renderer<RendererImpl>& renderer)
    : m_renderer(renderer) {}

  IRenderPass& get(RenderPassKey const& key)
  {
    auto it = m_renderPasses.find(key);
    if (it == m_renderPasses.end())
    {
      it = m_renderPasses.insert(
            {key,
             m_renderer.createRenderPass(key.getAttachments(), key.getSampleCount())
            }).first;

      LOG_INFO("Create render pass (Attachments ", key.getAttachments().size(), ", Samples: ", key.getSampleCount(), ")");
    }
    return *it->second;
  }

private:
  Renderer<RendererImpl>& m_renderer;

  struct Hash
  {
    std::size_t operator()(RenderPassKey const& key) const
    {
      std::size_t h = key.getAttachments().size();
      for (auto format : key.getAttachments())
      {
        hashCombine(h, static_cast<int>(format.first));
        hashCombine(h, static_cast<int>(format.second));
      }

      hashCombine(h, key.getSampleCount());
      return h;
    }
  };

  std::unordered_map<RenderPassKey, Scoped<IRenderPass>, Hash> m_renderPasses;
};
