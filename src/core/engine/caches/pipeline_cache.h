#pragma once

#include "render/renderer.h"
#include "engine/material.h"
#include "utils/hash.h"
#include "utils/log_manager.h"

struct PipelineKey
{
  RefWrap<IRenderPass> renderPass;
  Ref<Material> material;

  bool operator==(PipelineKey const& key) const
  {
    return
        &renderPass.get() == &key.renderPass.get() &&
        material == key.material;
  }
};

template<typename RendererImpl>
class PipelineCache
{
public:
  PipelineCache(Renderer<RendererImpl>& renderer)
    : m_renderer(renderer) {}

  IPipeline& get(PipelineKey const& key)
  {
    auto it = m_pipelines.find(key);
    if (it == m_pipelines.end())
    {
      int uniformBufferCount =
          !key.material->getProperties().isEmpty() +
          key.material->useCommonUniforms();

      it = m_pipelines.insert(
            {key,
             m_renderer.createPipeline(
             key.renderPass,
             key.material->getShader(),
             key.material->getRenderStates(),
             uniformBufferCount,
             key.material->getTextures())
            }).first;

      LOG_INFO("Create pipeline (Uniform blocks: ", uniformBufferCount, ")");
    }
    return *it->second;
  }

private:
  Renderer<RendererImpl>& m_renderer;

  struct Hash
  {
    std::size_t operator()(PipelineKey const& key) const
    {
      std::size_t h = reinterpret_cast<std::size_t>(&key.renderPass.get());
      hashCombine(h, reinterpret_cast<std::size_t>(key.material.get()));
      return h;
    }
  };

  std::unordered_map<PipelineKey, Scoped<IPipeline>, Hash> m_pipelines;
};
