#pragma once

#include "render/attribute.h"
#include "utils/ref.h"

template<typename RendererImpl>
class Engine;

class Geometry
{
private:
  template<typename> friend class Engine;
  static Ref<Geometry> create(AttributeLayout const& layout);
  Geometry(AttributeLayout const& layout);

  AttributeLayout const& getLayout() const;

private:
  AttributeLayout m_layout;
};
