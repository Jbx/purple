#pragma once

#include "utils/ref.h"
#include "render/render_states.h"
#include "render/shader.h"
#include "render/texture_container.h"
#include "render/property.h"

struct MaterialDefinition
{
  RenderStates renderStates;
  PropertyList properties;
  TextureList textures;
  bool useCommonUniforms = true;
};

class MaterialInstance;

class Material
{
public:
  virtual ~Material() = default;

  void setProperty(std::string const& name, Property const& value);
  void setTexture(std::string const& name, Ref<ITexture> const& texture);

  Shader const& getShader() const;
  RenderStates const& getRenderStates() const;
  PropertyContainer const& getProperties() const;
  TextureContainer const& getTextures() const;
  bool useCommonUniforms() const;

  WeakRef<MaterialInstance> const& defaultInstance() const;
  WeakRef<MaterialInstance> createInstance();

protected:
  template<typename> friend class Engine;

  static Ref<Material> create(
      Shader const& shader,
      MaterialDefinition const& definition = MaterialDefinition());

  Material(
      Shader const& shader,
      MaterialDefinition const& definition = MaterialDefinition());

  Shader m_shader;
  RenderStates m_renderStates;
  PropertyContainer m_properties;
  TextureContainer m_textures;
  bool m_useCommonUniforms = true;
  std::vector<Ref<MaterialInstance>> m_instances;
  WeakRef<MaterialInstance> m_defaultInstance;
};

class MaterialInstance
{
public:
  void setProperty(std::string const& name, Property const& value);
  void setTexture(std::string const& name, Ref<ITexture> const& texture);

  PropertyContainer getProperties() const;
  TextureContainer getTextures() const;

  Material const& getMaterial() const;

private:
  template<typename> friend class Engine;
  friend class Material;

  static Ref<MaterialInstance> create(
      Material const& material);

  MaterialInstance(Material const& material);
  Material const& m_material;

  PropertyList m_properties;
  TextureList m_textures;
};
