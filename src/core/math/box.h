#pragma once

#include "math/math.h"

// STL
#include <array>
#include <vector>
#include <string>

class Box
{
public:
  Box() = default;
  Box(glm::vec3 const& lhs, glm::vec3 const& rhs);

  glm::vec3 const& getMin() const;
  glm::vec3 const& getMax() const;

  void setExtents(glm::vec3 const& corner1, glm::vec3 const& corner2);

  std::array<glm::vec3, 8> getCorners() const;
  glm::vec3 getCenter() const;
  glm::vec3 getSize() const;
  glm::vec3 getHalfSize() const;
  float getRadius() const;

  bool intersects(Box const& bbox) const;
  bool contains(glm::vec3 const& vector, bool strict = false) const;
  bool contains(Box const& bbox, bool strict = false) const;

  void transform(glm::mat4 const& transformation);
  Box transformed(glm::mat4 const& transformation) const;

  Box scaledFromCenter(float x, float y, float z) const;
  Box scaledFromCenter(float factor) const;

  void merge(glm::vec3 const& point);
  void merge(Box const& bbox);
  Box united(Box const& bbox) const;

  void setNull();
  bool isNull() const;
  bool isValid() const;

  float distance(glm::vec3 const& v) const;

  static Box merge(std::vector<Box> const& boxes);

  std::string toString() const;

  static const Box Null;
  static const Box Invalid;

private:
  glm::vec3 m_min;
  glm::vec3 m_max;
};
