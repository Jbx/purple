#pragma once

// GLM
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtx/quaternion.hpp>

// STL
#include <algorithm>
#include <string>
#include <iostream>

namespace Math {

static constexpr double Pi  = 3.141592653589793238462643383279;
static constexpr double TwoPi = Pi * 2.;
static constexpr double HalfPi = Pi * 0.5;
static constexpr double Sqrt2 = 1.41421356237309504880;

template<typename T>
static T NaN()
{
  return std::numeric_limits<T>::quiet_NaN();
}

template<typename T>
static bool epsilonEqual(T lhs, T rhs)
{
  return glm::epsilonEqual(lhs, rhs, std::numeric_limits<T>::epsilon());
}

template<>
bool epsilonEqual(int lhs, int rhs)
{
  return lhs == rhs;
}

template<typename T>
static bool epsilonIsNull(T value)
{
  return glm::epsilonEqual(value, static_cast<T>(0), std::numeric_limits<T>::epsilon());
}

template<>
bool epsilonIsNull(int value)
{
  return value == 0;
}

static bool epsilonEqual(glm::vec3 const& lhs, glm::vec3 const& rhs)
{
  return
      epsilonEqual(lhs.x, rhs.x) &&
      epsilonEqual(lhs.y, rhs.y) &&
      epsilonEqual(lhs.z, rhs.z);
}

static bool epsilonIsNull(glm::vec3 const& value)
{
  return
      !epsilonIsNull(value.x) &&
      !epsilonIsNull(value.y) &&
      !epsilonIsNull(value.z);
}

static glm::vec3 floor(glm::vec3 const& lhs, glm::vec3 const& rhs)
{
  return glm::vec3(std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y), std::min(lhs.z, rhs.z));
}

static glm::vec3 ceil(glm::vec3 const& lhs, glm::vec3 const& rhs)
{
  return glm::vec3(std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y), std::max(lhs.z, rhs.z));
}

static glm::vec3 normal(glm::vec3 const& v1, glm::vec3 const& v2)
{
  return glm::normalize(glm::cross(v1, v2));
}

static glm::vec3 normal(glm::vec3 const& v1, glm::vec3 const& v2, glm::vec3 const& v3)
{
  return glm::normalize(glm::cross((v2 - v1), (v3 - v1)));
}

constexpr float degToRad(float angleDeg)
{
  return (angleDeg / 360.) * (2. * Pi);
}

constexpr float radToDeg(float angleRad)
{
  return (angleRad / (2. * Pi)) * 360.;
}

constexpr float squared(float value)
{
  return value * value;
}

static bool isNaN(glm::vec3 const& vec)
{
  return glm::any(glm::isnan(vec));
}

}

inline std::ostream& operator<<(std::ostream& out, glm::vec2 const& vec)
{
  out << glm::to_string(vec);
  return out;
}

inline std::ostream& operator<<(std::ostream& out, glm::vec3 const& vec)
{
  out << glm::to_string(vec);
  return out;
}

inline std::ostream& operator<<(std::ostream& out, glm::vec4 const& vec)
{
  out << glm::to_string(vec);
  return out;
}

inline std::ostream& operator<<(std::ostream& out, glm::mat2 const& mat)
{
  out << glm::to_string(mat);
  return out;
}

inline std::ostream& operator<<(std::ostream& out, glm::mat3 const& mat)
{
  out << glm::to_string(mat);
  return out;
}

inline std::ostream& operator<<(std::ostream& out, glm::mat4 const& mat)
{
  out << glm::to_string(mat);
  return out;
}
