#include "box.h"

// STL
#include <algorithm>
#include <cmath>
#include <string>
#include <limits>


const Box Box::Null(glm::vec3(0., 0., 0.), glm::vec3(0., 0., 0.));
const Box Box::Invalid(
    glm::vec3(Math::NaN<float>(), Math::NaN<float>(), Math::NaN<float>()),
    glm::vec3(Math::NaN<float>(), Math::NaN<float>(), Math::NaN<float>()));

Box::Box(glm::vec3 const& lhs, glm::vec3 const& rhs)
{
  setExtents(lhs, rhs);
}

glm::vec3 const& Box::getMin() const
{
  return m_min;
}

glm::vec3 const& Box::getMax() const
{
  return m_max;
}

void Box::setExtents(glm::vec3 const& corner1, glm::vec3 const& corner2)
{
  m_min = glm::vec3(
        std::min(corner1.x, corner2.x),
        std::min(corner1.y, corner2.y),
        std::min(corner1.z, corner2.z));
  m_max = glm::vec3(
        std::max(corner1.x, corner2.x),
        std::max(corner1.y, corner2.y),
        std::max(corner1.z, corner2.z));
}

std::array<glm::vec3, 8> Box::getCorners() const
{
  return std::array<glm::vec3, 8>
  {
    m_min,
        m_max,
        glm::vec3(m_min.x, m_min.y, m_max.z),
        glm::vec3(m_min.x, m_max.y, m_max.z),
        glm::vec3(m_min.x, m_max.y, m_min.z),
        glm::vec3(m_max.x, m_min.y, m_min.z),
        glm::vec3(m_max.x, m_min.y, m_max.z),
        glm::vec3(m_max.x, m_max.y, m_min.z)
  };
}

glm::vec3 Box::getCenter() const
{
  return glm::vec3(
        (m_max.x - m_min.x) / 2.f + m_min.x,
        (m_max.y - m_min.y) / 2.f + m_min.y,
        (m_max.z - m_min.z) / 2.f + m_min.z);
}

glm::vec3 Box::getSize() const
{
  if (isValid())
  {
    return glm::vec3(
          std::abs(m_max.x - m_min.x),
          std::abs(m_max.y - m_min.y),
          std::abs(m_max.z - m_min.z));
  }
  else return glm::vec3(0, 0, 0);
}


glm::vec3 Box::getHalfSize() const
{
  return getSize() / 2.0f;
}

float Box::getRadius() const
{
  return getSize().length() / 2.0f;
}

bool Box::intersects(Box const& bbox) const
{
  if (isValid())
  {
    if (m_max.x < bbox.m_min.x)
      return false;
    if (m_min.x > bbox.m_max.x)
      return false;
    if (m_max.y < bbox.m_min.y)
      return false;
    if (m_min.y > bbox.m_max.y)
      return false;
    if (m_max.z < bbox.m_min.z)
      return false;
    if (m_min.z > bbox.m_max.z)
      return false;
    return true;
  }
  return false;
}

bool Box::contains(glm::vec3 const& vector, bool strict) const
{
  if (isValid())
  {
    if (strict)
    {
      if (vector.x > m_min.x && vector.x < m_max.x &&
          vector.y > m_min.y && vector.y < m_max.y &&
          vector.z > m_min.z && vector.z < m_max.z)
      {
        return true;
      }
    }
    else if (vector.x >= m_min.x && vector.x <= m_max.x &&
             vector.y >= m_min.y && vector.y <= m_max.y &&
             vector.z >= m_min.z && vector.z <= m_max.z)
    {
      return true;
    }
  }

  return false;
}

bool Box::contains(Box const& bbox, bool strict) const
{
  for (glm::vec3 const& corner : bbox.getCorners())
  {
    if (!contains(corner, strict))
      return false;
  }
  return true;
}

void Box::merge(glm::vec3 const& point)
{
  if (isValid())
  {
    m_min = Math::floor(m_min, point);
    m_max = Math::ceil(m_max, point);
  }
  else
  {
    m_min = point;
    m_max = point;
  }
}

void Box::merge(Box const& bbox)
{
  if (!isValid())
  {
    *this = bbox;
  }
  else if (bbox.isValid())
  {
    m_min.x = std::min(getMin().x, bbox.getMin().x);
    m_min.y = std::min(getMin().y, bbox.getMin().y);
    m_min.z = std::min(getMin().z, bbox.getMin().z);

    m_max.x = std::max(getMax().x, bbox.getMax().x);
    m_max.y = std::max(getMax().y, bbox.getMax().y);
    m_max.z = std::max(getMax().z, bbox.getMax().z);
  }
}

Box Box::united(Box const& bbox) const
{
  if (!isValid()) return bbox;
  if (!bbox.isValid()) return *this;

  glm::vec3 minimum, maximum;

  minimum.x = std::min(getMin().x, bbox.getMin().x);
  minimum.y = std::min(getMin().y, bbox.getMin().y);
  minimum.z = std::min(getMin().z, bbox.getMin().z);

  maximum.x = std::max(getMax().x, bbox.getMax().x);
  maximum.y = std::max(getMax().y, bbox.getMax().y);
  maximum.z = std::max(getMax().z, bbox.getMax().z);

  return Box(minimum, maximum);
}

void Box::setNull()
{
  setExtents(glm::vec3(0., 0., 0.), glm::vec3(0., 0., 0.));
}

void Box::transform(const glm::mat4& transformation)
{
  *this = transformed(transformation);
}

Box Box::transformed(glm::mat4 const& transformation) const
{
  std::array<glm::vec3, 8> corners = getCorners();

  for (unsigned int i = 0; i < corners.size(); ++i)
  {
    corners[i] = transformation * glm::vec4(corners[i], 1.f);
  }

  float minX, minY, minZ, maxX, maxY, maxZ;
  minX = maxX = corners[0].x;
  minY = maxY = corners[0].y;
  minZ = maxZ = corners[0].z;

  for (unsigned int i = 1; i < corners.size(); ++i)
  {
    minX = std::min(minX, corners[i].x);
    minY = std::min(minY, corners[i].y);
    minZ = std::min(minZ, corners[i].z);
    maxX = std::max(maxX, corners[i].x);
    maxY = std::max(maxY, corners[i].y);
    maxZ = std::max(maxZ, corners[i].z);
  }

  return Box(glm::vec3(minX, minY, minZ), glm::vec3(maxX, maxY, maxZ));
}

Box Box::scaledFromCenter(float x, float y, float z) const
{
  auto center = getCenter();

  auto transformation =
      glm::translate(glm::vec3(+center.x, +center.y, +center.z)) *
      glm::scale(glm::vec3(x, y, z)) *
      glm::translate(glm::vec3(-center.x, -center.y, -center.z));

  return transformed(transformation);
}

Box Box::scaledFromCenter(float factor) const
{
  return scaledFromCenter(factor, factor, factor);
}

bool Box::isNull() const
{
  return Math::epsilonEqual(m_min, m_max);
}

bool Box::isValid() const
{
  return !Math::isNaN(m_min) || !Math::isNaN(m_max);
}

std::string Box::toString() const
{
  std::string result;
  result += "[Box ";
  result += "Min: " + std::to_string(m_min.x) + ", " + std::to_string(m_min.y) + ", " + std::to_string(m_min.z) + " ";
  result += "Max: " + std::to_string(m_max.x) + ", " + std::to_string(m_max.y) + ", " + std::to_string(m_max.z);
  result += "]";
  return result;
}

float Box::distance(glm::vec3 const& v) const
{
  if (contains(v))
    return 0;
  else
  {
    glm::vec3 maxDist;

    if (v.x < m_min.x)
      maxDist.x = m_min.x - v.x;
    else if (v.x > m_max.x)
      maxDist.x = v.x - m_max.x;

    if (v.y < m_min.y)
      maxDist.y = m_min.y - v.y;
    else if (v.y > m_max.y)
      maxDist.y = v.y - m_max.y;

    if (v.z < m_min.z)
      maxDist.z = m_min.z - v.z;
    else if (v.z > m_max.z)
      maxDist.z = v.z - m_max.z;

    return maxDist.length();
  }
}

Box Box::merge(std::vector<Box> const& boxes)
{
  Box mergedBox = Invalid;
  for (auto const& box : boxes)
  {
    mergedBox.merge(box);
  }
  return mergedBox;
}

