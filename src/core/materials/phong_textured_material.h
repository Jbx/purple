#pragma once

#include "engine/material.h"

class PhongTexturedMaterial : public Material
{
public:
  PhongTexturedMaterial(
      Ref<ITexture> const& diffuseTexture,
      RenderStates const& renderStates = RenderStates());

private:  
  static MaterialDefinition createMaterialDefinition(
      Ref<ITexture> const& texture,
      RenderStates const& renderStates);
};


