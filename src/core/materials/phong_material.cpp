#include "phong_material.h"

PhongMaterial::PhongMaterial(RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/phong.vert.json",
          "../resources/shaders/phong.frag.json"),
        {Vec3, Vec3}), createMaterialDefinition(renderStates))
{
}

MaterialDefinition PhongMaterial::createMaterialDefinition(RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;
  materialDefinition.properties = {
    {"ambient", Color{0.8f, 0.8f, 0.8f, 1.f}},
    {"diffuse", Color{0.8f, 0.8f, 0.8f, 1.f}},
    {"specular", Color{0.035f, 0.035f, 0.035f, 1.0}},
    {"shininess", 40.f}
  };
  return materialDefinition;
}
