#include "color_material.h"

ColorMaterial::ColorMaterial(RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/color.vert.json",
          "../resources/shaders/color.frag.json"),
        {Vec3}),
      createMaterialDefinition(renderStates))
{

}

MaterialDefinition ColorMaterial::createMaterialDefinition(RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;
  materialDefinition.properties = {{"color", Color{0.4f, 0.4f, 0.4f, 1.f}}};
  return materialDefinition;
}
