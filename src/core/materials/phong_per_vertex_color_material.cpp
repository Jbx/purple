#include "phong_per_vertex_color_material.h"

PhongPerVertexColorMaterial::PhongPerVertexColorMaterial(RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/phong_per_vertex_color.vert.json",
          "../resources/shaders/phong_per_vertex_color.frag.json"),
        {Vec3, Vec4, Vec3}),
      createMaterialDefinition(renderStates))
{

}

MaterialDefinition PhongPerVertexColorMaterial::createMaterialDefinition(RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;
  materialDefinition.properties = {
    {"ambient", Color{0.8f, 0.8f, 0.8f, 1.f}},
    {"specular", Color{0.035f, 0.035f, 0.035f, 1.0}},
    {"shininess", 40.f}
  };
  return materialDefinition;
}
