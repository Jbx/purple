#include "skybox_material.h"

SkyboxMaterial::SkyboxMaterial(
    Ref<ITexture> const& cubeMapTexture,
    RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/skybox.vert.json",
          "../resources/shaders/skybox.frag.json"),
        {Vec3}),
      createMaterialDefinition(
        cubeMapTexture,
        renderStates))
{
}

MaterialDefinition SkyboxMaterial::createMaterialDefinition(
    Ref<ITexture> const& cubeMapTexture,
    RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;
  materialDefinition.renderStates.cullMode = CullMode::Front;

  materialDefinition.properties = {
    {"exposure", 4.5f},
    {"gamma", 2.2f}
  };
  materialDefinition.textures = {
    {"cubeMapTexture", cubeMapTexture}
  };

  return materialDefinition;
}
