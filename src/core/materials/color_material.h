#pragma once

#include "engine/material.h"

class ColorMaterial : public Material
{
public:
  ColorMaterial(RenderStates const& renderStates = RenderStates());

private:
  static MaterialDefinition createMaterialDefinition(RenderStates const& renderStates);
};


