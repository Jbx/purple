#pragma once

#include "engine/material.h"

class PhongPerVertexColorMaterial : public Material
{
public:
  PhongPerVertexColorMaterial(RenderStates const& renderStates = RenderStates());

private:
  static MaterialDefinition createMaterialDefinition(RenderStates const& renderStates);
};


