#pragma once

#include "engine/material.h"

class SkyboxMaterial : public Material
{
public:
  SkyboxMaterial(
      Ref<ITexture> const& cubeMapTexture,
      RenderStates const& renderStates = RenderStates());

private:
  static MaterialDefinition createMaterialDefinition(
      Ref<ITexture> const& cubeMapTexture,
      RenderStates const& renderStates);
};

