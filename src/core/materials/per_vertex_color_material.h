#pragma once

#include "engine/material.h"

class PerVertexColorMaterial : public Material
{
public:
  PerVertexColorMaterial(RenderStates const& renderStates = RenderStates());

private:
  static MaterialDefinition createMaterialDefinition(RenderStates const& renderStates);
};


