#pragma once

#include "engine/material.h"

class PhongMaterial : public Material
{
public:
  PhongMaterial(RenderStates const& renderStates = RenderStates());

private:  
  static MaterialDefinition createMaterialDefinition(RenderStates const& renderStates);
};


