#include "per_vertex_color_material.h"

PerVertexColorMaterial::PerVertexColorMaterial(RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/per_vertex_color.vert.json",
          "../resources/shaders/per_vertex_color.frag.json"),
        {Vec3, Vec4}),
      createMaterialDefinition(renderStates))
{

}

MaterialDefinition PerVertexColorMaterial::createMaterialDefinition(RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;
  return materialDefinition;
}
