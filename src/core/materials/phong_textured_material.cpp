#include "phong_textured_material.h"

PhongTexturedMaterial::PhongTexturedMaterial(
    Ref<ITexture> const& diffuseTexture,
    RenderStates const& renderStates)
  : Material(
      Shader(
        ShaderProgram(
          "../resources/shaders/phong_textured.vert.json",
          "../resources/shaders/phong_textured.frag.json"),
        {Vec3, Vec3, Vec2}), createMaterialDefinition(diffuseTexture, renderStates))
{
}

MaterialDefinition PhongTexturedMaterial::createMaterialDefinition(
    Ref<ITexture> const& diffuseTexture,
    RenderStates const& renderStates)
{
  MaterialDefinition materialDefinition;
  materialDefinition.renderStates = renderStates;

  materialDefinition.properties = {
    {"ambient", Color{0.8f, 0.8f, 0.8f, 1.f}},
    {"diffuse", Color{0.8f, 0.8f, 0.8f, 1.f}},
    {"specular", Color{0.035f, 0.035f, 0.035f, 1.0}},
    {"shininess", 40.f},
    {"textured", false},
  };

  materialDefinition.textures = {
    {"diffuseTexture", diffuseTexture}
  };

  return materialDefinition;
}
