#pragma once

#include "image_interface.h"

// STL
#include <string>

// KTX
struct ktxTexture;

class KtxImage : public IImage
{
public:
  KtxImage(std::string const& filePath);
  ~KtxImage();

  bool isArray() const override;
  bool isCubemap() const override;
  bool isCompressed() const override;

  SizeI getSize() const override;
  int getChannelCount() const override;

  ImageFormat getFormat() const override;
  ImageDataType getDataType() const override;

  int getMipLevelCount() const override;
  int getLayerCount() const override;
  int getFaceCount() const override;

  ImageOrientationX getOrientationX() const override;
  ImageOrientationY getOrientationY() const override;
  ImageOrientationZ getOrientationZ() const override;

  size_t getDataSize() const override;
  unsigned char const* getData() const override;
  size_t getOffset(int faceIndex, int mipLevel) const override;
  size_t getDataSize(int mipLevel) const override;

private:
  ktxTexture* m_ktxTexture = nullptr;
  unsigned char* m_data = nullptr;
  size_t m_dataSize  = 0;
  ImageFormat m_format = ImageFormat::RGB8;
  ImageDataType m_dataType = ImageDataType::UByte;
};
