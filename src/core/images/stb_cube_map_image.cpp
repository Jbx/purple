#include "stb_cube_map_image.h"
#include "utils/assert.h"

StbCubeMapImage::StbCubeMapImage(std::array<RefWrap<IImage>, 6> const& images)
  : m_size(images.front().get().getSize())
  , m_imageDataSize(images.front().get().getDataSize())
  , m_channelCount(images.front().get().getChannelCount())
  , m_format(images.front().get().getFormat())
{
  m_data.reserve(m_imageDataSize * 6);

  for (IImage const& image : images)
  {
    m_data.insert(m_data.end(), image.getData(), image.getData() + image.getDataSize());
  }
}

bool StbCubeMapImage::isArray() const
{
  return false;
}

bool StbCubeMapImage::isCubemap() const
{
  return true;
}

bool StbCubeMapImage::isCompressed() const
{
  return false;
}

SizeI StbCubeMapImage::getSize() const
{
  return m_size;
}

int StbCubeMapImage::getChannelCount() const
{
  return m_channelCount;
}

ImageFormat StbCubeMapImage::getFormat() const
{
  return m_format;
}

ImageDataType StbCubeMapImage::getDataType() const
{
  return ImageDataType::UByte;
}

int StbCubeMapImage::getMipLevelCount() const
{
  return 1;
}

int StbCubeMapImage::getLayerCount() const
{
  return 1;
}

int StbCubeMapImage::getFaceCount() const
{
  return 6;
}

ImageOrientationX StbCubeMapImage::getOrientationX() const
{
  return ImageOrientationX::Left;
}

ImageOrientationY StbCubeMapImage::getOrientationY() const
{
  return ImageOrientationY::Up;
}

ImageOrientationZ StbCubeMapImage::getOrientationZ() const
{
  return ImageOrientationZ::In;
}

size_t StbCubeMapImage::getDataSize() const
{
  return m_imageDataSize * 6;
}

unsigned char const* StbCubeMapImage::getData() const
{
  return m_data.data();
}

size_t StbCubeMapImage::getOffset(int faceIndex, int mipLevel) const
{
  ASSERT(faceIndex < getFaceCount(), "Face index not defined!");
  ASSERT(mipLevel < getMipLevelCount(), "Mip level not defined!");

  return m_imageDataSize * faceIndex;
}

size_t StbCubeMapImage::getDataSize(int mipLevel) const
{
  return m_imageDataSize;
}
