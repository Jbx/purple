#pragma once

#include "image_interface.h"
#include "utils/ref.h"

// STL
#include <vector>

class StbCubeMapImage : public IImage
{
public:
  StbCubeMapImage(
      std::array<RefWrap<IImage>, 6> const& images);

  bool isArray() const override;
  bool isCubemap() const override;
  bool isCompressed() const override;

  SizeI getSize() const override;
  int getChannelCount() const override;

  ImageFormat getFormat() const override;
  ImageDataType getDataType() const override;

  int getMipLevelCount() const override;
  int getLayerCount() const override;
  int getFaceCount() const override;

  ImageOrientationX getOrientationX() const override;
  ImageOrientationY getOrientationY() const override;
  ImageOrientationZ getOrientationZ() const override;

  size_t getDataSize() const override;
  unsigned char const* getData() const override;
  size_t getOffset(int faceIndex, int mipLevel) const override;
  size_t getDataSize(int mipLevel) const override;

private:
  SizeI m_size;
  size_t m_imageDataSize;
  int m_channelCount;
  ImageFormat m_format;
  std::vector<unsigned char> m_data;
};
