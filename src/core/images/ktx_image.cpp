#include "ktx_image.h"
#include "utils/assert.h"
#include "utils/log_manager.h"

#define KHRONOS_STATIC
#include <ktx.h>

enum class OpenGLFormat
{
  RGB8 = 0x8051,
  RGBA8 = 0x8058,
  RGBA16F = 0x881A,
  RGBA_S3TC_DXT1 = 0x83F1,
  RGBA_S3TC_DXT3 = 0x83F2,
  RGBA_S3TC_DXT5 = 0x83F3,
  SRGB_ALPHA_BPTC_UNORM = 0x8E8D,
};

enum class VulkanFormat
{
  R8G8_UNORM = 16,
  R8G8B8A8_UNORM = 37,
  R16G16B16A16_SFLOAT = 90,
  BC1_RGBA_UNORM_BLOCK = 133,
  BC2_UNORM_BLOCK = 135,
  BC3_UNORM_BLOCK = 137,
  BC7_SRGB_BLOCK = 146,
};


ImageFormat fromOpenGLFormat(int format)
{
  switch (static_cast<OpenGLFormat>(format))
  {
  case OpenGLFormat::RGB8:
    return ImageFormat::RGB8;
  case OpenGLFormat::RGBA8:
    return ImageFormat::RGBA8;
  case OpenGLFormat::RGBA16F:
    return ImageFormat::RGBA16F;
  case OpenGLFormat::RGBA_S3TC_DXT1:
    return ImageFormat::RGBA_S3TC_DXT1;
  case OpenGLFormat::RGBA_S3TC_DXT3:
    return ImageFormat::RGBA_S3TC_DXT3;
  case OpenGLFormat::RGBA_S3TC_DXT5:
    return ImageFormat::RGBA_S3TC_DXT5;
  case OpenGLFormat::SRGB_ALPHA_BPTC_UNORM:
    return ImageFormat::SRGB_ALPHA_BPTC_UNORM;
  }
  ASSERT(false, "Format not handled");
  return ImageFormat::RGBA8;
}

ImageFormat fromVulkanFormat(int format)
{
  switch (static_cast<VulkanFormat>(format))
  {
  case VulkanFormat::R8G8_UNORM:
    return ImageFormat::RGB8;
  case VulkanFormat::R8G8B8A8_UNORM:
    return ImageFormat::RGBA8;
  case VulkanFormat::R16G16B16A16_SFLOAT:
    return ImageFormat::RGBA16F;
  case VulkanFormat::BC1_RGBA_UNORM_BLOCK:
    return ImageFormat::RGBA_S3TC_DXT1;
  case VulkanFormat::BC2_UNORM_BLOCK:
    return ImageFormat::RGBA_S3TC_DXT3;
  case VulkanFormat::BC3_UNORM_BLOCK:
    return ImageFormat::RGBA_S3TC_DXT5;
  case VulkanFormat::BC7_SRGB_BLOCK:
    return ImageFormat::SRGB_ALPHA_BPTC_UNORM;
  }
  ASSERT(false, "Format not handled");
  return ImageFormat::RGBA8;
}

enum KtxDataType
{
  UNSIGNED_BYTE = 0x1401,
  BYTE = 0x1400,
  UNSIGNED_SHORT = 0x1403,
  SHORT = 0x1402,
  UNSIGNED_INT = 0x1405,
  INT = 0x1404,
  HALF_FLOAT = 0x140B,
  FLOAT = 0x1406,
};

ImageDataType fromOpenGLDataType(int dataType)
{
  switch (dataType)
  {
  case UNSIGNED_BYTE:
    return ImageDataType::UByte;
  case BYTE:
    return ImageDataType::Byte;
  case UNSIGNED_SHORT:
    return ImageDataType::UShort;
  case SHORT:
    return ImageDataType::Short;
  case UNSIGNED_INT:
    return ImageDataType::UInt;
  case INT:
    return ImageDataType::Int;
  case HALF_FLOAT:
    return ImageDataType::HalfFloat;
  case FLOAT:
    return ImageDataType::Float;
  }
  ASSERT(false, "Data type not handled");
  return ImageDataType::UByte;
}

KtxImage::KtxImage(std::string const& filePath)
{
  ktxResult result = ktxTexture_CreateFromNamedFile(
      filePath.c_str(),
      KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT,
      &m_ktxTexture);

  if (result != KTX_SUCCESS)
  {
    LOG_ERROR("Error loading ", filePath, ": ", ktxErrorString(result));
    return;
  }

  LOG_DEBUG("Load image ", filePath, " successfully.");

  m_data = ktxTexture_GetData(m_ktxTexture);
  m_dataSize = ktxTexture_GetDataSize(m_ktxTexture);

  switch (m_ktxTexture->classId)
  {
  case ktxTexture1_c:
  {
    ktxTexture1* ktxTex1 = reinterpret_cast<ktxTexture1*>(m_ktxTexture);
    m_format = fromOpenGLFormat(ktxTex1->glInternalformat);
    if (!m_ktxTexture->isCompressed)
    {
      m_dataType = fromOpenGLDataType(ktxTex1->glType);
    }
  } break;

  case ktxTexture2_c:
  {
    ktxTexture2* ktxTex2 = reinterpret_cast<ktxTexture2*>(m_ktxTexture);
    m_format = fromVulkanFormat(ktxTex2->vkFormat);
  } break;
  }
}

KtxImage::~KtxImage()
{
  if (m_ktxTexture)
  {
    ktxTexture_Destroy(m_ktxTexture);
  }
}

bool KtxImage::isArray() const
{
  ASSERT(m_ktxTexture, "Invalid Ktx texture");
  return m_ktxTexture->isArray;
}

bool KtxImage::isCubemap() const
{
  ASSERT(m_ktxTexture, "Invalid Ktx texture");
  return m_ktxTexture->isCubemap;
}

bool KtxImage::isCompressed() const
{
  ASSERT(m_ktxTexture, "Invalid Ktx texture");
  return m_ktxTexture->isCompressed;
}

SizeI KtxImage::getSize() const
{
  return SizeI{
      static_cast<int>(m_ktxTexture->baseWidth),
      static_cast<int>(m_ktxTexture->baseHeight)
  };
}

int KtxImage::getChannelCount() const
{
  return m_ktxTexture->baseDepth;
}

ImageFormat KtxImage::getFormat() const
{
  return m_format;
}

ImageDataType KtxImage::getDataType() const
{
  return m_dataType;
}

int KtxImage::getMipLevelCount() const
{
  return m_ktxTexture->numLevels;
}

int KtxImage::getLayerCount() const
{
  return m_ktxTexture->numLayers;
}

int KtxImage::getFaceCount() const
{
  return m_ktxTexture->numFaces;
}

ImageOrientationX KtxImage::getOrientationX() const
{
  switch (m_ktxTexture->orientation.x)
  {
  case KTX_ORIENT_X_LEFT:
    return ImageOrientationX::Left;
  case KTX_ORIENT_X_RIGHT:
    return ImageOrientationX::Right;
  }
  return ImageOrientationX::Left;
}

ImageOrientationY KtxImage::getOrientationY() const
{
  switch (m_ktxTexture->orientation.y)
  {
  case KTX_ORIENT_Y_UP:
    return ImageOrientationY::Up;
  case KTX_ORIENT_Y_DOWN:
    return ImageOrientationY::Down;
  }
  return ImageOrientationY::Up;
}

ImageOrientationZ KtxImage::getOrientationZ() const
{
  switch (m_ktxTexture->orientation.z)
  {
  case KTX_ORIENT_Z_IN:
    return ImageOrientationZ::In;
  case KTX_ORIENT_Z_OUT:
    return ImageOrientationZ::Out;
  }
  return ImageOrientationZ::In;
}

size_t KtxImage::getDataSize() const
{
  return m_dataSize;
}

unsigned char const* KtxImage::getData() const
{
  return m_data;
}

size_t KtxImage::getOffset(int faceIndex, int mipLevel) const
{
  ktx_size_t offset = 0;
  ktxTexture_GetImageOffset(m_ktxTexture, mipLevel, 0, faceIndex, &offset);
  return offset;
}

size_t KtxImage::getDataSize(int mipLevel) const
{
  auto size = getSize();
  int mipWidth = std::max(1, size.width >> mipLevel);
  int mipHeight = std::max(1, size.height >> mipLevel);
  return mipWidth * mipHeight * getChannelCount();
}
