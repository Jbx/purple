#pragma once

#include "image_interface.h"
#include "utils/size.h"

// STL
#include <string>
#include <vector>

class StbImage : public IImage
{
public:
  StbImage(std::vector<unsigned char> const& buffer);
  StbImage(std::string const& filePath);

  ~StbImage() override;

  bool isArray() const override;
  bool isCubemap() const override;
  bool isCompressed() const override;

  SizeI getSize() const override;
  int getChannelCount() const override;

  ImageFormat getFormat() const override;
  ImageDataType getDataType() const override;

  int getMipLevelCount() const override;
  int getLayerCount() const override;
  int getFaceCount() const override;

  ImageOrientationX getOrientationX() const override;
  ImageOrientationY getOrientationY() const override;
  ImageOrientationZ getOrientationZ() const override;

  size_t getDataSize() const override;
  unsigned char const* getData() const override;
  size_t getOffset(int faceIndex, int mipLevel) const override;
  size_t getDataSize(int mipLevel) const override;

  bool save(std::string const& filePath);

private:
  unsigned char* m_data = nullptr;
  int m_width = 0;
  int m_height = 0;
  int m_channelCount = 0;
};


