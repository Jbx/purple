#pragma once

#include "utils/size.h"

// STL
#include <cstddef>

enum class ImageFormat
{
  RGB8,
  RGBA8,
  RGBA16F,
  RGBA_S3TC_DXT1,
  RGBA_S3TC_DXT3,
  RGBA_S3TC_DXT5,
  SRGB_ALPHA_BPTC_UNORM,
};

enum class ImageDataType
{
  UByte,
  Byte,
  UShort,
  Short,
  UInt,
  Int,
  HalfFloat,
  Float
};

enum class ImageOrientationX
{
  Left,
  Right,
};

enum class ImageOrientationY
{
  Up,
  Down,
};

enum class ImageOrientationZ
{
  In,
  Out
};

class IImage
{
public:
  virtual ~IImage() = default;

  virtual bool isArray() const = 0;
  virtual bool isCubemap() const = 0;
  virtual bool isCompressed() const = 0;

  virtual SizeI getSize() const = 0;
  virtual int getChannelCount() const = 0;

  virtual ImageFormat getFormat() const = 0;
  virtual ImageDataType getDataType() const = 0;

  virtual int getMipLevelCount() const = 0;
  virtual int getLayerCount() const = 0;
  virtual int getFaceCount() const = 0;

  virtual ImageOrientationX getOrientationX() const = 0;
  virtual ImageOrientationY getOrientationY() const = 0;
  virtual ImageOrientationZ getOrientationZ() const = 0;

  virtual size_t getDataSize() const = 0;
  virtual unsigned char const* getData() const = 0;
  virtual size_t getOffset(int faceIndex, int mipLevel) const = 0;
  virtual size_t getDataSize(int mipLevel) const = 0;
};
