#include "stb_image.h"
#include "utils/assert.h"
#include "utils/log_manager.h"

// stb
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

// STL
#include <filesystem>

StbImage::StbImage(std::vector<unsigned char> const& buffer)
{
  stbi_set_flip_vertically_on_load(true);
  m_data = stbi_load_from_memory(
        buffer.data(),
        static_cast<int>(buffer.size()),
        &m_width,
        &m_height,
        nullptr,
        STBI_rgb_alpha);

  m_channelCount = STBI_rgb_alpha;
  if (!m_data)
  {
    LOG_ERROR("Error loading image from memory");
  }
}

StbImage::StbImage(std::string const& filePath)
{
  stbi_set_flip_vertically_on_load(true);
  m_data = stbi_load(
        filePath.c_str(),
        &m_width,
        &m_height,
        nullptr,
        STBI_rgb_alpha);

  m_channelCount = STBI_rgb_alpha;
  if (!m_data)
  {
    LOG_ERROR("Error loading image from file ", filePath);
  }

  LOG_DEBUG("Load image ", filePath, " successfully.");
}

StbImage::~StbImage()
{
  if (m_data)
  {
    stbi_image_free(m_data);
  }
}

bool StbImage::isArray() const
{
  return false;
}

bool StbImage::isCubemap() const
{
  return false;
}

bool StbImage::isCompressed() const
{
  return false;
}

SizeI StbImage::getSize() const
{
  return {m_width, m_height};
}

int StbImage::getChannelCount() const
{
  return m_channelCount;
}

ImageFormat StbImage::getFormat() const
{
  return ImageFormat::RGBA8;
}

ImageDataType StbImage::getDataType() const
{
  return ImageDataType::UByte;
}

int StbImage::getMipLevelCount() const
{
  return 1;
}

int StbImage::getLayerCount() const
{
  return 1;
}

int StbImage::getFaceCount() const
{
  return 1;
}

ImageOrientationX StbImage::getOrientationX() const
{
  return ImageOrientationX::Left;
}

ImageOrientationY StbImage::getOrientationY() const
{
  return ImageOrientationY::Up;
}

ImageOrientationZ StbImage::getOrientationZ() const
{
  return ImageOrientationZ::In;
}

size_t StbImage::getDataSize() const
{
  return m_width * m_height * m_channelCount;
}

unsigned char const* StbImage::getData() const
{
  return m_data;
}

size_t StbImage::getOffset(int faceIndex, int mipLevel) const
{
  return 0;
}

size_t StbImage::getDataSize(int mipLevel) const
{
  ASSERT(false, "Stb does not support cubemaps");
  return 0;
}

bool StbImage::save(std::string const& filePath)
{
  std::filesystem::path path = filePath;
  if (path.extension() == ".png")
  {
    return stbi_write_png(filePath.c_str(), m_width, m_height, 4, m_data, m_width * 4);
  }
  ASSERT(false, "Image format not supported");
  return false;
}
