#pragma once

#include <array>

template<typename T> class Size;
using SizeI = Size<int>;
using SizeF = Size<float>;
using SizeD = Size<double>;

template<typename T>
class Size
{
public:
  Size() = default;
  Size(T width, T height);

  std::array<T, 2> toArray() const;

  bool isValid() const;
  bool isNull() const;

  bool operator==(Size const& size) const;
  bool operator!=(Size const& size) const;
  T width = 0;
  T height = 0;
};

template<typename T>
Size<T>::Size(T width, T height)
    : width(width), height(height)
{
}

template<typename T>
std::array<T, 2> Size<T>::toArray() const
{
  return {width, height};
}

template<typename T>
bool Size<T>::isValid() const
{
  return width >= 0 && height >= 0;
}

template<typename T>
bool Size<T>::isNull() const
{
  return width == 0 && height == 0;
}

template<typename T>
bool Size<T>::operator==(Size const& size) const
{
  return width == size.width &&
         height == size.height;
}

template<typename T>
bool Size<T>::operator!=(Size const& size) const
{
  return width != size.width ||
         height != size.height;
}
