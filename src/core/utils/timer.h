#pragma once

// STL
#include <chrono>

class Timer
{
public:
  Timer();

  void restart();

  int64_t getElapsedMs() const;
  int64_t getElapsedUs() const;

private:
  std::chrono::time_point<std::chrono::system_clock> m_startTime;
};


