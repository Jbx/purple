#pragma once

#include <memory>

template<typename T>
using Ref = std::shared_ptr<T>;

template <typename T, typename ...Args>
constexpr Ref<T> createRef(Args&& ...args)
{
  return std::make_shared<T>(std::forward<Args>(args)...);
}

template<typename T>
class WeakRef
{
public:
  WeakRef() = default;
  WeakRef(std::nullptr_t) {}

  WeakRef(Ref<T> const& ptr)
   : m_ptr(ptr) {}

  template<typename T2>
  WeakRef(WeakRef<T2> const& other)
  {
    m_ptr = other.m_ptr;
  }

  bool expired() const
  {
    return m_ptr.expired();
  }

  operator bool() const
  {
    return !m_ptr.expired();
  }

  T* operator->() const noexcept
  {
    return m_ptr.lock().get();
  }

  T* getRaw() const noexcept
  {
    return m_ptr.lock().get();
  }

  T& operator*() const noexcept
  {
    return *m_ptr.lock();
  }

  operator std::shared_ptr<T>() const noexcept
  {
    return m_ptr.lock();
  }

  friend bool operator ==(WeakRef<T> const& lhs, Ref<T> const& rhs)
  {
    return lhs.m_ptr.lock() == rhs;
  }

private:
  template<class T2>
  friend class WeakRef;

  std::weak_ptr<T> m_ptr;
};

template <typename T>
using Scoped = std::unique_ptr<T>;

template <typename T, typename ...Args>
constexpr Scoped<T> createScoped(Args&& ...args)
{
  return std::make_unique<T>(std::forward<Args>(args)...);
}

template<typename T>
using RefWrap = std::reference_wrapper<T>;

