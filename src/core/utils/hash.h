#pragma once

// STL
#include <functional>
#include <vector>

template <typename T>
void hashCombine(size_t& hash, T const& value)
{
  const size_t offset = 0x9e3779b9;
  hash ^= value + offset + (hash << 6) + (hash >> 2);
}
