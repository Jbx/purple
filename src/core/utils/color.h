#pragma once

#include <array>
#include <string>
#include <iostream>

class Color
{
public:
  constexpr std::array<float, 4> toArray() const;

  static Color fromRgb(int r, int g, int b, int a = 255);

  friend const Color operator*(Color const& color, float factor)
  {
    return Color{color.r * factor, color.g * factor, color.b * factor, color.a * factor};
  }

  friend const Color operator+(Color const& lhs, Color const& rhs)
  {
    return Color{lhs.r + rhs.r, lhs.g + rhs.g, lhs.b + rhs.b, lhs.a + rhs.a};
  }

  std::string toString() const;

  bool operator==(Color const& color) const;
  bool operator!=(Color const& color) const;

  float r = 0.0;
  float g = 0.0;
  float b = 0.0;
  float a = 1.0;

  static const Color BLACK;
  static const Color NAVY;
  static const Color BLUE;
  static const Color GREEN;
  static const Color TEAL;
  static const Color LIME;
  static const Color AQUA;
  static const Color MAROON;
  static const Color PURPLE;
  static const Color OLIVE;
  static const Color GRAY;
  static const Color SILVER;
  static const Color RED;
  static const Color FUSCHIA;
  static const Color YELLOW;
  static const Color WHITE;
};

inline std::ostream& operator<<(std::ostream& out, Color const& color)
{
  out << color.toString();
  return out;
}
