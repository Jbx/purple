#pragma once

#include "point.h"
#include "size.h"
#include "math/math.h"

// STL
#include <array>

template<typename T> class Rect;
using RectI = Rect<int>;
using RectF = Rect<float>;
using RectD = Rect<double>;


template<typename T>
class Rect
{
public:
  Rect() = default;
  Rect(T x, T y, T width, T height);
  Rect(Point<T> const& topLeft, Size<T> const& size);

  std::array<T, 4> toArray() const;

  constexpr inline T top() const noexcept { return y; }
  constexpr inline T bottom() const noexcept { return y + height; }
  constexpr inline T left() const noexcept { return x; }
  constexpr inline T right() const noexcept { return x + width; }

  void adjust(T dx1, T dy1, T dx2, T dy2);

  bool contains(T x, T y) const;

  bool operator==(Rect const& rect) const;
  bool operator!=(Rect const& rect) const;

  bool isNull() const;

  T x = 0;
  T y = 0;
  T width = 0;
  T height = 0;
};

template<typename T>
Rect<T>::Rect(T x, T y, T width, T height)
    : x(x), y(y), width(width), height(height)
{
}
template<typename T>
Rect<T>::Rect(Point<T> const& topLeft, Size<T> const& size)
: x(topLeft.x), y(topLeft.y), width(size.width), height(size.height)
{

}

template<typename T>
std::array<T, 4> Rect<T>::toArray() const
{
  return {x, y, width, height};
}

template<typename T>
void Rect<T>::adjust(T dx1, T dy1, T dx2, T dy2)
{
  x += dx1;
  y += dy1;
  width += dx2 - dx1;
  height += dy2 - dy1;
}

template<typename T>
bool Rect<T>::contains(T px, T py) const
{
  return
      px >= x && px <= x + width &&
      py >= y && py <= y + height;
}

template<typename T>
bool Rect<T>::operator==(Rect const &rect) const
{
  return x == rect.x &&
         y == rect.y &&
         width == rect.width &&
         height == rect.height;
}

template<typename T>
bool Rect<T>::operator!=(Rect const &rect) const
{
  return x != rect.x ||
         y != rect.y ||
         width != rect.width ||
         height != rect.height;
}

template<typename T>
bool Rect<T>::isNull() const
{
  return Math::epsilonIsNull(width) || Math::epsilonIsNull(height);
}
