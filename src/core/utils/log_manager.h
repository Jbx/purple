#pragma once

// STL
#include <string>
#include <iostream>
#include <sstream>
#include <functional>

enum class MessageType
{
  Error,
  Warning,
  Debug,
  Info
};

class LogManager
{
public:  
  static LogManager& getInstance();

  using OnMessageCallBack = std::function<void(MessageType, std::string const&)>;

  void setCallback(OnMessageCallBack const& onMessage);

  void log(MessageType messageType, std::string const& message);

  template<typename ...Args>
  void log(MessageType messageType, Args... message)
  {
    std::ostringstream oss;
    write(oss, message...);
    m_onMessage(messageType, oss.str());
  }

private:
  LogManager();

  template<typename Arg, typename... Args>
  void write(std::ostream& out, Arg&& arg, Args&&... args)
  {
    out << std::forward<Arg>(arg);
    ((out << std::forward<Args>(args)), ...);
  }

  OnMessageCallBack m_onMessage;
};

#define LOG_ERROR(...) LogManager::getInstance().log(MessageType::Error, __VA_ARGS__)
#define LOG_WARNING(...) LogManager::getInstance().log(MessageType::Warning, __VA_ARGS__)
#define LOG_INFO(...) LogManager::getInstance().log(MessageType::Info, __VA_ARGS__)

#ifdef NDEBUG
  #define LOG_DEBUG(...)
#else
  #define LOG_DEBUG(...) LogManager::getInstance().log(MessageType::Debug, __VA_ARGS__)
#endif
