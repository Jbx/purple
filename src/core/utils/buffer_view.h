#pragma once

// STL
#include <vector>
#include <array>

template<typename T>
class BufferView
{
public:
  constexpr BufferView() noexcept
    : m_datas(nullptr)
    , m_size(0) {}

  constexpr BufferView(T const* data, std::size_t size) noexcept
    : m_datas(data)
    , m_size(size) {}

  constexpr BufferView(std::vector<T> const& vector) noexcept
    : m_datas(vector.data())
    , m_size(vector.size()) {}

  template <typename T2>
  constexpr BufferView(std::vector<T2> const& vector) noexcept
    : m_datas(vector.data())
    , m_size(vector.size()) {}

  template<int N>
  constexpr BufferView(std::array<T, N> const& array)
    : m_datas(array.data())
    , m_size(N) {}

  T const& operator[](size_t i) const noexcept
  {
    return m_datas[i];
  }

  T const* data() const { return m_datas; }
  std::size_t size() const { return m_size; }
  bool empty() const { return m_size == 0; }

  auto begin() const noexcept { return m_datas; }
  auto end() const noexcept { return m_datas + m_size; }

private:
  T const* m_datas;
  std::size_t m_size = 0;
};
