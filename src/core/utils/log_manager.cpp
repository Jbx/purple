#include "log_manager.h"

LogManager &LogManager::getInstance()
{
  static LogManager logManager;
  return logManager;
}

void LogManager::setCallback(LogManager::OnMessageCallBack const& onMessage)
{
  m_onMessage = onMessage;
}

void LogManager::log(MessageType messageType, std::string const& message)
{
  m_onMessage(messageType, message);
}

LogManager::LogManager()
  : m_onMessage([](MessageType, std::string const& message) { std::cout << message << std::endl; std::cout.flush(); })
{}

