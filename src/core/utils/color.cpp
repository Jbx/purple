#include "color.h"

const Color Color::BLACK{0.f, 0.f, 0.f, 1.f};
const Color Color::NAVY{0.f, 0.5f, 0.f, 1.f};
const Color Color::BLUE{0.f, 0.f, 1.f, 1.f};
const Color Color::GREEN{0.f, 0.5f, 0.f, 1.f};
const Color Color::TEAL{0.f, 0.5f, 0.5f, 1.f};
const Color Color::LIME{0.f, 1.f, 0.f, 1.f};
const Color Color::AQUA{0.f, 1.f, 1.f, 1.f};
const Color Color::MAROON{0.5f, 0.f, 0.f, 1.f};
const Color Color::PURPLE{0.5f, 0.f, 0.5f, 1.f};
const Color Color::OLIVE{0.5f, 0.5f, 0.f, 1.f};
const Color Color::GRAY{0.5f, 0.5f, 0.5f, 1.f};
const Color Color::SILVER{0.75f, 0.75f, 0.75f, 1.f};
const Color Color::RED{1.f, 0.f, 0.f, 1.f};
const Color Color::FUSCHIA{1.f, 0.f, 1.f, 1.f};
const Color Color::YELLOW{1.f, 1.f, 0.f, 1.f};
const Color Color::WHITE{1.f, 1.f, 1.f, 1.f};

constexpr std::array<float, 4> Color::toArray() const
{
  return {r, g, b, a};
}

Color Color::fromRgb(int r, int g, int b, int a)
{
  return {r / 255.f, g / 255.f, b / 255.f, a / 255.f};
}

std::string Color::toString() const
{
  return
      std::to_string(r) + " " +
      std::to_string(g) + " " +
      std::to_string(b) + " " +
      std::to_string(a);
}

bool Color::operator==(Color const &color) const
{
  return r == color.r &&
         g == color.g &&
         b == color.b &&
         a == color.a;
}

bool Color::operator!=(Color const &color) const
{
  return r != color.r ||
         g != color.g ||
         b != color.b ||
         a != color.a;
}
