#include "timer.h"
#include "log_manager.h"

class Benchmark
{
public:
  Benchmark(std::string const& label = std::string())
    : m_label(label)
  {
  }

  ~Benchmark()
  {
    LOG_INFO(m_label, ": " + std::to_string(m_timer.getElapsedUs()) + " us");
  }

private:
  Timer m_timer;
  std::string m_label;
};

#define BENCHMARK(...) Benchmark bench(__VA_ARGS__)

