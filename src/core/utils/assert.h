#pragma once

#include "log_manager.h"

// STL
#include <cassert>

#ifdef NDEBUG
#define ASSERT(cond, ...)
#define ASSERT_EQUAL(a, b, ...)
#else
#define ASSERT(cond, ...) if (!(cond)) { LOG_ERROR("Assertion raised from ", __FILE__, " at ", __LINE__, ": ", __VA_ARGS__); std::abort(); }
#define ASSERT_EQUAL(a, b, ...) if (a != b) { LOG_ERROR("Assertion raised from ", __FILE__, " at ", __LINE__, ": ", __VA_ARGS__, " (", a, " != ", b, ")"); std::abort(); }
#endif

