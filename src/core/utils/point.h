#pragma once

#include <array>

template<typename T> class Point;
using PointI = Point<int>;
using PointF = Point<float>;
using PointD = Point<double>;


template<typename T>
class Point
{
public:
  Point() = default;
  Point(T x, T y);

  std::array<T, 2> toArray() const;

  bool operator==(Point const& Point) const;
  bool operator!=(Point const& Point) const;
  T x = 0;
  T y = 0;
};

template<typename T>
Point<T>::Point(T x, T y)
    : x(x), y(y)
{
}

template<typename T>
std::array<T, 2> Point<T>::toArray() const
{
  return {x, y};
}

template<typename T>
bool Point<T>::operator==(Point const& Point) const
{
  return x == Point.x &&
         y == Point.y;
}

template<typename T>
bool Point<T>::operator!=(Point const& Point) const
{
  return x != Point.x ||
         y != Point.y;
}
