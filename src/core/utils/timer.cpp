#include "timer.h"

Timer::Timer()
{
  m_startTime = std::chrono::system_clock::now();
}

void Timer::restart()
{
  m_startTime = std::chrono::system_clock::now();
}

int64_t Timer::getElapsedMs() const
{
  return
      std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now() - m_startTime).count();
}

int64_t Timer::getElapsedUs() const
{
  return
      std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now() - m_startTime).count();
}

