cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)

# Maps matrix depth ranges into Vulkans's [0..+1] range instead of OpenGL's [-1..+1] range
add_definitions(-DGLM_FORCE_RADIANS)
add_definitions(-DGLM_FORCE_DEPTH_ZERO_TO_ONE)
include_directories(${CMAKE_CURRENT_LIST_DIR}/include)
