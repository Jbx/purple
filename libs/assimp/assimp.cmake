cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)

include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

if(WIN32)
    if(MSVC)
        link_directories(${CMAKE_CURRENT_LIST_DIR}/msvc64)
        set(ASSIMP_LIBS assimp-vc142-mt)
    else()
        link_directories(${CMAKE_CURRENT_LIST_DIR}/mingw64)
        set(ASSIMP_LIBS assimp)
    endif()
endif()

if(UNIX)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/gcc64)
    set(ASSIMP_LIBS assimp)
endif()
