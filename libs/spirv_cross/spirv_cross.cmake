cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)

include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

if(WIN32)
    if(MSVC)
        if(CMAKE_BUILD_TYPE STREQUAL "Debug")
            link_directories(${CMAKE_CURRENT_LIST_DIR}/msvc64/debug)
            set(SPIRV_CROSS_LIBS
                spirv-cross-cd
                spirv-cross-cored
                spirv-cross-cppd
                spirv-cross-glsld
                spirv-cross-hlsld
                spirv-cross-msld
                spirv-cross-reflectd
                spirv-cross-utild)
        elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
            link_directories(${CMAKE_CURRENT_LIST_DIR}/msvc64/release)
            set(SPIRV_CROSS_LIBS
                spirv-cross-c
                spirv-cross-core
                spirv-cross-cpp
                spirv-cross-glsl
                spirv-cross-hlsl
                spirv-cross-msl
                spirv-cross-reflect
                spirv-cross-util)
        endif()
    else()
        link_directories(${CMAKE_CURRENT_LIST_DIR}/mingw64)
        set(SPIRV_CROSS_LIBS
            spirv-cross-c
            spirv-cross-core
            spirv-cross-cpp
            spirv-cross-glsl
            spirv-cross-hlsl
            spirv-cross-msl
            spirv-cross-reflect
            spirv-cross-util)
    endif()
endif()

if(UNIX)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/gcc64)
    set(SPIRV_CROSS_LIBS
        spirv-cross-c
        spirv-cross-core
        spirv-cross-cpp
        spirv-cross-glsl
        spirv-cross-hlsl
        spirv-cross-msl
        spirv-cross-reflect
        spirv-cross-util)
endif()
