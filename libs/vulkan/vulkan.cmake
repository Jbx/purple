cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)


include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

if(WIN32)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/win64)
    set(VULKAN_LIBS vulkan-1)
endif()

if(UNIX)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/gcc64)
    set(VULKAN_LIBS vulkan)
endif()

if(ANDROID)
    if(${ANDROID_NATIVE_API_LEVEL} LESS 26)
        message("Error: Android API >= 26 is required. Link will fail.")
    endif()
    add_definitions("-DVK_USE_PLATFORM_ANDROID_KHR")
    include_directories(${ANDROID_NDK}/sources/third_party/vulkan/src/include)
    include_directories(${ANDROID_NDK}/sources/third_party/vulkan/src/common)
    set(VULKAN_LIBS vulkan)
endif()
