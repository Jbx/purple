cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)

include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

if(WIN32)
    if(MSVC)
        link_directories(${CMAKE_CURRENT_LIST_DIR}/msvc64)
    else()
        link_directories(${CMAKE_CURRENT_LIST_DIR}/mingw64)
    endif()
    set(KTX_LIBS ktx ktx_read)
endif()

if(UNIX AND NOT EMSCRIPTEN)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/gcc64)
    set(KTX_LIBS ktx ktx_read)
endif()

if(EMSCRIPTEN)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/wasm)
    set(KTX_LIBS ktx ktx_read)
endif()
