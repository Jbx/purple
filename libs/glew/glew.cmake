cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
cmake_policy(VERSION 3.18)

include_directories(${CMAKE_CURRENT_LIST_DIR}/include)

if(WIN32)
    if(MSVC)
        link_directories(${CMAKE_CURRENT_LIST_DIR}/msvc64)
    else()
        link_directories(${CMAKE_CURRENT_LIST_DIR}/mingw64)
    endif()
    set(GLEW_LIBS glew32 opengl32)
endif()

if (UNIX)
    link_directories(${CMAKE_CURRENT_LIST_DIR}/gcc64)
    add_compile_definitions(GLEW_NO_GLU)
    set(GLEW_LIBS GLEW GL)
endif()
