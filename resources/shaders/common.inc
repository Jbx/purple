
const int MAX_LIGHTS = 8;
const int TYPE_POINT = 0;
const int TYPE_DIRECTIONAL = 1;
const int TYPE_SPOT = 2;

struct Light {
    int type;
    vec3 position;
    vec3 color;
    float intensity;
    vec3 direction;
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
    float cutOffAngle;
};

layout(binding = 0) uniform CommonUBO {
    mat4 modelMatrix;
    mat4 modelNormalMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    mat4 viewProjectionMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewNormalMatrix;
    mat4 mvpMatrix;
    vec3 viewPosition;
    vec3 viewCenter;
    Light lights[MAX_LIGHTS];
    int lightCount;
} commonUbo;
