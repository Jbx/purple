#version 450

#include "common.inc"

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;

layout (location = 0) out vec3 worldPos;
layout (location = 1) out vec3 normal;
layout (location = 2) out vec2 uv;


void main() 
{
    worldPos = vec3(commonUbo.modelMatrix * vec4(inPosition, 1.0));
    normal = (commonUbo.modelNormalMatrix * vec4(inNormal, 1.0)).xyz;
    uv = inUV;
    uv.t = 1.0 - inUV.t;
    gl_Position =  commonUbo.projectionMatrix * commonUbo.viewMatrix * vec4(worldPos, 1.0);
}
