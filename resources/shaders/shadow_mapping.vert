#version 450 core

#include "common.inc"

// inputs
layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;
layout (location = 2) in vec2 vertexTexCoord;

layout (location = 0) out vec3 fragPosition;
layout (location = 1) out vec3 normal;
layout (location = 2) out vec2 texCoord;
layout (location = 3) out vec4 fragPositionLightSpace;

layout(binding = 1) uniform MaterialUBO
{
    mat4 lightTransformation;
    vec3 lightPosition;
    vec2 shadowMapSize;
} ubo;

const mat4 biasMat = mat4(
            0.5, 0.0, 0.0, 0.0,
            0.0, 0.5, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.5, 0.5, 0.0, 1.0);

void main()
{
    fragPosition = vec3(commonUbo.modelMatrix * vec4(vertexPosition, 1.0));
    normal = transpose(inverse(mat3(commonUbo.modelMatrix))) * vertexNormal;
    texCoord = vertexTexCoord;
    fragPositionLightSpace = biasMat * ubo.lightTransformation * vec4(fragPosition, 1.0);
    gl_Position = commonUbo.mvpMatrix * vec4(vertexPosition, 1.0);
}
