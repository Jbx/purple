#version 450

#include "common.inc"

// inputs
layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;
layout (location = 2) in vec2 vertexTexCoord;

void main()
{
    gl_Position = commonUbo.mvpMatrix * vec4(vertexPosition, 1.0);
}
