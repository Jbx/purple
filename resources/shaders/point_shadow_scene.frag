#version 450

layout (binding = 2) uniform samplerCube shadowCubeMap;

layout (location = 0) in vec3 normal;
layout (location = 1) in vec4 color;
layout (location = 2) in vec3 eyePos;
layout (location = 3) in vec3 lightVec;
layout (location = 4) in vec3 worldPos;
layout (location = 5) in vec3 lightPos;

layout (location = 0) out vec4 fragColor;

#define EPSILON 0.15
#define SHADOW_OPACITY 0.5

void main()
{
    // Lighting
    vec4 ambient = vec4(vec3(0.05), 1.0);
    vec4 diffuse = vec4(1.0) * max(dot(normal, lightVec), 0.0);

    fragColor = vec4(ambient + diffuse * color);

    // Shadow
    vec3 lightVec = worldPos - lightPos;
    float sampledDist = texture(shadowCubeMap, lightVec).r;
    float dist = length(lightVec);

    // Check if fragment is in shadow
    float shadow = (dist <= sampledDist + EPSILON) ? 1.0 : SHADOW_OPACITY;

    fragColor.rgb *= shadow;
}
