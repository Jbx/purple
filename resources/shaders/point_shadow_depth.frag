#version 450

layout (location = 0) out float fragColor;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec3 lightPos;

void main() 
{
    // Store distance to light as 32 bit float value
    vec3 lightVec = pos.xyz - lightPos;
    fragColor = length(lightVec);
}
