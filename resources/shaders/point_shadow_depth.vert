#version 450

layout (location = 0) in vec3 inPos;

layout (location = 0) out vec4 pos;
layout (location = 1) out vec3 lightPos;

layout (binding = 0) uniform MaterialUBO
{	
    mat4 model;
    mat4 view;
    mat4 lightTransformation;
    mat4 projection;
    vec4 lightPos;
} ubo;

void main()
{
    gl_Position = ubo.projection * ubo.view * ubo.lightTransformation * ubo.model * vec4(inPos, 1.0);

    pos = vec4(inPos, 1.0);
    lightPos = ubo.lightPos.xyz;
}
