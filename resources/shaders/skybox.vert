#version 450

#include "common.inc"

// inputs
layout (location = 0) in vec3 vertexPosition;

layout (location = 0) out vec3 normal;

void main()
{
    gl_Position = commonUbo.mvpMatrix * vec4(vertexPosition, 1.0);
    normal = normalize(vertexPosition);
}
