#version 450

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 fragColor;

layout(binding = 0) uniform MaterialUBO
{
    float near;
    float far;
} plane;

layout(binding = 1) uniform sampler2D depthMap;


// required when using a perspective projection matrix
float linearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC 
    return (2.0 * plane.near * plane.far) / (plane.far + plane.near - z * (plane.far - plane.near));
}

void main()
{             
    float depthValue = texture(depthMap, fragTexCoord).r;
    // fragColor = vec4(vec3(linearizeDepth(depthValue) / far_plane), 1.0); // perspective
    fragColor = vec4(vec3(depthValue), 1.0); // orthographic
}
