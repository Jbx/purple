#version 450

#include "common.inc"

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inColor;

layout(location = 0) out vec4 color;

void main()
{    
    gl_Position = commonUbo.mvpMatrix * vec4(inPosition, 1.0);
    color = inColor;
}
