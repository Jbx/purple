#version 450

#include "common.inc"

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV0;
layout (location = 3) in vec2 inUV1;
layout (location = 4) in vec4 inColor0;


layout (location = 0) out vec3 worldPos;
layout (location = 1) out vec3 normal;
layout (location = 2) out vec2 uv0;
layout (location = 3) out vec2 uv1;
layout (location = 4) out vec4 color0;

void main() 
{
    color0 = inColor0;

    worldPos = (commonUbo.modelMatrix * vec4(inPos, 1.0)).xyz;
    normal = (commonUbo.modelNormalMatrix * vec4(inNormal, 1.0)).xyz;
    uv0 = inUV0;
    uv1 = inUV1;
    gl_Position =  commonUbo.projectionMatrix * commonUbo.viewMatrix * vec4(worldPos, 1.0);
}
