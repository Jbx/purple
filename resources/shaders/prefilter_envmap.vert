#version 450

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec3 position;

layout(binding = 0) uniform MaterialUBO {
    mat4 view;
    mat4 projection;
    float roughness;
} material;

void main()
{
    position = inPosition;
    gl_Position = material.projection * material.view * vec4(inPosition, 1.0);
}
