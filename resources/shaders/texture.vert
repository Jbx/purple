#version 450

#include "common.inc"

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;

layout(location = 1) out vec2 fragTexCoord;

void main() {
    gl_Position = commonUbo.mvpMatrix * vec4(inPosition, 1.0);
    fragTexCoord = inTexCoord;
}
