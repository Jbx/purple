#version 450

#include "common.inc"

layout(binding = 1) uniform sampler2D tex;

layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = texture(tex, fragTexCoord);
}
