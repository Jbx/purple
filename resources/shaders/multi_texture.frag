#version 450

layout(binding = 1) uniform sampler2D texture1;
layout(binding = 2) uniform sampler2D texture2;

layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

void main()
{
    if (fragTexCoord.x > 0.5)
        outColor = texture(texture1, fragTexCoord);
    else
        outColor = texture(texture2, fragTexCoord);
}
