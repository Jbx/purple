#version 450

layout (location = 0) in vec3 normal;

layout (location = 0) out vec4 fragColor;

layout (binding = 1) uniform MaterialUBO {
    float exposure;
    float gamma;
} material;

layout (binding = 2) uniform samplerCube cubeMapTexture;

// From http://filmicworlds.com/blog/filmic-tonemapping-operators/
vec3 uncharted2Tonemap(vec3 color)
{
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    float W = 11.2;
    return ((color * (A * color + C * B)+ D * E) / (color * (A * color + B) + D * F)) -E / F;
}

vec3 tonemap(vec3 color)
{
    vec3 outcol = uncharted2Tonemap(color * material.exposure);
    outcol = outcol * (1.0f / uncharted2Tonemap(vec3(11.2f)));
    return pow(outcol, vec3(1.0f / material.gamma));
}

#define MANUAL_SRGB 1

vec3 SRGBtoLINEAR(vec3 srgbIn)
{
#ifdef MANUAL_SRGB
#ifdef SRGB_FAST_APPROXIMATION
    vec3 linOut = pow(srgbIn, vec3(2.2));
#else //SRGB_FAST_APPROXIMATION
    vec3 bLess = step(vec3(0.04045),srgbIn);
    vec3 linOut = mix(srgbIn / vec3(12.92), pow((srgbIn + vec3(0.055)) / vec3(1.055), vec3(2.4)), bLess);
#endif //SRGB_FAST_APPROXIMATION
    return linOut;
#else //MANUAL_SRGB
    return srgbIn;
#endif //MANUAL_SRGB
}

void main()
{
    vec4 color = texture(cubeMapTexture, vec3(normal.x, -normal.y, normal.z));
    vec3 rgb = color.rgb;

    // Tone mapping
    rgb = SRGBtoLINEAR(uncharted2Tonemap(rgb * material.exposure));
    rgb = rgb * (1.0f / uncharted2Tonemap(vec3(11.2f)));
    // Gamma correction
    rgb = pow(rgb, vec3(1.0f / material.gamma));

    fragColor = vec4(rgb, color.a);
}
