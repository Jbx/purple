#version 450

layout(location = 0) out vec4 fragColor;

layout(binding = 1) uniform MaterialUBO
{
    vec4 color;
} material;

void main()
{
    fragColor = vec4(material.color);
}
