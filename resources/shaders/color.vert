#version 450

#include "common.inc"

layout(location = 0) in vec3 inPosition;

void main()
{
    gl_Position = commonUbo.mvpMatrix * vec4(inPosition, 1.0);
}
