#version 450

#include "phong.inc"

layout (location = 0) in vec3 worldPosition;
layout (location = 1) in vec3 worldNormal;
layout (location = 2) in vec2 texCoord;

layout (location = 0) out vec4 fragColor;

layout(binding = 1) uniform MaterialUBO
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
    bool textured;
} material;

layout (binding = 2) uniform sampler2D diffuseTexture;

void main()
{
    vec3 worldView = normalize(commonUbo.viewPosition.xyz - worldPosition);
    fragColor = phongFunction(
                material.ambient,
                material.textured ? texture(diffuseTexture, texCoord) : material.diffuse,
                material.specular,
                material.shininess,
                worldPosition,
                worldView,
                worldNormal);
}

