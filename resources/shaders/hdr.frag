#version 450

layout(location = 0) in vec2 texCoord;

layout(location = 0) out vec4 fragColor;

layout(binding = 0) uniform MaterialUBO
{
    bool enabled;
    float exposure;
} hdr;

layout(binding = 1) uniform sampler2D hdrBuffer;

void main()
{             
    const float gamma = 2.2;
    vec3 hdrColor = texture(hdrBuffer, texCoord).rgb;
    if (hdr.enabled)
    {
        // reinhard
        // vec3 result = hdrColor / (hdrColor + vec3(1.0));
        // exposure
        vec3 result = vec3(1.0) - exp(-hdrColor * hdr.exposure);
        // also gamma correct while we're at it
        result = pow(result, vec3(1.0 / gamma));
        fragColor = vec4(result, 1.0);
    }
    else
    {
        vec3 result = pow(hdrColor, vec3(1.0 / gamma));
        fragColor = vec4(result, 1.0);
    }
}
