#version 450

#include "common.inc"

layout (binding = 1) uniform MaterialUBO
{
    vec4 lightPos;
} ubo;

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec4 inColor;

layout (location = 0) out vec3 normal;
layout (location = 1) out vec4 color;
layout (location = 2) out vec3 eyePos;
layout (location = 3) out vec3 lightVec;
layout (location = 4) out vec3 worldPos;
layout (location = 5) out vec3 lightPos;

void main() 
{
    normal = inNormal;
    color = inColor;

    gl_Position = commonUbo.mvpMatrix * vec4(inPos.xyz, 1.0);
    eyePos = vec3(commonUbo.modelMatrix * vec4(inPos, 1.0));
    lightVec = normalize(ubo.lightPos.xyz - inPos.xyz);
    worldPos = inPos;

    lightPos = ubo.lightPos.xyz;
}

