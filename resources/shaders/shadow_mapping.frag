#version 450 core

#include "common.inc"

layout (location = 0) out vec4 fragColor;

layout (location = 0) in vec3 fragPosition;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec4 fragPositionLightSpace;

layout(binding = 1) uniform MaterialUBO
{
    mat4 lightTransformation;
    vec3 lightPosition;
    vec2 shadowMapSize;
} ubo;

layout (binding = 2) uniform sampler2D diffuseTexture;
layout (binding = 3) uniform sampler2D shadowMap;

float textureProj(vec4 shadowCoord, vec2 off)
{
    float shadow = 0.0;
    if (shadowCoord.z > -1.0 && shadowCoord.z < 1.0)
    {
        float dist = texture(shadowMap, shadowCoord.st + off).r;
        if (shadowCoord.w > 0.0 && dist < shadowCoord.z)
        {
            shadow = 1.0;
        }
    }
    return shadow;
}

float filterPCF(vec4 sc)
{
    float scale = 1.5;
    float dx = scale * 1.0 / float(ubo.shadowMapSize.x);
    float dy = scale * 1.0 / float(ubo.shadowMapSize.y);

    float shadowFactor = 0.0;
    int count = 0;
    int range = 1;

    for (int x = -range; x <= range; x++)
    {
        for (int y = -range; y <= range; y++)
        {
            shadowFactor += textureProj(sc, vec2(dx*x, dy*y));
            count++;
        }
    }
    return shadowFactor / count;
}

void main()
{           
    vec3 color = texture(diffuseTexture, texCoord).rgb;
    vec3 n = normalize(normal);
    vec3 lightColor = vec3(0.3);
    // ambient
    vec3 ambient = 0.3 * lightColor;
    // diffuse
    vec3 lightDir = normalize(ubo.lightPosition - fragPosition);
    float diff = max(dot(lightDir, n), 0.0);
    vec3 diffuse = diff * lightColor;
    // specular
    vec3 viewDir = normalize(commonUbo.viewPosition - fragPosition);
    vec3 reflectDir = reflect(-lightDir, n);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(n, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;    
    // calculate shadow
    float shadow = filterPCF(fragPositionLightSpace / fragPositionLightSpace.w);
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;
    
    fragColor = vec4(lighting, 1.0);
}
