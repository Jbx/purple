#version 450

#include "common.inc"

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexNormal;

layout (location = 0) out vec3 worldPosition;
layout (location = 1) out vec3 worldNormal;

void main()
{
    // Transform position, normal, to world space
    worldPosition = vec3(commonUbo.modelMatrix * vec4(vertexPosition, 1.0));
    worldNormal = normalize(mat3(commonUbo.modelNormalMatrix) * vertexNormal);

    // Calculate vertex position in clip coordinates
    gl_Position = commonUbo.mvpMatrix * vec4(vertexPosition, 1.0);
}

