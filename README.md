# Purple Engine

Purple Engine is a 3D rendering engine which combines modern C++ with Vulkan and OpenGL API.

Purple Engine is cross platform and independant of a GUI library. GLFW and Qt implementations are provided.

Purple can be cross-compiled to WebAssembly from Linux and Windows.

## Building Purple

### Prerequisites

To build Purple, you must first install the following tools:

- CMake 3.18 (or more recent)

To build Purple on Linux, OpenGL is required:

```
sudo apt-get install libgl1-mesa-dev
```

To build Purple for WASM from Windows you must also install the following:

- [emscripten](https://emscripten.org/docs/getting_started/downloads.html)
- [ninja](https://ninja-build.org/)

### Build for Native platform

Scripts are provided to build with Vulkan or OpenGL back-end

```
$ ./build_vulkan.sh
```

or 

```
$ ./build_opengl.sh
```

### Build for WebAssembly


```
$ ./build_web.sh
```

## Examples

![WebGL PBR rendering](docs/images/webgl.png)
![Vulkan PBR rendering](docs/images/vulkan.png)
