#include "qt/qt_vulkan_window.h"
#include "materials/phong_textured_material.h"
#include "engine/engine.h"
#include "assimp/assimp_importer.h"
#include "images/stb_image.h"

// Qt
#include <QGuiApplication>
#include <QLoggingCategory>


class QtModelViewer : public IQtVulkanRenderer
{
public:
  void init(VulkanEngine& engine) override
  {
    auto defaultTexture = engine.createTexture2D(StbImage("../resources/textures/missing.png"));
    auto material = engine.createMaterial<PhongTexturedMaterial>(defaultTexture);

    auto layout = {AssimpImporter::VertexPosition, AssimpImporter::VertexNormal, AssimpImporter::VertexTexCoord};

    //auto assimpScene = AssimpImporter::import("../resources/models/teapot.dae", {.normalize = true});
    auto assimpScene = AssimpImporter::import("../resources/models/porsche_911_carrera_4s/scene.gltf", {.layout = layout, .normalize = true});

    LOG_INFO("Mesh count ", assimpScene.meshes.size());    

    m_scene = engine.createScene();

    auto light = engine.createLight(Light::Point);
    light->setPosition(glm::vec3(0.f, 0.f, 4.f));
    light->setColor(Color{0.7, 0.7, 0.7, 1.0});
    light->setIntensity(1.0);
    m_scene->addLight(light);

    for (auto const& mesh : assimpScene.meshes)
    {
      auto geometry = engine.createGeometry(mesh.layout, mesh.vertices, mesh.indices);

      auto const& assimpMaterial = assimpScene.materials.at(*mesh.materialIndex);

      WeakRef<MaterialInstance> materialInstance;
      if (mesh.isTextured)
      {
        if (assimpMaterial.diffuseTextureFileName)
        {
          StbImage img("../resources/models/porsche_911_carrera_4s/" + assimpMaterial.diffuseTextureFileName.value());
          auto texture = engine.createTexture2D(img);
          materialInstance = material->createInstance();
          materialInstance->setProperty("textured", true);
          materialInstance->setTexture("diffuseTexture", texture);
        }
        else
        {
          materialInstance = material->createInstance();
          if (assimpMaterial.diffuseColor)
          {
            materialInstance->setProperty("diffuse", *assimpMaterial.diffuseColor);
          }
        }
      }
      else
      {
        materialInstance = material->createInstance();
        if (assimpMaterial.diffuseColor)
        {
          materialInstance->setProperty("diffuse", *assimpMaterial.diffuseColor);
        }
      }

      if (materialInstance)
      {
        auto entity = engine.createEntity(geometry, materialInstance);
        entity->setTransformation(mesh.transformation);
        m_scene->addEntity(entity);
      }
    }

    m_view = engine.getScreenView();
    m_view->setScene(m_scene);

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.5f, 0.5f, 0.5f));
    camera.setCenter(glm::vec3(0.f, 0.f, 0.f));
    camera.setFieldOfView(45.);
  }

  void draw(VulkanEngine& engine) override
  {
    engine.draw();
  }

  void onResize(VulkanEngine& engine, int width, int height) override
  {
    auto& camera = m_view->getCamera();
    if (width > 0 && height > 0)
    {
      camera.setAspectRatio(width / (float) height);
    }
  }

private:
  WeakRef<View> m_view;
  WeakRef<Scene> m_scene;
};

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

  QVulkanInstance inst;
  inst.setApiVersion(QVersionNumber(1, 2));
  inst.setLayers({"VK_LAYER_KHRONOS_validation"});

  if (!inst.create())
    qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

  QtModelViewer renderer;
  QtVulkanWindow window(renderer, 8);
  window.setVulkanInstance(&inst);

  window.resize(1024, 768);
  window.show();

  return app.exec();
}
