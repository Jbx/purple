#include "engine/engine.h"

#if defined PURPLE_SUPPORT_VULKAN

#include "render/vulkan/vulkan_renderer.h"
#include "glfw/glfw_vulkan_window.h"

GLFWWindowOptions modify(GLFWWindowOptions const& options)
{
  auto o = options;
  o.title += " - Vulkan Renderer";
  return o;
}

class SampleWindow : public GLFWVulkanWindow
{
public:
  SampleWindow(GLFWWindowOptions const& options)
    : GLFWVulkanWindow(modify(options))
    , m_renderer(*this)
    , m_engine(m_renderer)
  {
  }

protected:
  VulkanRenderer m_renderer;
  Engine<VulkanRenderer> m_engine;
};

#elif defined PURPLE_SUPPORT_OPENGL

#include "render/opengl/opengl_renderer.h"
#include "glfw/glfw_opengl_window.h"

GLFWWindowOptions modify(GLFWWindowOptions const& options)
{
  auto o = options;
  o.title += " - OpenGL Renderer";
  return o;
}

class SampleWindow : public GLFWOpenGLWindow
{
public:
  SampleWindow(GLFWWindowOptions const& options)
    : GLFWOpenGLWindow(modify(options))
    , m_renderer(*this)
    , m_engine(m_renderer)
  {
  }

protected:
  OpenGLRenderer m_renderer;
  Engine<OpenGLRenderer> m_engine;
};

#endif
