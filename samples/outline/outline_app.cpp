#include "gui/glfw/glfw_vulkan_window.h"
#include "render/vulkan/vulkan_renderer.h"
#include "core/engine.h"

// STL
#include <iostream>
#include <memory>

int main()
{
  GLFWVulkanWindow<VulkanRenderer> window("EngineApp", 4);
  Engine engine(window.getRenderer());

  auto silhouetteScene = engine.createScene();
  auto sphere = engine.createGeometry();
  auto silhouetteShaderProgram = engine.createShaderProgram("", "");
  auto silhouetteMaterial = engine.createMaterial(
        silhouetteShaderProgram,
        AttributesDescription{},
        RenderStates{},
        ParameterSet({}));

  auto silhouetteEntity = engine.createEntity(sphere, silhouetteMaterial);
  silhouetteScene->addEntity(silhouetteEntity);

  auto silhouetteRenderTarget = engine.createRenderTarget();
  auto silhouetteRenderTargetOutput = silhouetteRenderTarget->createOutput();

  auto silhouetteView = engine.createView(silhouetteRenderTarget);
  silhouetteView->setScene(silhouetteScene);

  auto scene = engine.createScene();
  auto shaderProgram = engine.createShaderProgram("", "");
  auto material = engine.createMaterial(
        shaderProgram,
        AttributesDescription{},
        RenderStates{},
        ParameterSet({}));
  auto entity = engine.createEntity(sphere, material);
  scene->addEntity(entity);

  auto screenQuad = engine.createGeometry();
  auto outlineShaderProgram = engine.createShaderProgram("", "");
  auto outlineMaterial = engine.createMaterial(
        outlineShaderProgram,
        AttributesDescription{},
        RenderStates{},
        ParameterSet({/*{"silhouette", Parameter{silhouetteRenderTargetOutput}}*/}));

  auto outlineEntity = engine.createEntity(screenQuad, outlineMaterial);
  scene->addEntity(outlineEntity);

  auto view = engine.createView();
  view->setScene(scene);

  try {
    window.run();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
