#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/ktx_image.h"
#include "images/stb_image.h"
#include "materials/skybox_material.h"
#include "images/stb_cube_map_image.h"

std::vector<glm::vec3> createBox(float xSize, float ySize, float zSize)
{
  float xHalfSize = xSize / 2.0f;
  float yHalfSize = ySize / 2.0f;
  float zHalfSize = zSize / 2.0f;

  return  {
    {-xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, -zHalfSize},
    {+xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, -zHalfSize},
    {-xHalfSize, -yHalfSize, -zHalfSize},
    {-xHalfSize, +yHalfSize, -zHalfSize},

    {-xHalfSize, -yHalfSize, +zHalfSize},
    {+xHalfSize, -yHalfSize, +zHalfSize},
    {+xHalfSize, +yHalfSize, +zHalfSize},
    {+xHalfSize, +yHalfSize, +zHalfSize},
    {-xHalfSize, +yHalfSize, +zHalfSize},
    {-xHalfSize, -yHalfSize, +zHalfSize},

    {-xHalfSize, +yHalfSize, +zHalfSize},
    {-xHalfSize, +yHalfSize, -zHalfSize},
    {-xHalfSize, -yHalfSize, -zHalfSize},
    {-xHalfSize, -yHalfSize, -zHalfSize},
    {-xHalfSize, -yHalfSize, +zHalfSize},
    {-xHalfSize, +yHalfSize, +zHalfSize},

    {+xHalfSize, +yHalfSize, +zHalfSize},
    {+xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, -zHalfSize},
    {+xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, +zHalfSize},
    {+xHalfSize, -yHalfSize, +zHalfSize},

    {-xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, -yHalfSize, -zHalfSize},
    {+xHalfSize, -yHalfSize, +zHalfSize},
    {+xHalfSize, -yHalfSize, +zHalfSize},
    {-xHalfSize, -yHalfSize, +zHalfSize},
    {-xHalfSize, -yHalfSize, -zHalfSize},

    {-xHalfSize, +yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, +zHalfSize},
    {+xHalfSize, +yHalfSize, -zHalfSize},
    {+xHalfSize, +yHalfSize, +zHalfSize},
    {-xHalfSize, +yHalfSize, -zHalfSize},
    {-xHalfSize, +yHalfSize, +zHalfSize},
  };
}

class CubeMapWindow : public SampleWindow
{
public:
  CubeMapWindow()
    : SampleWindow(GLFWWindowOptions{.title = "CubeMapApp", .vsync = false})
  {
    StbImage px = StbImage("../resources/textures/cube/Park3Med/px.jpg");
    StbImage nx = StbImage("../resources/textures/cube/Park3Med/nx.jpg");
    StbImage py = StbImage("../resources/textures/cube/Park3Med/ny.jpg");
    StbImage ny = StbImage("../resources/textures/cube/Park3Med/py.jpg");
    StbImage pz = StbImage("../resources/textures/cube/Park3Med/pz.jpg");
    StbImage nz = StbImage("../resources/textures/cube/Park3Med/nz.jpg");

    StbCubeMapImage cubemap0 = StbCubeMapImage({px, nx, py, ny, pz, nz});

    KtxImage cubemap1 = KtxImage("../resources/textures/cube/pisa_cube.ktx");
    KtxImage cubemap2 = KtxImage("../resources/textures/cube/cubemap_yokohama_bc3_unorm.ktx");
    KtxImage cubemap3 = KtxImage("../resources/textures/cube/papermill.ktx");
    KtxImage cubemap4 = KtxImage("../resources/textures/cube/papermill.ktx2");

    m_entities = {
      createSkyBox(cubemap0, 2.0, 2.2),
      createSkyBox(cubemap1, 2.0, 2.2),
      createSkyBox(cubemap2, 1.5, 1.2),
      createSkyBox(cubemap3, 4.5, 2.2),
      createSkyBox(cubemap4, 4.5, 2.2),
    };

    m_scene = m_engine.createScene();
    m_scene->addEntity(m_entities.front());

    m_view = m_engine.getScreenView();
    m_view->setClearColor(Color{0.0, 0.0, 0.0, 1.0});
    m_view->setScene(m_scene);

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.f, 0.f, 0.f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 1.0f));
    camera.setAspectRatio(getWidth() / (float) getHeight());
  }

  WeakRef<Entity> createSkyBox(
      IImage const& image,
      float exposure,
      float gamma)
  {
    auto material = m_engine.createMaterial<SkyboxMaterial>(m_engine.createTextureCube(image));
    material->setProperty("exposure", exposure);
    material->setProperty("gamma", gamma);
    auto geometry = m_engine.createGeometry({Vec3}, createBox(1.f, 1.f, 1.f));
    return m_engine.createEntity(geometry, material->defaultInstance());
  }

  void drawFrame() override
  {
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    auto& camera = m_view->getCamera();
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void onMouseButton(int button, int action, int mods) override
  {
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
      m_mouseButtons.right = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    {
      m_mouseButtons.right = false;
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
      m_mouseButtons.left = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
      m_mouseButtons.left = false;
    }

    if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
    {
      m_mouseButtons.middle = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_RELEASE)
    {
      m_mouseButtons.middle = false;
    }
  }

  void onMouseMove(double x, double y) override
  {
    float dx = m_mousePos.x - x;
    float dy = m_mousePos.y - y;

    auto& camera = m_view->getCamera();

    if (m_mouseButtons.left)
    {
      const float rotationSpeed = 0.2;
      camera.pan(glm::radians(-dx * rotationSpeed), glm::vec3(0.f, 1.f, 0.f));
      camera.tilt(glm::radians(dy * rotationSpeed));
    }

    if (m_mouseButtons.right)
    {
      camera.translate(glm::vec3(0.0f, 0.0f, dy * .005f));
    }

    if (m_mouseButtons.middle)
    {
      camera.translate(glm::vec3(-dx * 0.01f, -dy * 0.01f, 0.0f));
    }

    m_mousePos = glm::vec2((float)x, (float)y);
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_R && action == GLFW_PRESS)
    {
      static int i = 0;
      m_scene->removeEntity(m_entities[i]);
      i = (i + 1) % m_entities.size();
      m_scene->addEntity(m_entities[i]);
    }

    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize({1280, 720});
      }
    }
  }

private:
  WeakRef<View> m_view;

  struct
  {
    bool left = false;
    bool right = false;
    bool middle = false;
  } m_mouseButtons;

  glm::vec2 m_mousePos;

  WeakRef<Scene> m_scene;
  std::vector<WeakRef<Entity>> m_entities;
};

int main()
{
  GLFWApp app;
  CubeMapWindow window;
  window.run();
}
