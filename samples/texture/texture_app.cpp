#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/stb_image.h"

// STL
#include <chrono>

class TextureWindow : public SampleWindow
{
public:
  TextureWindow()
    : SampleWindow(GLFWWindowOptions{.title = "TextureApp"})
  {
    struct Vertex
    {
      glm::vec3 pos;
      glm::vec2 uv;
    };

    const std::vector<Vertex> vertices = {
      {{-0.5f, -0.5f, 0.0}, {1.0f, 0.0f}},
      {{0.5f, -0.5f, 0.0}, {0.0f, 0.0f}},
      {{0.5f, 0.5f, 0.0}, {0.0f, 1.0f}},
      {{-0.5f, 0.5f, 0.0}, {1.0f, 1.0f}}
    };

    const std::vector<unsigned int> indices = {
      0, 1, 2, 2, 3, 0
    };

    auto quad = m_engine.createGeometry({Vec3, Vec2}, vertices, indices);

    Shader shader(
      ShaderProgram(
        "../resources/shaders/multi_texture.vert.json",
        "../resources/shaders/multi_texture.frag.json"),
      {Vec3, Vec2});

    StbImage img1("../resources/textures/bubble.png");
    auto texture1 = m_engine.createTexture2D(img1);

    StbImage img2("../resources/textures/halo.png");
    auto texture2 = m_engine.createTexture2D(img2);

    MaterialDefinition materialDef;
    materialDef.renderStates.depthTestEnabled = false;
    materialDef.renderStates.blendingEnabled = true;
    materialDef.textures = {{"texture1", texture1}, {"texture2", texture2}};

    auto material = m_engine.createMaterial(
          shader,
          materialDef);

    auto scene = m_engine.createScene();

    m_entity = m_engine.createEntity(quad, material->defaultInstance());
    scene->addEntity(m_entity);

    m_view = m_engine.getScreenView();
    m_view->setClearColor(Color{0.0, 0.0, 0.1, 1.0});
    m_view->setScene(scene);

    updateCamera();
    updatePositions();
  }

  void drawFrame() override
  {
    updatePositions();
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    updateCamera();
  }

  void updateCamera()
  {
    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(2.0f, 0.0f, 2.0f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));
    camera.setUp(glm::vec3(0.f, 0.f, 1.f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void updatePositions()
  {
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    auto model = glm::rotate(time * glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    m_entity->setTransformation(model);
  }

private:
  WeakRef<Entity> m_entity;
  WeakRef<View> m_view;
};

int main()
{
  GLFWApp app;
  TextureWindow window;
  window.run();
}
