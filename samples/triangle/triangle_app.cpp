#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "materials/per_vertex_color_material.h"
#include "math/math.h"

// STL
#include <chrono>

class TriangleWindow : public SampleWindow
{
public:
  TriangleWindow()
    : SampleWindow(GLFWWindowOptions{.title = "TriangleApp"})
  {
    struct Vertex
    {
      glm::vec3 pos;
      glm::vec4 color;
    };

    const std::vector<Vertex> vertices = {
      {{0.0f, 0.5f, 0.0f}, {1.0f, 0.0f, 0.0f, 1.0f}},
      {{-0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f, 1.0f}},
      {{0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f, 1.0f}}
    };

    auto triangle = m_engine.createGeometry({AttributeType::Vec3, AttributeType::Vec4}, vertices);

    auto material = m_engine.createMaterial<PerVertexColorMaterial>();
    auto materialLine = m_engine.createMaterial<PerVertexColorMaterial>(RenderStates{.polygonMode = PolygonMode::Line});

    auto scene = m_engine.createScene();

    m_entity = m_engine.createEntity(triangle, material->defaultInstance());
    scene->addEntity(m_entity);

    m_entityLine = m_engine.createEntity(triangle, materialLine->defaultInstance());
    scene->addEntity(m_entityLine);

    m_view = m_engine.getScreenView();
    m_view->setScene(scene);
    updateCamera();
    updatePositions();
  }

  void drawFrame() override
  {
    updatePositions();
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    updateCamera();
  }

  void updateCamera()
  {
    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(2.0f, 2.0f, 2.0f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));
    camera.setUp(glm::vec3(0.f, 0.f, 1.f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void updatePositions()
  {
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    auto model1 = glm::rotate(time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    auto model2 = model1 * glm::scale(glm::vec3(0.9, 0.9, 0.9));

    m_entity->setTransformation(model2);
    m_entityLine->setTransformation(model1);
  }

private:
  WeakRef<Entity> m_entity;
  WeakRef<Entity> m_entityLine;
  WeakRef<View> m_view;
};

int main()
{
  GLFWApp app;
  TriangleWindow window;
  window.run();
}
