#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "assimp/assimp_importer.h"
#include "utils/timer.h"

const int shadowMapSize = 1024;
const float zNear = 0.1f;
const float zFar = 1024.0f;
glm::vec4 lightPos(0.0f, 12.5f, 0.0f, 1.0);

class PointShadowMappingWindow : public SampleWindow
{
public:
  PointShadowMappingWindow()
    : SampleWindow(
        GLFWWindowOptions{
        .title = "PointShadowMappingApp",
        .size = {1280, 720},
        .sampleCount = 2,
        .vsync = true})
  {
    auto layout = {AssimpImporter::VertexPosition, AssimpImporter::VertexNormal, AssimpImporter::VertexColor};

    auto model = AssimpImporter::import("../resources/models/shadowscene_fire.dae", {.layout = layout, .preTransform = true});

    struct GeometryAndTransformation
    {
      WeakRef<Geometry> geometry;
      glm::mat4 transf;
    };

    std::vector<GeometryAndTransformation> geometries;

    for (auto const& mesh : model.meshes)
    {
      auto geometry = geometries.emplace_back(GeometryAndTransformation{
            m_engine.createGeometry(mesh.layout, mesh.vertices, mesh.indices), mesh.transformation});
    }

    // Offscreen views
    Shader offscreenShader(
          ShaderProgram(
            "../resources/shaders/point_shadow_depth.vert.json",
            "../resources/shaders/point_shadow_depth.frag.json"),
          {Vec3, Vec3, Vec4});

    MaterialDefinition offscreenMaterialDef;
    offscreenMaterialDef.useCommonUniforms = false;

    offscreenMaterialDef.properties = {
      {"model", glm::mat4(1.f)},
      {"view", glm::mat4(1.f)},
      {"lightTransformation", glm::translate(glm::mat4(1.f), glm::vec3(-lightPos.x, -lightPos.y, -lightPos.z))},
      {"projection", glm::perspective(static_cast<float>(Math::Pi) / 2.0f, 1.0f, zNear, zFar)},
      {"lightPos", lightPos},
    };

    m_offscreenMaterial = m_engine.createMaterial(
          offscreenShader,
          offscreenMaterialDef);

    auto attachmentDepth = m_engine.createTexture2D(
          TextureFormat::Depth,
          {shadowMapSize, shadowMapSize},
          false,
          {.wrapModeS = TextureWrapMode::ClampToBorder,
           .wrapModeT = TextureWrapMode::ClampToBorder});

    auto cubeMap = m_engine.createTextureCube(
          TextureFormat::RGBA_16F,
          {shadowMapSize, shadowMapSize},
          false,
          Sampler{
            .minFilter = TextureFilter::Linear,
            .wrapModeS = TextureWrapMode::ClampToBorder,
            .wrapModeT = TextureWrapMode::ClampToBorder,
            .wrapModeR = TextureWrapMode::ClampToBorder
          });

    for (auto face : {
         CubeMapFace::PositiveX,
         CubeMapFace::NegativeX,
         CubeMapFace::PositiveY,
         CubeMapFace::NegativeY,
         CubeMapFace::PositiveZ,
         CubeMapFace::NegativeZ})
    {
      auto view = m_engine.createOffscreenView(
            {shadowMapSize, shadowMapSize},
            {{.texture = cubeMap, .attachmentPoint = AttachmentPoint::Color, .face = face},
             {.texture = attachmentDepth, .attachmentPoint = AttachmentPoint::Depth}});

      auto scene = m_engine.createScene();
      view->setScene(scene);

      glm::mat4 viewMatrix = glm::mat4(1.0f);
      switch (face)
      {
      case CubeMapFace::PositiveX:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        break;
      case CubeMapFace::NegativeX:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        break;
      case CubeMapFace::PositiveY:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        break;
      case CubeMapFace::NegativeY:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        break;
      case CubeMapFace::PositiveZ:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        break;
      case CubeMapFace::NegativeZ:
        viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        break;
      default:
        break;
      }

      auto instance = m_offscreenMaterial->createInstance();
      instance->setProperty("view", viewMatrix);

      for (auto const& geometry : geometries)
      {
        auto entity = m_engine.createEntity(geometry.geometry, instance);
        scene->addEntity(entity);
      }
    }

    // Screen view

    Shader shader(
          ShaderProgram(
            "../resources/shaders/point_shadow_scene.vert.json",
            "../resources/shaders/point_shadow_scene.frag.json"),
          {Vec3, Vec3, Vec4});

    MaterialDefinition materialDef;

    materialDef.properties = {
      {"lightPos", lightPos},
    };

    materialDef.textures = {
      {"shadowCubeMap", cubeMap}
    };

    m_material = m_engine.createMaterial(
          shader,
          materialDef);

    m_view = m_engine.getScreenView();
    auto& camera = m_view->getCamera();
    camera.setPosition({-30.f, 20.0f, 30.f});
    camera.setAspectRatio(getWidth() / (float) getHeight());

    auto scene = m_engine.createScene();
    m_view->setScene(scene);

    for (auto const& geometry : geometries)
    {
      auto entity = m_engine.createEntity(geometry.geometry, m_material->defaultInstance());
      scene->addEntity(entity);
    }
  }

  void drawFrame() override
  {
    static Timer timer;
    float time = timer.getElapsedMs() / 10000.f;

    lightPos.x = sin(glm::radians(time * 360.0f)) * 1.0f;
    lightPos.z = cos(glm::radians(time * 360.0f)) * 1.0f;

    m_material->setProperty("lightPos", lightPos);
    m_offscreenMaterial->setProperty("lightTransformation", glm::translate(glm::mat4(1.f), glm::vec3(-lightPos.x, -lightPos.y, -lightPos.z)));
    m_offscreenMaterial->setProperty("lightPos", lightPos);

    auto& camera = m_view->getCamera();
    float speed = m_run ? 0.4f : 0.2f;

    if (m_moveForward)
    {
      camera.translate(+camera.getForward() * speed);
    }

    if (m_moveBackward)
    {
      camera.translate(-camera.getForward() * speed);
    }

    if (m_moveLeft)
    {
      auto quat = glm::angleAxis(glm::radians(+90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    if (m_moveRight)
    {
      auto quat = glm::angleAxis(glm::radians(-90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);

    auto& camera = m_view->getCamera();
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize(SizeI{800, 600});
      }
    }

    if (key == GLFW_KEY_LEFT_SHIFT)
    {
      if (action == GLFW_PRESS) m_run = true;
      if (action == GLFW_RELEASE) m_run = false;
    }

    if (key == GLFW_KEY_W)
    {
      if (action == GLFW_PRESS) m_moveForward = true;
      if (action == GLFW_RELEASE) m_moveForward = false;
    }

    if (key == GLFW_KEY_S)
    {
      if (action == GLFW_PRESS) m_moveBackward = true;
      if (action == GLFW_RELEASE) m_moveBackward = false;
    }

    if (key == GLFW_KEY_A)
    {
      if (action == GLFW_PRESS) m_moveLeft = true;
      if (action == GLFW_RELEASE) m_moveLeft = false;
    }

    if (key == GLFW_KEY_D)
    {
      if (action == GLFW_PRESS) m_moveRight = true;
      if (action == GLFW_RELEASE) m_moveRight = false;
    }
  }

  void onMouseButton(int button, int action, int mods) override
  {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
      m_mouseButtons.left = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
      m_mouseButtons.left = false;
    }
  }

  void onMouseMove(double x, double y) override
  {
    float dx = m_mousePos.x - x;
    float dy = m_mousePos.y - y;

    auto& camera = m_view->getCamera();

    if (m_mouseButtons.left)
    {
      const float rotationSpeed = 0.2;
      camera.pan(glm::radians(-dx * rotationSpeed), glm::vec3(0.f, 1.f, 0.f));
      camera.tilt(glm::radians(dy * rotationSpeed));
    }

    m_mousePos = glm::vec2((float)x, (float)y);
  }

private:
  WeakRef<View> m_view;
  WeakRef<Material> m_offscreenMaterial;
  WeakRef<Material> m_material;

  struct
  {
    bool left = false;
    bool right = false;
    bool middle = false;
  } m_mouseButtons;

  glm::vec2 m_mousePos;
  bool m_moveForward = false;
  bool m_moveBackward = false;
  bool m_moveLeft = false;
  bool m_moveRight = false;
  bool m_run = false;
};

int main()
{
  GLFWApp app;
  PointShadowMappingWindow window;
  window.run();
}
