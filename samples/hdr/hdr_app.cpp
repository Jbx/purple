#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/stb_image.h"
#include "materials/phong_textured_material.h"

std::vector<float> cubeVertices = {
  // back face
  -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
  -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
  -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
  // front face
  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
  1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
  -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
  // left face
  -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
  -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
  -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
  -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
  -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
  -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
  // right face
  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
  1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left
  // bottom face
  -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
  1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
  1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
  -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
  -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
  // top face
  -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
  1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right
  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
  -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
  -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left
};

std::vector<float> quadVertices = {
  // positions        // texture Coords
  -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
  1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
  1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
};

const int shadowMapSize = 2048;

class HDRWindow : public SampleWindow
{
public:
  HDRWindow()
    : SampleWindow(
        GLFWWindowOptions{
        .title = "ShadowMappingWindowApp",
        .size = {1280, 720},
        .sampleCount = 4,
        .vsync = true})
  {
    auto cube = m_engine.createGeometry({Vec3, Vec3, Vec2}, cubeVertices);
    auto quad = m_engine.createGeometry({Vec3, Vec2}, quadVertices);

    m_scene = m_engine.createScene();
    m_screenSpaceScene = m_engine.createScene();

    auto attachmentColor = m_engine.createTexture2D(
          TextureFormat::RGBA_16F,
          {shadowMapSize, shadowMapSize},
          false,
          {.wrapModeS = TextureWrapMode::ClampToEdge, .wrapModeT = TextureWrapMode::ClampToEdge});

    auto attachmentDepth = m_engine.createTexture2D(
          TextureFormat::Depth,
          {shadowMapSize, shadowMapSize},
          false,
          {.wrapModeS = TextureWrapMode::ClampToEdge, .wrapModeT = TextureWrapMode::ClampToEdge});

    m_view = m_engine.createOffscreenView(
          {shadowMapSize, shadowMapSize},
          {{.texture = attachmentColor, .attachmentPoint = AttachmentPoint::Color},
           {.texture = attachmentDepth, .attachmentPoint = AttachmentPoint::Depth}});

    m_view->setScene(m_scene);
    m_view->setClearColor({0.1f, 0.1f, 0.1f, 1.0f});

    std::vector<glm::vec3> lightPositions;
    lightPositions.push_back({0.0f,  0.0f, 49.5f}); // back light
    lightPositions.push_back({-1.4f, -1.9f, 9.0f});
    lightPositions.push_back({0.0f, -1.8f, 4.0f});
    lightPositions.push_back({0.8f, -1.7f, 6.0f});

    std::vector<Color> lightColors;
    lightColors.push_back({200.0f, 200.0f, 200.0f});
    lightColors.push_back({0.1f, 0.0f, 0.0f});
    lightColors.push_back({0.0f, 0.0f, 0.2f});
    lightColors.push_back({0.0f, 0.1f, 0.0f});

    for (unsigned int i = 0; i < lightPositions.size(); i++)
    {
      auto light = m_engine.createLight(Light::Point);
      light->setPosition(lightPositions[i]);
      light->setColor(lightColors[i]);
      light->setLinearAttenuation(0.0);
      light->setQuadraticAttenuation(1.0);
      light->setIntensity(1.0);
      m_scene->addLight(light);
    }

    m_screenSpaceView = m_engine.getScreenView();
    m_screenSpaceView->setScene(m_screenSpaceScene);

    auto woodTexture = m_engine.createTexture2D(
          StbImage("../resources/textures/wood.png"),
          true,
          {.minFilter = TextureFilter::LinearMipMapLinear,
           .wrapModeS = TextureWrapMode::Repeat,
           .wrapModeT = TextureWrapMode::Repeat});

    auto material = m_engine.createMaterial<PhongTexturedMaterial>(woodTexture, RenderStates{.cullMode = CullMode::None});
    material->setProperty("textured", true);

    auto cubeEntity = m_engine.createEntity(cube, material->defaultInstance());
    auto cubeTransformation = glm::mat4(1.0f);
    cubeTransformation = glm::translate(cubeTransformation, glm::vec3(0.0f, 0.0f, 25.0));
    cubeTransformation = glm::scale(cubeTransformation, glm::vec3(2.5f, 2.5f, 27.5f));
    cubeEntity->setTransformation(cubeTransformation);
    m_scene->addEntity(cubeEntity);

    Shader hdrShader(
          ShaderProgram(
            "../resources/shaders/hdr.vert.json",
            "../resources/shaders/hdr.frag.json"),
          {Vec3, Vec2});

    MaterialDefinition hdrMaterialDef;
    hdrMaterialDef.useCommonUniforms = false;
    hdrMaterialDef.renderStates.primitiveTopology = PrimitiveTopology::TriangleStrip;
    hdrMaterialDef.properties = {
      {"enabled", true},
      {"exposure", 1.0f}
    };

    hdrMaterialDef.textures = {
      {"hdrBuffer", attachmentColor}
    };

    auto hdrMaterial = m_engine.createMaterial(
          hdrShader,
          hdrMaterialDef);

    auto quadEntity = m_engine.createEntity(quad, hdrMaterial->defaultInstance());
    m_screenSpaceScene->addEntity(quadEntity);

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.0f, 0.0f, 3.0f));
    camera.setCenter(glm::vec3(0.0f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void drawFrame() override
  {
    auto& camera = m_view->getCamera();
    float speed = m_run ? 0.1f : 0.05f;

    if (m_moveForward)
    {
      camera.translate(+camera.getForward() * speed);
    }

    if (m_moveBackward)
    {
      camera.translate(-camera.getForward() * speed);
    }

    if (m_moveLeft)
    {
      auto quat = glm::angleAxis(glm::radians(+90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    if (m_moveRight)
    {
      auto quat = glm::angleAxis(glm::radians(-90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);

    auto& camera = m_view->getCamera();
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize(SizeI{800, 600});
      }
    }

    if (key == GLFW_KEY_LEFT_SHIFT)
    {
      if (action == GLFW_PRESS) m_run = true;
      if (action == GLFW_RELEASE) m_run = false;
    }

    if (key == GLFW_KEY_W)
    {
      if (action == GLFW_PRESS) m_moveForward = true;
      if (action == GLFW_RELEASE) m_moveForward = false;
    }

    if (key == GLFW_KEY_S)
    {
      if (action == GLFW_PRESS) m_moveBackward = true;
      if (action == GLFW_RELEASE) m_moveBackward = false;
    }

    if (key == GLFW_KEY_A)
    {
      if (action == GLFW_PRESS) m_moveLeft = true;
      if (action == GLFW_RELEASE) m_moveLeft = false;
    }

    if (key == GLFW_KEY_D)
    {
      if (action == GLFW_PRESS) m_moveRight = true;
      if (action == GLFW_RELEASE) m_moveRight = false;
    }
  }

  void onMouseButton(int button, int action, int mods) override
  {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
      m_mouseButtons.left = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
      m_mouseButtons.left = false;
    }
  }

  void onMouseMove(double x, double y) override
  {
    float dx = m_mousePos.x - x;
    float dy = m_mousePos.y - y;

    auto& camera = m_view->getCamera();

    if (m_mouseButtons.left)
    {
      const float rotationSpeed = 0.2;
      camera.pan(glm::radians(-dx * rotationSpeed), glm::vec3(0.f, 1.f, 0.f));
      camera.tilt(glm::radians(dy * rotationSpeed));
    }

    m_mousePos = glm::vec2((float)x, (float)y);
  }

private:  
  WeakRef<View> m_view;
  WeakRef<Scene> m_scene;

  WeakRef<View> m_screenSpaceView;
  WeakRef<Scene> m_screenSpaceScene;

  struct
  {
    bool left = false;
    bool right = false;
    bool middle = false;
  } m_mouseButtons;

  glm::vec2 m_mousePos;
  bool m_moveForward = false;
  bool m_moveBackward = false;
  bool m_moveLeft = false;
  bool m_moveRight = false;
  bool m_run = false;
};

int main()
{
  GLFWApp app;
  HDRWindow window;
  window.run();
}
