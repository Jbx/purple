#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/stb_image.h"

//#define DISPLAY_DEBUG_QUAD

std::vector<float> cubeVertices = {
  // back face
  -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
  -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
  -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
  // front face
  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
  1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
  -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
  -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
  // left face
  -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
  -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
  -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
  -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
  -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
  -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
  // right face
  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
  1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
  1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left
  // bottom face
  -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
  1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
  1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
  1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
  -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
  -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
  // top face
  -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
  1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
  1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right
  1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
  -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
  -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left
};

std::vector<float> planeVertices = {
  // positions            // normals         // texcoords
  25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
  -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
  -25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,

  25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
  25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f,
  -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
};

std::vector<float> quadVertices = {
  // positions        // texture Coords
  -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
  1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
  1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
};

const int shadowMapSize = 2048;

class ShadowMappingWindow : public SampleWindow
{
public:
  ShadowMappingWindow()
    : SampleWindow(
        GLFWWindowOptions{
        .title = "ShadowMappingWindowApp",
        .size = {1280, 720},
        .sampleCount = 4,
        .vsync = true})
  {
    auto cube = m_engine.createGeometry({Vec3, Vec3, Vec2}, cubeVertices);
    auto plane = m_engine.createGeometry({Vec3, Vec3, Vec2}, planeVertices);
    auto quad = m_engine.createGeometry({Vec3, Vec3}, quadVertices);

    m_scene = m_engine.createScene();
    m_lightScene = m_engine.createScene();

    auto attachmentDepth = m_engine.createTexture2D(
          TextureFormat::Depth,
          {shadowMapSize, shadowMapSize},
          false,
          {.wrapModeS = TextureWrapMode::ClampToEdge, .wrapModeT = TextureWrapMode::ClampToEdge});

    m_lightView = m_engine.createOffscreenView(
          {shadowMapSize, shadowMapSize},
          {{.texture = attachmentDepth, .attachmentPoint = AttachmentPoint::Depth}});

    m_lightView->setScene(m_lightScene);

    m_view = m_engine.getScreenView();
    m_view->setScene(m_scene);
    m_view->setClearColor({0.1f, 0.1f, 0.1f, 1.0f});

    const float near = 1.0f, far = 7.5f;

    auto& lightCamera = m_lightView->getCamera();
    lightCamera.setPosition(glm::vec3(-2.0f, 4.0f, -1.0f));
    lightCamera.setProjectionType(Camera::ProjectionType::Orthographic);
    lightCamera.setOrtho(-10.0f, 10.0f, -10.0f, 10.0f, near, far);
    lightCamera.setCenter(glm::vec3(0.0f));

    Shader depthShader(
      ShaderProgram(
        "../resources/shaders/shadow_mapping_depth.vert.json",
        "../resources/shaders/shadow_mapping_depth.frag.json"),
      {Vec3, Vec3, Vec2});

    MaterialDefinition depthMaterialDef;
    depthMaterialDef.renderStates.colorWriteEnabled = false;
    depthMaterialDef.renderStates.depthBiasEnabled = true;
    depthMaterialDef.renderStates.depthBiasSlopeFactor = 2.0f;
    auto depthMaterial = m_engine.createMaterial(depthShader, depthMaterialDef);
    m_depthMaterialInstance = depthMaterial->defaultInstance();

    auto woodTexture = m_engine.createTexture2D(
          StbImage("../resources/textures/wood.png"),
          true,
          {.minFilter = TextureFilter::LinearMipMapLinear,
           .magFilter = TextureFilter::Linear,
           .wrapModeS = TextureWrapMode::Repeat,
           .wrapModeT = TextureWrapMode::Repeat});

    auto metalTexture = m_engine.createTexture2D(
          StbImage("../resources/textures/metal.png"),
          true,
          {.minFilter = TextureFilter::LinearMipMapLinear,
           .magFilter = TextureFilter::Linear,
           .wrapModeS = TextureWrapMode::Repeat,
           .wrapModeT = TextureWrapMode::Repeat});

    Shader shadowMappingShader(
      ShaderProgram(
        "../resources/shaders/shadow_mapping.vert.json",
        "../resources/shaders/shadow_mapping.frag.json"),
      {Vec3, Vec3, Vec2});

    MaterialDefinition shadowMappingMaterialDef;
    shadowMappingMaterialDef.properties = {
      {"lightTransformation", lightCamera.getViewProjectionMatrix()},
      {"lightPosition", lightCamera.getPosition()},
      {"shadowMapSize", glm::vec2(shadowMapSize, shadowMapSize)}
    };

    shadowMappingMaterialDef.textures = {{"diffuseTexture", woodTexture}, {"shadowMap", attachmentDepth}};

    auto shadowMappingMaterial = m_engine.createMaterial(shadowMappingShader, shadowMappingMaterialDef);
    auto shadowMappingWoodMaterialInstance = shadowMappingMaterial->createInstance();
    auto shadowMappingMetalMaterialInstance = shadowMappingMaterial->createInstance();

    shadowMappingMetalMaterialInstance->setTexture("diffuseTexture", metalTexture);

    // create entities
    auto planeEntity = createEntity(plane, shadowMappingWoodMaterialInstance);

    auto cube1Entity = createEntity(cube, shadowMappingMetalMaterialInstance);
    auto cube1Transformation = glm::mat4(1.0f);
    cube1Transformation = glm::translate(cube1Transformation, glm::vec3(0.0f, 1.5f, 0.0));
    cube1Transformation = glm::scale(cube1Transformation, glm::vec3(0.5f));
    cube1Entity.setTransformation(cube1Transformation);

    auto cube2Entity = createEntity(cube, shadowMappingMetalMaterialInstance);
    auto cube2Transformation = glm::mat4(1.0f);
    cube2Transformation = glm::translate(cube2Transformation, glm::vec3(2.0f, 0.0f, 1.0f));
    cube2Transformation = glm::scale(cube2Transformation, glm::vec3(0.5f));
    cube2Entity.setTransformation(cube2Transformation);

    auto cube3Entity = createEntity(cube, shadowMappingMetalMaterialInstance);
    auto cube3Transformation = glm::mat4(1.0f);
    cube3Transformation = glm::translate(cube3Transformation, glm::vec3(-1.0f, 0.0f, 2.0f));
    cube3Transformation = glm::rotate(cube3Transformation, glm::radians(60.0f), glm::normalize(glm::vec3(1.0, 0.0, 1.0)));
    cube3Transformation = glm::scale(cube3Transformation, glm::vec3(0.25f));
    cube3Entity.setTransformation(cube3Transformation);

#ifdef DISPLAY_DEBUG_QUAD
    Shader debugQuadShader(
          ShaderProgram(
            "../resources/shaders/debug_quad.vert.json",
            "../resources/shaders/debug_quad_depth.frag.json"),
          {Vec3, Vec2});

    MaterialDefinition debugQuadMaterialDef;
    debugQuadMaterialDef.useCommonUniforms = false;
    debugQuadMaterialDef.renderStates.primitiveTopology = PrimitiveTopology::TriangleStrip;
    debugQuadMaterialDef.properties = {{"near", near}, {"far", far}};
    debugQuadMaterialDef.textures = {{"depthMap", attachmentDepth}};

    auto debugQuadMaterial = m_engine.createMaterial(
          debugQuadShader,
          debugQuadMaterialDef);

    auto quadEntity = m_engine.createEntity(quad, debugQuadMaterial->defaultInstance());
    m_scene->addEntity(quadEntity);

#endif // DISPLAY_DEBUG_QUAD

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.0f, 0.0f, 3.0f));
    camera.setCenter(glm::vec3(0.0f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void drawFrame() override
  {
    auto& camera = m_view->getCamera();
    float speed = m_run ? 0.1f : 0.05f;

    if (m_moveForward)
    {
      camera.translate(+camera.getForward() * speed);
    }

    if (m_moveBackward)
    {
      camera.translate(-camera.getForward() * speed);
    }

    if (m_moveLeft)
    {
      auto quat = glm::angleAxis(glm::radians(+90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    if (m_moveRight)
    {
      auto quat = glm::angleAxis(glm::radians(-90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);

    auto& camera = m_view->getCamera();
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize(SizeI{800, 600});
      }
    }

    if (key == GLFW_KEY_LEFT_SHIFT)
    {
      if (action == GLFW_PRESS) m_run = true;
      if (action == GLFW_RELEASE) m_run = false;
    }

    if (key == GLFW_KEY_W)
    {
      if (action == GLFW_PRESS) m_moveForward = true;
      if (action == GLFW_RELEASE) m_moveForward = false;
    }

    if (key == GLFW_KEY_S)
    {
      if (action == GLFW_PRESS) m_moveBackward = true;
      if (action == GLFW_RELEASE) m_moveBackward = false;
    }

    if (key == GLFW_KEY_A)
    {
      if (action == GLFW_PRESS) m_moveLeft = true;
      if (action == GLFW_RELEASE) m_moveLeft = false;
    }

    if (key == GLFW_KEY_D)
    {
      if (action == GLFW_PRESS) m_moveRight = true;
      if (action == GLFW_RELEASE) m_moveRight = false;
    }
  }

  void onMouseButton(int button, int action, int mods) override
  {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
      m_mouseButtons.left = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
      m_mouseButtons.left = false;
    }
  }

  void onMouseMove(double x, double y) override
  {
    float dx = m_mousePos.x - x;
    float dy = m_mousePos.y - y;

    auto& camera = m_view->getCamera();

    if (m_mouseButtons.left)
    {
      const float rotationSpeed = 0.2;
      camera.pan(glm::radians(-dx * rotationSpeed), glm::vec3(0.f, 1.f, 0.f));
      camera.tilt(glm::radians(dy * rotationSpeed));
    }

    m_mousePos = glm::vec2((float)x, (float)y);
  }

  struct AppEntity
  {
    WeakRef<Entity> entity;
    WeakRef<Entity> castShadowEntity;

    void setTransformation(glm::mat4 const& transformation)
    {
      if (entity)
      {
        entity->setTransformation(transformation);
      }
      if (castShadowEntity)
      {
        castShadowEntity->setTransformation(transformation);
      }
    }
  };

  AppEntity createEntity(
      WeakRef<Geometry> const& geometry,
      WeakRef<MaterialInstance> const& materialInstance,
      bool castShadow = true)
  {
    AppEntity entity;
    entity.entity = m_engine.createEntity(geometry, materialInstance);
    m_scene->addEntity(entity.entity);

    if (castShadow)
    {
      entity.castShadowEntity = m_engine.createEntity(geometry, m_depthMaterialInstance);
      m_lightScene->addEntity(entity.castShadowEntity);
    }
    return entity;
  }

private:
  WeakRef<View> m_lightView;
  WeakRef<View> m_view;
  WeakRef<Scene> m_scene;
  WeakRef<Scene> m_lightScene;
  WeakRef<MaterialInstance> m_depthMaterialInstance;

  struct
  {
    bool left = false;
    bool right = false;
    bool middle = false;
  } m_mouseButtons;

  glm::vec2 m_mousePos;
  bool m_moveForward = false;
  bool m_moveBackward = false;
  bool m_moveLeft = false;
  bool m_moveRight = false;
  bool m_run = false;
};

int main()
{
  GLFWApp app;
  ShadowMappingWindow window;
  window.run();
}
