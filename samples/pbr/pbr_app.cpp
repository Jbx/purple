#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/ktx_image.h"
#include "materials/skybox_material.h"

std::vector<float> cube {
  -0.5f, -0.5f, -0.5f,
  +0.5f, +0.5f, -0.5f,
  +0.5f, -0.5f, -0.5f,
  +0.5f, +0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,
  -0.5f, +0.5f, -0.5f,

  -0.5f, -0.5f, +0.5f,
  +0.5f, -0.5f, +0.5f,
  +0.5f, +0.5f, +0.5f,
  +0.5f, +0.5f, +0.5f,
  -0.5f, +0.5f, +0.5f,
  -0.5f, -0.5f, +0.5f,

  -0.5f, +0.5f, +0.5f,
  -0.5f, +0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,
  -0.5f, -0.5f, +0.5f,
  -0.5f, +0.5f, +0.5f,

  +0.5f, +0.5f, +0.5f,
  +0.5f, -0.5f, -0.5f,
  +0.5f, +0.5f, -0.5f,
  +0.5f, -0.5f, -0.5f,
  +0.5f, +0.5f, +0.5f,
  +0.5f, -0.5f, +0.5f,

  -0.5f, -0.5f, -0.5f,
  +0.5f, -0.5f, -0.5f,
  +0.5f, -0.5f, +0.5f,
  +0.5f, -0.5f, +0.5f,
  -0.5f, -0.5f, +0.5f,
  -0.5f, -0.5f, -0.5f,

  -0.5f, +0.5f, -0.5f,
  +0.5f, +0.5f, +0.5f,
  +0.5f, +0.5f, -0.5f,
  +0.5f, +0.5f, +0.5f,
  -0.5f, +0.5f, -0.5f,
  -0.5f, +0.5f, +0.5,
};

std::vector<float> quad {
  // positions        // texture Coords
  -1.0f, +1.0f, 0.0f, 0.0f, 1.0f,
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
  +1.0f, +1.0f, 0.0f, 1.0f, 1.0f,
  +1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
};

struct Vertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 uv;
};

struct SphereMesh
{
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
};

SphereMesh createSphere()
{
  SphereMesh mesh;

  const unsigned int X_SEGMENTS = 64;
  const unsigned int Y_SEGMENTS = 64;

  for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
  {
    for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
    {
      float xSegment = (float)x / (float)X_SEGMENTS;
      float ySegment = (float)y / (float)Y_SEGMENTS;
      float xPos = std::cos(xSegment * 2.0f * Math::Pi) * std::sin(ySegment * Math::Pi);
      float yPos = std::cos(ySegment * Math::Pi);
      float zPos = std::sin(xSegment * 2.0f * Math::Pi) * std::sin(ySegment * Math::Pi);

      mesh.vertices.push_back(Vertex{
                                {xPos, yPos, zPos},
                                {xPos, yPos, zPos},
                                {xSegment, ySegment}
                              });
    }
  }

  bool oddRow = false;
  for (unsigned int y = 0; y < Y_SEGMENTS; ++y)
  {
    if (!oddRow) // even rows: y == 0, y == 2; and so on
    {
      for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
      {
        mesh.indices.push_back(y * (X_SEGMENTS + 1) + x);
        mesh.indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
      }
    }
    else
    {
      for (int x = X_SEGMENTS; x >= 0; --x)
      {
        mesh.indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
        mesh.indices.push_back(y * (X_SEGMENTS + 1) + x);
      }
    }
    oddRow = !oddRow;
  }
  return mesh;
}

auto sphere = createSphere();

const float zNear = 0.01f;
const float zFar = 1024.0f;

class PBRWindow : public SampleWindow
{
public:
  PBRWindow()
    : SampleWindow(GLFWWindowOptions{.title = "PBRApp", .sampleCount = 4, .vsync = true})
  {
    auto cubeGeometry = m_engine.createGeometry({Vec3}, cube);
    auto quadGeometry = m_engine.createGeometry({Vec3, Vec2}, quad);
    auto sphereGeometry = m_engine.createGeometry({Vec3, Vec3, Vec2}, sphere.vertices, sphere.indices);

    //KtxImage envmap("../resources/textures/cube/papermill.ktx");
    //KtxImage envmap("../resources/textures/cube/cubemap_yokohama_bc3_unorm.ktx");
    KtxImage envmap("../resources/textures/cube/pisa_cube.ktx");

    auto skyboxTexture = m_engine.createTextureCube(
          envmap,
          true,
          {.minFilter = TextureFilter::LinearMipMapLinear,
           .magFilter = TextureFilter::Linear,
           .wrapModeS = TextureWrapMode::ClampToEdge,
           .wrapModeT = TextureWrapMode::ClampToEdge,
           .wrapModeR = TextureWrapMode::ClampToEdge
          });

    // Generate a BRDF integration map used as a look-up-table (stores roughness / NdotV)
    int dim = 512;

    auto lutBRDF = m_engine.createTexture2D(
          TextureFormat::RGBA_16F,
          {dim, dim},
          false,
          {.minFilter = TextureFilter::Linear,
           .magFilter = TextureFilter::Linear,
           .wrapModeS = TextureWrapMode::ClampToEdge,
           .wrapModeT = TextureWrapMode::ClampToEdge,
           .wrapModeR = TextureWrapMode::ClampToEdge
          });

    m_brdfLUTView = m_engine.createOffscreenView(
          {dim, dim},
          {{.texture = lutBRDF, .attachmentPoint = AttachmentPoint::Color}});

    MaterialDefinition materialDef;
    materialDef.useCommonUniforms = false;
    materialDef.renderStates.primitiveTopology = PrimitiveTopology::TriangleStrip;

    auto lutBRDFMaterial = m_engine.createMaterial(
          Shader(
            ShaderProgram(
              "../resources/shaders/gen_brdf_lut.vert.json",
              "../resources/shaders/gen_brdf_lut.frag.json"),
            {Vec3, Vec2}),
          materialDef);

    auto lutBRDFVScene = m_engine.createScene();
    m_brdfLUTView->setScene(lutBRDFVScene);

    auto quadEntity = m_engine.createEntity(quadGeometry, lutBRDFMaterial->defaultInstance());
    lutBRDFVScene->addEntity(quadEntity);

    // Generate irradiance and prefilter cube maps
    WeakRef<ITexture> irradianceCubemap;
    WeakRef<ITexture> prefilterEnvCubemap;

    enum
    {
      Irradiance,
      PrefilterEnv
    };

    for (auto target : {Irradiance, PrefilterEnv})
    {
      int dim;
      TextureFormat format;

      MaterialDefinition materialDef;
      materialDef.useCommonUniforms = false;
      materialDef.renderStates.cullMode = CullMode::None;

      materialDef.textures = {
        {"samplerEnv", skyboxTexture}
      };

      WeakRef<Material> material;

      switch (target)
      {
      case Irradiance:
      {
        dim = 64;
        format = TextureFormat::RGBA_32F;

        materialDef.properties = {
          {"view", glm::mat4(1.f)},
          {"projection", glm::perspective(static_cast<float>(Math::Pi) / 2.0f, 1.0f, zNear, zFar)},
          {"deltaPhi", (2.0f * float(Math::Pi)) / 180.0f},
          {"deltaTheta", (0.5f * float(Math::Pi)) / 64.0f},
        };

        material = m_engine.createMaterial(
              Shader(
                ShaderProgram(
                  "../resources/shaders/irradiance_cube.vert.json",
                  "../resources/shaders/irradiance_cube.frag.json"),
                {Vec3}),
              materialDef);
      } break;

      case PrefilterEnv:
      {
        dim = 512;
        format = TextureFormat::RGBA_16F;

        materialDef.properties = {
          {"view", glm::mat4(1.f)},
          {"projection", glm::perspective(static_cast<float>(Math::Pi) / 2.0f, 1.0f, zNear, zFar)},
          {"roughness", 1.0f}
        };

        material = m_engine.createMaterial(
              Shader(
                ShaderProgram(
                  "../resources/shaders/prefilter_envmap.vert.json",
                  "../resources/shaders/prefilter_envmap.frag.json"),
                {Vec3}),
              materialDef);
      } break;
      }

      auto cubemap = m_engine.createTextureCube(
            format,
            {dim, dim},
            true,
            {.minFilter = TextureFilter::LinearMipMapLinear,
             .magFilter = TextureFilter::Linear,
             .wrapModeS = TextureWrapMode::ClampToEdge,
             .wrapModeT = TextureWrapMode::ClampToEdge,
             .wrapModeR = TextureWrapMode::ClampToEdge
            });

      switch (target)
      {
      case Irradiance:
        irradianceCubemap = cubemap;
        break;
      case PrefilterEnv:
        prefilterEnvCubemap = cubemap;
        break;
      }

      const unsigned int mipLevels = static_cast<uint32_t>(std::floor(std::log2(dim))) + 1;

      for (int m = 0; m < mipLevels; ++m)
      {
        int mipWidth = static_cast<int>(dim * std::pow(0.5, m));
        int mipHeight = static_cast<int>(dim * std::pow(0.5, m));

        for (auto face : {
             CubeMapFace::PositiveX,
             CubeMapFace::NegativeX,
             CubeMapFace::PositiveY,
             CubeMapFace::NegativeY,
             CubeMapFace::PositiveZ,
             CubeMapFace::NegativeZ})
        {
          glm::mat4 viewMatrix = glm::mat4(1.0f);

          switch (face)
          {
          case CubeMapFace::PositiveX:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
            viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            break;
          case CubeMapFace::NegativeX:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
            viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            break;
          case CubeMapFace::PositiveY:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            break;
          case CubeMapFace::NegativeY:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            break;
          case CubeMapFace::PositiveZ:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
            break;
          case CubeMapFace::NegativeZ:
            viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
            break;
          default:
            break;
          }

          auto view = m_cubemapViews.emplace_back(
                m_engine.createOffscreenView(
                  {mipWidth, mipHeight},
                  {{.texture = cubemap, .attachmentPoint = AttachmentPoint::Color, .face = face, .mipLevel = m}}));

          auto instance = material->createInstance();

          switch (target)
          {
          case Irradiance:
            instance->setProperty("view", viewMatrix);
            break;
          case PrefilterEnv:
            instance->setProperty("view", viewMatrix);
            instance->setProperty("roughness", static_cast<float>(m) / static_cast<float>(mipLevels - 1));
            break;
          }

          auto scene = m_engine.createScene();
          view->setScene(scene);

          auto entity = m_engine.createEntity(cubeGeometry, instance);
          scene->addEntity(entity);
        }
      }
    }

    MaterialDefinition pbrMaterialDef;
    pbrMaterialDef.renderStates.primitiveTopology = PrimitiveTopology::TriangleStrip;

    pbrMaterialDef.properties = {
      {"exposure", 4.5f},
      {"gamma", 2.2f},
      {"roughness", 0.15f},
      {"metallic", 0.85f},
      {"specular", 0.f},
      {"albedo", Color::BLACK},
    };

    pbrMaterialDef.textures = {
      {"samplerIrradiance", irradianceCubemap},
      {"samplerBRDFLUT", lutBRDF},
      {"prefilteredMap", prefilterEnvCubemap},
    };

    m_pbrMaterial = m_engine.createMaterial(
          Shader(
            ShaderProgram(
              "../resources/shaders/pbr_ibl.vert.json",
              "../resources/shaders/pbr_ibl.frag.json"),
            {Vec3, Vec3, Vec2}),
          pbrMaterialDef);

    auto scene = m_engine.createScene();

    auto skyboxMaterial = m_engine.createMaterial<SkyboxMaterial>(irradianceCubemap);
    auto skyBoxEntity = m_engine.createEntity(cubeGeometry, skyboxMaterial->defaultInstance());
    auto scale = glm::scale(glm::vec3(300.f));
    skyBoxEntity->setTransformation(scale);
    scene->addEntity(skyBoxEntity);

    for (int i = 0; i < 10; ++i)
    {
      auto instance = m_pbrMaterial->createInstance();
      instance->setProperty("roughness", 1.0f-glm::clamp((float)i / 10.f, 0.005f, 1.0f));
      instance->setProperty("metallic", glm::clamp((float)i / 10.f, 0.005f, 1.0f));

      auto sphereEntity = m_engine.createEntity(sphereGeometry, instance);
      auto transformation = glm::scale(glm::vec3(0.025f));
      transformation = glm::translate(transformation, glm::vec3(-10.0f + i * 2.0, 0.0f, 12.0f));
      sphereEntity->setTransformation(transformation);
      scene->addEntity(sphereEntity);
    }

    const float p = 15.0f;
    auto light1 = m_engine.createLight(Light::Point);
    light1->setPosition({-p, - p * 0.5f, -p});
    scene->addLight(light1);

    auto light2 = m_engine.createLight(Light::Point);
    light2->setPosition({-p, - p * 0.5f, p});
    scene->addLight(light2);

    auto light3 = m_engine.createLight(Light::Point);
    light3->setPosition({p, - p * 0.5f, p});
    scene->addLight(light3);

    auto light4 = m_engine.createLight(Light::Point);
    light4->setPosition({p, - p * 0.5f, -p});
    scene->addLight(light4);

    m_view = m_engine.getScreenView();
    m_view->setClearColor(Color{0.2, 0.2, 0.2, 1.0});
    m_view->setScene(scene);

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.f, 0.f, 0.f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 1.0f));
    camera.setNearPlane(zNear);
    camera.setFarPlane(zFar);
    camera.setAspectRatio(getWidth() / (float) getHeight());
  }

  void drawFrame() override
  {
    auto& camera = m_view->getCamera();
    float speed = m_run ? 0.01f : 0.005f;

    if (m_moveForward)
    {
      camera.translate(+camera.getForward() * speed);
    }

    if (m_moveBackward)
    {
      camera.translate(-camera.getForward() * speed);
    }

    if (m_moveLeft)
    {
      auto quat = glm::angleAxis(glm::radians(+90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    if (m_moveRight)
    {
      auto quat = glm::angleAxis(glm::radians(-90.f), camera.getUp());
      auto dir = quat * camera.getForward();
      camera.translate(dir * speed);
    }

    static bool first = true;
    if (first)
    {
      m_engine.draw({m_brdfLUTView});
      m_engine.draw(m_cubemapViews);

      first = false;
    }
    m_engine.draw({m_view});
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    auto& camera = m_view->getCamera();
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void onMouseButton(int button, int action, int mods) override
  {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
      m_mouseButtons.left = true;
      double x, y;
      glfwGetCursorPos(m_window, &x, &y);
      m_mousePos = {x, y};
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
      m_mouseButtons.left = false;
    }
  }

  void onMouseMove(double x, double y) override
  {
    float dx = m_mousePos.x - x;
    float dy = m_mousePos.y - y;

    auto& camera = m_view->getCamera();

    if (m_mouseButtons.left)
    {
      const float rotationSpeed = 0.2;
      camera.pan(glm::radians(-dx * rotationSpeed), glm::vec3(0.f, 1.f, 0.f));
      camera.tilt(glm::radians(dy * rotationSpeed));
    }

    m_mousePos = glm::vec2((float)x, (float)y);
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize({1280, 720});
      }
    }

    if (key == GLFW_KEY_LEFT_SHIFT)
    {
      if (action == GLFW_PRESS) m_run = true;
      if (action == GLFW_RELEASE) m_run = false;
    }

    if (key == GLFW_KEY_W)
    {
      if (action == GLFW_PRESS) m_moveForward = true;
      if (action == GLFW_RELEASE) m_moveForward = false;
    }

    if (key == GLFW_KEY_S)
    {
      if (action == GLFW_PRESS) m_moveBackward = true;
      if (action == GLFW_RELEASE) m_moveBackward = false;
    }

    if (key == GLFW_KEY_A)
    {
      if (action == GLFW_PRESS) m_moveLeft = true;
      if (action == GLFW_RELEASE) m_moveLeft = false;
    }

    if (key == GLFW_KEY_D)
    {
      if (action == GLFW_PRESS) m_moveRight = true;
      if (action == GLFW_RELEASE) m_moveRight = false;
    }
  }

private:
  WeakRef<View> m_brdfLUTView;
  std::vector<WeakRef<View>> m_cubemapViews;
  WeakRef<View> m_view;
  WeakRef<Material> m_pbrMaterial;

  struct
  {
    bool left = false;
    bool right = false;
    bool middle = false;
  } m_mouseButtons;

  glm::vec2 m_mousePos;
  bool m_moveForward = false;
  bool m_moveBackward = false;
  bool m_moveLeft = false;
  bool m_moveRight = false;
  bool m_run = false;
};

int main()
{
  GLFWApp app;
  PBRWindow window;
  window.run();
}
