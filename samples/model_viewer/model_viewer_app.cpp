#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "images/stb_image.h"
#include "assimp/assimp_importer.h"
#include "materials/phong_textured_material.h"


class ModelWindow : public SampleWindow
{
public:
  ModelWindow()
    : SampleWindow(
        GLFWWindowOptions{
        .title = "ModelApp",
        .size = {1280, 720},
        .sampleCount = 8})
  {
    auto defaultTexture = m_engine.createTexture2D(StbImage("../resources/textures/missing.png"));
    auto material = m_engine.createMaterial<PhongTexturedMaterial>(defaultTexture);

    auto layout = {AssimpImporter::VertexPosition, AssimpImporter::VertexNormal, AssimpImporter::VertexTexCoord};

    // load avocado model
    {
      auto assimpScene = AssimpImporter::import("../resources/models/avocado/Avocado.gltf", {.layout = layout, .normalize = true});
      auto texture = m_engine.createTexture2D(StbImage("../resources/models/avocado/" + *assimpScene.materials.front().baseColorTextureFileName));      
      auto materialInstance = material->createInstance();
      materialInstance->setProperty("textured", true);
      materialInstance->setTexture("diffuseTexture", texture);

      for (auto const& mesh : assimpScene.meshes)
      {
        auto geometry = m_engine.createGeometry(mesh.layout, mesh.vertices, mesh.indices);
        auto entity = m_engine.createEntity(geometry, materialInstance);
        entity->setTransformation(mesh.transformation);
        m_avocadoEntities.push_back(entity);
      }
    }

    // load cerberus model
    {
      auto assimpScene = AssimpImporter::import("../resources/models/cerberus/cerberus.fbx", {.layout = layout, .normalize = true});
      auto texture = m_engine.createTexture2D(StbImage("../resources/models/cerberus/cerberus.png"));
      auto materialInstance = material->createInstance();
      materialInstance->setProperty("textured", true);
      materialInstance->setTexture("diffuseTexture", texture);

      for (auto const& mesh : assimpScene.meshes)
      {
        auto geometry = m_engine.createGeometry(mesh.layout, mesh.vertices, mesh.indices);
        auto entity = m_engine.createEntity(geometry, materialInstance);
        entity->setTransformation(mesh.transformation);
        m_cerberusEntities.push_back(entity);
      }
    }

    m_scene = m_engine.createScene();
    setSceneEntities(m_avocadoEntities);

    auto light = m_engine.createLight(Light::Point);
    light->setPosition(glm::vec3(0.f, 0.f, 4.f));
    light->setColor(Color{0.4, 0.4, 0.4, 1.0});
    light->setIntensity(1.0);
    m_scene->addLight(light);

    m_view = m_engine.getScreenView();
    m_view->setScene(m_scene);
    updateCamera();
  }

  void setSceneEntities(std::vector<WeakRef<Entity>> const& entities)
  {
    auto sceneEntities = m_scene->getEntities();
    for (auto const& entity : sceneEntities)
    {
      m_scene->removeEntity(entity);
    }

    for (auto const& entity : entities)
    {
      m_scene->addEntity(entity);
    }
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_R && action == GLFW_PRESS)
    {
      m_meshIndex++;
      m_meshIndex %= 2;
      std::vector<AssimpImporter::Mesh> meshes;

      if (m_meshIndex == 0)
      {
        setSceneEntities(m_avocadoEntities);
      }
      else if (m_meshIndex == 1)
      {
        setSceneEntities(m_cerberusEntities);
      }
    }

    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize(SizeI{800, 600});
      }
    }

    SampleWindow::onKey(key, scancode, action, mods);
  }

  void drawFrame() override
  {
    if (m_meshIndex == 0)
    {
      updatePositions(m_avocadoEntities);
    }
    else if (m_meshIndex == 1)
    {
      updatePositions(m_cerberusEntities);
    }
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    updateCamera();
  }

  void updateCamera()
  {
    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.75f, 0.75f, 0.75f));
    camera.setCenter(glm::vec3(0.f, 0.f, 0.f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void updatePositions(std::vector<WeakRef<Entity>> entities)
  {
    for (auto const& entity : entities)
    {
      auto model = glm::rotate(entity->getTransformation(), glm::radians(1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
      entity->setTransformation(model);
    }
  }

private:
  std::vector<WeakRef<Entity>> m_avocadoEntities;
  std::vector<WeakRef<Entity>> m_cerberusEntities;
  WeakRef<View> m_view;
  WeakRef<Scene> m_scene;
  int m_meshIndex = 0;
};

int main()
{
  GLFWApp app;
  ModelWindow window;
  window.run();
}
