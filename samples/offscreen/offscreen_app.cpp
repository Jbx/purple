#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "materials/phong_per_vertex_color_material.h"

// STL
#include <iostream>
#include <memory>
#include <chrono>

struct Vertex
{
  glm::vec3 pos;
  Color color;
  glm::vec3 normal;
};

std::vector<Vertex> createColoredBox(float xSize, float ySize, float zSize)
{
  float xHalfSize = xSize / 2.0f;
  float yHalfSize = ySize / 2.0f;
  float zHalfSize = zSize / 2.0f;

  return  {
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},

    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},

    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},

    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},

    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},

    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
  };
}

class OffscreenWindow : public SampleWindow
{
public:
  OffscreenWindow()
    : SampleWindow(
        GLFWWindowOptions{
        .title = "OffscreenApp",
        .size = {1280, 720},
        .sampleCount = 4})
  {
    auto cube = m_engine.createGeometry(
          {Vec3, Vec4, Vec3},
          createColoredBox(2.0, 2.0, 2.0));
    auto phongMaterial = m_engine.createMaterial<PhongPerVertexColorMaterial>();

    auto offscreenScene = m_engine.createScene();

    auto light = m_engine.createLight(Light::Point);
    light->setPosition(glm::vec3(0.f, 0.f, 4.f));
    light->setColor(Color{0.9, 0.6, 0.6, 1.0});
    light->setIntensity(1.0);
    offscreenScene->addLight(light);

    m_offscreenEntity = m_engine.createEntity(cube, phongMaterial->defaultInstance());
    offscreenScene->addEntity(m_offscreenEntity);

    const int offscreenViewSize = 2048;

    auto attachmentColor = m_engine.createTexture2D(
          TextureFormat::RGBA_UNorm,
          {offscreenViewSize, offscreenViewSize},
          false);

    auto attachmentDepth = m_engine.createTexture2D(
          TextureFormat::Depth,
          {offscreenViewSize, offscreenViewSize},
          false);

    m_offscreenView = m_engine.createOffscreenView(
          {offscreenViewSize, offscreenViewSize},
          {{.texture = attachmentColor, .attachmentPoint = AttachmentPoint::Color},
           {.texture = attachmentDepth, .attachmentPoint = AttachmentPoint::Depth}});

    m_offscreenView->setClearColor(Color{0.2, 0.2, 0.2, 1.0});
    m_offscreenView->setScene(offscreenScene);

    m_view = m_engine.getScreenView();
    m_view->setClearColor(Color{0.0, 0.0, 0.0, 1.0});

    Shader shader(
      ShaderProgram(
        "../resources/shaders/texture.vert.json",
        "../resources/shaders/texture.frag.json"),
      {Vec3, Vec2});

    MaterialDefinition materialDef;
    materialDef.textures = {{"tex", attachmentColor}};
    materialDef.renderStates.cullMode = CullMode::None;

    auto material = m_engine.createMaterial(
          shader,
          materialDef);

    auto scene = m_engine.createScene();

    struct Vertex
    {
      glm::vec3 pos;
      glm::vec2 uv;
    };

    const std::vector<Vertex> vertices = {
      {{-0.5f, -0.5f, 0.0}, {1.0f, 0.0f}},
      {{0.5f, -0.5f, 0.0}, {0.0f, 0.0f}},
      {{0.5f, 0.5f, 0.0}, {0.0f, 1.0f}},
      {{-0.5f, 0.5f, 0.0}, {1.0f, 1.0f}}
    };

    const std::vector<unsigned int> indices = {
      0, 1, 2, 2, 3, 0
    };

    auto quad = m_engine.createGeometry({Vec3, Vec2}, vertices, indices);

    m_entity = m_engine.createEntity(quad, material->defaultInstance());
    scene->addEntity(m_entity);

    m_view->setScene(scene);

    updatePositions();

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(1.f, 1.f, 1.f));
    camera.setUp(glm::vec3(0.f, 0.f, 1.f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));    

    auto& offscreenCamera = m_offscreenView->getCamera();
    offscreenCamera.setPosition(glm::vec3(3.f, 3.f, 3.f));
    offscreenCamera.setUp(glm::vec3(0.f, 0.f, 1.f));
    offscreenCamera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));
  }

  void drawFrame() override
  {
    updatePositions();
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    auto& camera = m_view->getCamera();
    camera.setAspectRatio(getWidth() / (float) getHeight());
  }

  void updatePositions()
  {
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    auto model = glm::rotate(time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::rotate(model, time * glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));

    m_offscreenEntity->setTransformation(model);

    model = glm::rotate(time * glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    m_entity->setTransformation(model);
  }

private:
  WeakRef<Entity> m_offscreenEntity;
  WeakRef<View> m_offscreenView;
  WeakRef<Entity> m_entity;
  WeakRef<View> m_view;
};

int main()
{
  GLFWApp app;
  OffscreenWindow window;
  window.run();
}
