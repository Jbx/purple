#include "common/sample_window.h"
#include "glfw/glfw_app.h"
#include "materials/phong_per_vertex_color_material.h"
#include "utils/timer.h"

// STL
#include <iostream>
#include <memory>

struct Vertex
{
  glm::vec3 pos;
  Color color;
  glm::vec3 normal;
};

std::vector<Vertex> createColoredBox(float xSize, float ySize, float zSize)
{
  float xHalfSize = xSize / 2.0f;
  float yHalfSize = ySize / 2.0f;
  float zHalfSize = zSize / 2.0f;

  return  {
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::WHITE, {0.0f, 0.0, -1.0f}},

    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::PURPLE, {0.0f, 0.0, +1.0f}},

    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::RED, {-1.0f, 0.0, 0.0f}},

    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::YELLOW, {+1.0f, 0.0, 0.0f}},

    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{+xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, +zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},
    {{-xHalfSize, -yHalfSize, -zHalfSize}, Color::BLUE, {0.0f, -1.0, 0.0f}},

    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{+xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, -zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
    {{-xHalfSize, +yHalfSize, +zHalfSize}, Color::LIME, {0.0f, +1.0, 0.0f}},
  };
}


class CubeWindow : public SampleWindow
{
public:
  CubeWindow()
    : SampleWindow(GLFWWindowOptions{.title = "CubeApp"})
  {
    auto cube = m_engine.createGeometry(
          {Vec3, Vec4, Vec3},
          createColoredBox(1.0, 1.0, 1.0));

    auto material = m_engine.createMaterial<PhongPerVertexColorMaterial>();

    auto scene = m_engine.createScene();

    m_entity = m_engine.createEntity(cube, material->defaultInstance());
    scene->addEntity(m_entity);

    auto light = m_engine.createLight(Light::Point);
    light->setPosition(glm::vec3(0.f, 0.f, 4.f));
    light->setColor(Color{0.9, 0.6, 0.6, 1.0});
    light->setIntensity(1.0);
    scene->addLight(light);

    m_view = m_engine.getScreenView();
    m_view->setScene(scene);
    updateCamera();
    updatePositions();
  }

  void drawFrame() override
  {
    updatePositions();
    m_engine.draw();
  }

  void onResize(int width, int height) override
  {
    SampleWindow::onResize(width, height);
    updateCamera();
  }

  void onKey(int key, int scancode, int action, int mods) override
  {
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
    {
      static bool fullscreen = false;
      fullscreen = !fullscreen;

      if (fullscreen)
      {
        setSize(getScreenSize(0));
        setFullscreen(true);
      }
      else
      {
        setFullscreen(false);
        setSize(SizeI{800, 600});
      }
    }
  }

  void updateCamera()
  {
    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(0.0f, 0.0f, 2.0f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));
    if (getWidth() > 0 && getHeight() > 0)
    {
      camera.setAspectRatio(getWidth() / (float) getHeight());
    }
  }

  void updatePositions()
  {
    static Timer timer;
    float time = timer.getElapsedMs() / 1000.f;

    auto model = glm::rotate(time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::rotate(model, time * glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    m_entity->setTransformation(model);
  }

private:
  WeakRef<Entity> m_entity;
  WeakRef<View> m_view;
};

int main()
{
  GLFWApp app;
  CubeWindow window;
  window.run();
}
