#include "qt/qt_vulkan_window.h"
#include "images/stb_image.h"

// STL
#include <chrono>

// Qt
#include <QGuiApplication>
#include <QLoggingCategory>


class QtOffscreenRenderer : public IQtVulkanRenderer
{
public:
  void init(VulkanEngine& engine) override
  {
    struct Vertex
    {
      glm::vec3 pos;
      glm::vec2 uv;
    };

    const std::vector<Vertex> vertices = {
      {{-0.5f, -0.5f, 0.0}, {1.0f, 0.0f}},
      {{0.5f, -0.5f, 0.0}, {0.0f, 0.0f}},
      {{0.5f, 0.5f, 0.0}, {0.0f, 1.0f}},
      {{-0.5f, 0.5f, 0.0}, {1.0f, 1.0f}}
    };

    const std::vector<unsigned int> indices = {
      0, 1, 2, 2, 3, 0
    };

    auto quad = engine.createGeometry({AttributeType::Vec3, AttributeType::Vec2}, vertices, indices);

    Shader shader(
      ShaderProgram(
        "../resources/shaders/texture.vert.json",
        "../resources/shaders/texture.frag.json"),
      {Vec3, Vec2});

    StbImage img1("../resources/textures/bubble.png");
    auto texture1 = engine.createTexture2D(img1);

    MaterialDefinition material1Def;
    material1Def.renderStates.blendingEnabled = true;
    material1Def.renderStates.srcAlphaBlendFactor = BlendFactor::SrcAlpha;
    material1Def.renderStates.dstAlphaBlendFactor = BlendFactor::OneMinusSrcAlpha;
    material1Def.textures = {{"texture", texture1}};

    auto material1 = engine.createMaterial(
          shader,
          material1Def);

    auto offscreenScene = engine.createScene();

    m_offscreenEntity = engine.createEntity(quad, material1->defaultInstance());
    offscreenScene->addEntity(m_offscreenEntity);

    const int offscreenViewSize = 1024;

    auto attachmentColor = engine.createTexture2D(
          TextureFormat::RGBA_UNorm,
          {offscreenViewSize, offscreenViewSize},
          false);

    auto attachmentDepth = engine.createTexture2D(
          TextureFormat::Depth,
          {offscreenViewSize, offscreenViewSize},
          false);

    m_offscreenView = engine.createOffscreenView(
          {offscreenViewSize, offscreenViewSize},
          {{.texture = attachmentColor, .attachmentPoint = AttachmentPoint::Color},
           {.texture = attachmentDepth, .attachmentPoint = AttachmentPoint::Depth}});

    m_offscreenView->setClearColor(Color{0.2, 0.2, 0.2, 1.0});
    m_offscreenView->setScene(offscreenScene);

    m_view = engine.getScreenView();
    m_view->setClearColor(Color{0.0, 0.0, 0.0, 1.0});

    MaterialDefinition materialDef;
    materialDef.renderStates.blendingEnabled = true;
    materialDef.textures = {{"texture", attachmentColor}};

    auto material = engine.createMaterial(
          shader,
          materialDef);

    auto scene = engine.createScene();

    m_entity = engine.createEntity(quad, material->defaultInstance());
    scene->addEntity(m_entity);

    m_view->setScene(scene);

    updatePositions();

    auto& camera = m_view->getCamera();
    camera.setPosition(glm::vec3(1.f, 1.f, 1.f));
    camera.setUp(glm::vec3(0.f, 0.f, 1.f));
    camera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));

    auto& offscreenCamera = m_offscreenView->getCamera();
    offscreenCamera.setPosition(glm::vec3(1.f, 1.f, 1.f));
    offscreenCamera.setUp(glm::vec3(0.f, 0.f, 1.f));
    offscreenCamera.setCenter(glm::vec3(0.0f, 0.0f, 0.0f));
  }

  void draw(VulkanEngine& engine) override
  {
    updatePositions();
    engine.draw();
  }

  void onResize(VulkanEngine& engine, int width, int height) override
  {
    auto& camera = m_view->getCamera();
    if (width > 0 && height > 0)
    {
      camera.setAspectRatio(width / (float) height);
    }
  }

  void updatePositions()
  {
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    auto model = glm::rotate(time * glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    m_entity->setTransformation(model);
    m_offscreenEntity->setTransformation(model);
  }

private:
  WeakRef<Entity> m_offscreenEntity;
  WeakRef<View> m_offscreenView;
  WeakRef<Entity> m_entity;
  WeakRef<View> m_view;
};


int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

  QVulkanInstance inst;
  inst.setApiVersion(QVersionNumber(1, 2));
  inst.setLayers({"VK_LAYER_KHRONOS_validation"});

  if (!inst.create())
    qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

  QtOffscreenRenderer renderer;
  QtVulkanWindow window(renderer, 4);
  window.setVulkanInstance(&inst);

  window.resize(1024, 768);
  window.show();

  return app.exec();
}

