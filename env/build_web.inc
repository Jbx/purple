#!/bin/sh

# Build shaders
. ./env/build_shaders.inc

# Source emscripten
ROOT_DIR=$PWD
cd env/tools/emsdk
. ./emsdk_env.sh
cd $ROOT_DIR


