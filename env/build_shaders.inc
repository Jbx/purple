#!/bin/sh

# Build shaders

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGW;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ "${machine}" = "MinGW" ]; then 
  PSB=./tools/psb/psb.exe
elif [ "${machine}" = "Linux" ]; then 
  PSB=./tools/psb/psb
else
  echo "PSB not provided for this platform, try to build it!"
fi

SHADER_DIR=./resources/shaders/

for SHADER_FILE in "$SHADER_DIR"*.vert
do
  ${PSB} $SHADER_FILE $OUTPUT_DIR/resources/shaders
done

for SHADER_FILE in "$SHADER_DIR"*.frag
do
  ${PSB} $SHADER_FILE $OUTPUT_DIR/resources/shaders
done

