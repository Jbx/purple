#!/bin/sh

mkdir -p env/tools
cd env/tools

# EMSDK
EMSDK_VERSION=latest
FOLDER=emsdk
URL=https://github.com/emscripten-core/emsdk.git

if [ ! -d "$FOLDER" ] ; then
	git clone $URL $FOLDER
	cd "$FOLDER"
else
    cd "$FOLDER"
    git pull $URL
fi

./emsdk install $EMSDK_VERSION
./emsdk activate $EMSDK_VERSION
