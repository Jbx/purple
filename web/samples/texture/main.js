function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180;
}


let canvas
let engine
let view
let scene
let quad
let entity

function updateCamera() {
  let camera = view.getCamera()
  camera.position = [2.0, 0.0, 2.0]
  camera.center = [0.0, 0.0, 0.0]
  camera.up = [0.0, 0.0, 1.0]

  if (canvas.width > 0 && canvas.height > 0) {
    camera.aspectRatio = canvas.width / canvas.height
  }
}

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  engine = Purple.WebGLEngine.create(canvas, options)

  const vertices = [
    // position       // tex coord
    -0.5, -0.5, 0.0, 1.0, 0.0,
    0.5, -0.5, 0.0, 0.0, 0.0,
    0.5, 0.5, 0.0, 0.0, 1.0,
    -0.5, 0.5, 0.0, 1.0, 1.0
  ]

  const indices = [
    0, 1, 2, 2, 3, 0
  ]

  quad = engine.createGeometryIndexed(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    vertices, indices)

  scene = engine.createScene()

  view = engine.getScreenView()
  view.clearColor = [0.0, 0.0, 0.1, 1.0]
  view.scene = scene
  updateCamera()

  let texture1 = engine.createTexture2DFromUrl(
    "../../../resources/textures/bubble.png", 
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  let texture2 = engine.createTexture2DFromUrl(
    "../../../resources/textures/halo.png", 
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  const shader = new Purple.Shader(
    "../resources/shaders/multi_texture.vert.json",
    "../resources/shaders/multi_texture.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let materialDefinition = new Purple.MaterialDefinition()
  materialDefinition.depthTestEnabled = false
  materialDefinition.blendingEnabled = true
  materialDefinition.setTexture("texture1", texture1)
  materialDefinition.setTexture("texture2", texture2)

  let material = engine.createMaterial(
    shader,
    materialDefinition)

  entity = engine.createEntity(quad, material.defaultInstance())
  scene.addEntity(entity)

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    if (entity) {
      let model = Purple.createRotationMatrix(now * Math.radians(45.), [0.0, 0.0, 1.0])
      entity.setTransformation(model)
    }

    engine.draw()
    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]
    updateCamera()
  })

  window.addEventListener('keydown', (event) => {
  })

  let lastX, lastY
  let pressed = false

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      if (lastX != event.clientX || lastY != event.clientY) {
        lastX = event.clientX
        lastY = event.clientY
        pressed = true
      }
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      pressed = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    if (pressed) {
      let dX = lastX - event.clientX
      let dY = lastY - event.clientY

      // TODO

      lastX = event.clientX
      lastY = event.clientY
    }
  })

  window.addEventListener('wheel', (event) => {
    let mx = event.clientX
    let my = event.clientY
  })

  requestAnimationFrame(render)
}
