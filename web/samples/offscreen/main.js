function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180;
}

function createColoredBox(xSize, ySize, zSize) {
  let xHalfSize = xSize / 2.0
  let yHalfSize = ySize / 2.0
  let zHalfSize = zSize / 2.0

  return [
    // postion                          // color            // normal
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,

    -xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,

    -xHalfSize, +yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,

    +xHalfSize, +yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,

    -xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,

    -xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
  ]
}

let canvas
let engine
let offscreenEntity
let offscreenView
let view
let entity

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  engine = Purple.WebGLEngine.create(canvas, options)

  let cube = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec4, Purple.AttributeType.Vec3],
    createColoredBox(1.0, 1.0, 1.0))

  let phongMaterial = engine.createPhongPerVertexColorMaterial()

  let offscreenScene = engine.createScene()

  let light = engine.createLight(Purple.LightType.Point)
  light.position = [0.0, 0.0, 4.0]
  light.color = [0.9, 0.6, 0.6, 1.0]
  light.intensity = 1.0
  offscreenScene.addLight(light)

  offscreenEntity = engine.createEntity(cube, phongMaterial.defaultInstance())
  offscreenScene.addEntity(offscreenEntity)

  const depthTextureSize = 1024;

  let attachmentColor = engine.createEmptyTexture2D(
    Purple.TextureFormat.RGBA_UNorm,
    [depthTextureSize, depthTextureSize],
    false,
    Purple.TextureFilter.Linear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  let attachmentDepth = engine.createEmptyTexture2D(
    Purple.TextureFormat.Depth,
    [depthTextureSize, depthTextureSize],
    false,
    Purple.TextureFilter.Linear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  offscreenView = engine.createOffscreenView(
    [depthTextureSize, depthTextureSize],
    [
      {texture: attachmentColor, attachmentPoint: Purple.AttachmentPoint.Color},
      {texture: attachmentDepth, attachmentPoint: Purple.AttachmentPoint.Depth}
    ])

  offscreenView.clearColor = [0.2, 0.2, 0.2, 1.0]
  offscreenView.scene = offscreenScene

  view = engine.getScreenView()
  view.clearColor = [0.0, 0.0, 0.0, 1.0]

  let shader = new Purple.Shader(
    "../resources/shaders/texture.vert.json",
    "../resources/shaders/texture.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let materialDefinition = new Purple.MaterialDefinition()
  materialDefinition.setTexture("texture", attachmentColor)
  materialDefinition.cullMode = Purple.CullMode.None

  let material = engine.createMaterial(
    shader,
    materialDefinition)

  let scene = engine.createScene()

  const vertices = [
    -0.5, -0.5, 0.0, 1.0, 0.0,
    0.5, -0.5, 0.0, 0.0, 0.0,
    0.5, 0.5, 0.0, 0.0, 1.0,
    -0.5, 0.5, 0.0, 1.0, 1.0,
  ]

  const indices = [
    0, 1, 2, 2, 3, 0
  ]

  let quad = engine.createGeometryIndexed(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    vertices, indices)

  entity = engine.createEntity(quad, material.defaultInstance())
  scene.addEntity(entity);
  view.scene = scene

  let camera = view.getCamera()
  camera.position = [1.0, 1.0, 1.0]
  camera.up = [0.0, 0.0, 1.0]
  camera.center = [0.0, 0.0, 0.0]

  let offscreenCamera = offscreenView.getCamera()
  offscreenCamera.position = [3.0, 3.0, 3.0]
  offscreenCamera.up = [0.0, 0.0, 1.0]
  offscreenCamera.center = [0.0, 0.0, 0.0]

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    let offModel = Purple.createRotationMatrix(now * Math.radians(90.), [0.0, 0.0, 1.0])
    offModel = Purple.rotateMatrix(offModel, now * Math.radians(90.), [0.0, 1.0, 0.0])
    offscreenEntity.setTransformation(offModel)

    let model = Purple.createRotationMatrix(now * Math.radians(90.), [1.0, 0.0, 1.0])
    entity.setTransformation(model);

    engine.draw()
    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]

    let camera = view.getCamera()
    if (canvas.width > 0 && canvas.height > 0) {
      camera.aspectRatio = canvas.width / canvas.height
    }
  })

  window.addEventListener('keydown', (event) => {
  })

  let lastX, lastY
  let pressed = false

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      if (lastX != event.clientX || lastY != event.clientY) {
        lastX = event.clientX
        lastY = event.clientY
        pressed = true
      }
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      pressed = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    if (pressed) {
      let dX = lastX - event.clientX
      let dY = lastY - event.clientY

      // TODO

      lastX = event.clientX
      lastY = event.clientY
    }
  })

  window.addEventListener('wheel', (event) => {
    let mx = event.clientX
    let my = event.clientY
  })

  requestAnimationFrame(render)
}
