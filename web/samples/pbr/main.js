function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180
}

Math.clamp = function (num, min, max) {
  return Math.min(Math.max(num, min), max)
}

let cube = [
  -1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, +1.0, -1.0,

  -1.0, -1.0, +1.0,
  +1.0, -1.0, +1.0,
  +1.0, +1.0, +1.0,
  +1.0, +1.0, +1.0,
  -1.0, +1.0, +1.0,
  -1.0, -1.0, +1.0,

  -1.0, +1.0, +1.0,
  -1.0, +1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, -1.0, +1.0,
  -1.0, +1.0, +1.0,

  +1.0, +1.0, +1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, +1.0,
  +1.0, -1.0, +1.0,

  -1.0, -1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, -1.0, +1.0,
  +1.0, -1.0, +1.0,
  -1.0, -1.0, +1.0,
  -1.0, -1.0, -1.0,

  -1.0, +1.0, -1.0,
  +1.0, +1.0, +1.0,
  +1.0, +1.0, -1.0,
  +1.0, +1.0, +1.0,
  -1.0, +1.0, -1.0,
  -1.0, +1.0, +1.0,
]

let quad = [
  // positions     // texture Coords
  -1.0, +1.0, 0.0, 0.0, 1.0,
  -1.0, -1.0, 0.0, 0.0, 0.0,
  +1.0, +1.0, 0.0, 1.0, 1.0,
  +1.0, -1.0, 0.0, 1.0, 0.0,
]

function createSphere() {
  let vertices = []
  let indices = []

  const X_SEGMENTS = 64
  const Y_SEGMENTS = 64

  for (let x = 0; x <= X_SEGMENTS; ++x) {
    for (let y = 0; y <= Y_SEGMENTS; ++y) {
      let xSegment = x / X_SEGMENTS
      let ySegment = y / Y_SEGMENTS
      let xPos = Math.cos(xSegment * 2.0 * Math.PI) * Math.sin(ySegment * Math.PI)
      let yPos = Math.cos(ySegment * Math.PI)
      let zPos = Math.sin(xSegment * 2.0 * Math.PI) * Math.sin(ySegment * Math.PI)

      vertices.push(
        xPos, yPos, zPos,
        xPos, yPos, zPos,
        xSegment, ySegment)
    }
  }

  let oddRow = false
  for (let y = 0; y < Y_SEGMENTS; ++y) {
    if (!oddRow) // even rows: y == 0, y == 2 and so on
    {
      for (let x = 0; x <= X_SEGMENTS; ++x) {
        indices.push(y * (X_SEGMENTS + 1) + x)
        indices.push((y + 1) * (X_SEGMENTS + 1) + x)
      }
    }
    else {
      for (let x = X_SEGMENTS; x >= 0; --x) {
        indices.push((y + 1) * (X_SEGMENTS + 1) + x)
        indices.push(y * (X_SEGMENTS + 1) + x)
      }
    }
    oddRow = !oddRow
  }
  return { vertices, indices }
}

let sphere = createSphere()

const zNear = 0.1
const zFar = 512.0

let canvas
let engine
let first = true
let screenView
let brdfLUTView
let cubemapViews = []

const envmapSize = 512

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  let mouseBtnLeft = false
  let mouseBtnRight = false
  let mousePos = [0, 0]

  engine = Purple.WebGLEngine.create(canvas, options)

  let cubeGeometry = engine.createGeometry(
    [Purple.AttributeType.Vec3],
    cube)

  let quadGeometry = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    quad)

  let sphereGeometry = engine.createGeometryIndexed(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    sphere.vertices, sphere.indices)

  let envmap = Purple.ImageLoader.load("../resources/textures/pisa_cube.ktx")

  let envmapTexture = engine.createTextureCube(
    envmap,
    true,
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  // Generate a BRDF integration map used as a look-up-table (stores roughness / NdotV)
  const dim = 512

  let lutBRDF = engine.createEmptyTexture2D(
    Purple.TextureFormat.RGBA_16F,
    [dim, dim],
    false,
    Purple.TextureFilter.Linear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  brdfLUTView = engine.createOffscreenView(
    [dim, dim],
    [{ texture: lutBRDF, attachmentPoint: Purple.AttachmentPoint.Color }])

  let lutMaterialDef = new Purple.MaterialDefinition()
  lutMaterialDef.useCommonUniforms = false
  lutMaterialDef.primitiveTopology = Purple.PrimitiveTopology.TriangleStrip

  let luBRDFShader = new Purple.Shader(
    "../resources/shaders/gen_brdf_lut.vert.json",
    "../resources/shaders/gen_brdf_lut.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let lutBRDFMaterial = engine.createMaterial(
    luBRDFShader,
    lutMaterialDef)

  let lutBRDFVScene = engine.createScene()
  brdfLUTView.scene = lutBRDFVScene

  let quadEntity = engine.createEntity(quadGeometry, lutBRDFMaterial.defaultInstance())
  lutBRDFVScene.addEntity(quadEntity)

  // Generate irradiance and prefilter cube maps
  let irradianceCubemap
  let prefilterEnvCubemap

  const targets = ["Irradiance", "PrefilterEnv"]

  for (let value in targets) {
    const target = targets[value]

    let dim
    let format

    let materialDef = new Purple.MaterialDefinition()
    materialDef.useCommonUniforms = false
    materialDef.cullMode = Purple.CullMode.None

    materialDef.setTexture("samplerEnv", envmapTexture)

    let material

    switch (target) {
      case "Irradiance":
        dim = 64
        format = Purple.TextureFormat.RGBA_32F

        materialDef.setProperty("view", Purple.PropertyType.Mat4, Purple.createIdentityMatrix())
        materialDef.setProperty("projection", Purple.PropertyType.Mat4, Purple.createPerspectiveMatrix(Math.PI / 2., 1., zNear, zFar))
        materialDef.setProperty("deltaPhi", Purple.PropertyType.Float, (2.0 * Math.PI) / 180.0)
        materialDef.setProperty("deltaTheta", Purple.PropertyType.Float, (0.5 * Math.PI) / 64.0)

        let irradianceShader = new Purple.Shader(
          "../resources/shaders/irradiance_cube.vert.json",
          "../resources/shaders/irradiance_cube.frag.json",
          [Purple.AttributeType.Vec3])

        material = engine.createMaterial(
          irradianceShader,
          materialDef)
        break

      case "PrefilterEnv":
        dim = 512
        format = Purple.TextureFormat.RGBA_16F

        materialDef.setProperty("view", Purple.PropertyType.Mat4, Purple.createIdentityMatrix())
        materialDef.setProperty("projection", Purple.PropertyType.Mat4, Purple.createPerspectiveMatrix(Math.PI / 2., 1., zNear, zFar))
        materialDef.setProperty("roughness", Purple.PropertyType.Float, 1.0)

        let prefilterEnvShader = new Purple.Shader(
          "../resources/shaders/prefilter_envmap.vert.json",
          "../resources/shaders/prefilter_envmap.frag.json",
          [Purple.AttributeType.Vec3])

        material = engine.createMaterial(
          prefilterEnvShader,
          materialDef)
        break
    }

    let cubemap = engine.createEmptyTextureCube(
      format,
      [dim, dim],
      true,
      Purple.TextureFilter.LinearMipMapLinear,
      Purple.TextureFilter.Linear,
      Purple.TextureWrapMode.ClampToEdge)

    switch (target) {
      case "Irradiance":
        irradianceCubemap = cubemap
        break
      case "PrefilterEnv":
        prefilterEnvCubemap = cubemap
        break
    }

    const maxMipLevels = Math.floor(Math.log2(dim)) + 1

    for (let m = 0; m < maxMipLevels; ++m) {
      const mipWidth = dim * Math.pow(0.5, m)
      const mipHeight = dim * Math.pow(0.5, m)

      for (const f of [
        Purple.CubeMapFace.PositiveX,
        Purple.CubeMapFace.NegativeX,
        Purple.CubeMapFace.PositiveY,
        Purple.CubeMapFace.NegativeY,
        Purple.CubeMapFace.PositiveZ,
        Purple.CubeMapFace.NegativeZ]) {

        let viewMatrix = Purple.createIdentityMatrix()

        switch (f) {
          case Purple.CubeMapFace.PositiveX:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(90.0), [0.0, 1.0, 0.0])
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(180.0), [1.0, 0.0, 0.0])
            break
          case Purple.CubeMapFace.NegativeX:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(-90.0), [0.0, 1.0, 0.0])
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(180.0), [1.0, 0.0, 0.0])
            break
          case Purple.CubeMapFace.PositiveY:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(-90.0), [1.0, 0.0, 0.0])
            break
          case Purple.CubeMapFace.NegativeY:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(90.0), [1.0, 0.0, 0.0])
            break
          case Purple.CubeMapFace.PositiveZ:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(180.0), [1.0, 0.0, 0.0])
            break
          case Purple.CubeMapFace.NegativeZ:
            viewMatrix = Purple.rotateMatrix(viewMatrix, Math.radians(180.0), [0.0, 0.0, 1.0])
            break
          default:
            break
        }

        cubemapViews.push(
          engine.createOffscreenView(
            [mipWidth, mipHeight],
            [{ texture: cubemap, attachmentPoint: Purple.AttachmentPoint.Color, face: f, mipLevel: m }])
        )

        let view = cubemapViews[cubemapViews.length - 1]

        let instance = material.createInstance()

        switch (target) {
          case "Irradiance":
            instance.setProperty("view", Purple.PropertyType.Mat4, viewMatrix)
            break
          case "PrefilterEnv":
            instance.setProperty("view", Purple.PropertyType.Mat4, viewMatrix)
            instance.setProperty("roughness", Purple.PropertyType.Float, m / (maxMipLevels - 1))
            break
        }

        let scene = engine.createScene()
        view.scene = scene

        let entity = engine.createEntity(cubeGeometry, instance)
        scene.addEntity(entity)
      }
    }
  }

  let pbrMaterialDef = new Purple.MaterialDefinition()
  pbrMaterialDef.primitiveTopology = Purple.PrimitiveTopology.TriangleStrip

  pbrMaterialDef.setProperty("exposure", Purple.PropertyType.Float, 4.5)
  pbrMaterialDef.setProperty("gamma", Purple.PropertyType.Float, 2.2)
  pbrMaterialDef.setProperty("roughness", Purple.PropertyType.Float, 0.15)
  pbrMaterialDef.setProperty("metallic", Purple.PropertyType.Float, 0.85)
  pbrMaterialDef.setProperty("specular", Purple.PropertyType.Float, 0.)
  pbrMaterialDef.setProperty("albedo", Purple.PropertyType.Color, [0., 0., 0., 1.])

  pbrMaterialDef.setTexture("samplerIrradiance", irradianceCubemap)
  pbrMaterialDef.setTexture("samplerBRDFLUT", lutBRDF)
  pbrMaterialDef.setTexture("prefilteredMap", prefilterEnvCubemap)

  let pbrShader = new Purple.Shader(
    "../resources/shaders/pbr_ibl.vert.json",
    "../resources/shaders/pbr_ibl.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let pbrMaterial = engine.createMaterial(
    pbrShader,
    pbrMaterialDef)

  let scene = engine.createScene()

  let skyboxMaterial = engine.createSkyboxMaterial(irradianceCubemap)
  let skyBoxEntity = engine.createEntity(cubeGeometry, skyboxMaterial.defaultInstance())
  const scale = Purple.createScalingMatrix([300., 300., 300.])
  skyBoxEntity.setTransformation(scale)
  scene.addEntity(skyBoxEntity)

  for (let i = 0; i < 10; ++i) {
    let instance = pbrMaterial.createInstance()
    instance.setProperty("roughness", Purple.PropertyType.Float, 1.0 - Math.clamp(i / 10., 0.005, 1.0))
    instance.setProperty("metallic", Purple.PropertyType.Float, Math.clamp(i / 10., 0.005, 1.0))

    let sphereEntity = engine.createEntity(sphereGeometry, instance)
    let transformation = Purple.createScalingMatrix([0.025, 0.025, 0.025])
    transformation = Purple.translateMatrix(transformation, [-10.0 + i * 2.0, 0.0, 12.0])
    sphereEntity.setTransformation(transformation)
    scene.addEntity(sphereEntity)
  }

  const p = 15.0;
  let light1 = engine.createLight(Purple.LightType.Point)
  light1.position = [-p, - p * 0.5, -p]
  scene.addLight(light1)

  let light2 = engine.createLight(Purple.LightType.Point)
  light2.position = [-p, - p * 0.5, p]
  scene.addLight(light2)

  let light3 = engine.createLight(Purple.LightType.Point)
  light3.position = [p, - p * 0.5, p]
  scene.addLight(light3)

  let light4 = engine.createLight(Purple.LightType.Point)
  light4.position = [p, - p * 0.5, -p]
  scene.addLight(light4)

  screenView = engine.getScreenView()
  screenView.clearColor = [0.2, 0.2, 0.2, 1.0]
  screenView.scene = scene

  let camera = screenView.getCamera()
  camera.position = [0., 0., 0.]
  camera.center = [0., 0., 1.]
  camera.nearPlane = zNear
  camera.farPlane = zFar
  camera.aspectRatio = canvas.width / canvas.height

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    if (first) {
      engine.drawViews([brdfLUTView])
      engine.drawViews(cubemapViews)
      first = false
    }
    engine.drawViews([screenView])

    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]

    let camera = screenView.getCamera()
    if (canvas.width > 0 && canvas.height > 0) {
      camera.aspectRatio = canvas.width / canvas.height
    }
  })

  window.addEventListener('keydown', (event) => {
  })

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = true
      mousePos = [event.clientX, event.clientY]
    }
    if (event.button == 2) {
      mouseBtnRight = true
      mousePos = [event.clientX, event.clientY]
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = false
    }
    if (event.button == 2) {
      mouseBtnRight = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    let dx = mousePos[0] - event.clientX
    let dy = mousePos[1] - event.clientY

    let camera = screenView.getCamera()

    if (mouseBtnLeft) {
      const rotationSpeed = 0.2
      camera.panAboutAxis(Math.radians(-dx * rotationSpeed), [0.0, 1.0, 0.0])
      camera.tilt(Math.radians(dy * rotationSpeed))
    }

    if (mouseBtnRight) {
      camera.translate([0.0, 0.0, dy * .005], true)
    }

    mousePos[0] = event.clientX
    mousePos[1] = event.clientY
  })

  requestAnimationFrame(render)
}
