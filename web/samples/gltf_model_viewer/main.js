function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180
}


Purple.init(() => main(), locateFile)

let canvas
let engine
let renderer

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  let mouseBtnLeft = false
  let mouseBtnRight = false
  let mousePos = [0, 0]

  engine = Purple.WebGLEngine.create(canvas, options)
  
  renderer = Purple.GLTFRenderer.create(engine)
  renderer.load(
    "../resources/textures/pisa_cube.ktx",
    "../resources/models/porsche_911_carrera_4s/scene.gltf")

  let camera = engine.getScreenView().getCamera()
  camera.position = [1., 0., 1.]
  camera.center = [0., 0., 0.]
  camera.nearPlane = 0.01
  camera.farPlane = 512.0
  camera.fieldOfView = Math.radians(45.)
  camera.aspectRatio = canvas.width / canvas.height

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    renderer.draw()

    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]
    let camera = engine.getScreenView().getCamera()
    camera.aspectRatio = canvas.width / canvas.height
  })

  window.addEventListener('keydown', (event) => {
  })

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = true
      mousePos = [event.clientX, event.clientY]
    }
    if (event.button == 2) {
      mouseBtnRight = true
      mousePos = [event.clientX, event.clientY]
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = false
    }
    if (event.button == 2) {
      mouseBtnRight = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    let dx = mousePos[0] - event.clientX
    let dy = mousePos[1] - event.clientY

    let camera = engine.getScreenView().getCamera()

    if (mouseBtnLeft)
    {
      const rotationSpeed = 0.2

      camera.rotateAboutCenter(Purple.createQuaternion(Math.radians(dx * rotationSpeed), [0., 1., 0.]))

      let xAxis = Purple.cross(camera.up, camera.getForward())

      let savedViewUp = camera.up
      camera.rotateAboutCenter(Purple.createQuaternion(Math.radians(-dy * rotationSpeed), xAxis))
      camera.up = savedViewUp
    }

    mousePos[0] = event.clientX
    mousePos[1] = event.clientY
  })

  window.addEventListener('wheel', (event) => {
    let zoomFactor = 0.001 * -event.deltaY

    let camera = engine.getScreenView().getCamera()

    camera.position = Purple.add(camera.position, Purple.mul(camera.getForward(), zoomFactor))
  })

  requestAnimationFrame(render)
}
