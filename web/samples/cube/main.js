function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function(degrees) {
	return degrees * Math.PI / 180;
}

function createColoredBox(xSize, ySize, zSize) {
  let xHalfSize = xSize / 2.0
  let yHalfSize = ySize / 2.0
  let zHalfSize = zSize / 2.0

  return [
    // postion                          // color            // normal
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0,

    -xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 0.5, 0.0, 0.5, 1.0, 0.0, 0.0, +1.0,

    -xHalfSize, +yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 1.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0,

    +xHalfSize, +yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 1.0, 1.0, 0.0, 1.0, +1.0, 0.0, 0.0,

    -xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    +xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    -xHalfSize, -yHalfSize, +zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,
    -xHalfSize, -yHalfSize, -zHalfSize, 0.0, 0.0, 1.0, 1.0, 0.0, -1.0, 0.0,

    -xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    +xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    -xHalfSize, +yHalfSize, -zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
    -xHalfSize, +yHalfSize, +zHalfSize, 0.0, 1.0, 0.0, 1.0, 0.0, +1.0, 0.0,
  ]
}

let canvas
let engine
let view
let entity

function updateCamera() {
  let camera = view.getCamera()
  camera.position = [0.0, 0.0, 2.0]
  camera.center = [0.0, 0.0, 0.0]
  
  if (canvas.width > 0 && canvas.height > 0) {
    camera.aspectRatio = canvas.width / canvas.height
  }  
}

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  engine = Purple.WebGLEngine.create(canvas, options)

  let cube = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec4, Purple.AttributeType.Vec3],
    createColoredBox(1.0, 1.0, 1.0))

  let material = engine.createPhongPerVertexColorMaterial()

  let scene = engine.createScene()

  entity = engine.createEntity(cube, material.defaultInstance())
  scene.addEntity(entity)

  let light = engine.createLight(Purple.LightType.Point)
  light.position = [0.0, 0.0, 4.0]
  light.color = [0.9, 0.6, 0.6, 1.0]
  light.intensity = 1.0
  scene.addLight(light)

  view = engine.getScreenView()
  view.clearColor = [0.0, 0.0, 0.1, 1.0]
  view.scene = scene
  updateCamera()

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now
    
    let model = Purple.createRotationMatrix(now * Math.radians(90.), [0.0, 0.0, 1.0])
    model = Purple.rotateMatrix(model, now * Math.radians(90.), [0.0, 1.0, 0.0])
    entity.setTransformation(model)

    engine.draw()
    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]
    updateCamera()
  })

  window.addEventListener('keydown', (event) => {
  })

  let lastX, lastY
  let pressed = false

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      if (lastX != event.clientX || lastY != event.clientY) {
        lastX = event.clientX
        lastY = event.clientY
        pressed = true
      }
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      pressed = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    if (pressed) {
      let dX = lastX - event.clientX
      let dY = lastY - event.clientY

      // TODO

      lastX = event.clientX
      lastY = event.clientY
    }
  })

  window.addEventListener('wheel', (event) => {
    let mx = event.clientX
    let my = event.clientY
  })

  requestAnimationFrame(render)
}
