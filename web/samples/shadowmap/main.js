function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180
}

const cubeVertices = [
  // back face
  -1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
  1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
  1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
  1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
  -1.0, -1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
  -1.0, 1.0, -1.0, 0.0, 0.0, -1.0, 0.0, 1.0,
  // front face
  -1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
  1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
  1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
  -1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
  -1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
  // left face
  -1.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
  -1.0, 1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 1.0,
  -1.0, -1.0, -1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
  -1.0, -1.0, -1.0, -1.0, 0.0, 0.0, 0.0, 1.0,
  -1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
  -1.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
  // right face
  1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0,
  1.0, -1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0,
  1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 1.0,
  1.0, -1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0,
  1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0,
  1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0,
  // bottom face
  -1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 0.0, 1.0,
  1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 1.0, 1.0,
  1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
  1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0,
  -1.0, -1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0,
  -1.0, -1.0, -1.0, 0.0, -1.0, 0.0, 0.0, 1.0,
  // top face
  -1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 0.0, 1.0,
  1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
  1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
  -1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 0.0, 1.0,
  -1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0
]

const planeVertices = [
  25.0, -0.5, 25.0, 0.0, 1.0, 0.0, 25.0, 0.0,
  -25.0, -0.5, -25.0, 0.0, 1.0, 0.0, 0.0, 25.0,
  -25.0, -0.5, 25.0, 0.0, 1.0, 0.0, 0.0, 0.0,

  25.0, -0.5, 25.0, 0.0, 1.0, 0.0, 25.0, 0.0,
  25.0, -0.5, -25.0, 0.0, 1.0, 0.0, 25.0, 25.0,
  -25.0, -0.5, -25.0, 0.0, 1.0, 0.0, 0.0, 25.0,
]

const quadVertices = [
  -1.0, 1.0, 0.0, 0.0, 1.0,
  -1.0, -1.0, 0.0, 0.0, 0.0,
  1.0, 1.0, 0.0, 1.0, 1.0,
  1.0, -1.0, 0.0, 1.0, 0.0,
]

function loadImage(url) {
  return new Promise((resolve, reject) => {
    const img = new Image()
    img.crossOrigin = "anonymous"
    img.onload = () => resolve(img)
    img.onerror = reject
    img.src = url
  })
}

let canvas
let engine

let cube
let plane
let quad

let lightScene
let scene

let lightView
let view

let depthMaterialInstance

const shadowMapSize = 2048


function createEntity(
  geometry,
  materialInstance) {
  let entity = engine.createEntity(geometry, materialInstance)
  scene.addEntity(entity)

  let castShadowEntity = engine.createEntity(geometry, depthMaterialInstance)
  lightScene.addEntity(castShadowEntity)

  return [entity, castShadowEntity]
}

function setTransformation(entities, transformation) {
  entities.forEach(entity => entity.setTransformation(transformation))
}

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  engine = Purple.WebGLEngine.create(canvas, options)

  cube = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    cubeVertices)

  plane = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2],
    planeVertices)

  quad = engine.createGeometry(
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3],
    quadVertices)

  scene = engine.createScene()
  lightScene = engine.createScene()

  let attachmentDepth = engine.createEmptyTexture2D(
    Purple.TextureFormat.Depth,
    [shadowMapSize, shadowMapSize],
    false,
    Purple.TextureFilter.Nearest,
    Purple.TextureFilter.Nearest,
    Purple.TextureWrapMode.ClampToEdge)

  lightView = engine.createOffscreenView(
    [shadowMapSize, shadowMapSize],
    [{ texture: attachmentDepth, attachmentPoint: Purple.AttachmentPoint.Depth }]
  )

  lightView.scene = lightScene

  view = engine.getScreenView()
  view.clearColor = [0.1, 0.1, 0.1, 1.0]
  view.scene = scene

  const near = 1.0, far = 7.5

  let lightCamera = lightView.getCamera()
  lightCamera.position = [-2.0, 4.0, -1.0]
  lightCamera.projectionType = Purple.ProjectionType.Orthographic
  lightCamera.setOrtho(-10.0, 10.0, -10.0, 10.0, near, far)
  lightCamera.center = [0.0, 0.0, 0.0]

  let depthShader = new Purple.Shader(
    "../resources/shaders/shadow_mapping_depth.vert.json",
    "../resources/shaders/shadow_mapping_depth.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let depthMaterialDef = new Purple.MaterialDefinition()
  depthMaterialDef.colorWriteEnabled = false
  depthMaterialDef.depthBiasEnabled = true
  depthMaterialDef.depthBiasSlopeFactor = 2.0
  let depthMaterial = engine.createMaterial(depthShader, depthMaterialDef)
  depthMaterialInstance = depthMaterial.defaultInstance()

  let woodTexture = engine.createTexture2DFromUrl(
    "../../../resources/textures/wood.png",
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.Repeat)

  let metalTexture = engine.createTexture2DFromUrl(
    "../../../resources/textures/metal.png",
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.Repeat)

  let shadowMappingShader = new Purple.Shader(
    "../resources/shaders/shadow_mapping.vert.json",
    "../resources/shaders/shadow_mapping.frag.json",
    [Purple.AttributeType.Vec3, Purple.AttributeType.Vec3, Purple.AttributeType.Vec2])

  let shadowMappingMaterialDef = new Purple.MaterialDefinition()
  shadowMappingMaterialDef.setProperty("lightTransformation", Purple.PropertyType.Mat4, lightCamera.getViewProjectionMatrix())
  shadowMappingMaterialDef.setProperty("lightPosition", Purple.PropertyType.Vec3, lightCamera.position)
  shadowMappingMaterialDef.setProperty("shadowMapSize", Purple.PropertyType.Vec2, [shadowMapSize, shadowMapSize])

  shadowMappingMaterialDef.setTexture("diffuseTexture", woodTexture)
  shadowMappingMaterialDef.setTexture("shadowMap", attachmentDepth)

  let shadowMappingMaterial = engine.createMaterial(shadowMappingShader, shadowMappingMaterialDef)
  let shadowMappingWoodMaterialInstance = shadowMappingMaterial.createInstance()
  let shadowMappingMetalMaterialInstance = shadowMappingMaterial.createInstance()

  shadowMappingMetalMaterialInstance.setTexture("diffuseTexture", metalTexture)

  // create entities
  let planeEntity = createEntity(plane, shadowMappingWoodMaterialInstance)

  let cube1Entity = createEntity(cube, shadowMappingMetalMaterialInstance)
  let cube1Transformation = Purple.createTranslationMatrix([0.0, 1.5, 0.0])
  cube1Transformation = Purple.scaleMatrix(cube1Transformation, [0.5, 0.5, 0.5])
  setTransformation(cube1Entity, cube1Transformation)

  let cube2Entity = createEntity(cube, shadowMappingMetalMaterialInstance)
  let cube2Transformation = Purple.createTranslationMatrix([2.0, 0.0, 1.0])
  cube2Transformation = Purple.scaleMatrix(cube2Transformation, [0.5, 0.5, 0.5])
  setTransformation(cube2Entity, cube2Transformation)

  let cube3Entity = createEntity(cube, shadowMappingMetalMaterialInstance)
  let cube3Transformation = Purple.createTranslationMatrix([-1.0, 0.0, 2.0])
  cube3Transformation = Purple.rotateMatrix(cube3Transformation, Math.radians(60.0), Purple.normalizeVec3([1.0, 0.0, 1.0]))
  cube3Transformation = Purple.scaleMatrix(cube3Transformation, [0.25, 0.25, 0.25])
  setTransformation(cube3Entity, cube3Transformation)

  let camera = view.getCamera()
  camera.position = [0.0, 0.0, 3.0]
  camera.center = [0.0, 0.0, 0.0]
  camera.aspectRatio = canvas.width / canvas.height

  let mouseBtnLeft = false
  let mouseBtnRight = false
  let mousePos = [0, 0]

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    engine.draw()
    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]

    let camera = view.getCamera()
    if (canvas.width > 0 && canvas.height > 0) {
      camera.aspectRatio = canvas.width / canvas.height
    }
  })

  window.addEventListener('keydown', (event) => {
  })

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = true
      mousePos = [event.clientX, event.clientY]
    }
    if (event.button == 2) {
      mouseBtnRight = true
      mousePos = [event.clientX, event.clientY]
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = false
    }
    if (event.button == 2) {
      mouseBtnRight = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    let dx = mousePos[0] - event.clientX
    let dy = mousePos[1] - event.clientY

    let camera = view.getCamera()

    if (mouseBtnLeft) {
      const rotationSpeed = 0.2
      camera.panAboutAxis(Math.radians(-dx * rotationSpeed), [0.0, 1.0, 0.0])
      camera.tilt(Math.radians(dy * rotationSpeed))
    }

    if (mouseBtnRight) {
      camera.translate([0.0, 0.0, dy * .005], true)
    }

    mousePos[0] = event.clientX
    mousePos[1] = event.clientY
  })

  requestAnimationFrame(render)
}
