function resizeCanvasToDisplaySize(canvas, multiplier) {
  multiplier = multiplier || 1
  const width = canvas.clientWidth * multiplier | 0
  const height = canvas.clientHeight * multiplier | 0
  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width
    canvas.height = height
    return true
  }
  return false
}

function locateFile(path, prefix) {
  // if it's a data init file, use a custom dir
  if (path.endsWith(".data")) return "../../../build-web/bin/" + path
  // otherwise, use the default, the prefix (JS file's dir) + the path
  return prefix + path
}

Purple.init(() => main(), locateFile)

function rand(min, max) {
  return Math.random() * (max - min) + min
}

Math.radians = function (degrees) {
  return degrees * Math.PI / 180;
}

let cube = [
  -1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, +1.0, -1.0,

  -1.0, -1.0, +1.0,
  +1.0, -1.0, +1.0,
  +1.0, +1.0, +1.0,
  +1.0, +1.0, +1.0,
  -1.0, +1.0, +1.0,
  -1.0, -1.0, +1.0,

  -1.0, +1.0, +1.0,
  -1.0, +1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, -1.0, -1.0,
  -1.0, -1.0, +1.0,
  -1.0, +1.0, +1.0,

  +1.0, +1.0, +1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, +1.0, +1.0,
  +1.0, -1.0, +1.0,

  -1.0, -1.0, -1.0,
  +1.0, -1.0, -1.0,
  +1.0, -1.0, +1.0,
  +1.0, -1.0, +1.0,
  -1.0, -1.0, +1.0,
  -1.0, -1.0, -1.0,

  -1.0, +1.0, -1.0,
  +1.0, +1.0, +1.0,
  +1.0, +1.0, -1.0,
  +1.0, +1.0, +1.0,
  -1.0, +1.0, -1.0,
  -1.0, +1.0, +1.0,
]

let canvas
let engine
let view
let scene

function main() {
  canvas = document.querySelector('#glcanvas')
  resizeCanvasToDisplaySize(canvas)

  const options = {
    version: 2,
    antialias: true,
    depth: true,
    alpha: false
  }

  engine = Purple.WebGLEngine.create(canvas, options)

  scene = engine.createScene()

  view = engine.getScreenView()
  view.clearColor = [0.0, 0.0, 0.1, 1.0]
  view.scene = scene

  let camera = view.getCamera()
  camera.position = [0.0, 0.0, 0.0]
  camera.center = [0.0, 0.0, 1.0]
  camera.up = [0.0, 1.0, 0.0]
  camera.aspectRatio = canvas.width / canvas.height

  let mouseBtnLeft = false;
  let mouseBtnRight = false;
  let mousePos = [0, 0]

  let texture = engine.createTextureCubeFromUrls(
    ["../../../resources/textures/cube/Park3Med/px.jpg",
      "../../../resources/textures/cube/Park3Med/nx.jpg",
      "../../../resources/textures/cube/Park3Med/ny.jpg",
      "../../../resources/textures/cube/Park3Med/py.jpg",
      "../../../resources/textures/cube/Park3Med/pz.jpg",
      "../../../resources/textures/cube/Park3Med/nz.jpg"],
    Purple.TextureFilter.LinearMipMapLinear,
    Purple.TextureFilter.Linear,
    Purple.TextureWrapMode.ClampToEdge)

  let material = engine.createSkyboxMaterial(texture)

  let cubeGeometry = engine.createGeometry(
    [Purple.AttributeType.Vec3],
    cube)

  let entity = engine.createEntity(cubeGeometry, material.defaultInstance())
  scene.addEntity(entity)

  let then = 0

  function render(now) {
    now *= 0.001  // convert to seconds
    const deltaTime = now - then
    then = now

    engine.draw()
    requestAnimationFrame(render)
  }

  window.addEventListener('resize', () => {
    resizeCanvasToDisplaySize(canvas)
    engine.size = [canvas.width, canvas.height]

    let camera = view.getCamera()
    if (canvas.width > 0 && canvas.height > 0) {
      camera.aspectRatio = canvas.width / canvas.height
    }
  })

  window.addEventListener('keydown', (event) => {
  })

  window.addEventListener('contextmenu', function (event) {
    event.preventDefault()

    return false
  }, false)

  window.addEventListener('mousedown', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = true
      mousePos = [event.clientX, event.clientY]
    }
    if (event.button == 2) {
      mouseBtnRight = true
      mousePos = [event.clientX, event.clientY]
    }
  })

  window.addEventListener('mouseup', (event) => {
    if (event.button == 0) {
      mouseBtnLeft = false
    }
    if (event.button == 2) {
      mouseBtnRight = false
    }
  })

  window.addEventListener('mousemove', (event) => {
    let dx = mousePos[0] - event.clientX
    let dy = mousePos[1] - event.clientY

    let camera = view.getCamera()

    if (mouseBtnLeft) {
      const rotationSpeed = 0.2
      camera.panAboutAxis(Math.radians(-dx * rotationSpeed), [0.0, 1.0, 0.0])
      camera.tilt(Math.radians(dy * rotationSpeed))
    }

    if (mouseBtnRight) {
      camera.translate([0.0, 0.0, dy * .005], true)
    }

    mousePos[0] = event.clientX
    mousePos[1] = event.clientY
  })

  requestAnimationFrame(render)
}
