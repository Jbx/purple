#pragma once

#include "engine/engine.h"
#include "render/opengl/opengl_renderer.h"

class OpenGLContext : public IOpenGLContext
{
public:
  OpenGLVersion getOpenGLVersion() const override;

  void setSize(SizeI const &size);
  SizeI getSize() const override;

  void setSampleCount(int sampleCount);
  int getSampleCount() const override;

private:
  SizeI m_size;
  int m_sampleCount;
};

using OpenGLEngine = Engine<OpenGLRenderer>;

class WebGLEngine : public Engine<OpenGLRenderer>
{
public:
  WebGLEngine(
      std::shared_ptr<OpenGLRenderer> const &renderer,
      std::shared_ptr<OpenGLContext> const &context);

  void setSize(SizeI const &size);
  SizeI getSize() const;

  void setSampleCount(int sampleCount);
  int getSampleCount() const;

private:
  std::shared_ptr<OpenGLRenderer> m_renderer;
  std::shared_ptr<OpenGLContext> m_context;
};