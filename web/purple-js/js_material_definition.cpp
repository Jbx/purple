#include "js_material_definition.h"
#include "js_types.h"

// emscripten
using namespace emscripten;

MaterialDefinition const &JSMaterialDefinition::getDefinition() const
{
  return m_definition;
}

PrimitiveTopology JSMaterialDefinition::getPrimitiveTopology() const
{
  return m_definition.renderStates.primitiveTopology;
}

PolygonMode JSMaterialDefinition::getPolygonMode() const
{
  return m_definition.renderStates.polygonMode;
}

float JSMaterialDefinition::getLineWidth() const
{
  return m_definition.renderStates.lineWidth;
}

CullMode JSMaterialDefinition::getCullMode() const
{
  return m_definition.renderStates.cullMode;
}

bool JSMaterialDefinition::isDepthTestEnabled() const
{
  return m_definition.renderStates.depthTestEnabled;
}

bool JSMaterialDefinition::isDepthWriteEnabled() const
{
  return m_definition.renderStates.depthWriteEnabled;
}

bool JSMaterialDefinition::isColorWriteEnabled() const
{
  return m_definition.renderStates.colorWriteEnabled;
}

DepthCompareOperation JSMaterialDefinition::getDepthCompareOperation() const
{
  return m_definition.renderStates.depthCompareOperation;
}

bool JSMaterialDefinition::isBlendingEnabled() const
{
  return m_definition.renderStates.blendingEnabled;
}

BlendFactor JSMaterialDefinition::getSrcColorBlendFactor() const
{
  return m_definition.renderStates.srcColorBlendFactor;
}

BlendFactor JSMaterialDefinition::getDstColorBlendFactor() const
{
  return m_definition.renderStates.dstColorBlendFactor;
}

BlendOperation JSMaterialDefinition::getColorBlendOperation() const
{
  return m_definition.renderStates.colorBlendOperation;
}

BlendFactor JSMaterialDefinition::getSrcAlphaBlendFactor() const
{
  return m_definition.renderStates.srcAlphaBlendFactor;
}

BlendFactor JSMaterialDefinition::getDstAlphaBlendFactor() const
{
  return m_definition.renderStates.dstAlphaBlendFactor;
}

BlendOperation JSMaterialDefinition::getAlphaBlendOperation() const
{
  return m_definition.renderStates.alphaBlendOperation;
}

bool JSMaterialDefinition::isDepthBiasEnabled() const
{
  return m_definition.renderStates.depthBiasEnabled;
}

float JSMaterialDefinition::getDepthBiasConstantFactor() const
{
  return m_definition.renderStates.depthBiasConstantFactor;
}

float JSMaterialDefinition::getDepthBiasClamp() const
{
  return m_definition.renderStates.depthBiasClamp;
}

float JSMaterialDefinition::getDepthBiasSlopeFactor() const
{
  return m_definition.renderStates.depthBiasSlopeFactor;
}

void JSMaterialDefinition::JSMaterialDefinition::setPrimitiveTopology(PrimitiveTopology primitiveTopology)
{
  m_definition.renderStates.primitiveTopology = primitiveTopology;
}

void JSMaterialDefinition::JSMaterialDefinition::setPolygonMode(PolygonMode polygonMode)
{
  m_definition.renderStates.polygonMode = polygonMode;
}

void JSMaterialDefinition::setLineWidth(float lineWidth)
{
  m_definition.renderStates.lineWidth = lineWidth;
}

void JSMaterialDefinition::setCullMode(CullMode cullMode)
{
  m_definition.renderStates.cullMode = cullMode;
}

void JSMaterialDefinition::setDepthTestEnabled(bool enabled)
{
  m_definition.renderStates.depthTestEnabled = enabled;
}

void JSMaterialDefinition::setDepthWriteEnabled(bool enabled)
{
  m_definition.renderStates.depthWriteEnabled = enabled;
}

void JSMaterialDefinition::setColorWriteEnabled(bool enabled)
{
  m_definition.renderStates.colorWriteEnabled = enabled;
}

void JSMaterialDefinition::setDepthCompareOperation(DepthCompareOperation operation)
{
  m_definition.renderStates.depthCompareOperation = operation;
}

void JSMaterialDefinition::setBlendingEnabled(bool enabled)
{
  m_definition.renderStates.blendingEnabled = enabled;
}

void JSMaterialDefinition::setSrcColorBlendFactor(BlendFactor factor)
{
  m_definition.renderStates.srcColorBlendFactor = factor;
}

void JSMaterialDefinition::setDstColorBlendFactor(BlendFactor factor)
{
  m_definition.renderStates.dstColorBlendFactor = factor;
}

void JSMaterialDefinition::setColorBlendOperation(BlendOperation operation)
{
  m_definition.renderStates.colorBlendOperation = operation;
}

void JSMaterialDefinition::setSrcAlphaBlendFactor(BlendFactor factor)
{
  m_definition.renderStates.srcAlphaBlendFactor = factor;
}

void JSMaterialDefinition::setDstAlphaBlendFactor(BlendFactor factor)
{
  m_definition.renderStates.dstAlphaBlendFactor = factor;
}

void JSMaterialDefinition::setAlphaBlendOperation(BlendOperation operation)
{
  m_definition.renderStates.alphaBlendOperation = operation;
}

void JSMaterialDefinition::setDepthBiasEnabled(bool enabled)
{
  m_definition.renderStates.depthBiasEnabled = enabled;
}

void JSMaterialDefinition::setDepthBiasConstantFactor(float factor)
{
  m_definition.renderStates.depthBiasConstantFactor = factor;
}

void JSMaterialDefinition::setDepthBiasClamp(float clamp)
{
  m_definition.renderStates.depthBiasClamp = clamp;
}

void JSMaterialDefinition::setDepthBiasSlopeFactor(float factor)
{
  m_definition.renderStates.depthBiasSlopeFactor = factor;
}

void JSMaterialDefinition::setProperty(std::string const &name, PropertyType type, val const &value)
{
  m_definition.properties.push_back({name, toProperty(type, value)});
}

void JSMaterialDefinition::setTexture(std::string const &name, WeakRef<ITexture> const &texture)
{
  m_definition.textures.push_back({name, texture});
}

bool JSMaterialDefinition::getUseCommonUniforms() const
{
  return m_definition.useCommonUniforms;
}

void JSMaterialDefinition::setUseCommonUniforms(bool useCommonUniforms)
{
  m_definition.useCommonUniforms = useCommonUniforms;
}
