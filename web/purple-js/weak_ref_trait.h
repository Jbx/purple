#pragma once

#include "utils/ref.h"

// emscripten
#include <emscripten.h>
#include <emscripten/bind.h>

namespace emscripten
{
  template <typename T>
  struct smart_ptr_trait<WeakRef<T>>
  {
    typedef WeakRef<T> pointer_type;
    typedef T element_type;

    static sharing_policy get_sharing_policy()
    {
      return sharing_policy::NONE;
    }

    static T *get(const WeakRef<T> &p)
    {
      return p.getRaw();
    }

    static void *share(void *v)
    {
      return 0; // no sharing
    }

    static pointer_type *construct_null()
    {
      return new pointer_type;
    }
  };
}