#include "js_bindings_helpers.h"
#include "js_material_definition.h"
#include "js_types.h"
#include "js_image_loader.h"
#include "js_gltf_renderer.h"
#include "weak_ref_trait.h"
#include "webgl_engine.h"

// Purple
#include "utils/color.h"
#include "utils/rect.h"
#include "render/shader.h"
#include "render/opengl/opengl_texture.h"
#include "materials/color_material.h"
#include "materials/phong_material.h"
#include "materials/phong_textured_material.h"
#include "materials/phong_per_vertex_color_material.h"
#include "materials/skybox_material.h"
#include "images/stb_image.h"
#include "images/ktx_image.h"

// STL
#include <vector>

// emscripten
#include <emscripten.h>
#include <emscripten/bind.h>
using namespace emscripten;

EMSCRIPTEN_BINDINGS(jsbindings)
{
    value_array<Color>("Color")
        .element(&Color::r)
        .element(&Color::g)
        .element(&Color::b)
        .element(&Color::a);

    value_array<RectI>("RectI")
        .element(&RectI::x)
        .element(&RectI::y)
        .element(&RectI::width)
        .element(&RectI::height);

    value_array<RectF>("RectF")
        .element(&RectF::x)
        .element(&RectF::y)
        .element(&RectF::width)
        .element(&RectF::height);

    value_array<SizeI>("SizeI")
        .element(&SizeI::width)
        .element(&SizeI::height);

    value_array<SizeF>("SizeF")
        .element(&SizeF::width)
        .element(&SizeF::height);

    value_array<glm::vec2>("vec2")
        .element(&glm::vec2::x)
        .element(&glm::vec2::y);

    value_array<glm::vec3>("vec3")
        .element(&glm::vec3::x)
        .element(&glm::vec3::y)
        .element(&glm::vec3::z);

    value_array<glm::vec4>("vec4")
        .element(&glm::vec4::x)
        .element(&glm::vec4::y)
        .element(&glm::vec4::z)
        .element(&glm::vec4::w);

    function(
        "normalizeVec2",
        EMBIND_LAMBDA(
            glm::vec2,
            (glm::vec2 const &v),
            {
                return glm::normalize(v);
            }));

    function(
        "normalizeVec3",
        EMBIND_LAMBDA(
            glm::vec3,
            (glm::vec3 const &v),
            {
                return glm::normalize(v);
            }));

    function(
        "normalizeVec4",
        EMBIND_LAMBDA(
            glm::vec4,
            (glm::vec4 const &v),
            {
                return glm::normalize(v);
            }));

    function(
        "cross",
        EMBIND_LAMBDA(
            glm::vec3,
            (glm::vec3 const &lhs, glm::vec3 const &rhs),
            {
                return glm::cross(lhs, rhs);
            }));

    function(
        "mul",
        EMBIND_LAMBDA(
            glm::vec3,
            (glm::vec3 const &v, float factor),
            {
                return v * factor;
            }));

    function(
        "add",
        EMBIND_LAMBDA(
            glm::vec3,
            (glm::vec3 const &lhs, glm::vec3 const &rhs),
            {
                return lhs + rhs;
            }));

    value_array<glm::quat>("quat")
        .element(&glm::quat::x)
        .element(&glm::quat::y)
        .element(&glm::quat::z)
        .element(&glm::quat::w);

    function(
        "createQuaternion",
        EMBIND_LAMBDA(
            glm::quat,
            (float angle, glm::vec3 const &axis),
            {
                return glm::angleAxis(angle, axis);
            }));

    value_array<mat4>("mat4")
        .element(emscripten::index<0>())
        .element(emscripten::index<1>())
        .element(emscripten::index<2>())
        .element(emscripten::index<3>())
        .element(emscripten::index<4>())
        .element(emscripten::index<5>())
        .element(emscripten::index<6>())
        .element(emscripten::index<7>())
        .element(emscripten::index<8>())
        .element(emscripten::index<9>())
        .element(emscripten::index<10>())
        .element(emscripten::index<11>())
        .element(emscripten::index<12>())
        .element(emscripten::index<13>())
        .element(emscripten::index<14>())
        .element(emscripten::index<15>());

    value_array<mat3>("mat3")
        .element(emscripten::index<0>())
        .element(emscripten::index<1>())
        .element(emscripten::index<2>())
        .element(emscripten::index<3>())
        .element(emscripten::index<4>())
        .element(emscripten::index<5>())
        .element(emscripten::index<6>())
        .element(emscripten::index<7>())
        .element(emscripten::index<8>());

    function(
        "createIdentityMatrix",
        EMBIND_LAMBDA(
            mat4,
            (),
            {
                return mat4{glm::mat4(1.f)};
            }));
    function(
        "createRotationMatrix",
        EMBIND_LAMBDA(
            mat4,
            (float angle, glm::vec3 const &axis),
            {
                return mat4{glm::rotate(angle, axis)};
            }));
    function(
        "rotateMatrix",
        EMBIND_LAMBDA(
            mat4,
            (mat4 const &m, float angle, glm::vec3 const &axis),
            { return mat4{glm::rotate(m.m, angle, axis)}; }));
    function(
        "createTranslationMatrix",
        EMBIND_LAMBDA(
            mat4,
            (glm::vec3 const &v),
            { return mat4{glm::translate(v)}; }));
    function(
        "translateMatrix",
        EMBIND_LAMBDA(
            mat4,
            (mat4 const &m, glm::vec3 const &v),
            { return mat4{glm::translate(m.m, v)}; }));
    function(
        "createScalingMatrix",
        EMBIND_LAMBDA(
            mat4,
            (glm::vec3 const &v),
            { return mat4{glm::scale(v)}; }));
    function(
        "scaleMatrix",
        EMBIND_LAMBDA(
            mat4,
            (mat4 const &m, glm::vec3 const &v),
            { return mat4{glm::scale(m.m, v)}; }));

    function(
        "createPerspectiveMatrix",
        EMBIND_LAMBDA(
            mat4,
            (float fovy, float aspect, float zNear, float zFar),
            {
                return mat4{glm::perspective(fovy, aspect, zNear, zFar)};
            }));
    function(
        "createOrthoMatrix",
        EMBIND_LAMBDA(
            mat4,
            (float left, float right, float bottom, float top, float zNear, float zFar),
            {
                return mat4{glm::ortho(left, right, bottom, top, zNear, zFar)};
            }));
    function(
        "createLookAtMatrix",
        EMBIND_LAMBDA(
            mat4,
            (glm::vec3 const &eye, glm::vec3 const &center, glm::vec3 const &up),
            {
                return mat4{glm::lookAt(eye, center, up)};
            }));

    enum_<Camera::ProjectionType>("ProjectionType")
        .value("Perspective", Camera::Perspective)
        .value("Orthographic", Camera::Orthographic);

    class_<Camera>("Camera")
        .property("projectionType", &Camera::getProjectionType, &Camera::setProjectionType)
        .property("position", &Camera::getPosition, &Camera::setPosition)
        .property("center", &Camera::getCenter, &Camera::setCenter)
        .property("up", &Camera::getUp, &Camera::setUp)
        .property("aspectRatio", &Camera::getAspectRatio, &Camera::setAspectRatio)
        .property("fieldOfView", &Camera::getFieldOfView, &Camera::setFieldOfView)
        .property("left", &Camera::getLeft, &Camera::setLeft)
        .property("right", &Camera::getRight, &Camera::setRight)
        .property("bottom", &Camera::getBottom, &Camera::setBottom)
        .property("top", &Camera::getTop, &Camera::setTop)
        .property("nearPlane", &Camera::getNearPlane, &Camera::setNearPlane)
        .property("farPlane", &Camera::getFarPlane, &Camera::setFarPlane)
        .function("getForward", &Camera::getForward)
        .function("setOrtho", &Camera::setOrtho)
        .function("translate", &Camera::translate)
        .function("rotate", &Camera::rotate)
        .function("rotateAboutCenter", &Camera::rotateAboutCenter)
        .function("tilt", &Camera::tilt)
        .function("pan", select_overload<void(float)>(&Camera::pan))
        .function("panAboutAxis", select_overload<void(float, glm::vec3 const &)>(&Camera::pan))
        .function("roll", &Camera::roll)
        .function(
            "getViewMatrix",
            EMBIND_LAMBDA(
                mat4,
                (Camera const &self),
                { return mat4{self.getViewMatrix()}; }))
        .function(
            "getProjectionMatrix",
            EMBIND_LAMBDA(
                mat4,
                (Camera const &self),
                { return mat4{self.getProjectionMatrix()}; }))
        .function(
            "getViewProjectionMatrix",
            EMBIND_LAMBDA(
                mat4,
                (Camera const &self),
                { return mat4{self.getViewProjectionMatrix()}; }));

    enum_<Light::Type>("LightType")
        .value("Point", Light::Point)
        .value("Directional", Light::Directional)
        .value("Spot", Light::Spot);

    class_<Light>("Light")
        .smart_ptr<WeakRef<Light>>("Light")
        .smart_ptr<std::shared_ptr<Light>>("Light")
        .property("enabled", &Light::isEnabled, &Light::setEnabled)
        .property("position", &Light::getPosition, &Light::setPosition)
        .property("direction", &Light::getDirection, &Light::setDirection)
        .property("color", &Light::getColor, &Light::setColor)
        .property("intensity", &Light::getIntensity, &Light::setIntensity)
        .property("constantAttenuation", &Light::getConstantAttenuation, &Light::setConstantAttenuation)
        .property("linearAttenuation", &Light::getLinearAttenuation, &Light::setLinearAttenuation)
        .property("quadraticAttenuation", &Light::getQuadraticAttenuation, &Light::setQuadraticAttenuation)
        .property("cutOffAngle", &Light::getCutOffAngle, &Light::setCutOffAngle);

    enum_<ImageFormat>("ImageFormat")
        .value("RGB8", ImageFormat::RGB8)
        .value("RGBA8", ImageFormat::RGBA8)
        .value("RGBA16F", ImageFormat::RGBA16F)
        .value("RGBA_S3TC_DXT1", ImageFormat::RGBA_S3TC_DXT1)
        .value("RGBA_S3TC_DXT3", ImageFormat::RGBA_S3TC_DXT3)
        .value("RGBA_S3TC_DXT5", ImageFormat::RGBA_S3TC_DXT5);

    class_<IImage>("IImage")
        .smart_ptr<WeakRef<IImage>>("IImage")
        .smart_ptr<std::shared_ptr<IImage>>("IImage")
        .function("isArray", &IImage::isArray)
        .function("isCubemap", &IImage::isCubemap)
        .function("isCompressed", &IImage::isCompressed)
        .function("getSize", &IImage::getSize)
        .function("getChannelCount", &IImage::getChannelCount)
        .function("getFormat", &IImage::getFormat)
        .function("getMipLevelCount", &IImage::getMipLevelCount)
        .function("getLayerCount", &IImage::getLayerCount)
        .function("getFaceCount", &IImage::getFaceCount);

    class_<StbImage, base<IImage>>("StbImage")
        .smart_ptr<WeakRef<StbImage>>("StbImage")
        .smart_ptr<std::shared_ptr<StbImage>>("StbImage");

    class_<KtxImage, base<IImage>>("KtxImage")
        .smart_ptr<WeakRef<KtxImage>>("KtxImage")
        .smart_ptr<std::shared_ptr<KtxImage>>("KtxImage");

    class_<JSImageLoader>("ImageLoader")
        .class_function("load", &JSImageLoader::load);

    enum_<TextureFormat>("TextureFormat")
        .value("RGBA_UNorm", TextureFormat::RGBA_UNorm)
        .value("RGBA_16F", TextureFormat::RGBA_16F)
        .value("RGBA_32F", TextureFormat::RGBA_32F)
        .value("Depth", TextureFormat::Depth);

    enum_<TextureFilter>("TextureFilter")
        .value("Nearest", TextureFilter::Nearest)
        .value("Linear", TextureFilter::Linear)
        .value("NearestMipMapNearest", TextureFilter::NearestMipMapNearest)
        .value("NearestMipMapLinear", TextureFilter::NearestMipMapLinear)
        .value("LinearMipMapNearest", TextureFilter::LinearMipMapNearest)
        .value("LinearMipMapLinear", TextureFilter::LinearMipMapLinear);

    enum_<TextureWrapMode>("TextureWrapMode")
        .value("Repeat", TextureWrapMode::Repeat)
        .value("MirrorRepeat", TextureWrapMode::MirrorRepeat)
        .value("ClampToEdge", TextureWrapMode::ClampToEdge)
        .value("ClampToBorder", TextureWrapMode::ClampToBorder);

    class_<ITexture>("ITexture")
        .smart_ptr<WeakRef<ITexture>>("ITexture")
        .smart_ptr<std::shared_ptr<ITexture>>("ITexture")
        .function("getSize", &ITexture::getSize)
        .function("getFormat", &ITexture::getFormat)
        .function("getId",
                  EMBIND_LAMBDA(
                      unsigned int,
                      (ITexture const &texture),
                      { return static_cast<IOpenGLTexture const &>(texture).getId(); }));

    enum_<AttachmentPoint>("AttachmentPoint")
        .value("Color", AttachmentPoint::Color)
        .value("Depth", AttachmentPoint::Depth);

    enum_<CubeMapFace>("CubeMapFace")
        .value("PositiveX", CubeMapFace::PositiveX)
        .value("NegativeX", CubeMapFace::NegativeX)
        .value("PositiveY", CubeMapFace::PositiveY)
        .value("NegativeY", CubeMapFace::NegativeY)
        .value("PositiveZ", CubeMapFace::PositiveZ)
        .value("NegativeZ", CubeMapFace::NegativeZ)
        .value("All", CubeMapFace::All);

    value_object<Attachment>("Attachment")
        .field("texture", &Attachment::texture)
        .field("attachmentPoint", &Attachment::attachmentPoint)
        .field("face", &Attachment::face)
        .field("mipLevel", &Attachment::mipLevel);

    class_<Shader>("Shader")
        .constructor(
            EMBIND_LAMBDA(
                Shader,
                (std::string const &vertexShader, std::string const &fragmentShader, val const &attributeLayout),
                {
                    return Shader(
                        ShaderProgram(vertexShader, fragmentShader),
                        vecFromJSArray<AttributeType>(attributeLayout));
                }));

    enum_<BlendFactor>("BlendFactor")
        .value("Zero", BlendFactor::Zero)
        .value("One", BlendFactor::One)
        .value("SrcColor", BlendFactor::SrcColor)
        .value("OneMinusSrcColor", BlendFactor::OneMinusSrcColor)
        .value("DstColor", BlendFactor::DstColor)
        .value("OneMinusDstColor", BlendFactor::OneMinusDstColor)
        .value("SrcAlpha", BlendFactor::SrcAlpha)
        .value("OneMinusSrcAlpha", BlendFactor::OneMinusSrcAlpha)
        .value("DstAlpha", BlendFactor::DstAlpha)
        .value("OneMinusDstAlpha", BlendFactor::OneMinusDstAlpha)
        .value("ConstantColor", BlendFactor::ConstantColor)
        .value("OneMinusConstantColor", BlendFactor::OneMinusConstantColor)
        .value("ConstantAlpha", BlendFactor::ConstantAlpha)
        .value("OneMinusConstantAlpha", BlendFactor::OneMinusConstantAlpha)
        .value("SrcAlphaSaturate", BlendFactor::SrcAlphaSaturate);

    enum_<BlendOperation>("BlendOperation")
        .value("Add", BlendOperation::Add)
        .value("Substract", BlendOperation::Substract)
        .value("ReverseSubstract", BlendOperation::ReverseSubstract)
        .value("Min", BlendOperation::Min)
        .value("Max", BlendOperation::Max);

    enum_<CullMode>("CullMode")
        .value("None", CullMode::None)
        .value("Front", CullMode::Front)
        .value("Back", CullMode::Back)
        .value("FrontAndBack", CullMode::FrontAndBack);

    enum_<DepthCompareOperation>("DepthCompareOperation")
        .value("Never", DepthCompareOperation::Never)
        .value("Less", DepthCompareOperation::Less)
        .value("Equal", DepthCompareOperation::Equal)
        .value("LessOrEqual", DepthCompareOperation::LessOrEqual)
        .value("Greater", DepthCompareOperation::Greater)
        .value("NotEqual", DepthCompareOperation::NotEqual)
        .value("GreaterOrEqual", DepthCompareOperation::GreaterOrEqual)
        .value("Always", DepthCompareOperation::Always);

    enum_<PolygonMode>("PolygonMode")
        .value("Fill", PolygonMode::Fill)
        .value("Line", PolygonMode::Line)
        .value("Point", PolygonMode::Point);

    enum_<PrimitiveTopology>("PrimitiveTopology")
        .value("Points", PrimitiveTopology::Points)
        .value("Lines", PrimitiveTopology::Lines)
        .value("LineStrip", PrimitiveTopology::LineStrip)
        .value("Triangles", PrimitiveTopology::Triangles)
        .value("TriangleStrip", PrimitiveTopology::TriangleStrip)
        .value("TriangleFan", PrimitiveTopology::TriangleFan);

    enum_<PropertyType>("PropertyType")
        .value("Int", PropertyType::Int)
        .value("Float", PropertyType::Float)
        .value("Vec2", PropertyType::Vec2)
        .value("Vec3", PropertyType::Vec3)
        .value("Vec4", PropertyType::Vec4)
        .value("Mat2", PropertyType::Mat2)
        .value("Mat4", PropertyType::Mat4)
        .value("Color", PropertyType::Color);

    class_<JSMaterialDefinition>("MaterialDefinition")
        .constructor()
        .property("primitiveTopology", &JSMaterialDefinition::getPrimitiveTopology, &JSMaterialDefinition::setPrimitiveTopology)
        .property("polygonMode", &JSMaterialDefinition::getPolygonMode, &JSMaterialDefinition::setPolygonMode)
        .property("lineWidth", &JSMaterialDefinition::getLineWidth, &JSMaterialDefinition::setLineWidth)
        .property("cullMode", &JSMaterialDefinition::getCullMode, &JSMaterialDefinition::setCullMode)
        .property("depthTestEnabled", &JSMaterialDefinition::isDepthTestEnabled, &JSMaterialDefinition::setDepthTestEnabled)
        .property("depthWriteEnabled", &JSMaterialDefinition::isDepthWriteEnabled, &JSMaterialDefinition::setDepthWriteEnabled)
        .property("colorWriteEnabled", &JSMaterialDefinition::isColorWriteEnabled, &JSMaterialDefinition::setColorWriteEnabled)
        .property("depthCompareOperation", &JSMaterialDefinition::getDepthCompareOperation, &JSMaterialDefinition::setDepthCompareOperation)
        .property("blendingEnabled", &JSMaterialDefinition::isBlendingEnabled, &JSMaterialDefinition::setBlendingEnabled)
        .property("srcColorBlendFactor", &JSMaterialDefinition::getSrcColorBlendFactor, &JSMaterialDefinition::setSrcColorBlendFactor)
        .property("dstColorBlendFactor", &JSMaterialDefinition::getDstColorBlendFactor, &JSMaterialDefinition::setDstColorBlendFactor)
        .property("colorBlendOperation", &JSMaterialDefinition::getColorBlendOperation, &JSMaterialDefinition::setColorBlendOperation)
        .property("srcAlphaBlendFactor", &JSMaterialDefinition::getSrcAlphaBlendFactor, &JSMaterialDefinition::setSrcAlphaBlendFactor)
        .property("dstAlphaBlendFactor", &JSMaterialDefinition::getDstAlphaBlendFactor, &JSMaterialDefinition::setDstAlphaBlendFactor)
        .property("alphaBlendOperation", &JSMaterialDefinition::getAlphaBlendOperation, &JSMaterialDefinition::setAlphaBlendOperation)
        .property("depthBiasEnabled", &JSMaterialDefinition::isDepthBiasEnabled, &JSMaterialDefinition::setDepthBiasEnabled)
        .property("depthBiasConstantFactor", &JSMaterialDefinition::getDepthBiasConstantFactor, &JSMaterialDefinition::setDepthBiasConstantFactor)
        .property("depthBiasClamp", &JSMaterialDefinition::getDepthBiasClamp, &JSMaterialDefinition::setDepthBiasClamp)
        .property("depthBiasSlopeFactor", &JSMaterialDefinition::getDepthBiasSlopeFactor, &JSMaterialDefinition::setDepthBiasSlopeFactor)
        .function("setProperty", &JSMaterialDefinition::setProperty)
        .function("setTexture", &JSMaterialDefinition::setTexture)
        .property("useCommonUniforms", &JSMaterialDefinition::getUseCommonUniforms, &JSMaterialDefinition::setUseCommonUniforms);

    class_<MaterialInstance>("MaterialInstance")
        .smart_ptr<WeakRef<MaterialInstance>>("MaterialInstance")
        .smart_ptr<std::shared_ptr<MaterialInstance>>("MaterialInstance")
        .function(
            "setProperty",
            EMBIND_LAMBDA(
                void,
                (MaterialInstance & self, std::string const &name, PropertyType type, val const &value),
                { self.setProperty(name, toProperty(type, value)); }))
        .function("setTexture", &MaterialInstance::setTexture);

    class_<Material>("Material")
        .smart_ptr<WeakRef<Material>>("Material")
        .smart_ptr<std::shared_ptr<Material>>("Material")
        .function(
            "setProperty",
            EMBIND_LAMBDA(
                void,
                (Material & self, std::string const &name, PropertyType type, val const &value),
                { self.setProperty(name, toProperty(type, value)); }))
        .function("setTexture", &Material::setTexture)
        .function("defaultInstance", &Material::defaultInstance)
        .function("createInstance", &Material::createInstance);

    class_<ColorMaterial, base<Material>>("ColorMaterial")
        .smart_ptr<WeakRef<ColorMaterial>>("ColorMaterial")
        .smart_ptr<std::shared_ptr<ColorMaterial>>("ColorMaterial");

    class_<PhongMaterial, base<Material>>("PhongMaterial")
        .smart_ptr<WeakRef<PhongMaterial>>("PhongMaterial")
        .smart_ptr<std::shared_ptr<PhongMaterial>>("PhongMaterial");

    class_<PhongTexturedMaterial, base<Material>>("PhongTexturedMaterial")
        .smart_ptr<WeakRef<PhongTexturedMaterial>>("PhongTexturedMaterial")
        .smart_ptr<std::shared_ptr<PhongTexturedMaterial>>("PhongTexturedMaterial");

    class_<PhongPerVertexColorMaterial, base<Material>>("PhongPerVertexColorMaterial")
        .smart_ptr<WeakRef<PhongPerVertexColorMaterial>>("PhongPerVertexColorMaterial")
        .smart_ptr<std::shared_ptr<PhongPerVertexColorMaterial>>("PhongPerVertexColorMaterial");

    class_<SkyboxMaterial, base<Material>>("SkyboxMaterial")
        .smart_ptr<WeakRef<SkyboxMaterial>>("SkyboxMaterial")
        .smart_ptr<std::shared_ptr<SkyboxMaterial>>("SkyboxMaterial");

    enum_<AttributeType>("AttributeType")
        .value("Float", AttributeType::Float)
        .value("Vec2", AttributeType::Vec2)
        .value("Vec3", AttributeType::Vec3)
        .value("Vec4", AttributeType::Vec4);

    class_<Geometry>("Geometry")
        .smart_ptr<WeakRef<Geometry>>("Geometry")
        .smart_ptr<std::shared_ptr<Geometry>>("Geometry");

    class_<Entity>("Entity")
        .smart_ptr<WeakRef<Entity>>("Entity")
        .smart_ptr<std::shared_ptr<Entity>>("Entity")
        .function(
            "getTransformation",
            EMBIND_LAMBDA(
                mat4,
                (Entity & self),
                { return mat4{self.getTransformation()}; }))
        .function(
            "setTransformation",
            EMBIND_LAMBDA(
                void,
                (Entity & self, mat4 const &m),
                { self.setTransformation(m.m); }));

    class_<View>("View")
        .smart_ptr<WeakRef<View>>("View")
        .smart_ptr<std::shared_ptr<View>>("View")
        .property("scene", &View::getScene, &View::setScene)
        .function(
            "getCamera",
            EMBIND_LAMBDA(
                Camera *,
                (View & self),
                { return &self.getCamera(); }),
            allow_raw_pointers())
        .property("clearColor", &View::getClearColor, &View::setClearColor)
        .property("viewport", &View::getViewport, &View::setViewport);

    class_<Scene>("Scene")
        .smart_ptr<WeakRef<Scene>>("Scene")
        .smart_ptr<std::shared_ptr<Scene>>("Scene")
        .function("addEntity", &Scene::addEntity)
        .function("removeEntity", &Scene::removeEntity)
        .function("addLight", &Scene::addLight)
        .function("removeLight", &Scene::removeLight);

    class_<OpenGLEngine>("OpenGLEngine")
        .smart_ptr<std::shared_ptr<OpenGLEngine>>("OpenGLEngine")
        .function("draw", select_overload<void()>(&OpenGLEngine::draw))
        .function(
            "drawViews",
            EMBIND_LAMBDA(
                void,
                (OpenGLEngine & self, val const &views),
                {
                    self.draw(vecFromJSArray<WeakRef<View>>(views));
                }))

        .function("createScene", &OpenGLEngine::createScene)
        .function("removeScene", &OpenGLEngine::removeScene)
        .function("getScreenView", &OpenGLEngine::getScreenView)
        .function(
            "createOffscreenView",
            EMBIND_LAMBDA(
                WeakRef<View>,
                (OpenGLEngine & self, SizeI const &size, val const &attachments),
                {
                    auto valArray = vecFromJSArray<val>(attachments);
                    std::vector<Attachment> attchmentVec;
                    for (auto const &val : valArray)
                    {
                        attchmentVec.emplace_back(
                            Attachment{
                                val["texture"].as<Ref<ITexture>>(),
                                val["attachmentPoint"].as<AttachmentPoint>(),
                                getOptional(val, "face", CubeMapFace::PositiveX),
                                getOptional(val, "mipLevel", 0)});
                    }

                    return self.createOffscreenView(
                        size,
                        attchmentVec);
                }))
        .function("removeView", &OpenGLEngine::removeView)
        .function(
            "createGeometry",
            EMBIND_LAMBDA(
                WeakRef<Geometry>,
                (OpenGLEngine & self, val const &attributeLayout, val const &vertices),
                {
                    return self.createGeometry(
                        vecFromJSArray<AttributeType>(attributeLayout),
                        convertJSArrayToNumberVector<float>(vertices));
                }))
        .function(
            "createGeometryIndexed",
            EMBIND_LAMBDA(
                WeakRef<Geometry>,
                (OpenGLEngine & self, val const &attributeLayout, val const &vertices, val const &indices),
                {
                    return self.createGeometry(
                        vecFromJSArray<AttributeType>(attributeLayout),
                        convertJSArrayToNumberVector<float>(vertices),
                        convertJSArrayToNumberVector<unsigned int>(indices));
                }))
        .function("removeGeometry", &OpenGLEngine::removeGeometry)
        .function(
            "createEmptyTexture2D",
            EMBIND_LAMBDA(
                WeakRef<ITexture>,
                (OpenGLEngine & self,
                 TextureFormat format,
                 SizeI const &size,
                 bool mipmap,
                 TextureFilter minFilter,
                 TextureFilter magFilter,
                 TextureWrapMode wrapMode),
                {
                    return self.createTexture2D(format, size, mipmap, {minFilter, magFilter, wrapMode, wrapMode, wrapMode});
                }))
        .function(
            "createEmptyTextureCube",
            EMBIND_LAMBDA(
                WeakRef<ITexture>,
                (OpenGLEngine & self,
                 TextureFormat format,
                 SizeI const &size,
                 bool mipmap,
                 TextureFilter minFilter,
                 TextureFilter magFilter,
                 TextureWrapMode wrapMode),
                {
                    return self.createTextureCube(format, size, mipmap, {minFilter, magFilter, wrapMode, wrapMode, wrapMode});
                }))
        .function(
            "createTexture2D",
            EMBIND_LAMBDA(
                WeakRef<ITexture>,
                (OpenGLEngine & self,
                 Ref<IImage> const &image,
                 bool mipmap,
                 TextureFilter minFilter,
                 TextureFilter magFilter,
                 TextureWrapMode wrapMode),
                {
                    return self.createTexture2D(*image, mipmap, {minFilter, magFilter, wrapMode, wrapMode, wrapMode});
                }))
        .function(
            "createTextureCube",
            EMBIND_LAMBDA(
                WeakRef<ITexture>,
                (OpenGLEngine & self,
                 Ref<IImage> const &image,
                 bool mipmap,
                 TextureFilter minFilter,
                 TextureFilter magFilter,
                 TextureWrapMode wrapMode),
                {
                    return self.createTextureCube(*image, mipmap, {minFilter, magFilter, wrapMode, wrapMode, wrapMode});
                }))
        .function("removeTexture", &OpenGLEngine::removeTexture)
        .function(
            "createMaterial",
            EMBIND_LAMBDA(
                WeakRef<Material>,
                (OpenGLEngine & self, Shader const &shader, JSMaterialDefinition const &definition),
                {
                    return self.createMaterial(shader, definition.getDefinition());
                }))

        .function(
            "createColorMaterial",
            EMBIND_LAMBDA(
                WeakRef<ColorMaterial>,
                (OpenGLEngine & self),
                {
                    return self.createMaterial<ColorMaterial>();
                }))

        .function(
            "createPhongMaterial",
            EMBIND_LAMBDA(
                WeakRef<PhongMaterial>,
                (OpenGLEngine & self),
                {
                    return self.createMaterial<PhongMaterial>();
                }))
        .function(
            "createPhongTexturedMaterial",
            EMBIND_LAMBDA(
                WeakRef<PhongTexturedMaterial>,
                (OpenGLEngine & self, WeakRef<ITexture> const &texture),
                {
                    return self.createMaterial<PhongTexturedMaterial>(texture);
                }))
        .function(
            "createPhongPerVertexColorMaterial",
            EMBIND_LAMBDA(
                WeakRef<PhongPerVertexColorMaterial>,
                (OpenGLEngine & self),
                {
                    return self.createMaterial<PhongPerVertexColorMaterial>();
                }))
        .function(
            "createSkyboxMaterial",
            EMBIND_LAMBDA(
                WeakRef<SkyboxMaterial>,
                (OpenGLEngine & self, WeakRef<ITexture> const &texture),
                {
                    return self.createMaterial<SkyboxMaterial>(texture);
                }))
        .function("removeMaterial", &OpenGLEngine::removeMaterial)
        .function("createEntity", &OpenGLEngine::createEntity)
        .function("removeEntity", &OpenGLEngine::removeEntity)
        .function("createLight", &OpenGLEngine::createLight)
        .function("removeLight", &OpenGLEngine::removeLight);

    class_<WebGLEngine, base<OpenGLEngine>>("WebGLEngine")
        .smart_ptr<std::shared_ptr<WebGLEngine>>("WebGLEngine")
        .class_function(
            "_create",
            EMBIND_LAMBDA(
                std::shared_ptr<WebGLEngine>,
                (),
                {
                    EM_ASM_INT(
                        {
                            const options = window.purple_glOptions;
                            const context = window.purple_glContext;
                            const handle = GL.registerContext(context, options);
                            window.purple_contextHandle = handle;
                            window.purple_textures = GL.textures;
                            GL.makeContextCurrent(handle);
                        });
                    auto glContext = std::make_shared<OpenGLContext>();
                    auto renderer = std::make_shared<OpenGLRenderer>(*glContext);
                    return std::make_shared<WebGLEngine>(renderer, glContext);
                }))
        .property("size", &WebGLEngine::getSize, &WebGLEngine::setSize);

    class_<GLTFRenderer>("GLTFRenderer")
        .smart_ptr<std::shared_ptr<GLTFRenderer>>("GLTFRenderer")
        .class_function(
            "create",
            EMBIND_LAMBDA(
                std::shared_ptr<GLTFRenderer>,
                (std::shared_ptr<WebGLEngine> const &engine),
                {
                    return std::make_shared<GLTFRenderer>(engine);
                }))
        .function("load", &GLTFRenderer::load)
        .function("draw", &GLTFRenderer::draw);
}
