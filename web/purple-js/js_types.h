#pragma once

#include "math/math.h"
#include "render/property.h"

struct mat2
{
  glm::mat2 m;
  float &operator[](int i) { return m[i / 2][i % 2]; }
};

struct mat3
{
  glm::mat3 m;
  float &operator[](int i) { return m[i / 3][i % 3]; }
};

struct mat4
{
  glm::mat4 m;
  float &operator[](int i) { return m[i / 4][i % 4]; }
};


static Property toProperty(PropertyType type, emscripten::val const &value)
{
  switch (type)
  {
  case PropertyType::Int:
    return Property(value.as<int>());
    break;

  case PropertyType::Float:
    return Property(value.as<float>());
    break;

  case PropertyType::Vec2:
  {
    auto vec = convertJSArrayToNumberVector<float>(value);
    return Property(glm::make_vec2(vec.data()));
  }
  break;

  case PropertyType::Vec3:
  {
    auto vec = convertJSArrayToNumberVector<float>(value);
    return Property(glm::make_vec3(vec.data()));
  }
  break;

  case PropertyType::Vec4:
  {
    auto vec = convertJSArrayToNumberVector<float>(value);
    return Property(glm::make_vec4(vec.data()));
  }
  break;

  case PropertyType::Mat2:
    return Property(value.as<mat2>().m);
    break;

  case PropertyType::Mat4:
    return Property(value.as<mat4>().m);
    break;

  case PropertyType::Color:
  {
    auto vec = convertJSArrayToNumberVector<float>(value);
    return Property(Color{vec[0], vec[1], vec[2], vec[3]});
  }
  break;
  }
}
