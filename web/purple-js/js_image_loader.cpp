#include "js_image_loader.h"

#include "images/stb_image.h"
#include "images/ktx_image.h"

// STL
#include <filesystem>

Ref<IImage> JSImageLoader::load(std::string const &filePath)
{
  auto extension = std::filesystem::path(filePath).extension();

  if (extension == ".ktx")
  {
    return createRef<KtxImage>(filePath);
  }
  else
  {
    return createRef<StbImage>(filePath);
  }
}