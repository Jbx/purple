#include "webgl_engine.h"

OpenGLVersion OpenGLContext::getOpenGLVersion() const
{
  return {3, 0, true};
}

void OpenGLContext::setSize(SizeI const &size)
{
  m_size = size;
}

SizeI OpenGLContext::getSize() const
{
  return m_size;
}

void OpenGLContext::setSampleCount(int sampleCount)
{
  m_sampleCount = sampleCount;
}

int OpenGLContext::getSampleCount() const
{
  return m_sampleCount;
}

WebGLEngine::WebGLEngine(
    std::shared_ptr<OpenGLRenderer> const &renderer,
    std::shared_ptr<OpenGLContext> const &context)
    : Engine(*renderer), m_renderer(renderer), m_context(context) {}

void WebGLEngine::setSize(SizeI const &size)
{
  m_context->setSize(size);
}

SizeI WebGLEngine::getSize() const
{
  return m_context->getSize();
}

void WebGLEngine::setSampleCount(int sampleCount)
{
  m_context->setSampleCount(sampleCount);
}

int WebGLEngine::getSampleCount() const
{
  return m_context->getSampleCount();
}