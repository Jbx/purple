#pragma once

#include "webgl_engine.h"

// STL
#include <string>

class GLTFRenderer
{
public:
  GLTFRenderer(Ref<WebGLEngine> const &engine);

  void load(
    std::string const &envmapFilePath,
    std::string const &gltfFilePath);
    
  void draw();

private:
  WebGLEngine& m_engine;
  WeakRef<View> m_brdfLUTView;
  std::vector<WeakRef<View>> m_cubemapViews;
  WeakRef<View> m_view;
};
