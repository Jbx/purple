// STL
#include <vector>

// emscripten
#include <emscripten.h>
#include <emscripten/bind.h>

#define EMBIND_LAMBDA(retval, arglist, impl) (retval(*) arglist)[] arglist impl

template <typename T>
std::vector<T> vectorFromJSArray(emscripten::val const &jsArray)
{
  std::vector<T> result;
  const auto length = jsArray["length"].as<unsigned>();
  result.reserve(length);
  result.resize(length);
  emscripten::val memoryView{emscripten::typed_memory_view(length, result.data())};
  memoryView.call<void>("set", jsArray);
  return result;
}

template <typename T, T>
struct proxy;

template <typename T, typename R, typename... Args, R (T::*mf)(Args...)>
struct proxy<R (T::*)(Args...), mf>
{
  using ptr = typename std::add_pointer<typename std::remove_reference<R>::type>::type;
  static ptr call(T &obj, Args &&...args)
  {
    return &((obj.*mf)(std::forward<Args>(args)...));
  }
};

#define RETURN_POINTER(value) &proxy<decltype(value), (value)>::call

template <typename T>
T getOptional(emscripten::val const &valueObject, std::string const& name, T const& defaultValue)
{
  return valueObject.hasOwnProperty(name.c_str()) ? valueObject[name.c_str()].as<T>() : defaultValue;
}
