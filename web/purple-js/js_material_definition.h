#pragma once

#include "engine/material.h"

// emscripten
#include <emscripten.h>
#include <emscripten/bind.h>

class JSMaterialDefinition
{
public:
  MaterialDefinition const& getDefinition() const;

  PrimitiveTopology getPrimitiveTopology() const;
  PolygonMode getPolygonMode() const;
  float getLineWidth() const;
  CullMode getCullMode() const;
  bool isDepthTestEnabled() const;
  bool isDepthWriteEnabled() const;
  bool isColorWriteEnabled() const;
  DepthCompareOperation getDepthCompareOperation() const;
  bool isBlendingEnabled() const;
  BlendFactor getSrcColorBlendFactor() const;
  BlendFactor getDstColorBlendFactor() const;
  BlendOperation getColorBlendOperation() const;
  BlendFactor getSrcAlphaBlendFactor() const;
  BlendFactor getDstAlphaBlendFactor() const;
  BlendOperation getAlphaBlendOperation() const;
  bool isDepthBiasEnabled() const;
  float getDepthBiasConstantFactor() const;
  float getDepthBiasClamp() const;
  float getDepthBiasSlopeFactor() const;

  void setPrimitiveTopology(PrimitiveTopology primitiveTopology);
  void setPolygonMode(PolygonMode polygonMode);
  void setLineWidth(float lineWidth);
  void setCullMode(CullMode cullMode);
  void setDepthTestEnabled(bool enabled);
  void setDepthWriteEnabled(bool enabled);
  void setColorWriteEnabled(bool enabled);
  void setDepthCompareOperation(DepthCompareOperation operation);
  void setBlendingEnabled(bool enabled);
  void setSrcColorBlendFactor(BlendFactor factor);
  void setDstColorBlendFactor(BlendFactor factor);
  void setColorBlendOperation(BlendOperation operation);
  void setSrcAlphaBlendFactor(BlendFactor factor);
  void setDstAlphaBlendFactor(BlendFactor factor);
  void setAlphaBlendOperation(BlendOperation operation);
  void setDepthBiasEnabled(bool enabled);
  void setDepthBiasConstantFactor(float factor);
  void setDepthBiasClamp(float clamp);
  void setDepthBiasSlopeFactor(float factor);

  void setProperty(std::string const &name, PropertyType type, emscripten::val const &value);
  void setTexture(std::string const &name, WeakRef<ITexture> const &texture);

  bool getUseCommonUniforms() const;
  void setUseCommonUniforms(bool useCommonUniforms);

private:
  MaterialDefinition m_definition;
};
