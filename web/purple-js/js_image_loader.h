#pragma once

#include "images/image_interface.h"
#include "utils/ref.h"

// STL
#include <string>

class JSImageLoader
{
public:
  static Ref<IImage> load(std::string const &filePath);
};

