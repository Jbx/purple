Purple.init = (onReady, locateFile) => {
  onReady = onReady || (() => { })
  locateFile = locateFile || (() => { })

  // Emscripten creates a global function called "Purple" that returns a promise that
  // resolves to a module. Here we replace the function with the module.
  Purple({ 'locateFile': locateFile }).then(module => {
    Purple = Object.assign(module, Purple)

    // At this point, emscripten has finished compiling and instancing the WebAssembly module.
    // The JS classes that correspond to core Purple classes (e.g., Engine) are not guaranteed
    // to exist until now.

    Purple.loadClassExtensions()

    onReady()
  })
}

Purple.loadClassExtensions = function () {
  Purple.WebGLEngine.create = function (canvas, options) {
    const defaults = {
      version: 2,
      antialias: true,
      depth: true,
      stencil: true,
      alpha: true
    }

    options = Object.assign(defaults, options)

    // Create the WebGL context.
    let ctx

    if (options.version == 2) {
      // try to create WebGL2 context
      ctx = canvas.getContext("webgl2", options)
    }

    if (!ctx) {
      ctx = canvas.getContext("webgl", options)
      if (!ctx) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.')
        return
      }
    }

    console.log("WebGL Version:", ctx.getParameter(ctx.VERSION))
    console.log("GLSL Version:", ctx.getParameter(ctx.SHADING_LANGUAGE_VERSION))
    console.log("Vendor:", ctx.getParameter(ctx.VENDOR))

    // Enable all desired extensions by calling getExtension on each one.
    ctx.getExtension('WEBGL_compressed_texture_s3tc')
    ctx.getExtension('WEBGL_compressed_texture_s3tc_srgb')
    ctx.getExtension('WEBGL_compressed_texture_astc')
    ctx.getExtension('WEBGL_compressed_texture_etc')
    ctx.getExtension('WEBGL_depth_texture')

    // These transient globals are used temporarily during engine construction.
    window.purple_glOptions = options
    window.purple_glContext = ctx

    // Register the GL context with emscripten and create the engine.
    const engine = Purple.WebGLEngine._create()
    engine.size = [canvas.width, canvas.height]
    engine.samples = ctx.getParameter(ctx.SAMPLES);

    console.log("Samples: ", engine.samples)

    // Annotate the engine with the GL context to support multiple canvases.
    engine.context = window.purple_glContext
    engine.handle = window.purple_contextHandle
    engine.textures = window.purple_textures

    // Ensure that we do not pollute the global namespace.
    delete window.purple_glOptions
    delete window.purple_glContext
    delete window.purple_contextHandle
    delete window.purple_textures

    return engine
  }

  Purple.WebGLEngine.prototype.createTexture2DFromUrl = function (url, minFilterMode, magFilterMode, wrapMode) {
    let texture =
      this.createEmptyTexture2D(
        Purple.TextureFormat.RGBA_UNorm,
        [1, 1],
        false,
        minFilterMode,
        magFilterMode,
        wrapMode)

    const img = new Image()
    img.crossOrigin = "anonymous"

    img.onload = () => {
      const gl = this.context
      gl.bindTexture(gl.TEXTURE_2D, this.textures[texture.getId()])
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img)
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false)
      gl.generateMipmap(gl.TEXTURE_2D)
    }
    img.onerror = () => {
      console.error("Error loading ", url)
    }
    img.src = url

    return texture
  }

  Purple.WebGLEngine.prototype.createTextureCubeFromUrls = function (
    urls, 
    minFilterMode, 
    magFilterMode, 
    wrapMode,
    onLoad) {

    let texture =
      this.createEmptyTextureCube(
        Purple.TextureFormat.RGBA_UNorm,
        [1, 1],
        false,
        minFilterMode,
        magFilterMode,
        wrapMode)

    let loadImage = (url) => {
      return new Promise((resolve, reject) => {
        const img = new Image()
        img.crossOrigin = "anonymous"
        img.onload = () => resolve(img)
        img.onerror = reject
        img.src = url
      })
    }

    let promises = []
    urls.forEach(url => promises.push(loadImage(url)))

    Promise.all(promises).then((images) => {

      const gl = this.context
      gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.textures[texture.getId()])
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)

      const targets = [
        gl.TEXTURE_CUBE_MAP_POSITIVE_X,
        gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
        gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
        gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
        gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
        gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
      ]

      for (let i = 0; i < 6; i++) {
        gl.texImage2D(targets[i], 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, images[i])
      }
      
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false)
      gl.generateMipmap(gl.TEXTURE_CUBE_MAP)

      if (onLoad) onLoad()
    })

    return texture
  }
}
