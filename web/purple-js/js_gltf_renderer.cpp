#include "js_gltf_renderer.h"
#include "gltf/gltf_importer.h"
#include "images/ktx_image.h"
#include "images/stb_image.h"
#include "materials/skybox_material.h"

std::vector<float> cube{
    -0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    -0.5f,

    -0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,

    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,

    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,

    -0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    -0.5f,

    -0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    +0.5f,
    +0.5f,
    -0.5f,
    +0.5f,
    -0.5f,
    -0.5f,
    +0.5f,
    +0.5,
};

std::vector<float> quad{
    // positions        // texture Coords
    -1.0f,
    +1.0f,
    0.0f,
    0.0f,
    1.0f,
    -1.0f,
    -1.0f,
    0.0f,
    0.0f,
    0.0f,
    +1.0f,
    +1.0f,
    0.0f,
    1.0f,
    1.0f,
    +1.0f,
    -1.0f,
    0.0f,
    1.0f,
    0.0f,
};

const float zNear = 0.1f;
const float zFar = 512.0f;

class GLTFMaterial : public Material
{
public:
  GLTFMaterial(
      WeakRef<ITexture> const &samplerIrradiance,
      WeakRef<ITexture> const &prefilteredMap,
      WeakRef<ITexture> const &samplerBRDFLUT,
      int prefilteredCubeMipLevels,
      RenderStates const &renderStates = RenderStates{})
      : Material(
            Shader(
                ShaderProgram(
                    "../resources/shaders/pbr.vert.json",
                    "../resources/shaders/pbr_khr.frag.json"),
                {Vec3, Vec3, Vec2, Vec2, Vec4}),
            createMaterialDefinition(
                samplerIrradiance,
                prefilteredMap,
                samplerBRDFLUT,
                prefilteredCubeMipLevels,
                renderStates))
  {
  }

  MaterialDefinition createMaterialDefinition(
      WeakRef<ITexture> const &samplerIrradiance,
      WeakRef<ITexture> const &prefilteredMap,
      WeakRef<ITexture> const &samplerBRDFLUT,
      int prefilteredCubeMipLevels,
      RenderStates const &renderStates)
  {
    MaterialDefinition materialDefinition;
    materialDefinition.renderStates = renderStates;

    materialDefinition.properties = {
        {"lightDir", glm::vec4(0.74f, 0.64f, 0.2f, 0.f)},
        {"exposure", 4.5f},
        {"gamma", 2.2f},
        {"prefilteredCubeMipLevels", static_cast<float>(prefilteredCubeMipLevels)},
        {"scaleIBLAmbient", 2.0f},
        {"debugViewInputs", 0.f},
        {"debugViewEquation", 0.f},
        {"baseColorFactor", glm::vec4(1.f)},
        {"emissiveFactor", glm::vec4(1.f)},
        {"diffuseFactor", glm::vec4(0.f)},
        {"specularFactor", glm::vec4(0.f, 0.f, 0.f, 1.f)},
        {"workflow", 0.f},
        {"baseColorTextureSet", -1},
        {"physicalDescriptorTextureSet", -1},
        {"normalTextureSet", -1},
        {"occlusionTextureSet", -1},
        {"emissiveTextureSet", -1},
        {"metallicFactor", 1.0f},
        {"roughnessFactor", 1.0f},
        {"alphaMask", 0.0f},
        {"alphaMaskCutoff", 1.0f},
    };

    materialDefinition.textures = {
        {"samplerIrradiance", samplerIrradiance},
        {"prefilteredMap", prefilteredMap},
        {"samplerBRDFLUT", samplerBRDFLUT},
        {"colorMap", nullptr},
        {"physicalDescriptorMap", nullptr},
        {"normalMap", nullptr},
        {"aoMap", nullptr},
        {"emissiveMap", nullptr},
    };

    return materialDefinition;
  }
};

GLTFRenderer::GLTFRenderer(Ref<WebGLEngine> const &engine)
    : m_engine(*engine) {}

void GLTFRenderer::load(
    std::string const &envmapFilePath,
    std::string const &gltfFilePath)
{
  auto [success, model] = GLTFImporter::import(gltfFilePath);
  if (!success)
  {
    LOG_ERROR("Error loading GLTF file");
    return;
  }

  auto cubeGeometry = m_engine.createGeometry({Vec3}, cube);
  auto quadGeometry = m_engine.createGeometry({Vec3, Vec2}, quad);

  KtxImage envmap(envmapFilePath);

  auto skyboxTexture = m_engine.createTextureCube(
      envmap,
      true,
      {.minFilter = TextureFilter::LinearMipMapLinear,
       .magFilter = TextureFilter::Linear,
       .wrapModeS = TextureWrapMode::ClampToEdge,
       .wrapModeT = TextureWrapMode::ClampToEdge,
       .wrapModeR = TextureWrapMode::ClampToEdge});

  // Generate a BRDF integration map used as a look-up-table (stores roughness / NdotV)
  int dim = 512;

  auto lutBRDF = m_engine.createTexture2D(
      TextureFormat::RGBA_16F,
      {dim, dim},
      false,
      {.minFilter = TextureFilter::Linear,
       .magFilter = TextureFilter::Linear,
       .wrapModeS = TextureWrapMode::ClampToEdge,
       .wrapModeT = TextureWrapMode::ClampToEdge,
       .wrapModeR = TextureWrapMode::ClampToEdge});

  m_brdfLUTView = m_engine.createOffscreenView(
      {dim, dim},
      {{.texture = lutBRDF, .attachmentPoint = AttachmentPoint::Color}});

  MaterialDefinition materialDef;
  materialDef.useCommonUniforms = false;
  materialDef.renderStates.primitiveTopology = PrimitiveTopology::TriangleStrip;

  auto lutBRDFMaterial = m_engine.createMaterial(
      Shader(
          ShaderProgram(
              "../resources/shaders/gen_brdf_lut.vert.json",
              "../resources/shaders/gen_brdf_lut.frag.json"),
          {Vec3, Vec2}),
      materialDef);

  auto lutBRDFVScene = m_engine.createScene();
  m_brdfLUTView->setScene(lutBRDFVScene);

  auto quadEntity = m_engine.createEntity(quadGeometry, lutBRDFMaterial->defaultInstance());
  lutBRDFVScene->addEntity(quadEntity);

  // Generate irradiance and prefilter cube maps
  WeakRef<ITexture> irradianceCubemap;
  WeakRef<ITexture> prefilterEnvCubemap;
  int prefilterEnvCubemapMipLevels = 0;

  enum
  {
    Irradiance,
    PrefilterEnv
  };

  for (auto target : {Irradiance, PrefilterEnv})
  {
    int dim;
    TextureFormat format;

    MaterialDefinition materialDef;
    materialDef.useCommonUniforms = false;
    materialDef.renderStates.cullMode = CullMode::None;

    materialDef.textures = {
        {"samplerEnv", skyboxTexture}};

    WeakRef<Material> material;

    switch (target)
    {
    case Irradiance:
    {
      dim = 64;
      format = TextureFormat::RGBA_32F;

      materialDef.properties = {
          {"view", glm::mat4(1.f)},
          {"projection", glm::perspective(static_cast<float>(Math::Pi) / 2.0f, 1.0f, zNear, zFar)},
          {"deltaPhi", (2.0f * float(Math::Pi)) / 180.0f},
          {"deltaTheta", (0.5f * float(Math::Pi)) / 64.0f},
      };

      material = m_engine.createMaterial(
          Shader(
              ShaderProgram(
                  "../resources/shaders/irradiance_cube.vert.json",
                  "../resources/shaders/irradiance_cube.frag.json"),
              {Vec3}),
          materialDef);
    }
    break;

    case PrefilterEnv:
    {
      dim = 512;
      format = TextureFormat::RGBA_16F;

      materialDef.properties = {
          {"view", glm::mat4(1.f)},
          {"projection", glm::perspective(static_cast<float>(Math::Pi) / 2.0f, 1.0f, zNear, zFar)},
          {"roughness", 1.0f}};

      material = m_engine.createMaterial(
          Shader(
              ShaderProgram(
                  "../resources/shaders/prefilter_envmap.vert.json",
                  "../resources/shaders/prefilter_envmap.frag.json"),
              {Vec3}),
          materialDef);
    }
    break;
    }

    auto cubemap = m_engine.createTextureCube(
        format,
        {dim, dim},
        true,
        {.minFilter = TextureFilter::LinearMipMapLinear,
         .magFilter = TextureFilter::Linear,
         .wrapModeS = TextureWrapMode::ClampToEdge,
         .wrapModeT = TextureWrapMode::ClampToEdge,
         .wrapModeR = TextureWrapMode::ClampToEdge});

    const unsigned int mipLevels = static_cast<uint32_t>(std::floor(std::log2(dim))) + 1;

    switch (target)
    {
    case Irradiance:
      irradianceCubemap = cubemap;
      break;
    case PrefilterEnv:
      prefilterEnvCubemap = cubemap;
      prefilterEnvCubemapMipLevels = mipLevels;
      break;
    }

    for (int m = 0; m < mipLevels; ++m)
    {
      int mipWidth = static_cast<int>(dim * std::pow(0.5, m));
      int mipHeight = static_cast<int>(dim * std::pow(0.5, m));

      for (auto face : {
               CubeMapFace::PositiveX,
               CubeMapFace::NegativeX,
               CubeMapFace::PositiveY,
               CubeMapFace::NegativeY,
               CubeMapFace::PositiveZ,
               CubeMapFace::NegativeZ})
      {
        glm::mat4 viewMatrix = glm::mat4(1.0f);

        switch (face)
        {
        case CubeMapFace::PositiveX:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
          viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          break;
        case CubeMapFace::NegativeX:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
          viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          break;
        case CubeMapFace::PositiveY:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          break;
        case CubeMapFace::NegativeY:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          break;
        case CubeMapFace::PositiveZ:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          break;
        case CubeMapFace::NegativeZ:
          viewMatrix = glm::rotate(viewMatrix, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
          break;
        default:
          break;
        }

        auto view = m_cubemapViews.emplace_back(
            m_engine.createOffscreenView(
                {mipWidth, mipHeight},
                {{.texture = cubemap, .attachmentPoint = AttachmentPoint::Color, .face = face, .mipLevel = m}}));

        auto instance = material->createInstance();

        switch (target)
        {
        case Irradiance:
          instance->setProperty("view", viewMatrix);
          break;
        case PrefilterEnv:
          instance->setProperty("view", viewMatrix);
          instance->setProperty("roughness", static_cast<float>(m) / static_cast<float>(mipLevels - 1));
          break;
        }

        auto scene = m_engine.createScene();
        view->setScene(scene);

        auto entity = m_engine.createEntity(cubeGeometry, instance);
        scene->addEntity(entity);
      }
    }
  }

  auto pbrMaterial = m_engine.createMaterial<GLTFMaterial>(
      irradianceCubemap,
      prefilterEnvCubemap,
      lutBRDF,
      prefilterEnvCubemapMipLevels);

  auto pbrDoubleSidedMaterial = m_engine.createMaterial<GLTFMaterial>(
      irradianceCubemap,
      prefilterEnvCubemap,
      lutBRDF,
      prefilterEnvCubemapMipLevels,
      RenderStates{.cullMode = CullMode::None});

  auto pbrAlphaBlendMaterial = m_engine.createMaterial<GLTFMaterial>(
      irradianceCubemap,
      prefilterEnvCubemap,
      lutBRDF,
      prefilterEnvCubemapMipLevels,
      RenderStates{.cullMode = CullMode::None, .blendingEnabled = true});

  auto scene = m_engine.createScene();

  auto skyboxMaterial = m_engine.createMaterial<SkyboxMaterial>(irradianceCubemap);
  auto skyBoxEntity = m_engine.createEntity(cubeGeometry, skyboxMaterial->defaultInstance());
  auto scale = glm::scale(glm::vec3(100.f));
  skyBoxEntity->setTransformation(scale);
  scene->addEntity(skyBoxEntity);

  auto emptyTexture = m_engine.createTexture2D(KtxImage("../resources/textures/empty.ktx"));

  auto loadTexture = [this, &emptyTexture](std::optional<GLTFImporter::Texture> const &optTexture)
  {
    if (optTexture)
    {
      auto extension = std::filesystem::path(optTexture->path).extension();
      Scoped<IImage> image;

      if (extension == ".ktx")
      {
        image = createScoped<KtxImage>(optTexture->path);
      }
      else
      {
        image = createScoped<StbImage>(optTexture->path);
      }
      return m_engine.createTexture2D(*image, true, optTexture->sampler);
    }

    return emptyTexture;
  };

  std::vector<WeakRef<MaterialInstance>> materials;
  for (auto const &material : model.materials)
  {
    auto instance = materials.emplace_back(
        material.alphaMode == GLTFImporter::AlphaMode::Blend ? pbrAlphaBlendMaterial->createInstance() : (material.doubleSided ? pbrDoubleSidedMaterial->createInstance() : pbrMaterial->createInstance()));

    instance->setProperty("baseColorFactor", material.baseColorFactor);
    instance->setProperty("emissiveFactor", glm::vec4(1.f));
    instance->setProperty("diffuseFactor", glm::vec4(0.f));
    instance->setProperty("specularFactor", glm::vec4(0.f));
    instance->setProperty("workflow", material.workflow == GLTFImporter::Workflow::MetallicRoughness ? 0.f : 1.f);
    instance->setProperty("metallicFactor", material.metallicFactor);
    instance->setProperty("roughnessFactor", material.roughnessFactor);
    instance->setProperty("alphaMask", material.alphaMode == GLTFImporter::AlphaMode::Mask ? 1.f : 0.f);
    instance->setProperty("alphaMaskCutoff", material.alphaCutoff);

    instance->setProperty("baseColorTextureSet", material.colorMap ? material.colorMap->uvIndex : -1);
    instance->setProperty("physicalDescriptorTextureSet", material.physicalDescriptorMap ? material.physicalDescriptorMap->uvIndex : -1);
    instance->setProperty("normalTextureSet", material.normalMap ? material.normalMap->uvIndex : -1);
    instance->setProperty("occlusionTextureSet", material.aoMap ? material.aoMap->uvIndex : -1);
    instance->setProperty("emissiveTextureSet", material.emissiveMap ? material.emissiveMap->uvIndex : -1);

    instance->setTexture("colorMap", loadTexture(material.colorMap));
    instance->setTexture("physicalDescriptorMap", loadTexture(material.physicalDescriptorMap));
    instance->setTexture("normalMap", loadTexture(material.normalMap));
    instance->setTexture("aoMap", loadTexture(material.aoMap));
    instance->setTexture("emissiveMap", loadTexture(material.emissiveMap));
  }

  struct Primitive
  {
    WeakRef<Geometry> geometry;
    int material;
    glm::vec3 min;
    glm::vec3 max;
  };

  struct Mesh
  {
    std::vector<Primitive> primitives;
  };

  std::vector<Mesh> meshes;

  const AttributeLayout attributeLayout = {
      AttributeType::Vec3, // position
      AttributeType::Vec3, // normal
      AttributeType::Vec2, // uv0
      AttributeType::Vec2, // uv1
      AttributeType::Vec4  // color
  };

  for (auto const &inMesh : model.meshes)
  {
    auto &mesh = meshes.emplace_back(Mesh{});

    for (auto const &inPrimitive : inMesh.primitives)
    {
      if (inPrimitive.indices)
      {
        mesh.primitives.emplace_back(
            Primitive{
                m_engine.createGeometry(
                    attributeLayout,
                    inPrimitive.vertices,
                    *inPrimitive.indices),
                inPrimitive.material,
                inPrimitive.min,
                inPrimitive.max});
      }
      else
      {
        mesh.primitives.emplace_back(
            Primitive{
                m_engine.createGeometry(
                    attributeLayout,
                    inPrimitive.vertices),
                inPrimitive.material,
                inPrimitive.min,
                inPrimitive.max});
      }
    }
  }

  struct BBox
  {
    glm::vec3 min = glm::vec3(std::numeric_limits<float>::max());
    glm::vec3 max = glm::vec3(std::numeric_limits<float>::min());
  };

  std::function<BBox(GLTFImporter::Node const &node, glm::mat4 transformation, BBox &bbox)> computeBoudingBox;

  computeBoudingBox = [&meshes, &computeBoudingBox](GLTFImporter::Node const &node, glm::mat4 transformation, BBox &bbox) -> BBox
  {
    transformation = transformation * node.localTransformation;

    if (node.mesh)
    {
      auto const &mesh = meshes.at(*node.mesh);

      for (auto const &primitive : mesh.primitives)
      {
        glm::vec3 tmin = transformation * glm::vec4(primitive.min, 1.f);
        glm::vec3 tmax = transformation * glm::vec4(primitive.max, 1.f);

        glm::vec3 min = glm::min(tmin, tmax);
        glm::vec3 max = glm::max(tmin, tmax);

        bbox.min = glm::min(bbox.min, min);
        bbox.max = glm::max(bbox.max, max);
      }
    }

    for (auto const &child : node.children)
    {
      computeBoudingBox(child, transformation, bbox);
    }

    return bbox;
  };

  BBox bbox;
  for (auto const &node : model.nodes)
  {
    computeBoudingBox(node, glm::mat4(1.f), bbox);
  }

  LOG_INFO("Min ", bbox.min, " Max ", bbox.max);

  std::function<void(GLTFImporter::Node const &node, glm::mat4 transformation)> convertNode;

  convertNode = [this, &scene, &materials, &meshes, &convertNode](GLTFImporter::Node const &node, glm::mat4 transformation)
  {
    transformation = transformation * node.localTransformation;

    if (node.mesh)
    {
      auto const &mesh = meshes.at(*node.mesh);

      for (auto const &primitive : mesh.primitives)
      {
        auto entity = m_engine.createEntity(primitive.geometry, materials.at(primitive.material));
        entity->setTransformation(transformation);
        scene->addEntity(entity);
      }
    }

    for (auto const &child : node.children)
    {
      convertNode(child, transformation);
    }
  };

  glm::vec3 size(
      std::abs(bbox.max.x - bbox.min.x),
      std::abs(bbox.max.y - bbox.min.y),
      std::abs(bbox.max.z - bbox.min.z));

  auto maxLength = std::max(std::max(size.x, size.y), size.z);
  auto scaleFactor = 1.f / maxLength;

  glm::vec3 center(
      (bbox.max.x - bbox.min.x) / 2.f + bbox.min.x,
      (bbox.max.y - bbox.min.y) / 2.f + bbox.min.y,
      (bbox.max.z - bbox.min.z) / 2.f + bbox.min.z);

  auto transformation =
      glm::scale(glm::vec3(scaleFactor)) *
      glm::translate(-center);

  for (auto const &node : model.nodes)
  {
    convertNode(node, transformation);
  }

  m_view = m_engine.getScreenView();
  m_view->setClearColor(Color{0.2, 0.2, 0.2, 1.0});
  m_view->setScene(scene);
}

void GLTFRenderer::draw()
{
  static bool first = true;
  if (first)
  {
    m_engine.draw({m_brdfLUTView});
    m_engine.draw(m_cubemapViews);

    first = false;
  }
  m_engine.draw({m_view});
}
